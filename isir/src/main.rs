use typechecker::{
    ast::{LambdaInternals, Term},
    ffi::{type_check, type_infer},
    ocaml,
};
fn main() {
    let rt = ocaml::runtime::init();
    // the polymorphic identity function at universe 5
    // lambda (T: univ 5) lambda (t: T) t : pi T: univ 5, pi (_: T), T
    // lambda (univ 5) (lambda (var 0) (var 0)): pi (univ 5) pi (var 0) (var 1)
    let term = Term::Lam(Box::new(LambdaInternals {
        arg_ty: Term::Univ(5),
        body: Term::Lam(Box::new(LambdaInternals {
            arg_ty: Term::Var(0),
            body: Term::Var(0),
        })),
    }));
    let ty = type_infer(&rt, &term).unwrap();
    assert!(type_check(&rt, &term, &ty));
    println!("{term:?} has type {ty:?}");
}
