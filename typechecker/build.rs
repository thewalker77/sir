/******************************************************************************/
/* Copyright 2022 thewalker77                                                 */
/*                                                                            */
/* Licensed under the Apache License, Version 2.0 (the "License");            */
/* you may not use this file except in compliance with the License.           */
/* You may obtain a copy of the License at                                    */
/*                                                                            */
/*     http://www.apache.org/licenses/LICENSE-2.0                             */
/*                                                                            */
/* Unless required by applicable law or agreed to in writing, software        */
/* distributed under the License is distributed on an "AS IS" BASIS,          */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   */
/* See the License for the specific language governing permissions and        */
/* limitations under the License.                                             */
/******************************************************************************/

use std::{env, io, path::Path, process::Command};

fn run_make(dir: &Path) -> io::Result<()> {
    let mut child = Command::new("make").current_dir(dir).arg("-j").spawn()?;
    let status = child.wait()?;
    if !status.success() {
        panic!("Failed to run `make -j` in {dir:?}, exist code = {status}",);
    }
    Ok(())
}
fn main() -> io::Result<()> {
    println!("cargo:rerun-if-changed=coq-typechecker");
    let mut work_dir = env::current_dir()?;
    work_dir.pop();
    work_dir.push("coq-typechecker");
    run_make(&work_dir)?;
    work_dir.push("extracted");
    ocaml_build::Dune::new("src").with_root(work_dir).build();
    Ok(())
}
