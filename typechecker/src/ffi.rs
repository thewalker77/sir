/******************************************************************************/
/* Copyright 2022 thewalker77                                                 */
/*                                                                            */
/* Licensed under the Apache License, Version 2.0 (the "License");            */
/* you may not use this file except in compliance with the License.           */
/* You may obtain a copy of the License at                                    */
/*                                                                            */
/*     http://www.apache.org/licenses/LICENSE-2.0                             */
/*                                                                            */
/* Unless required by applicable law or agreed to in writing, software        */
/* distributed under the License is distributed on an "AS IS" BASIS,          */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   */
/* See the License for the specific language governing permissions and        */
/* limitations under the License.                                             */
/******************************************************************************/
use crate::{ast::Term, error::Error};
use ocaml::Runtime;
use std::collections::LinkedList;

mod raw {
    use crate::{ast::Term, error::Error};
    use ocaml::import;
    use std::collections::LinkedList;
    import! {
        pub(super) fn type_infer(ctx: &LinkedList<Term>, term: &Term, fuel: u16) -> Result<Term, Error>;
        pub(super) fn type_check(ctx: &LinkedList<Term>, term: &Term, ty: &Term, fuel: u16) -> bool;

    }
}
const OCAML_EXCEPTION: &str = "[BUG] FFI failed, Ocaml generated run-time exception";

pub fn type_infer_with_ctx(
    rt: &Runtime,
    ctx: &LinkedList<Term>,
    term: &Term,
) -> Result<Term, Error> {
    unsafe { raw::type_infer(rt, ctx, term, u16::MAX) }.expect(OCAML_EXCEPTION)
}

pub fn type_check_with_ctx(rt: &Runtime, ctx: &LinkedList<Term>, term: &Term, ty: &Term) -> bool {
    unsafe { raw::type_check(rt, ctx, term, ty, u16::MAX) }.expect(OCAML_EXCEPTION)
}

pub fn type_infer(rt: &Runtime, term: &Term) -> Result<Term, Error> {
    let ctx = Default::default();
    type_infer_with_ctx(rt, &ctx, term)
}

pub fn type_check(rt: &Runtime, term: &Term, ty: &Term) -> bool {
    let ctx = Default::default();
    type_check_with_ctx(rt, &ctx, term, ty)
}
