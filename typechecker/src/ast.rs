/******************************************************************************/
/* Copyright 2022 thewalker77                                                 */
/*                                                                            */
/* Licensed under the Apache License, Version 2.0 (the "License");            */
/* you may not use this file except in compliance with the License.           */
/* You may obtain a copy of the License at                                    */
/*                                                                            */
/*     http://www.apache.org/licenses/LICENSE-2.0                             */
/*                                                                            */
/* Unless required by applicable law or agreed to in writing, software        */
/* distributed under the License is distributed on an "AS IS" BASIS,          */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   */
/* See the License for the specific language governing permissions and        */
/* limitations under the License.                                             */
/******************************************************************************/

use ocaml::{FromValue, Tag, ToValue, Value};

#[derive(Debug)]
pub struct PiInternals {
    /// Argument type
    pub arg_ty: Term,
    /// Return types
    pub ret_ty: Term,
}

#[derive(Debug)]
pub struct LambdaInternals {
    /// Argument type
    pub arg_ty: Term,
    /// Body of the lambda abstraction
    pub body: Term,
}

#[derive(Debug)]
pub struct ApplInternals {
    /// The lambda function to be called
    pub lam: Term,
    /// The argument passed to the lambda function
    pub arg: Term,
}

#[derive(Debug)]
pub struct SigmaInternals {
    /// Type of the first projection
    pub fst_ty: Term,
    /// Type of the second projections
    pub snd_ty: Term,
}

#[derive(Debug)]
pub struct PairInternals {
    /// Mandatory annotation for the dependent pair
    pub annots: SigmaInternals,
    /// the first projection of the pair
    pub fst: Term,
    /// The second projection of the pair
    pub snd: Term,
}

#[derive(Debug)]
pub enum Term {
    /// Deburuijn index
    Var(u16),
    /// universe level
    Univ(u16),
    /// Dependent function type
    Pi(Box<PiInternals>),
    /// Dependent function
    Lam(Box<LambdaInternals>),
    /// Function application
    Appl(Box<ApplInternals>),
    /// Dependent pair type
    Sigma(Box<SigmaInternals>),
    /// Dependent pair
    Pair(Box<PairInternals>),
    /// First projection of a pair
    Proj1(Box<Term>),
    /// Second projection of a pair
    Proj2(Box<Term>),
}

unsafe impl ToValue for Term {
    fn to_value(&self, rt: &ocaml::Runtime) -> ocaml::Value {
        let mut value;
        match self {
            Term::Var(idx) => {
                value = unsafe { Value::alloc(1, Tag(0)) };
                unsafe { value.store_field(rt, 0, idx) };
            }
            Term::Univ(lvl) => {
                value = unsafe { Value::alloc(1, Tag(1)) };
                unsafe { value.store_field(rt, 0, lvl) };
            }
            Term::Pi(pi) => {
                value = unsafe { Value::alloc(2, Tag(2)) };
                unsafe { value.store_field(rt, 0, &pi.arg_ty) };
                unsafe { value.store_field(rt, 1, &pi.ret_ty) };
            }
            Term::Lam(lam) => {
                value = unsafe { Value::alloc(2, Tag(3)) };
                unsafe { value.store_field(rt, 0, &lam.arg_ty) };
                unsafe { value.store_field(rt, 1, &lam.body) };
            }
            Term::Appl(appl) => {
                value = unsafe { Value::alloc(2, Tag(4)) };
                unsafe { value.store_field(rt, 0, &appl.lam) };
                unsafe { value.store_field(rt, 1, &appl.arg) };
            }
            Term::Sigma(sigma) => {
                value = unsafe { Value::alloc(2, Tag(5)) };
                unsafe { value.store_field(rt, 0, &sigma.fst_ty) };
                unsafe { value.store_field(rt, 1, &sigma.snd_ty) };
            }
            Term::Pair(pair) => {
                value = unsafe { Value::alloc(4, Tag(6)) };
                unsafe { value.store_field(rt, 0, &pair.annots.fst_ty) };
                unsafe { value.store_field(rt, 1, &pair.annots.snd_ty) };
                unsafe { value.store_field(rt, 2, &pair.fst) };
                unsafe { value.store_field(rt, 3, &pair.snd) };
            }
            Term::Proj1(m) => {
                value = unsafe { Value::alloc(1, Tag(7)) };
                unsafe { value.store_field(rt, 0, m.as_ref()) };
            }
            Term::Proj2(m) => {
                value = unsafe { Value::alloc(1, Tag(8)) };
                unsafe { value.store_field(rt, 0, m.as_ref()) };
            }
        }
        value
    }
}

unsafe impl FromValue for Term {
    fn from_value(value: ocaml::Value) -> Self {
        let is_block = unsafe { value.is_block() };
        if !is_block {
            panic!("[BUG] failed to deserialize `typechecker::ast::Term`, is_block = {is_block}");
        }
        let tag = unsafe { value.tag() }.0;
        match tag {
            0 => Term::Var(FromValue::from_value(unsafe { value.field(0) })),
            1 => Term::Univ(FromValue::from_value(unsafe { value.field(0) })),
            2 => {
                let arg_ty = FromValue::from_value(unsafe { value.field(0) });
                let ret_ty = FromValue::from_value(unsafe { value.field(1) });
                Term::Pi(Box::new(PiInternals { arg_ty, ret_ty }))
            }
            3 => {
                let arg_ty = FromValue::from_value(unsafe { value.field(0) });
                let body = FromValue::from_value(unsafe { value.field(1) });
                Term::Lam(Box::new(LambdaInternals { arg_ty, body }))
            }
            4 => {
                let lam = FromValue::from_value(unsafe { value.field(0) });
                let arg = FromValue::from_value(unsafe { value.field(1) });
                Term::Appl(Box::new(ApplInternals { lam, arg }))
            }
            5 => {
                let fst_ty = FromValue::from_value(unsafe { value.field(0) });
                let snd_ty = FromValue::from_value(unsafe { value.field(1) });
                Term::Sigma(Box::new(SigmaInternals { fst_ty, snd_ty }))
            }
            6 => {
                let fst_ty = FromValue::from_value(unsafe { value.field(0) });
                let snd_ty = FromValue::from_value(unsafe { value.field(1) });
                let annots = SigmaInternals { fst_ty, snd_ty };
                let fst = FromValue::from_value(unsafe { value.field(2) });
                let snd = FromValue::from_value(unsafe { value.field(3) });
                Term::Pair(Box::new(PairInternals { annots, fst, snd }))
            }
            7 => Term::Proj1(FromValue::from_value(unsafe { value.field(0) })),
            8 => Term::Proj2(FromValue::from_value(unsafe { value.field(0) })),
            unknown => panic!(
                "[BUG] failed to deserialize `typechecker::ast::Term`,\n\
                                   matching on unknown tag with tag id = {unknown}"
            ),
        }
    }
}
