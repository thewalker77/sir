(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect zify.
From Coq Require Import ZifyClasses ZArith_base.
Require Import Coq.Logic.FunctionalExtensionality.
From sir.common Require Import utils.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(* This file implements a notion of partial addition based on the standard    *)
(* implementation provided by mathcomp. The reason we a new function for      *)
(* addition is that we want to prevent eager partial evaluation. This is      *)
(* essential for the debruijn tactics to work properly.                       *)

Definition padd x := fun y => x + y.
Arguments padd x y/.

Lemma padd_nadd: forall x y, Z.of_nat(padd x y) =
    ((Z.of_nat x) + (Z.of_nat y))%Z.
Proof.
move => x y.
rewrite /padd.
by lia.
Qed.

#[global]
Instance Op_padd : BinOp padd :=
{ TBOp := Z.add; TBOpInj := padd_nadd }.
Add Zify BinOp Op_padd.

Lemma padd0_id: (padd 0) = id.
Proof.
by [].
Qed.

Lemma paddS m n: padd m.+1 n = (padd m n).+1.
Proof.
by lia.
Qed.

Lemma padd0 m : padd m 0 = m.
Proof.
by lia.
Qed.

Lemma paddA a b c: padd a (padd b c) = padd( padd a b) c.
Proof.
by lia.
Qed.

Lemma padd_comp m n: (padd n) ∘ (padd m) = (padd (n + m)).
Proof.
apply: functional_extensionality => x.
rewrite /comp.
by lia.
Qed.

Lemma padd_compR A n m (f: nat -> A):
    f ∘ (padd m) ∘ (padd n) = f ∘ (padd (m+n)).
Proof.
by rewrite -padd_comp.
Qed.

Lemma scons_eta {T} (f: _ -> T) n:
    f n .: f ∘ (padd n.+1) = f ∘ (padd n).
Proof.
apply: functional_extensionality.
rewrite /scons /comp.
move => [| x];
    f_equal;
    by lia.
Qed.

Lemma padd_eta n: n .: (padd (S n)) = (padd n).
Proof.
rewrite -{1}[n]/(id n).
by apply: scons_eta.
Qed.
