(*****************************************************************************)
(* Copyright 2022 TheWalker77                                                *)
(*                                                                           *)
(* Licensed under the Apache License, Version 2.0 (the "License");           *)
(* you may not use this file except in compliance with the License.          *)
(* You may obtain a copy of the License at                                   *)
(*                                                                           *)
(*     http://www.apache.org/licenses/LICENSE-2.0                            *)
(*                                                                           *)
(* Unless required by applicable law or agreed to in writing, software       *)
(* distributed under the License is distributed on an "AS IS" BASIS,         *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *)
(* See the License for the specific language governing permissions and       *)
(* limitations under the License.                                            *)
(*****************************************************************************)
From HB Require Import structures.
From mathcomp Require Import all_ssreflect zify.
From sir.common Require Import serde pos seq decide.
From sir.common.fin_maps Require Export fin_maps.
From sir.common.fin_maps Require Import trie.
Export serde fin_maps.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.


(******************************************************************************)
(* This file generalizes the trie implementation in "trie.v" into one with    *)
(* arbitrary key type and and arbitrary value type. The key must implement    *)
(* [eqType] and [serDe] canonical structures. Both structures are combined in *)
(* [eqSerDe].                                                                 *)
(*                                                                            *)
(* The main data structure is [GTrie]                                         *)
(*                                                                            *)
(* The main operations:                                                       *)
(* - [lookup]                                                                 *)
(* - [singleton]                                                              *)
(* - [map]                                                                    *)
(* - [fmap]                                                                   *)
(* - [alter]                                                                  *)
(* - [to_list]                                                                *)
(* - [merge]                                                                  *)
(*                                                                            *)
(* The main lemmas:                                                           *)
(* [extensionality]                                                           *)
(* [lookup_empty]                                                             *)
(* [lookup_alter]                                                             *)
(* [lookup_alter_neq]                                                         *)
(* [lookup_fmap]                                                              *)
(* [to_list_no_dup]                                                           *)
(* [elem_of_to_list]                                                          *)
(* [lookup_map]                                                               *)
(* [lookup_merge]                                                             *)
(******************************************************************************)


Module DecidableWF.


Definition serialize_deserialize (T : serDe) p : Prop :=
    serialize (@deserialize T p) = p.

Definition map_serde K V (t: Trie.Trie V) : Prop :=
    map_forall (fun p _ => serialize_deserialize K p) t.

Lemma GTrieWF_dec (K: eqSerDe) V (t: Trie.Trie V):
    {@map_serde K V t} + {~ @map_serde K V t}.
Proof.
rewrite /map_serde /=.
elim /@map_ind : t => /= [| p v t Hlookup [Hwf|HnotWf]].
-   left.
    by apply: map_forall_empty.
-   case (@eqP _ (serialize (@deserialize K p)) p) => [Heq| Hneq].
    +   left.
        by rewrite -!map_forall_insert /serialize_deserialize.
    +   right.
        rewrite -!map_forall_insert /serialize_deserialize => //.
        move => [Heq Hforall].
        by apply: Hneq.
-   right.
    rewrite -!map_forall_insert //.
    move => [Hserde Hwf].
    by apply: HnotWf.
Qed.

Definition GTrieWF (K: eqSerDe) V (t: Trie.Trie V) : Prop :=
    @bool_decide (@map_serde K V t) (@GTrieWF_dec K V t) = true.

End DecidableWF.

Record GTrie (K: eqSerDe) V := gtrie {
    g_trie: Trie.Trie V;
    g_prf: @DecidableWF.GTrieWF K V g_trie;
}.

Module GTrie.
Import DecidableWF.
Section GTrie_Operations.
Context {K: eqSerDe}.
Context {V V' V'': Type}.

Lemma empty_wf: GTrieWF K (@Trie.Tempty V).
Proof.
rewrite /GTrieWF bool_decide_true.
by apply: map_forall_empty.
Qed.

Definition GEmpty : GTrie K V := @gtrie K V (@Trie.Tempty V) empty_wf.

Definition lookup (k: K) (t: GTrie K V) : option V :=
    Trie.lookup (serialize k) (g_trie t).
Global Arguments lookup: simpl never.

Lemma singleton_wf (k: K) (v: V): @GTrieWF K V (Trie.singleton (serialize k) v).
Proof.
rewrite /GTrieWF bool_decide_true.
rewrite /map_serde mamp_forall_singleton /=.
by rewrite /serialize_deserialize deserialize_serialize.
Qed.

Definition singleton (k: K) (v: V) := @gtrie K V (Trie.singleton (serialize k) v) (@singleton_wf k v).
Global Arguments singleton: simpl never.

Lemma map_wf (f: V -> V') (t: Trie.Trie V) : GTrieWF K t  -> GTrieWF K (map_map f t).
Proof.
rewrite /GTrieWF !bool_decide_true /= => Hwf p v.
rewrite map_lookup_map_some.
move => [v' [Hf Hlookup]].
apply: Hwf.
by apply: Hlookup.
Qed.

Definition map (f: V -> V') (t: @GTrie K V) : GTrie K V' :=
    @gtrie K V' (Trie.map f (g_trie t)) (@map_wf f (g_trie t) (@g_prf K V t)).
Global Arguments map: simpl never.

Lemma fmap_wf (f: V -> option V') (t: Trie.Trie V) : GTrieWF K t  -> GTrieWF K (map_fmap f t).
Proof.
rewrite /GTrieWF !bool_decide_true => /= Hwf p v.
rewrite map_lookup_fmap_some.
move => [v' [Hf Hlookup]].
apply: Hwf.
by apply: Hlookup.
Qed.

Definition fmap (f: V -> option V') (t: GTrie K V) : GTrie K V' :=
    @gtrie K V' (Trie.fmap f (g_trie t)) (@fmap_wf f (g_trie t) (@g_prf K V t)).
Global Arguments fmap: simpl never.

Lemma alter_wf (f: option V -> option V) (k: K) (t: Trie.Trie V) : GTrieWF K t  -> GTrieWF K (map_alter f (serialize k) t).
Proof.
rewrite /GTrieWF !bool_decide_true => /= Hwf p v.
rewrite map_lookup_alter_some.
move => [[<- Hlookup]| [Hneq Hlookup]].
    by rewrite /serialize_deserialize deserialize_serialize.
apply: Hwf.
by apply:Hlookup.
Qed.

Definition alter (f: option V -> option V) (k: K) (t: GTrie K V): GTrie K V :=
    @gtrie K V (Trie.alter f (serialize k) (g_trie t)) (@alter_wf f k (g_trie t) (@g_prf K V t)).
Global Arguments alter: simpl never.

Definition to_list (t: GTrie K V) : list (K * V) :=
    seq.map (fun pv => (deserialize pv.1, pv.2)) (Trie.to_list (g_trie t)).
Global Arguments to_list: simpl never.

Definition none_handler (f: option V' -> option V'' -> option V) :=
fun v1 v2 =>
match v1 with
| None => match v2 with
          | None => None
          | _ => f v1 v2
          end
| _ => f v1 v2
end.

Lemma merget_wf (f: option V' -> option V'' -> option V)
                (t1: Trie.Trie V')
                (t2: Trie.Trie V'') :
                GTrieWF K t1 -> GTrieWF K t2 ->
                GTrieWF K (map_merge (none_handler f) t1 t2).
Proof.
rewrite /GTrieWF !bool_decide_true /= => Hwf1 Hwf2 p v.
have Hnone: (none_handler f) None None = None.
    by [].
rewrite (@map_lookup_merge _ _ _ _ _ _ _ _ _ Hnone).
case Heq1: (t1 !! p) => [v1|].
    move => Hf.
    apply: Hwf1.
    by apply: Heq1.
case Heq2: (t2 !! p) => [v2|] //= Hf.
apply: Hwf2.
by apply: Heq2.
Qed.

Definition merge (f: option V' -> option V'' -> option V)
                 (t1: GTrie K V') (t2: GTrie K V'') : GTrie K V :=
        @gtrie K V (Trie.merge (none_handler f) (g_trie t1) (g_trie t2))
        (@merget_wf f (g_trie t1) (g_trie t2) (@g_prf K V' t1) (@g_prf K V'' t2)).
Global Arguments merge: simpl never.
End GTrie_Operations.

Section GTrie_Props.
Context {K: eqSerDe}.
Context {V V' V'': Type}.
Lemma gmap_eq (t1 t2: GTrie K V):
    t1 = t2 <-> g_trie t1 = g_trie t2.
Proof.
split.
    by move ->.
move: t1 t2 => [t1 wf1] [t2 wf2] /= Heq.
subst.
f_equal.
apply eq_irrelevance.
Qed.

Lemma extensionality: forall (t1 t2: GTrie K V),
    (forall k, lookup k t1 = lookup k t2) -> t1 = t2.
Proof.
rewrite /lookup.
move => [t1 wf1] [t2 wf2] /= Heq.
rewrite gmap_eq /=.
apply: Trie.extensionality => /= p.
have := Heq (deserialize p).
move: wf1 wf2.
rewrite /GTrieWF !bool_decide_true.
rewrite /map_serde /serialize_deserialize /= => wf1 wf2.
case Heq1: (Trie.lookup p t1) => [v1|].
    by rewrite (wf1 _ v1) // Heq1.
case Heq2: (Trie.lookup p t2) => [v2|//].
by rewrite (wf2 _ v2) // Heq2 Heq1.
Qed.

Lemma lookup_empty: forall k, lookup k (@GEmpty K V) = None.
Proof.
move => k.
by rewrite /lookup /GEmpty Trie.lookup_empty.
Qed.

Lemma lookup_alter : forall g (t: GTrie K V) k,
    lookup k (alter g k t) = g (lookup k t).
Proof.
rewrite /lookup /alter.
move => t k g.
by rewrite Trie.lookup_alter.
Qed.

Lemma lookup_alter_neq: forall g (t: GTrie K V) k1 k2,
    k1 != k2 -> lookup k1 (alter g k2 t) = (lookup k1 t).
Proof.
move => g t k1 k2 /eqP Hneq.
rewrite /lookup /alter Trie.lookup_alter_neq.
    by [].
apply/eqP.
apply/serialize_neq_inv => Heq.
by apply: Hneq.
Qed.

Lemma lookup_fmap: forall (h: V -> option V') (t: GTrie K V) k,
    lookup k (fmap h t) = obind h (lookup k t).
Proof.
move => h t k.
by rewrite /lookup /fmap Trie.lookup_fmap.
Qed.

Lemma to_list_no_dup: forall (t: GTrie K V), PUniq (to_list t).
Proof.
move => [t wf].
rewrite /to_list /=.
apply: puniq_map_inv_fun;
    last by apply: Trie.to_list_no_dup.
move: wf => + [ka va] [kb vb] + + [+ ->]/=.
rewrite /GTrieWF !bool_decide_true /map_serde /=.
rewrite /serialize_deserialize !Trie.elem_of_to_list.
rewrite /map_forall/map_lookup /= => wf Hlookupa Hlookupb.
have Hserdea:= wf ka _ Hlookupa.
have Hserdeb:= wf kb _ Hlookupb.
move => Hdeserialize.
have : serialize (@deserialize K ka) = serialize(@deserialize K kb).
    by rewrite Hdeserialize.
by rewrite Hserdea Hserdeb => ->.
Qed.

Lemma elem_of_to_list: forall (t: GTrie K V) k v,
    PIn (k, v) (to_list t) <->
    lookup k t = Some v.
Proof.
move => [t wf] k v.
rewrite /to_list /lookup /=.
move: wf.
rewrite /GTrieWF bool_decide_true /map_serde /=.
rewrite /serialize_deserialize => wf.
split.
    move /pin_map => [[p v']] [] [-> ->] Hin.
    rewrite -Trie.elem_of_to_list (wf _ v') //.
    rewrite /map_lookup /=.
    by eapply Trie.elem_of_to_list.
rewrite -Trie.elem_of_to_list.
move => Hin.
apply/pin_map.
exists (serialize k, v) => /=.
rewrite deserialize_serialize.
by split.
Qed.

Lemma lookup_map: forall (f: V -> V') (t: GTrie K V) k,
        lookup k (map f t) = omap f (lookup k t).
Proof.
move => f t k.
by rewrite /lookup Trie.lookup_map.
Qed.

Lemma lookup_merge: forall (f: option V' -> option V'' -> option V)
                           (t1: GTrie K V') (t2: GTrie K V'') k,
    f None None = None ->
    lookup k (merge f t1 t2) = f (lookup k t1) (lookup k t2).
Proof.
move => f [t1 wf1] [t2 wf2] k Hnone.
rewrite /lookup /merge Trie.lookup_merge //=.
rewrite /none_handler /=.
case Heq1: (Trie.lookup (serialize k) t1) => [//|].
by case Heq2: (Trie.lookup (serialize k) t2).
Qed.

End GTrie_Props.

Definition Mixin (K :eqSerDe): hasFinMap.axioms_ K (GTrie K).
Proof.
econstructor;
    move => V /=.
-   by apply: extensionality.
-   by apply: lookup_empty.
-   move => f m k.
    by rewrite -lookup_alter.
-   move => f m k1 k2 Hneq.
    by rewrite lookup_alter_neq.
-   move => V2 f m k.
    by rewrite -lookup_fmap.
-   by apply: to_list_no_dup.
-   by apply: elem_of_to_list.
-   move => V' f m k.
    by rewrite -lookup_map.
-   move => V1 V2 f m1 m2 k Hnone.
    by rewrite -lookup_merge.
Qed.

End GTrie.

HB.instance  Definition _ K := GTrie.Mixin K.
