(*****************************************************************************)
(* Copyright 2022 TheWalker77                                                *)
(*                                                                           *)
(* Licensed under the Apache License, Version 2.0 (the "License");           *)
(* you may not use this file except in compliance with the License.          *)
(* You may obtain a copy of the License at                                   *)
(*                                                                           *)
(*     http://www.apache.org/licenses/LICENSE-2.0                            *)
(*                                                                           *)
(* Unless required by applicable law or agreed to in writing, software       *)
(* distributed under the License is distributed on an "AS IS" BASIS,         *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *)
(* See the License for the specific language governing permissions and       *)
(* limitations under the License.                                            *)
(*****************************************************************************)

From HB Require Import structures.
From mathcomp Require Import all_ssreflect zify.
From sir.common Require Import seq.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(*****************************************************************************)
(* This file provides API for dealing with arbitrary extentional map types.  *)
(* To use the APIs and notation described here here, The data type must      *)
(* imlement a the [finMap] canonical structure. That implies that the data   *)
(* type in question must implement the operations defind in                  *)
(* [FinMap.operations] and prov that they satisfies the lemmas described in  *)
(* [FinMap.lemmas].                                                          *)
(*****************************************************************************)
HB.mixin Record hasFinMap (K: eqType) (M: Type -> Type) := {
    map_empty V : M V;
    map_lookup {V} : K -> M V-> option V;
    map_alter {V}: (option V -> option V) -> K -> M V -> M V;
    map_map {V1 V2}: (V1 -> V2) -> M V1 -> M V2;
    map_fmap {V1 V2}: (V1 -> option V2) -> M V1 -> M V2;
    map_merge {V V1 V2}: (option V1 -> option V2 -> option V) ->
                         M V1 ->
                         M V2 ->
                         M V;
    map_to_list {V}: M V -> seq (K * V);
    map_extensionality V: forall (m1 m2: M V),
                (forall k , map_lookup k m1 = map_lookup k m2) -> m1 = m2;
    map_lookup_empty V: forall (k : K), map_lookup k (map_empty V) = None;
    map_lookup_alter: forall V f (m: M V) k,
                map_lookup k (map_alter f k m) = f (map_lookup k m);
    map_lookup_alter_neq: forall V f (m: M V) k1 k2,
                k1 != k2 -> map_lookup k1 (map_alter f k2 m) = map_lookup k1 m;
    map_lookup_fmap: forall V1 V2 (f :V1 -> option V2) (m: M V1) k,
                map_lookup k (map_fmap f m) = obind f (map_lookup k m);
    map_unique_to_list: forall V (m: M V), PUniq (map_to_list m);
    map_elem_of_to_list: forall V (m: M V) k v,
                PIn (k, v) (map_to_list m) <-> map_lookup k m = Some v;
    map_lookup_map: forall V1 V2 (f: V1 -> V2) (m: M V1) k,
                map_lookup k (map_map f m) = omap f (map_lookup k m);
    map_lookup_merge V V1 V2: forall (f :option V1 -> option V2 -> option V)
               (m1: M V1) (m2: M V2) k,
        f None None = None ->
        map_lookup k (map_merge f m1 m2) =
        f (map_lookup k m1) (map_lookup k m2);
}.

#[short(type="finMap")]
HB.structure Definition FinMap (K: eqType) := {T of hasFinMap K T}.

(* base operations *)

Arguments map_empty {_ _ _}: simpl never.
Notation "{[]}" := map_empty.

Arguments map_lookup {_ _ _} k m : simpl never.
Notation "m !! k" := (map_lookup k m) (at level 20).

Arguments map_alter {_ _ _} f k m: simpl never.

Arguments map_map {_ _ _ _} f m: simpl never.

Arguments map_fmap {_ _ _ _} f m: simpl never.

Arguments map_merge {_ _ _ _ _} f m1 m2: simpl never.

Arguments map_to_list {_ _ _} m: simpl never.

(* Derived operations *)
Definition map_insert {K V} {M: finMap K} k v (m : M V):=
    map_alter (fun _ => some v) k m.
Notation "<[ k := a ]>" := (map_insert k a)
    (at level 5, right associativity, format "<[ k := a ]>").

Definition map_delete {K V} {M: finMap K} k (m: M V) :=
    map_alter (fun _ => None) k m.

Definition map_singleton {K V} {M: finMap K} k v :=  <[k := v]>  ({[]}: M V).
Notation "{[ k := v ]}" := (map_singleton k v) (at level 1).

Definition list_to_map {K V e} :=
    foldr (fun kv t => @map_insert K V e kv.1 kv.2 t) {[]}.

Definition map_size {K V} {M: finMap K} (m: M V) := length (map_to_list m).

Definition map_union {K V} {M: finMap K} (m1 m2: M V):=
    let union (o1 o2: option V) :=
    match o1 with
    | Some v => Some v
    | _ => o2
    end in
    map_merge union m1 m2.

Definition map_forall {K: eqType} {V} {M: finMap K} (P: K -> V -> Prop):
    M V -> Prop :=
    fun m => forall k v, m !! k = Some v -> P k v.
Definition map_exists {K: eqType} {V} {M: finMap K} (P: K -> V -> Prop):
    M V -> Prop :=
    fun m => exists k v, m !! k = Some v /\ P k v.

(* maybe we want to separate stuff for options *)
Definition option_relation {T1 T2} (R: T1 -> T2 -> Prop) (R1: T1 -> Prop)
                                   (R2: T2 -> Prop)
                                   (o1 : option T1) (o2: option T2) : Prop :=
match o1, o2 with
| some t1, some t2 => R t1 t2
| some t1, None => R1 t1
| None, Some t2 => R2 t2
| None, None => True
end.

Definition map_lookup_relation {K: eqType} {V1 V2} {M: finMap K}
                        (R: V1 -> V2 -> Prop) (R1: V1 -> Prop)
                        (R2: V2 -> Prop) (m1: M V1) (m2: M V2) : Prop :=
forall (k: K), option_relation R R1 R2 (m1!!k) (m2!!k).

Definition map_included {K V} {M: finMap K} R (m1 m2: M V)
:= map_lookup_relation R (fun _ => False) (fun _ => True) m1 m2.

Definition map_agree {K V} {M: finMap K} (m1 m2: M V) :=
    map_lookup_relation (fun v1 v2 => v1 = v2)
                        (fun _ => True) (fun _ => True) m1 m2.

Definition map_disjoint {K V} {M: finMap K} (m1 m2: M V) :=
    map_lookup_relation (fun _ _ => False) (fun _ => True) (fun _ => True) m1 m2.
Infix "##ₘ" := map_disjoint (at level 70).

Definition map_subseteq {K V} {M: finMap K} (m1 m2: M V) :=
    map_included (fun v1 v2 => v1 = v2) m1 m2.
Infix "⊆ₘ" := map_subseteq (at level 70).
Notation "x ⊈ₘ y" := (~ x ⊆ₘ y) (at level 70).

Definition map_subset {K V} {M: finMap K} (m1 m2: M V):=
        m1 ⊆ₘ m2 /\ ~ m2 ⊆ₘ m1.
Infix "⊂ₘ" := map_subset (at level 70).
Notation "x ⊄ₘ y" := (~ x ⊂ₘ y) (at level 70).

Definition map_fold {K V A} {M: finMap K} (f: K -> V -> A -> A)
                    (a: A) (m: M V) : A :=
    foldr (fun kv => f kv.1 kv.2) a (map_to_list m).

(* derived lemmas *)

Definition map_eq_iff {K V} {M: finMap K} (m1 m2: M V):
    (forall k , m1 !! k = m2 !! k) <-> m1 = m2.
Proof.
split.
    by apply: map_extensionality.
by move ->.
Qed.

Lemma map_lookup_not_none {K V} {M: finMap K} (m: M V) k:
m !! k <> None <-> exists v, m !! k = Some v.
Proof.
split;
    last by move =>  [v ->].
case (m !! k) => [v|];
    last by [].
move => _.
by exists v.
Qed.


Lemma map_lookup_none {K V} {M: finMap K} (m: M V) k:
m !! k = None <-> ~ exists v, m !! k = Some v.
split.
    by move => -> [v HFalse].
case (m!!k) => [v|];
    last by [].
move => Hlookup.
contradict Hlookup.
by exists v.
Qed.

(* lemmas for subset/subseteq *)
Lemma map_subseteq_spec {K V} {M: finMap K}: forall (m1 m2: M V),
    m1 ⊆ₘ m2 <-> forall k v, m1 !! k = Some v -> m2 !! k = Some v.
Proof.
rewrite /map_subseteq /map_included /map_lookup_relation.
split.
    move => H_rel k.
    have := H_rel k.
    case : (m1 !! k) (m2!!k) => [v1|] [v2|] //=.
    by move ->.
move => H_spec k.
have HS_spec:= H_spec k.
case Hm1_lookup: (m1 !! k) => [v1|] //=;
    case Hm2_lookup: (m2!!k) => [v2|] //=.
-   have := HS_spec _ Hm1_lookup.
    rewrite Hm2_lookup.
    by move => [].
-   have := HS_spec _ Hm1_lookup.
    by rewrite Hm2_lookup.
Qed.

Lemma map_subseteq_refl {K V} {M: finMap K} (m : M V): m ⊆ₘ m.
Proof.
by rewrite map_subseteq_spec => k v.
Qed.

Lemma map_subseteq_trans {K V} {M: finMap K} (m1 m2 m3: M V):
    m1 ⊆ₘ m2 -> m2 ⊆ₘ m3 -> m1 ⊆ₘ m3.
Proof.
rewrite !map_subseteq_spec => Hsubseteq12 Hsubseteq23 k v Hlookup1.
apply: Hsubseteq23.
by apply: Hsubseteq12.
Qed.

Lemma map_subset_subseteq {K V} {M: finMap K} (m1 m2: M V):
    m1 ⊂ₘ m2 -> m1 ⊆ₘ m2.
Proof.
by move => [].
Qed.

Lemma map_subseteq_subseteq_eq {K V} {M: finMap K} (m1 m2: M V):
    m1 = m2 <-> m1 ⊆ₘ m2 /\ m2 ⊆ₘ m1.
Proof.
split.
    move ->.
    split;
    by apply: map_subseteq_refl.
rewrite !map_subseteq_spec.
move => [Hspec12 Hspec21].
apply/map_extensionality => k.
case Hlookup1: (m1 !! k) => [v1|];
    case Hlookup2: (m2 !! k) => [v2|].
-   have := Hspec21 k v2 Hlookup2.
    by rewrite Hlookup1.
-   have := Hspec12 k v1 Hlookup1.
    by rewrite Hlookup2.
-   have := Hspec21 k v2 Hlookup2.
    by rewrite Hlookup1.
-   by [].
Qed.
Lemma map_lookup_weaken {K V} {M: finMap K}: forall (m1 m2: M V) k v,
    m1 !! k = Some v -> m1 ⊆ₘ m2 -> m2 !! k = Some v.
Proof.
move => m1 m2 k v m1_lookup.
rewrite map_subseteq_spec => Hlookup.
by apply: Hlookup.
Qed.

Lemma map_lookup_weaken_None {K V} {M: finMap K}: forall (m1 m2: M V) k,
    m2 !! k = None -> m1 ⊆ₘ m2 -> m1 !! k = None.
Proof.
move => m1 m2 k m2_lookup.
rewrite map_subseteq_spec => Hlookup.
case  m1_lookup: (m1!!k) => [v1|] //=.
suff: exists v, m2 !! k = Some v.
    move => [v].
    by rewrite m2_lookup.
have Hfalse := (Hlookup _ _ m1_lookup).
rewrite Hfalse.
by exists v1.
Qed.

Lemma map_lookup_weaken_inv {K V} {M: finMap K}: forall (m1 m2: M V) k v1 v2,
    m1 !! k = Some v1 -> m1 ⊆ₘ m2 -> m2 !! k = Some v2 -> v1 = v2.
Proof.
move => m1 m2 k v1 v2 lookup_m1.
rewrite map_subseteq_spec => Hlookup.
have m2_lookup_v1: m2 !! k = Some v1.
    by apply: Hlookup.
rewrite m2_lookup_v1.
by move => [].
Qed.

Lemma lookup_neq {K V} {M: finMap K}: forall (m: M V) k1 k2,
    m !! k1 <> m !! k2 -> k1 <> k2.
Proof.
by congruence.
Qed.

Lemma map_empty_spec {K V} {M: finMap K}: forall (m: M V),
    m = {[]} <-> forall k, m !! k = None.
Proof.
split.
-   move => m_empty i.
    by rewrite m_empty map_lookup_empty.
-   move => H_empty.
    apply: map_extensionality => k.
    by rewrite H_empty map_lookup_empty.
Qed.

Lemma lookup_empty_some {K V} {M: finMap K}: forall k v,
    ~ ({[]} : M V) !! k = Some v.
Proof.
move => k v.
by rewrite map_lookup_empty.
Qed.

Lemma map_empty_subseteq {K V} {M: finMap K}: forall (m: M V), {[]} ⊆ₘ m.
Proof.
move => m.
rewrite map_subseteq_spec => k v.
by rewrite map_lookup_empty.
Qed.

Lemma map_subset_empty {K V} {M: finMap K}: forall (m: M V), m ⊄ₘ {[]}.
Proof.
move => m [_ []].
by apply: map_empty_subseteq.
Qed.

Lemma map_subset_alt1 {K V: eqType} {M: finMap K} (m1 m2: M V):
    m1 ⊂ₘ m2 ->
    m1 ⊆ₘ m2 /\ exists (k: K), m1 !! k = None /\ m2 !! k <> None.
Proof.
rewrite /map_subset.
move => [Hsubseteq Hnotsubseteq].
split.
    by [].
move Hhas : (has (fun kv => m1 !! kv.1 == None) (map_to_list m2)) => has_bool.
case : has_bool Hhas.
    move /hasP => [[k v]] /= /inP Hin_list /eqP Hlookup.
    exists k.
    split.
        by [].
    suff: m2 !! k = Some v.
        by move ->.
    by apply/map_elem_of_to_list.
move /hasPn => H_lookup.
case Hnotsubseteq.
apply/map_subseteq_spec => k v.
move /map_elem_of_to_list /inP => Hin_map_to_list.
have := H_lookup (k, v) Hin_map_to_list => /=.
move /eqP /map_lookup_not_none => [v' Hlookup_m1].
rewrite -(@map_lookup_weaken_inv _ _ _ m1 m2 k v' v) //.
by move : Hin_map_to_list => /inP /map_elem_of_to_list.
Qed.

Lemma map_subset_alt2 {K V} {M: finMap K} (m1 m2: M V):
    (m1 ⊆ₘ m2 /\ exists k, m1 !! k = None /\ m2 !! k <> None) ->
    m1 ⊂ₘ m2.
Proof.
rewrite /map_subset.
move => [Hsubseteq [k [Hlookup_m1 Hlookup_m2]]].
split.
    by [].
move => HFalse.
apply: Hlookup_m2.
apply: map_lookup_weaken_None.
    by apply: Hlookup_m1.
apply: HFalse.
Qed.

Lemma map_subset_alt {K V: eqType} {M: finMap K} (m1 m2: M V):
    m1 ⊂ₘ m2 <->
    m1 ⊆ₘ m2 /\ exists (k: K), m1 !! k = None /\ m2 !! k <> None.
Proof.
split.
    by apply: map_subset_alt1.
by apply: map_subset_alt2.
Qed.

(* properties for map_alter *)
Lemma map_alter_ext {K V} {M: finMap K} {f g: option V -> option V} (m: M V) k:
    (forall v, m!!k  = v -> f v = g v) -> map_alter f k m = map_alter g k m.
Proof.
move => Hfg_eq.
apply: map_extensionality => k'.
case: (@eqP _ k' k) => [->|Hneq_kk'].
    rewrite !map_lookup_alter.
    by apply: Hfg_eq.
rewrite !map_lookup_alter_neq;
    by [apply/eqP|].
Qed.

Lemma map_alter_compose {K V} {M: finMap K} {f g: option V -> option V}
                        (m: M V) k:
    map_alter (fun v => f (g v)) k m = map_alter f k (map_alter g k m).
Proof.
apply: map_extensionality => k'.
case: (@eqP _ k' k) => [->|Hneq_kk'].
    by rewrite !map_lookup_alter.
rewrite !map_lookup_alter_neq;
    by [apply/eqP|].
Qed.

Lemma map_alter_commute {K V} {M: finMap K} {f g: option V -> option V}
                        (m: M V) k1 k2:
    k1 <> k2 ->
    map_alter f k1 (map_alter g k2 m) = map_alter g k2 (map_alter f k1 m).
Proof.
move => Hneq.
apply: map_extensionality => k.
case : (@eqP _ k1 k)  Hneq => [->|Hneq_K1K] Hneq_k1k2.
    rewrite map_lookup_alter map_lookup_alter_neq;
        last by apply/eqP.
    rewrite map_lookup_alter_neq;
        last by apply/eqP.
    by rewrite map_lookup_alter.
have Hneq_k2k1: k2 <> k1.
    move => HFalse.
    by apply: Hneq_k1k2.
move: Hneq_k1k2 => _.
case : (@eqP _ k2 k) Hneq_k2k1 => [->|Hneq_K2K] Hneq_k2k1.
    rewrite map_lookup_alter map_lookup_alter_neq;
        last by apply/eqP.
    rewrite map_lookup_alter map_lookup_alter_neq.
        by [].
    by apply/eqP.
rewrite !map_lookup_alter_neq //;
    apply/eqP => Hfalse;
    by move : Hfalse Hneq_K1K Hneq_K2K Hneq_k2k1 ->.
Qed.

Lemma map_alter_self_alt {K V} {M: finMap K} (m: M V) (v: option V) k:
    m !! k = v -> map_alter (fun _ => v) k m = m.
Proof.
move => Hlookup.
apply: map_extensionality => k'.
case :(@eqP _ k' k) Hlookup => [->|Hneq] Hlookup.
    by rewrite map_lookup_alter.
rewrite map_lookup_alter_neq;
    by [apply/eqP|].
Qed.

Lemma map_alter_self {K V} {M: finMap K} (m: M V) k:
    map_alter (fun _ => m !! k) k m = m.
Proof.
by apply: map_alter_self_alt.
Qed.

Lemma map_alter_subseteq {K V} {M: finMap K} (m: M V) k f:
    m !! k = None -> m ⊆ₘ map_alter f k m.
Proof.
move => Hlookup_k.
rewrite map_subseteq_spec => k' v' Hlookup_k'.
rewrite map_lookup_alter_neq.
    by [].
apply/negP => /eqP Heq.
move : Heq Hlookup_k' ->.
by rewrite Hlookup_k.
Qed.

Lemma map_alter_subset {K V: eqType} {M: finMap K} (m: M V) (k: K) f:
    m !! k = None -> f (m !! k) <> None -> m ⊂ₘ map_alter f k m.
Proof.
move => Hlookup Hflookup.
apply/map_subset_alt.
split.
    by apply: map_alter_subseteq.
exists k.
by rewrite map_lookup_alter.
Qed.

Lemma map_lookup_alter_some {K V} {M: finMap K} (m: M V) k1 k2 v f:
    map_alter f k1 m !! k2 = Some v <->
    (k1 = k2 /\ f(m !! k1) = Some v) \/ (k1 <> k2 /\ m !! k2 = Some v).
Proof.
case (@eqP _ k2 k1) => [->|Hneq].
-   rewrite map_lookup_alter.
    split.
        move => Hlookup.
        by left.
    by move => [[_ Hlookup]| [Hfalse _]].
-   split.
        rewrite map_lookup_alter_neq;
            last by apply/eqP.
        move => Hlookup.
        right.
        suff: k1 <> k2.
            by [].
        move => Heq.
        by apply: Hneq.
    move => [[Hfalse]|[Hneq2 Hlookup]].
        by move: Hfalse Hneq ->.
    rewrite map_lookup_alter_neq;
        by [apply/eqP|].
Qed.

(* properties of map_delete *)

Lemma map_lookup_delete {K V} {M: finMap K} (m: M V) k:
    map_delete k m !! k = None.
Proof.
by apply:  map_lookup_alter.
Qed.

Lemma map_lookup_delete_neq {K V} {M: finMap K} (m: M V) k1 k2:
    k1 != k2 -> map_delete k2 m !! k1 = m !! k1.
Proof.
by apply: map_lookup_alter_neq.
Qed.

Lemma map_lookup_delete_some {K V} {M: finMap K} (m: M V) k1 k2 v:
    map_delete k1 m !! k2 = Some v <-> k2 <> k1 /\ m !! k2 = Some v.
Proof.
split;
    last first.
    move => [] /eqP Hneq Hlookup.
    by rewrite map_lookup_delete_neq.
case (@eqP _ k1 k2) => [->|].
    by rewrite map_lookup_delete.
move => Hneq Hlookup_delete.
split.
    move => Hfalse.
    by apply: Hneq.
move : Hlookup_delete.
rewrite map_lookup_delete_neq.
    by [].
apply/eqP.
move => Hfalse.
by apply: Hneq.
Qed.

Lemma map_lookup_delete_none {K V} {M: finMap K} (m: M V) k1 k2:
    map_delete k1 m !! k2 = None <-> k1 = k2 \/ m !! k2 = None.
Proof.
case (@eqP _ k1 k2) => [->| Hneq].
    rewrite map_lookup_delete.
    split => //.
    by left.
rewrite map_lookup_delete_neq;
    last first.
    apply/eqP.
    move => HFalse.
    by apply: Hneq.
split.
    by right.
by move => [].
Qed.

Lemma map_delete_empty {K V} {M: finMap K} k:
    map_delete k ({[]} : M V) = {[]}.
Proof.
have : map_delete k ({[]} : M V) =
       map_alter (fun=> ({[]} : M V) !! k) k {[]}.
    by rewrite map_lookup_empty.
by rewrite map_alter_self.
Qed.

Lemma map_delete_commute {K V} {M: finMap K} {m: M V} k1 k2:
    map_delete k1 (map_delete k2 m) = map_delete k2 (map_delete k1 m).
Proof.
case (@eqP _ k1 k2) => [->| Hneq].
    by [].
by apply: map_alter_commute.
Qed.

Lemma map_lookup_none_delete {K V} {M: finMap K} {m: M V} k:
    m !! k = None -> map_delete k m = m.
Proof.
move => Hlookup.
apply: map_extensionality => k'.
case (@eqP _ k' k) => [->|Hneq].
    by rewrite map_lookup_delete.
rewrite map_lookup_delete_neq.
    by [].
apply/eqP => HFalse.
by apply: Hneq.
Qed.

Lemma map_delete_delete {K V} {M: finMap K} {m: M V} k:
    map_delete k (map_delete k m) = map_delete k m.
Proof.
by rewrite /map_delete -map_alter_compose.
Qed.

Lemma map_delete_alter {K V} {M: finMap K} {m: M V} k f:
    m !! k = None -> map_delete k (map_alter f k m) = m.
Proof.
move => Hlookup.
rewrite /map_delete -map_alter_compose.
by apply: map_alter_self_alt.
Qed.

Lemma map_delete_insert {K V} {M: finMap K} {m: M V} k v:
    m !! k = None -> map_delete k (<[k := v]> m) = m.
Proof.
by apply: map_delete_alter.
Qed.

Lemma map_delete_insert_delete {K V} {M: finMap K} {m: M V} k v:
    map_delete k ( <[k:=v]> m) = map_delete k m.
Proof.
by rewrite /map_delete -map_alter_compose.
Qed.

Lemma map_delete_insert_neq {K V} {M: finMap K} {m: M V} k1 k2 v:
    k1 <> k2 ->
    map_delete k1 (<[k2 := v]> m) =  <[k2 := v]> (map_delete k1 m).
Proof.
move => Hneq.
by apply: map_alter_commute.
Qed.

Lemma map_delete_subseteq {K V} {M: finMap K} {m: M V} k: map_delete k m ⊆ₘ m.
Proof.
rewrite map_subseteq_spec => k' v'.
rewrite map_lookup_delete_some.
by move => [].
Qed.

Lemma map_delete_subset {K V: eqType} {M: finMap K} {m: M V} (k: K):
    m !! k <> None -> map_delete k m ⊂ₘ m.
Proof.
move /map_lookup_not_none => [v Hlookup].
apply/map_subset_alt.
split.
    apply: map_delete_subseteq.
exists k.
split.
    by rewrite map_lookup_delete.
apply/map_lookup_not_none.
by exists v.
Qed.

Lemma map_delete_preserve_subseteq {K V} {M: finMap K} {m1 m2: M V} k:
    m1 ⊆ₘ m2 -> map_delete k m1 ⊆ₘ map_delete k m2.
Proof.
rewrite !map_subseteq_spec => Hspec k' v'.
rewrite !map_lookup_delete_some.
move => [Hneq Hlookup].
split.
    by [].
by apply: Hspec.
Qed.

(* lemmas for map_insert *)

Lemma map_lookup_insert {K V} {M: finMap K} {m: M V} k v:
    <[k := v]> m !! k = Some v.
Proof.
apply: map_lookup_alter.
Qed.

Lemma map_lookup_insert_rev {K V} {M: finMap K} {m: M V} k v1 v2:
<[k := v1]> m !! k = Some v2 -> v1 = v2.
Proof.
rewrite map_lookup_insert.
by move => [].
Qed.

Lemma map_lookup_insert_neq {K V} {M: finMap K} {m: M V} k1 k2 v:
    k1 <> k2 -> <[k2 :=  v]> m !! k1 = m !! k1.
Proof.
move => Hneq.
rewrite map_lookup_alter_neq.
    by [].
by apply/eqP.
Qed.

Lemma map_insert_insert {K V} {M: finMap K} {m: M V} k v1 v2:
    <[k := v1]> ( <[k := v2]> m) = <[k := v1]> m.
Proof.
by rewrite /map_insert -map_alter_compose.
Qed.

Lemma map_insert_commute {K V} {M: finMap K} {m: M V} k1 k2 v1 v2:
    k1 <> k2 ->
    <[k1 := v1]> (<[k2 := v2]> m) =
    <[k2 := v2]> (<[k1 := v1]> m).
Proof.
apply: map_alter_commute.
Qed.

Lemma map_lookup_insert_some {K V} {M: finMap K} {m: M V} k1 k2 v1 v2:
    <[k1 := v1]> m !! k2 = Some v2 <->
    (k1 = k2 /\ v1 = v2) \/ (k2 <> k1 /\ m !! k2 = Some v2).
Proof.
split;
    last first.
    move => [[-> ->]|[]].
        apply: map_lookup_insert.
    move => Hneq Hlookup.
    by rewrite map_lookup_insert_neq.
case (@eqP _ k2 k1) => [->|Hneq].
    rewrite map_lookup_insert.
    move => [->].
    by left.
rewrite map_lookup_insert_neq.
    by right.
by [].
Qed.

Lemma map_lookup_insert_none {K V} {M: finMap K} {m: M V} k1 k2 v:
    <[k1 := v]> m !! k2 = None <-> m !! k2 = None /\ k2 <> k1.
Proof.
split;
    last first.
    move => [Hlookup Hneq].
    by rewrite map_lookup_insert_neq.
case (@eqP _ k2 k1) => [->|Hneq].
    by rewrite map_lookup_insert.
by rewrite map_lookup_insert_neq.
Qed.

Lemma map_insert_id {K V} {M: finMap K} {m: M V} k v:
    m !! k = Some v -> <[k := v]> m = m.
Proof.
move => Hlookup.
apply: map_extensionality => k'.
case : (@eqP _ k' k) => [->|Hneq].
    by rewrite map_lookup_insert.
by rewrite map_lookup_insert_neq.
Qed.

Lemma map_insert_empty {K: eqType} {V} {M: finMap K} (k: K) v:
    <[k := v]> {[]} = ({[k := v]} : (M V)).
Proof.
by [].
Qed.

Lemma map_insert_not_empty {K V} {M: finMap K} {m: M V} k v:
    <[k := v]> m <> {[]}.
Proof.
move => Hinsert.
have :( <[k := v]> m !! k = ({[]}: M V) !! k).
    by rewrite Hinsert.
by rewrite map_lookup_insert map_lookup_empty.
Qed.

Lemma map_insert_delete_insert {K V} {M: finMap K} {m: M V} k v:
    <[k := v]> (map_delete k m) = <[k := v]> m.
Proof.
by rewrite /map_insert /map_delete -map_alter_compose.
Qed.

Lemma map_insert_delete {K V} {M: finMap K} {m: M V} k v:
    m !! k = Some v ->  <[k := v]> (map_delete k m) = m.
Proof.
move => Hlookup.
by rewrite map_insert_delete_insert map_insert_id.
Qed.

Lemma map_insert_subseteq {K V} {M: finMap K} {m: M V} k v:
    m !! k = None ->
    m ⊆ₘ  <[k := v]> m.
Proof.
by apply: map_alter_subseteq.
Qed.

Lemma map_insert_subset {K V: eqType} {M: finMap K} {m: M V} (k: K) v:
    m !! k = None ->
    m ⊂ₘ <[k := v]> m.
Proof.
move => Hlookup.
by apply: map_alter_subset.
Qed.

Lemma map_insert_preserve_subseteq {K V} {M: finMap K} {m1 m2: M V} k v:
    m1 ⊆ₘ m2 ->
    <[k := v]> m1 ⊆ₘ <[k := v]> m2.
Proof.
rewrite !map_subseteq_spec => Hspec k' v'.
rewrite !map_lookup_insert_some.
move => [[-> ->]|[Hneq Hlookup]].
    by left.
right.
split.
    move => HFalse.
    by apply: Hneq.
by apply: Hspec.
Qed.

Lemma map_insert_subseteq_r {K V} {M: finMap K} {m1 m2: M V} k v:
    m1 !! k = None -> m1 ⊆ₘ m2 -> m1 ⊆ₘ  <[k := v]> m2.
Proof.
move => Hlookup Hsubseteq.
apply: (@map_subseteq_trans _ _ _ m1 (<[k := v]> m1)).
    by apply: map_insert_subseteq.
by apply: map_insert_preserve_subseteq.
Qed.

Lemma map_insert_subseteq_l {K V} {M: finMap K} {m1 m2: M V} k v:
    m2 !! k = Some v -> m1 ⊆ₘ m2 -> <[k := v]> m1 ⊆ₘ m2.
Proof.
move => Hlookup Hsubseteq.
apply: map_subseteq_trans.
    apply: map_insert_preserve_subseteq.
    apply: Hsubseteq.
rewrite map_insert_id.
    by apply: map_subseteq_refl.
by [].
Qed.

Lemma map_insert_delete_subseteq {K V} {M: finMap K} {m1 m2: M V} k v:
    m1 !! k = None -> <[k := v]> m1 ⊆ₘ m2 -> m1 ⊆ₘ map_delete k m2.
Proof.
rewrite !map_subseteq_spec => Hlookup_m1k Hspec k' v' Hlookup_m1k'.
case : (@eqP _ k' k) Hlookup_m1k' => [->| Hneq].
    by rewrite Hlookup_m1k.
rewrite map_lookup_delete_neq;
    last by apply/eqP.
move => Hlookup.
apply: Hspec.
by rewrite map_lookup_insert_neq.
Qed.

Lemma map_delete_insert_subseteq {K V} {M: finMap K} {m1 m2: M V} k v:
    m1 !! k = Some v -> map_delete k m1 ⊆ₘ m2 -> m1 ⊆ₘ <[k := v]> m2.
Proof.
rewrite !map_subseteq_spec => Hlookup_m1k Hspec k' v' Hlookup_m1k'.
case : (@eqP _ k' k) Hlookup_m1k' => [->| Hneq].
    by rewrite Hlookup_m1k map_lookup_insert.
move => Hlookup_m1k'.
rewrite map_lookup_insert_neq;
    last by [].
apply: Hspec.
rewrite map_lookup_delete_neq.
    by [].
by apply/eqP.
Qed.

Lemma map_insert_delete_subset {K V} {M: finMap K} {m1 m2: M V} k v:
    m1 !! k = None -> <[k := v]> m1 ⊂ₘ m2 -> m1 ⊂ₘ map_delete k m2.
Proof.
move => Hlookup [Hsubseteq Hnot_subseteq].
split.
    by apply: map_insert_delete_subseteq.
contradict Hnot_subseteq.
apply: map_delete_insert_subseteq;
    last by [].
apply: map_lookup_weaken;
    last by apply: Hsubseteq.
apply: map_lookup_insert.
Qed.

Lemma map_insert_subset_inv {K V} {M: finMap K} {m1 m2: M V} k v:
    m1 !! k = None ->
    <[k := v]> m1 ⊂ₘ m2 ->
    exists m2', m2 = <[k := v]>m2' /\ m1 ⊂ₘ m2' /\ m2' !! k = None.
Proof.
move => Hlookup Hsubset.
exists (map_delete k m2).
split;
    last split.
-   rewrite map_insert_delete.
        by [].
    apply: map_lookup_weaken;
        last first.
        apply: map_subset_subseteq.
        by apply: Hsubset.
    apply: map_lookup_insert.
-   apply: map_insert_delete_subset;
        by [|apply: Hsubset].
-   by rewrite map_lookup_delete.
Qed.

(* Lemmas abount singleton constructor *)
Lemma map_lookup_singleton_some {K V} {M: finMap K} k1 k2 v1 v2:
    ({[k1 := v1]} : M V) !! k2 = Some v2 <-> k1 = k2 /\ v1 = v2.
Proof.
rewrite -map_insert_empty map_lookup_insert_some map_lookup_empty.
split.
-   move => [[-> ->]| [_ HFalse]].
        by split.
    by [].
-   move => [-> ->].
    by left.
Qed.

Lemma map_lookup_singleton_none {K V} {M: finMap K} k1 k2 v:
    ({[k1 := v]} : M V) !! k2 = None <-> k2 <> k1.
Proof.
rewrite -map_insert_empty map_lookup_insert_none map_lookup_empty.
split.
    by case.
by [].
Qed.

Lemma map_lookup_singleton {K V} {M: finMap K} k v:
    ({[k := v]} : M V) !! k = Some v.
Proof.
by rewrite map_lookup_singleton_some.
Qed.

Lemma map_lookup_singleton_neq {K V} {M: finMap K} k1 k2 v:
    k2 <> k1 -> ({[k1 := v]}: M V) !! k2 = None.
Proof.
by rewrite map_lookup_singleton_none.
Qed.

Lemma map_lookup_singleton_inj {K V} {M: finMap K} k1 k2 v1 v2:
    ({[k1 := v1]} : M V) = {[k2 := v2]} ->
    k1 = k2 /\ v2 = v2.
Proof.
move => Heq.
have : ({[k1 := v1]} : M V) !! k2 =
        ({[k2 := v2]} : M V) !! k2.
    by rewrite Heq.
rewrite map_lookup_singleton.
case (@eqP _ k2 k1) => [->| Hneq].
    by rewrite map_lookup_singleton_some.
by rewrite map_lookup_singleton_neq.
Qed.

Lemma map_non_empty_singleton {K V} {M: finMap K} k v: ({[ k := v]}: M V) <> {[]}.
Proof.
move => HFalse.
have : ({[k := v]}: M V) !! k = ({[]}: M V) !! k.
    by rewrite HFalse.
by rewrite map_lookup_empty map_lookup_singleton.
Qed.

Lemma map_insert_singleton {K V} {M: finMap K} k  v1 v2:
    <[k := v1]> {[k := v2]} = ({[k := v1]} : M V).
Proof.
by rewrite /map_singleton /map_insert -map_alter_compose.
Qed.

Lemma map_delete_singleton {K V} {M: finMap K} k v:
    map_delete k ({[ k := v]}: M V) = {[]}.
Proof.
rewrite /map_delete /map_singleton -map_alter_compose.
apply: map_delete_empty.
Qed.

Lemma map_delete_singleton_neq {K V} {M: finMap K} k1 k2 v:
    k1 <> k2 ->
    map_delete k1 ({[ k2 := v]}: M V) = {[ k2 := v]}.
Proof.
move => Hneq.
apply: map_lookup_none_delete.
by apply: map_lookup_singleton_neq.
Qed.

Lemma map_singleton_subseteq_l {K V} {M: finMap K} k v (m: M V):
    {[k := v]} ⊆ₘ m <-> m !! k = Some v.
Proof.
rewrite map_subseteq_spec.
split.
    move => Hspec.
    apply: Hspec.
    by rewrite map_lookup_singleton_some.
move => Hlookupkv k' v'.
by move /map_lookup_singleton_some => [<- <-].
Qed.

Lemma map_singleton_subseteq {K V} {M: finMap K} k1 k2 v1 v2:
    ({[k1 := v1]} : M V) ⊆ₘ {[k2 := v2]} <-> k1 = k2 /\ v1 = v2.
Proof.
rewrite map_subseteq_spec.
split;
    last first.
    move => [-> ->] k' v'.
    move /map_lookup_singleton_some => [-> ->].
    by apply: map_lookup_singleton.
move => Hspec.
suff: k2 = k1 /\ v2 = v1.
    by move => [-> -> ].
apply/map_lookup_singleton_some.
apply: Hspec.
by apply: map_lookup_singleton.
Qed.

(* lemmas for map_map map_fmap *)

Lemma map_map_inj {K V1 V2} {M: finMap K} {m1 m2: M V1} (f :V1 -> V2):
    (forall v1 v2, f v1 = f v2 -> v1 = v2) ->
    map_map f m1 = map_map f m2 -> m1 = m2.
Proof.
move => Hinv Hmap.
apply: map_extensionality => k.
have: map_map f m1 !! k = map_map f m2 !! k.
    by rewrite Hmap.
rewrite !map_lookup_map.
case (m1 !!k) => [v1|];
    case (m2!!k) => [v2|] //=.
move => [Hf].
f_equal.
by apply: Hinv.
Qed.


Lemma map_lookup_map_some {K V1 V2} {M: finMap K} {m: M V1} (f :V1 -> V2) k v:
    map_map f m !! k = Some v <-> exists v', f v' = v /\ m !! k = Some v'.
Proof.
rewrite map_lookup_map.
case (m !! k) => [vs|] /=;
    last first.
    split;
        by [| move => [? []]].
split;
    last by move => [v'] [<-] [->].
move => [<-].
by exists vs.
Qed.

Lemma map_lookup_fmap_some {K V1 V2} {M: finMap K} {m: M V1}
    (f :V1 -> option V2) k v:
    map_fmap f m !! k = Some v <->
    exists v', f v' = Some v /\ m !! k = Some v'.
Proof.
rewrite map_lookup_fmap.
case (m !! k) => [vs|] /=;
    last first.
    split;
        by [| move => [? []]].
split;
    last by move => [v'] [<-] [->].
move => <-.
by exists vs.
Qed.

Lemma map_map_empty {K V1 V2} {M: finMap K} (f :V1 -> V2):
    map_map f {[]} = ({[]} : M V2).
Proof.
apply/map_empty_spec => k.
by rewrite map_lookup_map map_lookup_empty.
Qed.

Lemma map_fmap_empty {K V1 V2} {M: finMap K} (f :V1 -> option V2):
    map_fmap f {[]} = ({[]} : M V2).
Proof.
apply/map_empty_spec => k.
by rewrite map_lookup_fmap map_lookup_empty.
Qed.

Lemma map_map_empty_iff {K V1 V2} {M: finMap K} (m: M V1)(f :V1 -> V2):
    map_map f m = {[]} <-> m = {[]}.
Proof.
split;
    last first.
    move ->.
    by apply: map_map_empty.
move => Hmap.
apply: map_extensionality => k.
have: map_map f m !! k = ({[]}: M V2) !! k.
    by rewrite Hmap.
rewrite map_lookup_map !map_lookup_empty.
by case (m!!k) => [v|].
Qed.

Lemma map_map_insert {K V1 V2} {M: finMap K} (m: M V1)(f :V1 -> V2) k v:
    map_map f (<[k := v]> m) = <[k := f v]> (map_map f m).
Proof.
apply: map_extensionality => k'.
case (@eqP _ k' k) => [->| Hneq].
    by rewrite map_lookup_map !map_lookup_insert.
by rewrite map_lookup_map !map_lookup_insert_neq //= map_lookup_map.
Qed.

Lemma map_fmap_insert {K V1 V2} {M: finMap K} (m: M V1) (f :V1 -> option V2) k v:
    map_fmap f (<[k := v]> m) =
    match f v with
    | Some v' => <[k := v']> (map_fmap f m)
    | None => map_delete k (map_fmap f m)
    end.
Proof.
apply: map_extensionality => k'.
case (@eqP _ k' k) => [->|Hneq].
-   rewrite map_lookup_fmap map_lookup_insert /=.
    case (f v) => [v'|].
        by rewrite map_lookup_insert.
    by rewrite map_lookup_delete.
-   rewrite map_lookup_fmap map_lookup_insert_neq;
        last by [].
    case (f v) => [v'|].
        by rewrite map_lookup_insert_neq //= map_lookup_fmap.
    rewrite map_lookup_delete_neq;
        last by apply/eqP.
    by rewrite map_lookup_fmap.
Qed.

Lemma map_fmap_insert_some {K V1 V2} {M: finMap K} (m: M V1)
                           (f :V1 -> option V2) k v1 v2:
    f v1 = Some v2 -> map_fmap f (<[k := v1]> m) = <[k := v2]> (map_fmap f m).
Proof.
move => Hf.
by rewrite map_fmap_insert Hf.
Qed.

Lemma map_fmap_insert_none {K V1 V2} {M: finMap K} (m: M V1)
                           (f :V1 -> option V2) k v:
    f v = None -> map_fmap f (<[k := v]> m) = map_delete k (map_fmap f m).
Proof.
move => Hf.
by rewrite map_fmap_insert Hf.
Qed.

Lemma map_map_delete {K V1 V2} {M: finMap K} (m: M V1) (f :V1 -> V2) k:
    map_map f (map_delete k m) = map_delete k (map_map f m).
Proof.
apply: map_extensionality => k'.
case (@eqP _ k' k) => [->|Hneq].
    by rewrite map_lookup_map !map_lookup_delete.
rewrite map_lookup_map !map_lookup_delete_neq ?map_lookup_map;
by [apply/eqP|].
Qed.

Lemma map_fmap_delete {K V1 V2} {M: finMap K} (m: M V1) (f :V1 -> option V2) k:
    map_fmap f (map_delete k m) = map_delete k (map_fmap f m).
Proof.
apply: map_extensionality => k'.
case (@eqP _ k' k) => [->|Hneq].
    by rewrite map_lookup_fmap !map_lookup_delete.
rewrite map_lookup_fmap !map_lookup_delete_neq ?map_lookup_fmap;
by [apply/eqP|].
Qed.

Lemma map_map_singleton {K V1 V2} {M: finMap K} (f :V1 -> V2) k v:
    map_map f ({[ k := v]} : (M V1)) = {[ k := f v]}.
Proof.
by rewrite /map_singleton map_map_insert map_map_empty.
Qed.

Lemma map_map_singleton_inv {K V1 V2} {M: finMap K} (m: M V1)
                            (f :V1 -> V2) k v:
    map_map f m = {[ k := v]} -> exists v', v = f v' /\ m = {[k := v']}.
Proof.
move => Hmap.
have: map_map f m !! k = ({[k := v]} : M V2) !! k.
    by rewrite Hmap.
rewrite map_lookup_singleton.
move /map_lookup_map_some => [v' [Hf Hlookup]].
exists v'.
split.
    by [].
apply: map_extensionality => k'.
case (@eqP _ k' k) => [->|Hneq].
    by rewrite map_lookup_singleton.
rewrite map_lookup_singleton_neq;
    last by [].
suff : omap f (m !! k' ) = None.
    by case (m !! k').
by rewrite -map_lookup_map Hmap map_lookup_singleton_neq.
Qed.

Lemma map_fmap_singleton {K V1 V2} {M: finMap K} (f :V1 -> option V2) k v:
    map_fmap f ({[ k := v]} : (M V1)) =
    match f v with
    | Some v' => {[ k := v']}
    | None => {[]}
    end.
Proof.
rewrite -map_insert_empty map_fmap_insert.
case: (f v) => [v'|].
    by rewrite map_fmap_empty map_insert_empty.
by rewrite map_fmap_empty map_delete_empty.
Qed.

Lemma map_fmap_singleton_some {K V1 V2} {M: finMap K} (f :V1 -> option V2) k v1 v2:
    f v1 = Some v2 ->
    map_fmap f ({[k := v1]} : M V1) = {[ k := v2]}.
Proof.
move => Hf.
by rewrite map_fmap_singleton Hf.
Qed.

Lemma map_fmap_singleton_none {K V1 V2} {M: finMap K} (f :V1 -> option V2) k v:
    f v = None ->
    map_fmap f ({[k := v]} : M V1) = {[]}.
Proof.
move => Hf.
by rewrite map_fmap_singleton Hf.
Qed.

Lemma map_map_compose {K V1 V2 V3} {M: finMap K} (f :V1 -> V2)
                      (g: V2 -> V3) (m: M V1):
    map_map (fun x => g (f x)) m  = map_map g (map_map f m).
Proof.
apply: map_extensionality => k.
rewrite !map_lookup_map.
by case (m !! k).
Qed.


Lemma map_map_extentionality {K V1 V2} {M: finMap K} (f g : V1 -> V2) (m: M V1):
    (forall k v, m !! k = Some v -> f v = g v) ->
    map_map f m = map_map g m.
Proof.
move => Heq.
apply: map_extensionality => k.
rewrite !map_lookup_map.
case Hlookup: (m !! k) => [v|] /=;
    last by [].
by rewrite (Heq k).
Qed.

Lemma map_fmap_extentionality {K V1 V2} {M: finMap K} (f g : V1 -> option V2)
                              (m: M V1):
    (forall k v, m !! k = Some v -> f v = g v) ->
    map_fmap f m = map_fmap g m.
Proof.
move => Heq.
apply: map_extensionality => k.
rewrite !map_lookup_fmap.
case Hlookup: (m !! k) => [v|] /=;
    last by [].
by rewrite (Heq k).
Qed.

Lemma map_fmap_map {K V1 V2 V3} {M: finMap K} (f: V1 -> option V2)
                   (g: V2 -> V3) (m: M V1):
    map_map g (map_fmap f m) = map_fmap (fun x => omap g (f x)) m.
Proof.
apply: map_extensionality => k.
rewrite map_lookup_map !map_lookup_fmap.
by case: (m!!k).
Qed.

Lemma map_map_alt {K V1 V2} {M: finMap K} (f: V1 -> V2) (m: M V1):
    map_map f m = map_fmap (fun x => Some (f x)) m.
Proof.
apply: map_extensionality => k.
rewrite map_lookup_map map_lookup_fmap.
by case: (m!!k).
Qed.

Lemma map_map_preserve_subseteq {K V1 V2} {M: finMap K} (f: V1 -> V2)
                                (m1 m2: M V1):
    m1 ⊆ₘ m2 -> map_map f m1 ⊆ₘ map_map f m2.
Proof.
rewrite !map_subseteq_spec => Hspec k v.
rewrite map_lookup_map_some map_lookup_map.
move => [v' [Hf Hlookup_m1]].
have Hlookup_m2 := Hspec _ _ Hlookup_m1.
by rewrite Hlookup_m2 /= Hf.
Qed.


Lemma map_map_preserve_subset {K V1 V2: eqType} {M: finMap K} (f: V1 -> V2)
                                (m1 m2: M V1):
    m1 ⊂ₘ m2 -> map_map f m1 ⊂ₘ map_map f m2.
Proof.
move/map_subset_alt => [Hsubseteq [k [Hlookupm1 Hlookupm2]]].
apply/map_subset_alt.
split.
    by apply: map_map_preserve_subseteq.
exists k.
split.
    by rewrite map_lookup_map Hlookupm1.
rewrite map_lookup_map => HFalse.
apply: Hlookupm2.
by case : (m2!!k) HFalse.
Qed.

Lemma map_fmap_preserve_subseteq {K V1 V2} {M: finMap K} (f: V1 -> option V2)
                                (m1 m2: M V1):
    m1 ⊆ₘ m2 -> map_fmap f m1 ⊆ₘ map_fmap f m2.
Proof.
rewrite !map_subseteq_spec => Hspec k v.
rewrite map_lookup_fmap_some map_lookup_fmap.
move => [v' [Hf Hlookup_m1]].
have Hlookup_m2 := Hspec _ _ Hlookup_m1.
by rewrite Hlookup_m2 /= Hf.
Qed.

(* lemmas about map_to_list and list_to_map *)
Lemma map_pin_to_list {K V} {M: finMap K} (m: M V) kv:
    PIn kv (map_to_list m) <-> m !! kv.1 =Some (kv.2).
Proof.
case : kv => k v /=.
by apply: map_elem_of_to_list.
Qed.

Lemma map_to_list_unique {K V} {M: finMap K} (m: M V) k v1 v2:
    PIn (k, v1) (map_to_list m) -> PIn (k, v2) (map_to_list m) -> v1 = v2.
Proof.
rewrite !map_elem_of_to_list.
move ->.
by move => [].
Qed.

Lemma uniq_fst_map_to_list {K V} {M: finMap K} (m: M V):
    PUniq(map (fun kv => kv.1)  (map_to_list m)).
Proof.
apply: uniq_map_fst;
    last by apply: map_unique_to_list.
move => k v1 v2 .
move => /map_elem_of_to_list Hlookup_kv1.
move => /map_elem_of_to_list Hlookup_kv2.
have : Some v1 = Some v2.
    by rewrite -Hlookup_kv1.
by move => [].
Qed.

Lemma map_list_to_map_lookup {K: eqType} {V} {M: finMap K} (l: seq (K * V)) k v:
    (forall v', PIn (k, v') l -> v = v') ->
    PIn (k, v) l ->
    (list_to_map l : M V) !! k = Some v.
Proof.
elim : l => [| [k' v'] l IHl] /=.
    by rewrite PIn_empty_false_iff.
move => Hspec /PIn_cons_or.
move => [[<- <-]|Hin].
    by rewrite map_lookup_insert.
case : (@eqP _ k k') Hspec => [<-| Hneq] Hspec.
    rewrite map_lookup_insert.
    suff : v = v'.
        by move ->.
    apply: Hspec.
    by apply: pin_here.
rewrite map_lookup_insert_neq;
    last by [].
apply: IHl;
    last by [].
move => v'' Hin_kv'.
apply: Hspec.
by apply: pin_future.
Qed.

Lemma map_list_to_map_uniq_lookup {K: eqType} {V} {M: finMap K} (l: seq (K * V)) k v:
    PUniq (map (fun kv => kv.1) l) ->
    PIn (k, v) l ->
    (list_to_map l : M V) !! k = Some v.
Proof.
move => Huniq Hin.
apply: map_list_to_map_lookup;
    last by [].
move => v' Hinl'.
apply: uniq_map_in_in.
-   by apply: Huniq.
-   by apply: Hin.
-   by apply: Hinl'.
Qed.

Lemma map_list_to_map_lookup_in {K: eqType} {V} {M: finMap K} (l: seq (K * V)) k v:
    (list_to_map l : M V) !! k = Some v ->
    PIn (k, v) l.
Proof.
elim : l => [| [k' v'] l IHl] /=.
    by rewrite map_lookup_empty.
case (@eqP _ k k') => [->| Hneq].
    rewrite map_lookup_insert.
    move => [->].
    apply: pin_here.
rewrite map_lookup_insert_neq;
    last by [].
move => Hlookup.
apply: pin_future.
by apply: IHl.
Qed.

Lemma map_list_to_map_spec {K: eqType} {V} {M: finMap K} (l: seq (K * V)) k v:
    (forall v', PIn (k, v') l -> v = v') ->
    PIn (k, v) l <->
    (list_to_map l : M V) !! k = Some v.
Proof.
move => Hspec.
split.
    by apply: map_list_to_map_lookup.
by apply: map_list_to_map_lookup_in.
Qed.

Lemma map_list_to_map_uniq_spec {K: eqType} {V} {M: finMap K} (l: seq (K * V)) k v:
    PUniq (map (fun kv => kv.1) l) ->
    PIn (k, v) l <->
    (list_to_map l : M V) !! k = Some v.
Proof.
move => Huniq.
split.
    by apply: map_list_to_map_uniq_lookup.
by apply: map_list_to_map_lookup_in.
Qed.

Lemma map_not_in_list_to_map_lookup1 {K: eqType} {V} {M: finMap K} (l: seq (K * V)) k:
    ~ PIn k (map (fun kv => kv.1) l) ->
    (list_to_map l : M V) !! k = None.
Proof.
rewrite pin_map map_lookup_none.
move => Hin [v Hlookup].
apply: Hin.
exists (k, v).
split.
    by [].
apply: map_list_to_map_lookup_in.
by apply: Hlookup.
Qed.

Lemma map_not_in_list_to_map_lookup2 {K: eqType} {V} {M: finMap K} (l: seq (K * V)) k:
    (list_to_map l : M V) !! k = None ->
    ~ PIn k (map (fun kv => kv.1) l).
Proof.
elim : l =>[| [k' v'] l IHl] /=.
    move => Hlookup.
    by apply: PIn_empty_false.
rewrite PIn_cons_or.
case : (@eqP _ k k') IHl => [->|Hneq] IHl.
    by rewrite map_lookup_insert.
rewrite map_lookup_insert_neq;
    last by [].
move => Hlookup [Heq|Hin].
    by apply: Hneq.
by apply: IHl.
Qed.

Lemma map_not_in_list_to_map_lookup {K: eqType} {V} {M: finMap K} (l: seq (K * V)) k:
    (list_to_map l : M V) !! k = None <->
    ~ PIn k (map (fun kv => kv.1) l).
Proof.
split.
    by apply: map_not_in_list_to_map_lookup2.
by apply: map_not_in_list_to_map_lookup1.
Qed.

Lemma map_to_list_to_map {K V} {M: finMap K} (m: M V):
    list_to_map (map_to_list m) = m.
Proof.
apply: map_extensionality => k.
suff : forall v,
        (list_to_map (map_to_list m): M V) !! k = Some v <->
        m !! k = Some v.
    case : (list_to_map (map_to_list m) !! k) (m !! k) => [v|] [v'|] Heq.
    -   by rewrite Heq.
    -   symmetry.
        by apply/Heq.
    -   by apply/Heq.
    -   by [].
move => v.
rewrite -map_list_to_map_uniq_spec;
    last by apply: uniq_fst_map_to_list.
by apply: map_elem_of_to_list.
Qed.

Lemma map_list_to_map_nil {K V} {M: finMap K}: list_to_map [::] = ({[]} : (M V)).
Proof.
by [].
Qed.

Lemma map_list_to_map_cons {K: eqType} {V} {M: finMap K} (l: seq (K * V)) k v:
    list_to_map ((k, v)::l) = (<[k := v]> (list_to_map l) : (M V)).
Proof.
by [].
Qed.

Lemma map_list_to_map_snoc {K: eqType} {V} {M: finMap K} (l: seq (K * V)) k v:
    ~ PIn k (map (fun kv => fst kv) l) ->
    list_to_map (l ++ [::(k, v)]) =
    (<[k := v]> (list_to_map l) : (M V)).
Proof.
elim : l => [|[k' v'] l IHl] /=.
    by [].
move /not_PIn_cons_and => [Hneq Hnotin].
rewrite IHl;
    last by [].
rewrite map_insert_commute.
    by [].
move => Heq.
by apply: Hneq.
Qed.

Lemma map_list_to_map_map {K V1 V2} {M: finMap K} (f: V1 -> V2) l :
    list_to_map (map (fun kv => (kv.1, f kv.2)) l) =
    map_map f ((list_to_map l): M V1).
Proof.
elim : l => [| [k v] l IHl] /=.
    by rewrite map_map_empty.
by rewrite map_map_insert IHl.
Qed.

Lemma map_to_list_empty {K V} {M: finMap K}:
    map_to_list ({[]} : M V) = [::].
Proof.
apply/PIn_empty.
move => [k v].
rewrite map_elem_of_to_list.
apply: lookup_empty_some.
Qed.

(* induction principle *)
Lemma map_ind {K V} {M: finMap K} (P: M V -> Type):
    P {[]} ->
    (forall k v m, m !! k = None -> P m -> P (<[k := v]> m)) ->
    (forall m, P m).
Proof.
move => Pempty HIm.
suff Huniq: (forall l,
                PUniq ((map (fun kv => kv.1)) l) ->
                forall m, m = list_to_map l -> P m).
    move => m.
    apply: (Huniq (map_to_list m)).
        by apply: uniq_fst_map_to_list.
    by rewrite map_to_list_to_map.
elim => [|[k v] l IHl] /=.
    by move => _ m ->.
move /uniq_inv => [Hnotin Huniq] m ->.
apply: HIm.
    by apply: map_not_in_list_to_map_lookup1.
by apply: IHl.
Qed.

(* Lemmas for map_size *)
Lemma map_size_empty {K V} {M: finMap K}:
    map_size ({[]}: M V) = 0.
Proof.
by rewrite /map_size map_to_list_empty.
Qed.

Lemma map_size_empty_iff {K V} {M: finMap K} (m: M V):
    map_size m = 0 <-> m = {[]}.
Proof.
rewrite /map_size length_zero_iff_nil.
elim /(@map_ind K) : m => [| k v m Hlookup IHm].
    by rewrite map_to_list_empty.
split;
    last first.
    move ->.
    by rewrite map_to_list_empty.
move => Hlist2map.
have : list_to_map (map_to_list (<[k:=v]> m)) = ({[]} : M V).
    by rewrite Hlist2map map_list_to_map_nil.
by rewrite map_to_list_to_map.
Qed.

Lemma map_size_non_empty_iff {K V} {M: finMap K} (m: M V):
    map_size m <> 0 <-> m <> {[]}.
Proof.
by rewrite map_size_empty_iff.
Qed.

(* lemmas for map_merge *)
Section map_merge_lemmas.
Context {K: eqType} {V: Type} {M: finMap K}.
Context (f: option V -> option V -> option V).
Lemma map_merge_empty_l (m: M V):
    (forall (o: option V), f None o = o) -> map_merge f {[]} m = m.
Proof.
move => Heq.
apply: map_extensionality => k.
rewrite map_lookup_merge ?map_lookup_empty;
    by apply: Heq.
Qed.

Lemma map_merge_empty_r (m: M V):
    (forall (o: option V), f o None = o) -> map_merge f m {[]} = m.
Proof.
move => Heq.
apply: map_extensionality => k.
rewrite map_lookup_merge ?map_lookup_empty;
    by apply: Heq.
Qed.

Lemma map_merge_empty_l_empty (m: M V):
    (forall (o: option V), f None o = None) -> map_merge f {[]} m = {[]}.
Proof.
move => Heq.
apply: map_extensionality => k.
rewrite map_lookup_merge ?map_lookup_empty;
    by apply: Heq.
Qed.

Lemma map_merge_empty_r_empty (m: M V):
    (forall (o: option V), f o None = None) -> map_merge f m {[]} = {[]}.
Proof.
move => Heq.
apply: map_extensionality => k.
by rewrite map_lookup_merge ?map_lookup_empty.
Qed.

End map_merge_lemmas.

Section map_merge_lemmas_hyp.
Context {K: eqType} {V: Type} {M: finMap K}.
Context (f: option V -> option V -> option V).
Hypotheses (HNone: f None None = None).

Lemma map_merge_comm (m1 m2: M V):
    (forall k, f (m1 !! k) (m2 !! k) = f (m2 !! k) (m1 !! k)) ->
    map_merge f m1 m2 = map_merge f m2 m1.
Proof.
move => Heq.
apply: map_extensionality => k.
by rewrite !map_lookup_merge.
Qed.

Lemma map_merge_assoc (m1 m2 m3: M V):
    (forall k, f (m1 !! k) (f (m2 !! k) (m3 !! k)) =
               f (f (m1 !! k)  (m2 !! k)) (m3 !! k)) ->
    map_merge f m1 (map_merge f m2 m3) = map_merge f (map_merge f m1 m2) m3.
Proof.
move => Hassoc.
apply: map_extensionality => k.
by rewrite !map_lookup_merge.
Qed.

Lemma map_merge_self (m: M V):
    (forall k, f (m !! k) (m !! k) = m !! k) -> map_merge f m m = m.
Proof.
move => Heq.
apply: map_extensionality => k.
by rewrite map_lookup_merge.
Qed.

End map_merge_lemmas_hyp.

Section map_merge_tri.
Context {K: eqType} {V1 V2 V: Type} {M: finMap K}.
Context (f: option V1 -> option V2 -> option V).
Hypotheses (HNone: f None None = None).

Lemma map_merge_some (m1: M V1) (m2: M V2)
                      (m: M V):
    map_merge f m1 m2 = m <-> forall k, (m !! k = f (m1 !! k) (m2 !! k)).
Proof.
rewrite -map_eq_iff.
split;
    move => Heq k;
    have := Heq k;
    by rewrite map_lookup_merge.
Qed.

Lemma map_merge_empty:
    map_merge f ({[]} : M V1) ({[]} : M V2) =
    ({[]} : M V).
Proof.
apply: map_extensionality => k.
by rewrite map_lookup_merge ?map_lookup_empty.
Qed.

Lemma map_merge_alter (m1: M V1) (m2: M V2) g g1 g2 k:
    g( f (m1 !! k) (m2 !! k)) = f (g1 (m1 !! k)) (g2 (m2 !! k)) ->
    map_alter g k (map_merge f m1 m2) =
    map_merge f (map_alter g1 k m1) (map_alter g2 k m2).
Proof.
move => Heq.
apply: map_extensionality => k'.
case (@eqP _ k' k) => [->|].
    by rewrite map_lookup_alter !map_lookup_merge ?map_lookup_alter.
move /eqP => Hneq.
by rewrite map_lookup_alter_neq ?map_lookup_merge ?map_lookup_alter_neq.
Qed.

Lemma map_merge_alter_l (m1: M V1) (m2: M V2) g g1 k:
    g( f (m1 !! k) (m2 !! k)) = f (g1 (m1 !! k)) (m2 !! k) ->
    map_alter g k (map_merge f m1 m2) =  map_merge f (map_alter g1 k m1) m2.
Proof.
move => Heq.
apply: map_extensionality => k'.
case (@eqP _ k' k) => [->|].
    by rewrite map_lookup_alter !map_lookup_merge ?map_lookup_alter.
move /eqP => Hneq.
by rewrite map_lookup_alter_neq ?map_lookup_merge ?map_lookup_alter_neq.
Qed.

Lemma map_merge_alter_r (m1: M V1) (m2: M V2) g g2 k:
    g( f (m1 !! k) (m2 !! k)) = f (m1 !! k) (g2 (m2 !! k)) ->
    map_alter g k (map_merge f m1 m2) = map_merge f m1 (map_alter g2 k m2).
Proof.
move => Heq.
apply: map_extensionality => k'.
case (@eqP _ k' k) => [->|].
    by rewrite map_lookup_alter !map_lookup_merge ?map_lookup_alter.
move /eqP => Hneq.
by rewrite map_lookup_alter_neq ?map_lookup_merge ?map_lookup_alter_neq.
Qed.

Lemma map_merge_insert (m1: M V1) (m2: M V2) k v1 v2 v :
    f (Some v1) (Some v2) = Some v ->
    <[k := v]> (map_merge f m1 m2) =
    map_merge f (<[k:= v1]> m1) (<[k:= v2]> m2).
Proof.
move => Heq.
by apply: map_merge_alter.
Qed.

Lemma map_merge_delete (m1: M V1) (m2: M V2) k:
    map_delete k (map_merge f m1 m2) =
    map_merge f (map_delete k m1) (map_delete k m2).
Proof.
by apply: map_merge_alter.
Qed.

Lemma map_mege_singleton k v1 v2 v :
    f (Some v1) (Some v2) = Some v ->
    map_merge f {[k := v1]} {[ k := v2]} = ({[ k := v]} : M V).
Proof.
move => Heq.
rewrite -!map_insert_empty.
rewrite -(map_merge_insert _ _ _ Heq).
by rewrite map_merge_empty.
Qed.

Lemma map_merge_insert_l (m1: M V1) (m2: M V2) k v1 v :
    f (Some v1) (m2 !! k) = Some v ->
    <[k := v]> (map_merge f m1 m2) = map_merge f (<[k:= v1]> m1) m2.
Proof.
move => Heq.
by apply: map_merge_alter_l.
Qed.

Lemma map_merge_insert_r (m1: M V1) (m2: M V2) k v2 v :
    f (m1 !! k) (Some v2) = Some v ->
    <[k := v]> (map_merge f m1 m2) = map_merge f m1 (<[k:= v2]> m2).
Proof.
move => Heq.
by apply: map_merge_alter_r.
Qed.

Lemma map_merge_map V' (g: V -> V')  (m1: M V1) (m2: M V2):
    map_map g (map_merge f m1 m2) =
    map_merge (fun v1 v2 => omap g (f v1 v2) ) m1 m2.
Proof.
apply: map_extensionality => k.
rewrite map_lookup_map !map_lookup_merge;
    by [rewrite HNone |].
Qed.

Lemma map_merge_fmap V' (g: V -> option V')  (m1: M V1)
                     (m2: M V2):
    map_fmap g (map_merge f m1 m2) =
    map_merge (fun v1 v2 => obind g (f v1 v2) ) m1 m2.
Proof.
apply: map_extensionality => k.
rewrite map_lookup_fmap !map_lookup_merge;
    by [rewrite HNone |].
Qed.

Lemma map_merge_empty_fmap_l (m2: M V2) :
    map_merge f {[]} m2 = map_fmap (fun v => f None (Some v)) m2.
Proof.
apply: map_extensionality => k.
rewrite map_lookup_merge;
    last by [].
rewrite map_lookup_fmap map_lookup_empty.
by case (m2 !! k).
Qed.

Lemma map_merge_empty_fmap_r  (m1: M V1) :
    map_merge f m1 {[]} = map_fmap (fun v => f (Some v) None) m1.
Proof.
apply: map_extensionality => k.
rewrite map_lookup_merge;
    last by [].
rewrite map_lookup_fmap map_lookup_empty.
by case (m1 !! k).
Qed.

End map_merge_tri.

Lemma map_merge_diag {K V V'} {M: finMap K}
                     (f : option V -> option V -> option V') (m: M V) :
    f None None = None ->
    map_merge f m m = map_fmap (fun v => f (Some v) (Some v)) m.
Proof.
move => HNone.
apply: map_extensionality => k.
rewrite map_lookup_merge;
    last by [].
rewrite map_lookup_fmap.
by case (m !! k).
Qed.

(* lemmas for map_fold *)

Lemma map_fold_empty {K B V} {M: finMap K} f (b: B):
    map_fold f b ({[]}: M V) = b.
Proof.
by rewrite /map_fold map_to_list_empty.
Qed.

(* lemmas for map_forall *)

Section map_forall.
Context {K: eqType} {V: Type} {M: finMap K}.
Context (P: K -> V -> Prop).
Lemma map_forall_to_list (m: M V):
    map_forall P m <-> seq_forall (fun kv => P kv.1 kv.2) (map_to_list m).
Proof.
rewrite seq_forall_forall /map_forall.
split.
-   move => Hforall [k v] /=.
    move /map_pin_to_list => /= Hlookup.
    have Hforallkv := Hforall k v.
    by apply: Hforallkv.
-   move => HIn k v Hlookup.
    have := HIn (k, v) => /=.
    rewrite map_pin_to_list /= => Himp.
    by apply: Himp.
Qed.

Lemma map_forall_empty: map_forall P ({[]}: M V).
Proof.
rewrite /map_forall => k v.
by rewrite map_lookup_empty.
Qed.

Lemma map_forall_imp (Q: K -> V -> Prop) (m: M V):
    map_forall P m -> (forall k v, P k v -> Q k v) -> map_forall Q m.
Proof.
rewrite /map_forall.
move => HForall HPQ k v Hlookup.
apply: HPQ.
by apply: HForall.
Qed.

Lemma map_forall_insert_1_1 (m: M V) k v:
    map_forall P (<[k := v]> m) -> P k v.
Proof.
rewrite /map_forall.
move => Hforall.
apply: Hforall.
apply:map_lookup_insert.
Qed.

Lemma map_forall_insert_1_2  (m: M V) k v:
    m !! k = None ->
    map_forall P (<[k := v]> m) ->
    map_forall P m.
Proof.
rewrite /map_forall.
move => Hlookup_none Hforall k' v' Hlookup_some.
apply: Hforall.
rewrite map_lookup_insert_neq.
    by apply: Hlookup_some.
by congruence.
Qed.

Lemma map_forall_insert_2 (m: M V) k v:
    P k v ->
    map_forall P m ->
    map_forall P (<[k := v]> m).
Proof.
rewrite /map_forall.
move => HP HForall k' v'.
case (@eqP _ k' k) => [->|Hneq].
    rewrite map_lookup_insert_some.
    move => [[_ <-]|[HFalse]].
        by [].
    by contradict HFalse.
rewrite map_lookup_insert_neq;
    last by [].
by apply: HForall.
Qed.

Lemma map_forall_insert (m: M V) k v:
    m !! k = None ->
    P k v /\ map_forall P m <->
    map_forall P (<[k := v]> m).

Proof.
move => Hlookup.
split.
    move => [].
    by apply: map_forall_insert_2.
move => Hforall.
split.
    apply: map_forall_insert_1_1.
    by apply: Hforall.
apply: map_forall_insert_1_2.
    by apply: Hlookup.
by apply: Hforall.
Qed.

Lemma mamp_forall_singleton k v :
    map_forall P ({[k := v]} : M V) <-> P k v.
Proof.
rewrite /map_forall.
split.
    move => Hforall.
    apply: Hforall.
    by apply: map_lookup_singleton.
move => HP k' v'.
rewrite map_lookup_singleton_some.
by move => [<- <-].
Qed.

Lemma map_forall_delete (m: M V) k:
    map_forall P m -> map_forall P (map_delete k m).
Proof.
rewrite /map_forall.
move => Hforall k' v'.
rewrite map_lookup_delete_some.
move => [Hneq Hlookup].
by apply: Hforall.
Qed.

Lemma map_forall_lookup (m: M V):
    map_forall P m <-> forall k v, m !! k = Some v -> P k v.
Proof.
by [].
Qed.

Lemma map_Forall_foldr_delete (m: M V) s :
    map_forall P m -> map_forall P (foldr map_delete m s).
Proof.
elim : s => [| a s' IHs] Hforall/=.
    by [].
apply: map_forall_delete.
by apply: IHs.
Qed.

Lemma map_forall_ind (Q: M V -> Prop):
    Q {[]} ->
    (forall m k v, m !! k = None ->
                   P k v ->
                   map_forall P m ->
                   Q m ->
                   Q (<[k := v]> m)) ->
    (forall m, map_forall P m -> Q m).
Proof.
move => Hempty Hinsert.
elim/(@map_ind K) => [| k v m Hlookup Himp].
    by [].
rewrite -map_forall_insert;
    last by [].
move => [HPH Hforall].
apply: Hinsert;
    by [apply: Himp |].
Qed.

End map_forall.
(* TODO map_Forall_dec and map_not_Forall*)

(* lemma for map_exists*)
Section map_exists.
Context {K: eqType} {V: Type} {M: finMap K}.
Context (P: K -> V -> Prop).

Lemma map_exists_to_list (m: M V):
    map_exists P m <-> seq_exists (fun kv => P kv.1 kv.2) (map_to_list m).
Proof.
rewrite /map_exists seq_exists_exists.
split.
-   move => [k [v [Hlookup HP]]].
    exists (k, v) => /=.
    by rewrite map_pin_to_list /=.
-   move => [[k v] []].
    rewrite map_pin_to_list /=.
    move => Hlookup HP.
    by exists k, v.
Qed.

Lemma map_exists_empty: ~ map_exists P ({[]}: M V).
Proof.
rewrite /map_exists.
move => [k [v []]].
by rewrite map_lookup_empty.
Qed.

Lemma map_exists_impl (Q: K -> V -> Prop) (m: M V):
    map_exists P m ->
    (forall k v, P k v -> Q k v) ->
    map_exists Q m.
Proof.
rewrite /map_exists.
move => [k [v [Hlookup HP]]] HPQ.
exists k, v.
split.
    by [].
by apply: HPQ.
Qed.

Lemma map_exists_insert_1 (m: M V) k v:
    map_exists P (<[k := v]> m) -> P k v \/ map_exists P m.
Proof.
rewrite /map_exists.
move => [k' [v'[]]].
rewrite map_lookup_insert_some.
move => [[<- <-]|[Hneq Hlookup] HP].
    by left.
right.
by exists k', v'.
Qed.

Lemma map_exists_insert_2_1 (m: M V) k v:
    P k v ->
    map_exists P (<[k := v]> m).
Proof.
rewrite /map_exists.
move => HP.
exists k, v.
by rewrite map_lookup_insert.
Qed.

Lemma map_exists_insert_2_2 (m: M V) k v:
    m !! k = None ->
    map_exists P m ->
    map_exists P (<[k := v]> m).
Proof.
rewrite /map_exists.
move => Hlookup_none [k' [v' [Hlookup HP]]].
exists k', v'.
rewrite map_lookup_insert_neq.
    by [].
by congruence.
Qed.

Lemma map_exists_insert (m: M V) k v:
    m !! k = None ->
    map_exists P (<[k := v]> m) <->
    (P k v \/ map_exists P m).

Proof.
move => Hlookup_none.
split.
    by apply: map_exists_insert_1.
move => [].
    by apply: map_exists_insert_2_1.
by apply: map_exists_insert_2_2.
Qed.

Lemma map_exists_singleton k v:
    map_exists P ({[k := v]} : M V) <-> P k v.
Proof.
rewrite /map_exists.
split.
    move => [k' [v' []]].
    rewrite map_lookup_singleton_some.
    by move => [-> ->].
move => HP.
exists k, v.
by rewrite map_lookup_singleton.
Qed.

Lemma map_exists_delete (m: M V) k:
    map_exists P (map_delete k m) ->map_exists P m.
Proof.
rewrite /map_exists.
move => [k' [v' []]].
rewrite map_lookup_delete_some.
move => [Hneq Hlookup] HP.
by exists k', v'.
Qed.

Lemma map_exists_lookup (m: M V):
    map_exists P m <-> exists k v, m !! k = Some v /\ P k v.
Proof.
by [].
Qed.

Lemma map_exists_foldr_delete (m: M V) s:
    map_exists P (foldr map_delete m s) -> map_exists P m.
Proof.
elim : s => [| a s' IHs] /=.
    by [].
move /map_exists_delete => Hexists.
by apply: IHs.
Qed.

Lemma map_exists_ind (Q: M V -> Prop):
    (forall k v, P k v -> Q {[k := v]}) ->
    (forall m k v,
            m !! k = None ->
            map_exists P m ->
            Q m ->
            Q (<[ k := v]> m)) ->
    (forall m, map_exists P m -> Q m).
Proof.
move => Hsingleton Hinsert.
elim /(@map_ind K) => [| k v m Hlookup IHm] Hexists.
    by have := (map_exists_empty Hexists).
move : Hexists.
rewrite map_exists_insert;
    last by [].
move => [HP| Hexists];
    last first.
    apply: Hinsert => //.
    by apply: IHm.
clear IHm.
elim /(@map_ind K): m Hlookup => [| k' v' m Hlookupk' IHm].
    move => _.
    by apply: Hsingleton.
rewrite map_lookup_insert_none.
move => [Hlookupk Hneq] .
rewrite map_insert_commute;
    last by [].
have Hneq2: k' <> k.
    move => Heq2.
    by apply: Hneq.
apply: Hinsert.
-   by apply/map_lookup_insert_none.
-   apply/map_exists_insert;
        by [left|].
-   by apply: IHm.
Qed.

Lemma map_not_exists_forall (m: M V):
    ~ map_exists P m <-> map_forall (fun k v => ~ P k v) m.
Proof.
rewrite /map_exists /map_forall.
split.
    move => Hnotexists k v Hlookup HP.
    apply: Hnotexists.
    by exists k, v.
move => Hforall [k [v [Hlookup]]].
by have := Hforall k v Hlookup.
Qed.
End map_exists.
(* TODO map_Exists_dec *)

(* Lemmas for map_agree *)
Lemma map_agree_spec {K V} {M: finMap K} (m1 m2: M V):
    map_agree m1 m2 <->
    forall k v1 v2, m1 !! k = Some v1 -> m2 !! k = Some v2 -> v1 = v2.
Proof.
rewrite /map_agree /map_lookup_relation.
split.
    move => Hrel k v1 v2 Hm1 Hm2.
    have := Hrel k.
    by rewrite Hm1 Hm2 /=.
move => Heq k.
have := Heq k.
case (m1 !! k) => [v1|];
    case (m2 !! k) => [v2|];
    move => Heqk /=;
    by [apply: Heqk|].
Qed.

Lemma map_agree_alt {K V} {M: finMap K} (m1 m2: M V):
    map_agree m1 m2 <->
    forall k, m1 !! k = None \/ m2 !! k = None \/  m1 !! k = m2 !! k.
Proof.
rewrite /map_agree /map_lookup_relation.
split;
    move => Hrel k;
    have := Hrel k;
    case (m1 !! k) => [v1|];
    case (m2 !! k) => [v2|] /=;
    try by [|
        move ->;
        right;
        right                |
        left                 |
        right;
        left                 |
        move => [|[|[->]]] //;
        discriminate
    ].
Qed.

Lemma map_agree_refl {K V} {M: finMap K} (m: M V):
    map_agree m m.
Proof.
rewrite map_agree_spec => k v1 v2 ->.
by move => [].
Qed.

Lemma map_agree_sym1 {K V} {M: finMap K} (m1 m2: M V):
    map_agree m1 m2 -> map_agree m2 m1.
Proof.
rewrite !map_agree_spec.
move => Hagree k v1 v2 Hm2 Hm1.
symmetry.
by apply: (Hagree k v2 v1).
Qed.

Lemma map_agree_sym {K V} {M: finMap K} (m1 m2: M V):
map_agree m1 m2 <-> map_agree m2 m1.
Proof.
by split;
    move => Hagree;
    apply: map_agree_sym1.
Qed.

Lemma map_agree_empty_l {K V} {M: finMap K} (m: M V):
    map_agree {[]} m.
Proof.
rewrite map_agree_spec => k v1 v2.
by rewrite map_lookup_empty.
Qed.

Lemma map_agree_empty_r {K V} {M: finMap K} (m: M V):
    map_agree m {[]}.
Proof.
rewrite map_agree_spec => k v1 v2.
by rewrite map_lookup_empty.
Qed.

Lemma map_agree_weaken {K V} {M: finMap K} (m1 m1' m2 m2': M V):
    map_agree m1' m2' -> m1 ⊆ₘ m1' -> m2 ⊆ₘ m2' -> map_agree m1 m2.
Proof.
rewrite !map_agree_spec !map_subseteq_spec.
move => Hagree' Hsubset1 Hsubset2 k v1 v2 Hlookup1 Hlookup2.
apply: (Hagree' k).
    by apply: Hsubset1.
by apply: Hsubset2.
Qed.

Lemma map_agree_weaken_l {K V} {M: finMap K} (m1 m1' m2 m2': M V):
    map_agree m1' m2' -> m1 ⊆ₘ m1' -> map_agree m1 m2'.
Proof.
move => Hagree Hsubset.
apply: map_agree_weaken;
    by [| apply: map_subseteq_refl].
Qed.

Lemma map_agree_weaken_r {K V} {M: finMap K} (m1 m1' m2 m2': M V):
    map_agree m1' m2' -> m2 ⊆ₘ m2' -> map_agree m1' m2.
Proof.
move => Hagree Hsubset.
apply: map_agree_weaken;
    by [| apply: map_subseteq_refl].
Qed.

Lemma map_agree_some_l {K V} {M: finMap K} (m1 m2: M V) k v:
    map_agree m1 m2 -> m1 !! k = Some v -> m2 !! k = Some v \/ m2 !! k = None.
Proof.
rewrite !map_agree_spec => Hspec Hlookup1.
case Hlookup2: (m2 !! k) => [v'|];
    last by right.
left.
have := Hspec k v v' Hlookup1 Hlookup2.
by move ->.
Qed.

Lemma map_agree_some_r {K V} {M: finMap K} (m1 m2: M V) k v:
    map_agree m1 m2 -> m2 !! k = Some v -> m1 !! k = Some v \/ m1 !! k = None.
rewrite map_agree_sym.
by apply: map_agree_some_l.
Qed.

Lemma map_agree_singleton_l {K V} {M: finMap K} (m: M V) k v:
    map_agree {[k := v]} m <-> m !! k = Some v \/ m !! k = None.
Proof.
split.
    move => Hagree.
    apply: map_agree_some_l.
        by apply: Hagree.
    by apply: map_lookup_singleton.
rewrite map_agree_spec.
move => [Hlookup|Hlookup] k' v1 v2;
    rewrite map_lookup_singleton_some;
    move => [<- <-];
    rewrite Hlookup;
    by [|move => []].
Qed.

Lemma map_agree_singleton_r {K V} {M: finMap K} (m: M V) k v:
    map_agree m {[k := v]} <-> m !! k = Some v \/ m !! k = None.
Proof.
by rewrite map_agree_sym map_agree_singleton_l.
Qed.

Lemma map_agree_delete_l {K V} {M: finMap K} (m1 m2: M V) k:
    map_agree m1 m2 -> map_agree (map_delete k m1) m2.
Proof.
rewrite !map_agree_alt => Hagree k'.
rewrite map_lookup_delete_none.
case : (Hagree k') => [->|[->|<-]].
-   left.
    by right.
-   right. by left.
case (@eqP _ k' k) => [<-|Hneq].
-   by repeat left.
-   repeat right.
    apply: map_lookup_delete_neq.
    by apply/eqP.
Qed.

Lemma map_agree_delete_r {K V} {M: finMap K} (m1 m2: M V) k:
    map_agree m1 m2 -> map_agree m2 (map_delete k m1).
move => Hagree.
rewrite map_agree_sym.
by apply: map_agree_delete_l.
Qed.

Lemma map_agree_map1 {K V1 V2} {M: finMap K}
                     (f: V1 -> V2) (m1 m2: M V1):
    (forall v1 v2, f v1 = f v2 -> v1 = v2) ->
    map_agree (map_map f m1) (map_map f m2) ->
    map_agree m1 m2.
Proof.
rewrite !map_agree_spec => Hext Hagree k v1 v2 Hlookup1 Hlookup2.
apply: Hext.
apply: (Hagree k);
    rewrite map_lookup_map_some.
    by exists v1.
by exists v2.
Qed.

Lemma map_agree_map2 {K V1 V2} {M: finMap K}
                     (f: V1 -> V2) (m1 m2: M V1):
    map_agree m1 m2 ->
    map_agree (map_map f m1) (map_map f m2).
Proof.
rewrite !map_agree_spec => Hagree k v1 v2.
rewrite !map_lookup_map_some.
move => [v1' [<- Hlookup1]] [v2' [<- Hlookup2]].
have Heq := Hagree k v1' v2' Hlookup1 Hlookup2.
by rewrite Heq.
Qed.

Lemma map_agree_map {K V1 V2} {M: finMap K}
                     (f: V1 -> V2) (m1 m2: M V1):
    (forall v1 v2, f v1 = f v2 -> v1 = v2) ->
    map_agree (map_map f m1) (map_map f m2) <->
    map_agree m1 m2.
move => Hext.
split.
    by apply: map_agree_map1.
by apply: map_agree_map2.
Qed.

Lemma map_agree_fmap {K V1 V2} {M: finMap K}
                     (f: V1 -> option V2) (m1 m2: M V1):
    map_agree m1 m2 ->
    map_agree (map_fmap f m1) (map_fmap f m2).
Proof.
rewrite !map_agree_spec => Hagree k v1 v2.
rewrite !map_lookup_fmap_some.
move => [v1' [Hf1 Hlookup1]] [v2' [Hf2 Hlookup2]].
suff : Some v1 = Some v2.
    by move => [].
rewrite -Hf2 -Hf1.
have Heq := Hagree k v1' v2' Hlookup1 Hlookup2.
by rewrite Heq.
Qed.

(* Lemmas for map_disjoint *)
Lemma map_disjoint_spec {K V} {M: finMap K} (m1 m2: M V):
    m1 ##ₘ m2 <->
    forall k v1 v2, m1 !! k = Some v1 -> m2 !! k = Some v2 -> False.
Proof.
rewrite /map_disjoint /map_lookup_relation.
split.
    move => Hdisjoint k.
    have := Hdisjoint k.
    by case : (m1 !! k) (m2 !! k) => [v1|] [v2|].
move => Hspec k.
have := Hspec k.
case : (m1 !! k) (m2 !! k) => [v1|] [v2|] //.
move => Hspeck.
have HFalse := Hspeck v1 v2.
contradict HFalse.
move => HFalse.
by apply: HFalse.
Qed.

Lemma map_disjoint_alt {K V} {M: finMap K} (m1 m2: M V):
    m1 ##ₘ m2 <->
    forall k, m1 !! k = None \/ m2 !! k = None.
Proof.
rewrite /map_disjoint /map_lookup_relation.
split.
    move => Hdisjoint k.
    have := Hdisjoint k.
    case : (m1 !! k) (m2 !! k) => [v1|] [v2|] /=;
    try by [|left|right].
move => Hspec k.
have := Hspec k.
case : (m1 !! k) (m2 !! k) => [v1|] [v2|] //.
by move => [|].
Qed.

Lemma map_disjoint_sym1 {K V} {M: finMap K} (m1 m2: M V):
    m1 ##ₘ m2 -> m2 ##ₘ m1.
Proof.
rewrite !map_disjoint_spec.
move => Hspec k v1 v2 Hlookup1 Hlookup2.
by apply: (Hspec k v2 v1).
Qed.

Lemma map_disjoint_sym {K V} {M: finMap K} (m1 m2: M V):
    m1 ##ₘ m2 <-> m2 ##ₘ m1.
Proof.
split;
    move => Hdisjoint;
    apply: map_disjoint_sym1;
    by apply: Hdisjoint.
Qed.

Lemma map_dsjoin_empty_l {K V} {M: finMap K} (m: M V):
    {[]}  ##ₘ m.
Proof.
rewrite map_disjoint_spec.
move => k v1 v2.
by rewrite map_lookup_empty.
Qed.

Lemma map_dsjoint_empty_r {K V} {M: finMap K} (m: M V):
    m ##ₘ {[]}.
Proof.
rewrite map_disjoint_sym.
by apply: map_dsjoin_empty_l.
Qed.

Lemma map_disjoint_weaken {K V} {M: finMap K} (m1 m1' m2 m2': M V):
    m1' ##ₘ m2' -> m1 ⊆ₘ m1' -> m2 ⊆ₘ m2' -> m1 ##ₘ m2.
Proof.
rewrite !map_disjoint_spec !map_subseteq_spec.
move => Hdisjoint' Hsubset1 Hsubset2 k v1 v2 Hlookup1 Hlookup2.
apply: (Hdisjoint' k v1 v2).
    by apply: Hsubset1.
by apply: Hsubset2.
Qed.

Lemma map_disjoint_weaken_l {K V} {M: finMap K} (m1 m1' m2 m2': M V):
    m1' ##ₘ m2' -> m1 ⊆ₘ m1' -> m1 ##ₘ m2'.
Proof.
move => Hdisjoint Hsubset.
apply: map_disjoint_weaken;
    by [| apply: map_subseteq_refl].
Qed.

Lemma map_disjoint_weaken_r {K V} {M: finMap K} (m1 m1' m2 m2': M V):
    m1' ##ₘ m2' -> m2 ⊆ₘ m2' -> m1' ##ₘ m2.
Proof.
move => Hdisjoint Hsubset.
apply: map_disjoint_weaken;
    by [| apply: map_subseteq_refl].
Qed.

Lemma map_disjoint_some_l {K V} {M: finMap K} (m1 m2: M V) k v:
    m1 ##ₘ m2 -> m1 !! k = Some v -> m2 !! k = None.
Proof.
rewrite !map_disjoint_spec => Hspec Hlookup1.
case Hlookup2: (m2 !! k) => [v'|];
    last by [].
by have := Hspec k v v' Hlookup1 Hlookup2.
Qed.

Lemma map_disjoint_some_r {K V} {M: finMap K} (m1 m2: M V) k v:
    m1 ##ₘ m2 -> m2 !! k = Some v -> m1 !! k = None.
rewrite map_disjoint_sym.
by apply: map_disjoint_some_l.
Qed.

Lemma map_disjoint_singleton_l {K V} {M: finMap K} (m: M V) k v:
    {[k := v]} ##ₘ m <-> m !! k = None.
Proof.
rewrite map_disjoint_spec.
split.
-   move => Hspec.
    have := Hspec k.
    case (m !! k) => [v2|] //=.
    move => Hspeck.
    have := Hspeck v v2.
    rewrite map_lookup_singleton => HFalse.
    contradict HFalse.
    move => HFalse.
    by apply: HFalse.
-   move => Hlookup k' v1 v2.
    rewrite map_lookup_singleton_some.
    move => [<- ].
    by rewrite Hlookup.
Qed.

Lemma map_disjoint_singleton_r {K V} {M: finMap K} (m: M V) k v:
    m ##ₘ {[k := v]} <-> m !! k = None.
Proof.
by rewrite map_disjoint_sym map_disjoint_singleton_l.
Qed.

Lemma map_disjoint_delete_l {K V} {M: finMap K} (m1 m2: M V) k:
    m1 ##ₘ m2 -> (map_delete k m1) ##ₘ m2.
Proof.
rewrite !map_disjoint_alt => Hspec k'.
rewrite map_lookup_delete_none.
case : (Hspec k') => [->|->].
-   left.
    by right.
-   by right.
Qed.

Lemma map_disjoint_delete_r {K V} {M: finMap K} (m1 m2: M V) k:
    m1 ##ₘ m2 -> m2 ##ₘ (map_delete k m1).
Proof.
move => Hdisjoint.
rewrite map_disjoint_sym.
by apply: map_disjoint_delete_l.
Qed.

Lemma map_disjoint_map {K V1 V2} {M: finMap K}
                       (f1 f2: V1 -> V2) (m1 m2: M V1):
    map_map f1 m1 ##ₘ map_map f2 m2 <-> m1 ##ₘ m2.
Proof.
rewrite !map_disjoint_spec.
split;
    move => Hspec k v1 v2.
-   move => Hlookup1 Hlookup2.
    apply: (Hspec k (f1 v1) (f2 v2));
        rewrite map_lookup_map_some.
        by exists v1.
    by exists v2.
-   rewrite !map_lookup_map_some.
    move => [v1'[Hf1 Hlookup1]] [v2' [Hf2 Hlookup2]].
    by apply: (Hspec k v1' v2').
Qed.

Lemma map_disjoint_fmap {K V1 V2} {M: finMap K} (m1 m2: M V1)
                         (f1 f2: V1 -> option V2):
    m1 ##ₘ m2 -> map_fmap f1 m1 ##ₘ map_fmap f2 m2.
Proof.
rewrite !map_disjoint_spec => Hspec k v1 v2.
rewrite !map_lookup_fmap_some.
move => [v1' [Hf1 Hlookup1]] [v2' [Hf2 Hlookup2]].
by apply: (Hspec k v1' v2').
Qed.

Lemma map_disjoint_agree {K V} {M: finMap K} (m1 m2: M V):
    m1 ##ₘ m2 -> map_agree m1 m2.
Proof.
rewrite map_disjoint_spec map_agree_spec.
move => Hspec k v1 v2 Hlookup1 Hlookup2.
by have := Hspec k v1 v2 Hlookup1 Hlookup2.
Qed.

(* Lemmas for map_union *)
Lemma map_union_empty_l {K V} {M: finMap K} (m: M V):
    map_union {[]} m = m.
Proof.
by rewrite /map_union map_merge_empty_l.
Qed.

Lemma map_union_empty_r {K V} {M: finMap K} (m: M V):
    map_union m {[]} = m.
Proof.
rewrite /map_union map_merge_empty_r.
    by [].
by case .
Qed.

Lemma map_union_assoc {K V} {M: finMap K} (m1 m2 m3: M V):
    map_union m1 (map_union m2 m3) = map_union (map_union m1 m2) m3.
Proof.
rewrite /map_union.
apply: map_merge_assoc.
    by [].
move => k.
by case : (m1 !! k) (m2 !! k) => [v|] [v'|].
Qed.

Lemma map_union_id {K V} {M: finMap K} (m: M V): map_union m m = m.
Proof.
rewrite /map_union.
apply: map_merge_self.
    by [].
move => k.
by case: (m !! k).
Qed.

Lemma map_union_lookup {K V} {M: finMap K} (m1 m2: M V) k :
    (map_union m1 m2) !! k =
    match m1 !! k with
    | some v => some v
    | None => m2 !! k
    end.
Proof.
by rewrite /map_union map_lookup_merge.
Qed.

Lemma map_union_lookup_r {K V} {M: finMap K} (m1 m2: M V) k:
    m1 !! k = None ->
    (map_union m1 m2 !! k) = m2 !! k.
Proof.
move => Hlookup.
by rewrite map_union_lookup Hlookup.
Qed.

Lemma map_union_lookup_l {K V} {M: finMap K} (m1 m2: M V) k:
    m2 !! k = None ->
    (map_union m1 m2 !! k) = m1 !! k.
Proof.
move => Hlookup.
rewrite map_union_lookup Hlookup.
by case: (m1 !! k).
Qed.

Lemma map_union_lookup_some_l {K V} {M: finMap K} (m1 m2: M V) k:
    (exists v, m1 !! k = Some v) ->
    (map_union m1 m2 !! k) = m1 !! k.
Proof.
move => [v Hlookup].
by rewrite map_union_lookup Hlookup.
Qed.

Lemma map_lookup_union_some_raw {K V} {M: finMap K} (m1 m2: M V) k v:
    (map_union m1 m2) !! k = Some v <->
    m1 !! k = Some v \/ (m1 !! k = None /\ m2 !! k = Some v).
Proof.
rewrite map_union_lookup.
case (m1 !! k) => [v1|];
    case (m2 !! k) => [v2|];
    split.
-   move => [->].
    by left.
-   move => [->|[]];
        by [|discriminate].
-   move => [->].
    by left.
-   move => [->|[]];
        by [|discriminate].
-   move => [->].
    by right.
-   move => [|[_ ->]];
        by [|discriminate].
-   by discriminate.
-   move => [|[_ ->]];
        by [|discriminate].
Qed.

Lemma map_lookup_union_none {K V} {M: finMap K} (m1 m2: M V) k:
    (map_union m1 m2) !! k = None <-> m1 !! k = None /\ m2 !! k = None.
Proof.
Proof.
rewrite map_union_lookup.
case (m1 !! k) => [v1|];
    case (m2 !! k) => [v2|]; split;
    by [
        discriminate |
        move => [] |
    ].
Qed.

Lemma map_lookup_union_some {K V} {M: finMap K} (m1 m2: M V) k v:
    m1 ##ₘ m2 ->
    (map_union m1 m2 ) !! k = Some v <->
    m1 !! k = Some v \/ m2 !! k = Some v.
Proof.
rewrite map_lookup_union_some_raw.
move /map_disjoint_spec => Hspec.
split.
    move => [Hlookup|[ Hlookup1 Hlookup2]].
        by left.
    by right.
move => [Hlookup1|Hlookup2].
    by left.
right.
case Hlookup1 : (m1 !! k) => [v'|].
    by have := Hspec k v' v Hlookup1 Hlookup2.
by [].
Qed.


Lemma map_lookup_union_some_l {K V} {M: finMap K} (m1 m2: M V) k v:
    m1 !! k = Some v ->
    (map_union m1 m2 ) !! k = Some v.
Proof.
rewrite map_lookup_union_some_raw.
by left.
Qed.

Lemma map_lookup_union_some_r {K V} {M: finMap K} (m1 m2: M V) k v:
    m1 ##ₘ m2 ->
    m2 !! k = Some v ->
    (map_union m1 m2 ) !! k = Some v.
Proof.
move => Hdisjoint Hlookup2.
rewrite map_lookup_union_some.
    by right.
by [].
Qed.

Lemma map_lookup_inv_l {K V} {M: finMap K} (m1 m2: M V) k v:
    (map_union m1 m2 ) !! k = Some v ->
    m2 !! k = None ->
    m1 !! k = Some v.
Proof.
rewrite map_lookup_union_some_raw.
by move => [|[_ ->]].
Qed.

Lemma map_lookup_inv_r {K V} {M: finMap K} (m1 m2: M V) k v:
    (map_union m1 m2 ) !! k = Some v ->
    m1 !! k = None ->
    m2 !! k = Some v.
Proof.
rewrite map_lookup_union_some_raw.
by move => [->|[-> ->]].
Qed.

Lemma map_union_comm {K V} {M: finMap K} (m1 m2: M V):
    m1 ##ₘ m2 ->
    map_union m1 m2 = map_union m2 m1.
Proof.
move /map_disjoint_spec=> Hspec.
rewrite /map_union map_merge_comm //=.
move => k.
case Hlookup1 : (m1 !! k) => [v1|];
    case Hlookup2 : (m2 !! k) => [v2|];
    by [| have := Hspec k v1 v2 Hlookup1 Hlookup2].
Qed.

Lemma map_union_inv_empty_l {K V} {M: finMap K} (m1 m2: M V):
    map_union m1 m2 = {[]} -> m1 = {[]}.
Proof.
move /map_empty_spec => Hlookup.
apply/ map_empty_spec => k.
have := Hlookup k.
rewrite map_lookup_union_none.
by move => [].
Qed.

Lemma map_union_inv_empty_r {K V} {M: finMap K} (m1 m2: M V):
    map_union m1 m2 = {[]} -> m2 = {[]}.
Proof.
move /map_empty_spec => Hlookup.
apply/ map_empty_spec => k.
have := Hlookup k.
rewrite map_lookup_union_none.
by move => [].
Qed.

Lemma map_union_inv_empty_l_alt {K V} {M: finMap K} (m1 m2: M V):
    m1 <> {[]} ->map_union m1 m2 <> {[]}.
Proof.
move => Hneq Hunion.
apply: Hneq.
apply: map_union_inv_empty_l.
by apply: Hunion.
Qed.

Lemma map_union_inv_empty_r_alt {K V} {M: finMap K} (m1 m2: M V):
    m2 <> {[]} ->map_union m1 m2 <> {[]}.
Proof.
move => Hneq Hunion.
apply: Hneq.
apply: map_union_inv_empty_r.
by apply: Hunion.
Qed.

Lemma map_subseteq_union {K V} {M: finMap K} (m1 m2: M V):
    m1 ⊆ₘ m2 -> map_union m1 m2 = m2.
Proof.
rewrite map_subseteq_spec => Hspec.
apply: map_extensionality => k.
case Hlookup1: (m1 !! k) => [v1|].
    have := Hspec k v1 Hlookup1.
    move ->.
    by apply: map_lookup_union_some_l.
case Hlookup2: (m2 !! k) => [v2|].
    apply/ map_lookup_union_some_raw.
    by right.
by rewrite map_lookup_union_none.
Qed.

Lemma map_union_subseteq_l {K V} {M: finMap K} (m1 m2: M V):
    m1 ⊆ₘ map_union m1 m2.
Proof.
rewrite map_subseteq_spec => k v Hlookup.
rewrite map_lookup_union_some_raw.
by left.
Qed.

Lemma map_union_subseteq_r {K V} {M: finMap K} (m1 m2: M V):
    m1 ##ₘ m2 -> m2 ⊆ₘ map_union m1 m2.
Proof.
move /map_disjoint_spec => Hspec.
rewrite map_subseteq_spec => k v Hlookup2.
rewrite map_lookup_union_some_raw.
right.
case Hlookup1: (m1 !! k) => [v1|].
    by have := Hspec k v1 v Hlookup1 Hlookup2.
by [].
Qed.

Lemma map_subseteq_subseteq_union_l {K V} {M: finMap K}
                                    (m1 m2 m3: M V):
    m1 ⊆ₘ m2 -> m1 ⊆ₘ map_union m2 m3.
Proof.
move => Hsubset.
apply: map_subseteq_trans.
    by apply: Hsubset.
by apply: map_union_subseteq_l.
Qed.

Lemma map_subseteq_subseteq_union_r {K V} {M: finMap K}
                                    (m1 m2 m3: M V):
    m2 ##ₘ m3-> m1 ⊆ₘ m3 -> m1 ⊆ₘ map_union m2 m3.
Proof.
move => Hdisjoint Hsubset.
apply: map_subseteq_trans.
    by apply: Hsubset.
by apply: map_union_subseteq_r.
Qed.

Lemma map_union_least {K V} {M: finMap K} (m1 m2 m3: M V):
    m1 ⊆ₘ m3 -> m2 ⊆ₘ m3 -> map_union m1 m2 ⊆ₘ m3.
Proof.
move => Hsubset13 Hsubset23.
apply/map_subseteq_spec => k v.
rewrite map_lookup_union_some_raw.
move => [Hlookup1|[Hlookup1 Hlookup2]];
    apply: map_lookup_weaken.
-   by apply: Hlookup1.
-   by apply: Hsubset13.
-   by apply: Hlookup2.
-   by apply: Hsubset23.
Qed.

Lemma map_union_mono_l {K V} {M: finMap K} (m1 m2 m3: M V):
    m1 ⊆ₘ m3 -> map_union m2 m1  ⊆ₘ map_union m2 m3.
Proof.
rewrite !map_subseteq_spec => Hspec k v.
rewrite !map_lookup_union_some_raw.
move => [Hlookup2|[Hlookup2 Hlookup1]].
    by left.
have ? := Hspec k v Hlookup1.
by right.
Qed.


Lemma map_union_mono_r {K V} {M: finMap K} (m1 m2 m3: M V):
    m2 ##ₘ m3 -> m1 ⊆ₘ m3 -> map_union m1 m2  ⊆ₘ map_union m3 m2.
Proof.
rewrite map_disjoint_alt !map_subseteq_spec => Hdisjoint Hsubset k v.
rewrite !map_lookup_union_some_raw.
move => [Hlookup2|[]].
    have ? := Hsubset k v Hlookup2.
    by left.
case (Hdisjoint k) => [->|].
    by [].
by right.
Qed.

Lemma map_union_reflecting_l {K V} {M: finMap K} (m1 m2 m3: M V):
    m3 ##ₘ m1 -> m3 ##ₘ m2 -> map_union m3 m1 ⊆ₘ map_union m3 m2 -> m1 ⊆ₘ m2.
Proof.
rewrite !map_subseteq_spec => Hdisjoint31 Hdisjoint32 Hspec k v Hlookup1.
have := Hspec k v.
rewrite !map_lookup_union_some //= => Himp.
have : m3 !! k = Some v \/ m2 !! k = Some v.
    apply: Himp.
    by right.
move => [Hlookup3|] //=.
move : Hdisjoint31.
move /map_disjoint_spec => Hdisjoint13.
by have := Hdisjoint13 k v v Hlookup3 Hlookup1.
Qed.

Lemma map_union_reflecting_r {K V} {M: finMap K} (m1 m2 m3: M V):
    m1 ##ₘ m3 -> m2 ##ₘ m3 -> map_union m1 m3 ⊆ₘ map_union m2 m3 -> m1 ⊆ₘ m2.
Proof.
move => Hdisjoint13 Hdisjoint23.
rewrite !(@map_union_comm _ _ _ _ m3) //=.
apply: map_union_reflecting_l;
    by apply/ map_disjoint_sym.
Qed.

Lemma map_union_cancel_l {K V} {M: finMap K} (m1 m2 m3: M V):
     m3 ##ₘ m1 -> m3 ##ₘ m2 -> map_union m3 m1 = map_union m3 m2 -> m1 = m2.
Proof.
move => Hdisjoint13 Hdisjoint23.
move /map_subseteq_subseteq_eq => [Hsubset12 Hsubset21].
apply/map_subseteq_subseteq_eq.
split;
apply: map_union_reflecting_l => //=.
Qed.

Lemma map_union_cancel_r {K V} {M: finMap K} (m1 m2 m3: M V):
     m1 ##ₘ m3 -> m2 ##ₘ m3 -> map_union m1 m3 = map_union m2 m3 -> m1 = m2.
Proof.
move => Hdisjoint13 Hdisjoint23.
move /map_subseteq_subseteq_eq => [Hsubset12 Hsubset21].
apply/map_subseteq_subseteq_eq.
split;
by apply: map_union_reflecting_r => //=.
Qed.

Lemma map_disjoint_union_l1 {K V} {M: finMap K} (m1 m2 m3: M V):
    map_union m1 m2 ##ₘ m3 -> m1 ##ₘ m3 /\ m2 ##ₘ m3.
Proof.
rewrite !map_disjoint_alt => Hunionspec.
split => k;
    case (Hunionspec k) => [|Hlookup1];
    by [
        right |
        rewrite map_lookup_union_none;
        move => [];
        left
    ].
Qed.

Lemma map_disjoint_union_l2 {K V} {M: finMap K} (m1 m2 m3: M V):
    m1 ##ₘ m3 -> m2 ##ₘ m3 -> map_union m1 m2 ##ₘ m3.
Proof.
rewrite !map_disjoint_alt => Hspec13 Hspec23 k.
rewrite map_lookup_union_none.
case (Hspec13 k) => [|];
    last by right.
case (Hspec23 k) => [|];
    last by right.
by left.
Qed.

Lemma map_disjoint_union_l {K V} {M: finMap K} (m1 m2 m3: M V):
    map_union m1 m2 ##ₘ m3 <-> m1 ##ₘ m3 /\ m2 ##ₘ m3.
Proof.
split.
    by apply: map_disjoint_union_l1.
move => [].
by apply: map_disjoint_union_l2.
Qed.

Lemma map_disjoint_union_r {K V} {M: finMap K} (m1 m2 m3: M V):
    m1 ##ₘ map_union m2 m3 <-> m1 ##ₘ m2 /\ m1 ##ₘ m3.
Proof.
rewrite map_disjoint_sym map_disjoint_union_l.
by rewrite (@map_disjoint_sym _ _ _ m2) (@map_disjoint_sym _ _ _ m3).
Qed.

Lemma map_insert_union_singleten_l {K V} {M: finMap K} (m: M V) k v:
    map_union {[ k := v]} m = <[k := v]> m.
Proof.
apply/map_extensionality => k'.
case (@eqP _ k' k) => [->|Hneq].
    rewrite map_lookup_insert map_lookup_union_some_raw.
    left.
    by apply: map_lookup_singleton.
rewrite map_lookup_insert_neq;
    last by [].
case Hlookup: (m !! k') => [v'|].
    rewrite map_lookup_union_some_raw.
    right.
    by rewrite map_lookup_singleton_neq.
by rewrite map_lookup_union_none map_lookup_singleton_neq.
Qed.

Lemma map_insert_union_singleton_r {K V} {M: finMap K} (m: M V) k v:
    m !! k = None -> map_union m {[ k := v]} = <[k := v]> m.
Proof.
move => Hlookup.
rewrite -map_insert_union_singleten_l map_union_comm.
    by [].
by apply/ map_disjoint_singleton_r.
Qed.

Lemma map_union_singleton_r {K V} {M: finMap K} (m: M V) k v:
    m !! k = Some v -> map_union m {[k := v]} = m.
Proof.
move => Hlookup.
apply/map_extensionality => k'.
case Hlookup': (m !! k') => [v'|].
    rewrite map_lookup_union_some_raw.
    by left.
rewrite map_lookup_union_none.
rewrite map_lookup_singleton_neq.
    by [].
move => Heq.
move : Hlookup'.
by rewrite Heq Hlookup.
Qed.

Lemma map_disjoint_insert_l {K V} {M: finMap K} (m1 m2: M V) k v:
    <[k := v]> m1 ##ₘ m2 <-> m2 !! k = None /\ m1 ##ₘ m2.
Proof.
rewrite -map_insert_union_singleten_l map_disjoint_union_l.
by rewrite map_disjoint_singleton_l.
Qed.

Lemma map_disjoint_insert_r {K V} {M: finMap K} (m1 m2: M V) k v:
    m1 ##ₘ <[k := v]> m2 <-> m1 !! k = None /\ m1 ##ₘ m2.
Proof.
rewrite -map_insert_union_singleten_l map_disjoint_union_r.
by rewrite map_disjoint_singleton_r.
Qed.

Lemma map_insert_union_l {K V} {M: finMap K} (m1 m2: M V) k v:
    <[k := v]>(map_union m1 m2) = map_union (<[k := v]> m1) m2.
Proof.
by rewrite -!map_insert_union_singleten_l map_union_assoc.
Qed.

Lemma map_insert_union_r {K V} {M: finMap K} (m1 m2: M V) k v:
    m1 !! k =None ->
    <[k := v]>(map_union m1 m2) = map_union m1 (<[k := v]> m2).
Proof.
move => Hlookup.
rewrite -!map_insert_union_singleten_l map_union_assoc.
rewrite (@map_union_assoc _ _ _ m1) (@map_union_comm _ _ _ m1).
    by [].
by rewrite map_disjoint_singleton_r.
Qed.

Lemma map_foldr_insert_union {K V} {M: finMap K} (m: M V) l:
    foldr (fun kv => <[kv.1 := kv.2]>) m l = map_union (list_to_map l) m.
Proof.
elim : l => [|[k v] l IHl] /=.
    by rewrite map_union_empty_l.
by rewrite -map_insert_union_l IHl.
Qed.

Lemma map_delete_union {K V} {M: finMap K} (m1 m2: M V) k :
    map_delete k (map_union m1 m2) =
    map_union (map_delete k m1) (map_delete k m2).
Proof.
apply/map_extensionality => k'.
rewrite map_union_lookup.
case (@eqP _ k' k) => [->|Hneq].
    by rewrite !map_lookup_delete.
rewrite !map_lookup_delete_neq -?map_union_lookup //=;
    by apply/eqP.
Qed.

Lemma map_union_delete_insert {K V} {M: finMap K} (m1 m2: M V) k v:
    m1 !! k = Some v ->
    map_union (map_delete k m1) (<[k := v]> m2) = map_union m1 m2.
Proof.
move => Hlookup.
rewrite -map_insert_union_r;
    last by apply: map_lookup_delete.
by rewrite map_insert_union_l map_insert_delete.
Qed.

Lemma map_union_insert_delete {K V} {M: finMap K} (m1 m2: M V) k v:
    m1 !! k = None -> m2 !! k = Some v ->
    map_union (<[k := v]> m1) (map_delete k m2) = map_union m1 m2.
Proof.
move => Hlookup1 Hlookup2.
by rewrite -map_insert_union_l map_insert_union_r ?map_insert_delete.
Qed.

Lemma map_forall_union_1_1 {K V} {M: finMap K} (m1 m2: M V) P :
    map_forall P (map_union m1 m2) -> map_forall P m1.
Proof.
move => Hforall k v Hlookup.
apply: Hforall.
apply/ map_lookup_union_some_raw.
by left.
Qed.

Lemma map_forall_union_1_2 {K V} {M: finMap K} (m1 m2: M V) P :
    m1 ##ₘ m2 -> map_forall P (map_union m1 m2) -> map_forall P m2.
Proof.
move => Hdisjoint Hforall k v Hlookup.
apply: Hforall.
apply map_lookup_union_some.
    by [].
by right.
Qed.

Lemma map_forall_union_2 {K V} {M: finMap K} (m1 m2: M V) P:
    map_forall P m1 ->
    map_forall P m2 ->
    map_forall P (map_union m1 m2).
Proof.
move => Hforall1 Hforall2 k v.
rewrite map_lookup_union_some_raw.
move => [Hlookup1|[Hlookup1 Hlookup2]].
    by apply: Hforall1.
by apply: Hforall2.
Qed.

Lemma map_filter_union {K V} {M: finMap K} (m1 m2: M V) P:
    m1 ##ₘ m2 ->
    map_forall P m1 /\ map_forall P m2 <-> map_forall P (map_union m1 m2).
Proof.
move => Hdisjoint.
split.
    move => [].
    by apply: map_forall_union_2.
move => Hunion.
split.
    apply: map_forall_union_1_1.
    by apply: Hunion.
apply: map_forall_union_1_2.
    by apply: Hdisjoint.
by apply: Hunion.
Qed.

Lemma map_map_union {K V1 V2} {M: finMap K}
                    (m1 m2: M V1) (f: V1 -> V2):
    map_union (map_map f m1) (map_map f m2) = map_map f (map_union m1 m2).
Proof.
apply: map_extensionality => k.
rewrite map_lookup_map !map_union_lookup.
by case Hlookup1: (m1 !! k) => [v1|];
    case Hlookup2: (m2 !! k) => [v2|];
    rewrite !map_lookup_map Hlookup1 Hlookup2.
Qed.

Lemma map_fmap_union {K V1 V2} {M: finMap K}
                     (m1 m2: M V1) (f: V1 -> option V2):
    m1 ##ₘ m2 ->
    map_fmap f (map_union m1 m2) = map_union (map_fmap f m1) (map_fmap f m2).
Proof.
move /map_disjoint_spec=> Hspec.
apply:map_extensionality => k.
rewrite map_lookup_fmap !map_union_lookup.
case Hlookup1: (m1 !! k) => [v1|];
    case Hlookup2: (m2 !! k) => [v2|];
    rewrite !map_lookup_fmap Hlookup1 Hlookup2 /=.
-   by have := Hspec k v1 v2 Hlookup1 Hlookup2.
-   by case (f v1).
-   by [].
-   by [].
Qed.