(*****************************************************************************)
(* Copyright 2022 TheWalker77                                                *)
(*                                                                           *)
(* Licensed under the Apache License, Version 2.0 (the "License");           *)
(* you may not use this file except in compliance with the License.          *)
(* You may obtain a copy of the License at                                   *)
(*                                                                           *)
(*     http://www.apache.org/licenses/LICENSE-2.0                            *)
(*                                                                           *)
(* Unless required by applicable law or agreed to in writing, software       *)
(* distributed under the License is distributed on an "AS IS" BASIS,         *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *)
(* See the License for the specific language governing permissions and       *)
(* limitations under the License.                                            *)
(*****************************************************************************)

From HB Require Import structures.
From mathcomp Require Import all_ssreflect zify.
From sir.common Require Import pos seq fin_maps.fin_maps.
Import Pos.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(******************************************************************************)
(* This file implements a Binary trie with key drawn from [positive] and      *)
(* a value of any type. The original data structure is designed and analyzed  *)
(* by Andrew W Appel and Xavier Leroy in their paper:                         *)
(* "Efficient Extensional Binary Tries". The given implementation closely     *)
(* follows their general approach which is implemented at:                    *)
(*    https://github.com/xavierleroy/canonical-binary-tries/tree/v2           *)
(* which is Licensed under BSD-3-Clause license. The main differences is that *)
(* we design this library to have API and notations similar to that of stdpp  *)
(* while making the implementation small step reflection friendly.            *)
(*                                                                            *)
(* The main data structure is [Trie]                                          *)
(*                                                                            *)
(* The main operations:                                                       *)
(* - [lookup]                                                                 *)
(* - [singleton]                                                              *)
(* - [map]                                                                    *)
(* - [fmap]                                                                   *)
(* - [alter]                                                                  *)
(* - [to_list]                                                                *)
(* - [merge]                                                                  *)
(*                                                                            *)
(* The main lemmas:                                                           *)
(* - [lookup_empty]                                                           *)
(* - [lookup_singleton]                                                       *)
(* - [lookup_singleton_neq]                                                   *)
(* - [lookup_alter_empty]                                                     *)
(* - [lookup_alter]                                                           *)
(* - [lookup_alter_empty_neq]                                                 *)
(* - [lookup_alter_neq]                                                       *)
(* - [lookup_map]                                                             *)
(* - [lookup_fmap]                                                            *)
(* - [unroll_fmap]                                                            *)
(* - [extensionality_empty]                                                   *)
(* - [extensionality]                                                         *)
(* - [lookup_merge]                                                           *)
(* - [elem_of_to_list]                                                        *)
(* - [to_list_no_dup]                                                         *)
(******************************************************************************)

Module Trie.

(* TNode is the type of trie non empty node *)
Inductive TNode (A: Type) : Type :=
| TNode001: TNode A -> TNode A
| TNode010: A -> TNode A
| TNode011: A -> TNode A -> TNode A
| TNode100: TNode A -> TNode A
| TNode101: TNode A -> TNode A ->TNode A
| TNode110: TNode A -> A -> TNode A
| TNode111: TNode A -> A -> TNode A -> TNode A.
Arguments TNode001 {A} _.
Arguments TNode010 {A} _.
Arguments TNode011 {A} _ _.
Arguments TNode100 {A} _.
Arguments TNode101 {A} _ _.
Arguments TNode110 {A} _ _.
Arguments TNode111 {A} _ _ _.

(* [Trie] is the type of binary tries, it has a reperesentation for both      *)
(* empty tries and  non empty ones.                                           *)
Inductive Trie (A: Type): Type :=
| Tempty : Trie A
| Troot: TNode A -> Trie A.
Arguments Tempty {A}.
Arguments Troot {A} _.

Section Trie_Operations.
Context {A B: Type}.

Definition builder (l: Trie A) (o: option A) (r: Trie A) : Trie A :=
match l,o,r with
| Tempty, None, Tempty => Tempty
| Tempty, None, Troot r' => Troot (TNode001 r')
| Tempty, Some x, Tempty => Troot (TNode010 x)
| Tempty, Some x, Troot r' => Troot (TNode011 x r')
| Troot l', None, Tempty => Troot (TNode100 l')
| Troot l', None, Troot r' => Troot (TNode101 l' r')
| Troot l', Some x, Tempty => Troot (TNode110 l' x)
| Troot l', Some x, Troot r' => Troot (TNode111 l' x r')
end.
Global Arguments builder: simpl never.

Fixpoint tnode_lookup (p: positive) (n: TNode A) : option A :=
match p, n with
| xH, TNode001 _ => None
| xH, TNode010 x => Some x
| xH, TNode011 x _ => Some x
| xH, TNode100 _ => None
| xH, TNode101 _ _ => None
| xH, TNode110 _ x => Some x
| xH, TNode111 _ x _ => Some x
| xO q, TNode001 _ => None
| xO q, TNode010 _ => None
| xO q, TNode011 _ _ => None
| xO q, TNode100 m' => tnode_lookup q m'
| xO q, TNode101 m' _ => tnode_lookup q m'
| xO q, TNode110 m' _ => tnode_lookup q m'
| xO q, TNode111 m' _ _ => tnode_lookup q m'
| xI q, TNode001 m' => tnode_lookup q m'
| xI q, TNode010 _ => None
| xI q, TNode011 _ m' => tnode_lookup q m'
| xI q, TNode100 m' => None
| xI q, TNode101 _ m' => tnode_lookup q m'
| xI q, TNode110 _ _ => None
| xI q, TNode111 _ _ m' => tnode_lookup q m'
end.

Definition lookup (p: positive) (t: Trie A) : option A :=
match t with
| Tempty => None
| Troot n => tnode_lookup p n
end.
Global Arguments lookup: simpl never.

Fixpoint tnode_singleton (p: positive) (a: A) :=
 match p with
| xH => TNode010 a
| xO q => TNode100 (tnode_singleton q a)
| xI q => TNode001 (tnode_singleton q a)
end.

Definition singleton (p: positive) (a: A) := Troot (tnode_singleton p a).
Global Arguments singleton: simpl never.

Fixpoint tnode_map (f: A -> B) (n: TNode A) : TNode B :=
match n with
| TNode001 r => TNode001 (tnode_map f r)
| TNode010 a => TNode010 (f a)
| TNode011 a r => TNode011 (f a) (tnode_map f r)
| TNode100 l => TNode100 (tnode_map f l)
| TNode101 l r => TNode101 (tnode_map f l) (tnode_map f r)
| TNode110 l a => TNode110 (tnode_map f l) (f a)
| TNode111 l a r => TNode111 (tnode_map f l) (f a) (tnode_map f r)
end.

Definition map (f: A -> B) (t: Trie A) : Trie B :=
match t with
| Tempty => Tempty
| Troot n => Troot (tnode_map f n)
end.

Fixpoint tnode_fmap (f: B -> option A) (n: TNode B) : Trie A :=
match n with
| TNode001 r => builder Tempty None (tnode_fmap f r)
| TNode010 x => builder Tempty (f x) Tempty
| TNode011 x r => builder Tempty (f x) (tnode_fmap f r)
| TNode100 l => builder (tnode_fmap f l) None Tempty
| TNode101 l r => builder (tnode_fmap f l) None (tnode_fmap f r)
| TNode110 l x => builder (tnode_fmap f l) (f x) Tempty
| TNode111 l x r => builder (tnode_fmap f l) (f x) (tnode_fmap f r)
end.

Definition tnode_fmap_opt := Eval cbv [tnode_fmap builder] in tnode_fmap.

Definition fmap (f: B -> option A) (t: Trie B) : Trie A :=
match t with
| Tempty => Tempty
| Troot n => tnode_fmap_opt f n
end.
Global Arguments map: simpl never.

Definition alter_empty (f: option A -> option A) (p: positive) : Trie A :=
match f None with
| None => Tempty
| Some a => singleton p a
end.

Fixpoint tnode_alter (f: option A -> option A) (p: positive) (n: TNode A)
                     {struct n} : Trie A :=
match p, n with
| xH, TNode001 r => builder Tempty (f None) (Troot r)
| xH, TNode010 a => builder Tempty (f (Some a)) Tempty
| xH, TNode011 a r => builder Tempty (f (Some a)) (Troot r)
| xH, TNode100 l => builder (Troot l) (f None) Tempty
| xH, TNode101 l r => builder (Troot l) (f None) (Troot r)
| xH, TNode110 l a => builder (Troot l) (f (Some a)) Tempty
| xH, TNode111 l a r => builder (Troot l) (f (Some a)) (Troot r)
| xO q, TNode001 r => builder (alter_empty f q) None (Troot r)
| xO q, TNode010 a => builder (alter_empty f q) (Some a) Tempty
| xO q, TNode011 a r => builder (alter_empty f q) (Some a) (Troot r)
| xO q, TNode100 l => builder (tnode_alter f q l) None Tempty
| xO q, TNode101 l r => builder (tnode_alter f q l) None (Troot r)
| xO q, TNode110 l y => builder (tnode_alter f q l) (Some y) Tempty
| xO q, TNode111 l y r => builder (tnode_alter f q l) (Some y) (Troot r)
| xI q, TNode001 r => builder Tempty None (tnode_alter f q r)
| xI q, TNode010 a => builder Tempty (Some a) (alter_empty f q)
| xI q, TNode011 a r => builder Tempty (Some a) (tnode_alter f q r)
| xI q, TNode100 l => builder (Troot l) None (alter_empty f q)
| xI q, TNode101 l r => builder (Troot l) None (tnode_alter f q r)
| xI q, TNode110 l a => builder (Troot l) (Some a) (alter_empty f q)
| xI q, TNode111 l a r => builder (Troot l) (Some a) (tnode_alter f q r)
end.
Definition tnode_alter_opt := Eval cbv [tnode_alter builder] in tnode_alter.

Definition alter (f: option A -> option A)
                      (p: positive) (t: Trie A): Trie A :=
match t with
| Tempty => alter_empty f p
| Troot n => tnode_alter_opt f p n
end.

Fixpoint tnode_to_list (p: positive) (n: TNode A)
                      (acc: list (positive * A)) : list (positive * A) :=
let ttl := tnode_to_list in
match n with
| TNode001 r => ttl (xI p) r acc
| TNode010 a => (rev p, a)::acc
| TNode011 a r=> (rev p, a)::(ttl (xI p) r acc)
| TNode100 l => ttl (xO p) l acc
| TNode101 l r=> ttl (xO p) l (ttl (xI p) r acc)
| TNode110 l a => ttl (xO p) l ((rev p, a)::acc)
| TNode111 l a r=> ttl (xO p) l ((rev p, a)::(ttl (xI p) r acc))
end.

Definition to_list (t: Trie A) : list (positive * A) :=
match t with
| Tempty => [::]
| Troot n => tnode_to_list xH n [::]
end.

End Trie_Operations.

Section Trie_Props.
Context {A B: Type}.
Context {f: A -> B}.
Context {g: option A -> option A}.
Context {h: B -> option A}.
Lemma lookup_empty: forall p, lookup p (@Tempty A) = None.
Proof.
by case.
Qed.

Lemma lookup_singleton: forall p (a: A),
    lookup p (singleton p a) = Some a.
Proof.
by elim.
Qed.

Lemma lookup_singleton_neq: forall p q (a: A),
    p != q ->
    lookup p (singleton q a) = None.
Proof.
elim => /= [p IHp| p IHp|] [q|q|] a neq_pq//=;
    apply: IHp;
    apply: neq_pq;
    by rewrite eq_pq.
Qed.

Lemma lookup_builder_r:
    forall (l r: Trie A) p a, lookup (xI p) (builder l a r) = lookup p r.
Proof.
case => [|l] [|r] p [a|];
    by rewrite /builder /lookup /=.
Qed.

Lemma lookup_builder_l:
    forall (l r: Trie A) p a, lookup (xO p) (builder l a r) = lookup p l.
Proof.
case => [|l] [|r] p [a|];
    by rewrite /builder /lookup /=.
Qed.
Lemma lookup_builder_a:
    forall (l r: Trie A) a, lookup (xH) (builder l a r) = a.
Proof.
case => [|l] [|r] [a|];
    by rewrite /builder /lookup /=.
Qed.

Definition lookup_builderE :=
    (lookup_builder_r, lookup_builder_l, lookup_builder_a).

Lemma lookup_alter_empty : forall p,
    lookup p (alter_empty g p) = g (lookup p Tempty).
Proof.
move => p.
rewrite /alter_empty.
case g_none : (g None) => /=.
    by rewrite lookup_singleton.
by rewrite lookup_empty.
Qed.

Lemma lookup_alter : forall t p,
    lookup p (alter g p t) = g (lookup p t).
Proof.
case => [] /=.
    move => p.
    by rewrite lookup_alter_empty.
rewrite -[@tnode_alter_opt]/@tnode_alter.
elim => [r IHr| a |a r IHr|l IHl|l IHl r IHr|l IHl a|l IHl a r IHr];
    move => [p|p|] /=;
    by rewrite lookup_builderE ?IHr ?IHl ?lookup_alter_empty.
Qed.

Lemma lookup_alter_empty_neq : forall p q,
    p != q -> lookup p (alter_empty g q) = None.
Proof.
move => p q neq_pq.
rewrite /alter_empty.
case g_none : (g None) => /=.
    by rewrite lookup_singleton_neq.
by rewrite lookup_empty.
Qed.


Lemma lookup_alter_neq: forall t p q,
    p != q -> lookup p (alter g q t) = (lookup p t).
Proof.
case => [] /=.
    move => p q p_neq_q.
    by rewrite lookup_alter_empty_neq.
have ? := lookup_builder_a.

rewrite -[@tnode_alter_opt]/@tnode_alter.
elim => [r IHr| a |a r IHr|l IHl|l IHl r IHr|l IHl a|l IHl a r IHr];
    move => [p|p|] [q|q|] neq_pq /=;
    by rewrite lookup_builderE ?IHr ?IHl ?lookup_alter_empty_neq.
Qed.

Lemma tnode_lookup_map: forall p (t: TNode A),
        tnode_lookup p (tnode_map f t) = omap f (tnode_lookup p t).
Proof.
elim => [p IHp| p IHp|];
    by case => /=.
Qed.

Lemma lookup_map: forall (t: Trie A) p,
        lookup p (map f t) = omap f (lookup p t).
Proof.
case => [|n] p.
    by [].
by rewrite /lookup /= tnode_lookup_map.
Qed.
Lemma tnode_lookup_fmap: forall (n: TNode B) p,
    lookup p (tnode_fmap h n) = obind h (tnode_lookup p n).
Proof.
have ? := lookup_builder_a.
    elim => [r IHr| a| a r IHr| l IHl| l IHl r IHr| l IHl a| l IHl a r IHr];
    case => [p|p|] /=;
    by rewrite lookup_builderE ?IHr ?IHl ?lookup_empty.
Qed.

Lemma lookup_fmap: forall (t: Trie B) p,
    lookup p (fmap h t) = obind h (lookup p t).
Proof.
elim => [|n].
    by [].
by apply: tnode_lookup_fmap.
Qed.

Lemma unroll_fmap: forall l a r,
    fmap h (builder l a r) =
    builder (fmap h l) (obind h a) (fmap h r).
Proof.
rewrite /fmap -[@tnode_fmap_opt]/@tnode_fmap.
case => [|nl] [a|] [|nr] //=.
Qed.

Lemma tnode_not_empty: forall (n: TNode A), exists i, tnode_lookup i n <> None.
Proof.
elim => [r [pr IHr] | a | a r [pr IHr] | l [pl IHl] |
         l [pl IHl] r [pr IHr]| l [pl IHl] a|l [pl IHl] a r [pr IHr]].
- by exists (xI pr).
- by exists xH.
- by exists xH.
- by exists (xO pl).
- by exists (xO pl).
- by exists xH.
- by exists xH.
Qed.

Lemma extensionality_empty: forall (t: Trie A),
    (forall p, lookup p t = None) -> t = Tempty.
Proof.
elim => [|n].
    by [].
case ? : (tnode_not_empty n) => p lookup_not_empty H_lookup.
by have := (H_lookup p).
Qed.

Lemma tnode_extensionality: forall (n1 n2: TNode A),
    (forall p, tnode_lookup p n1 = tnode_lookup p n2) ->
    n1 = n2.
Proof.
elim => [r IHr| a| a r IHr| l IHl| l IHl r IHr| l IHl a| l IHl a r IHr];
case => [r'| a'| a' r'| l'| l' r'| l' a'| l' a' r'] H_lookup;
have H_lookup_H := H_lookup (xH);
f_equal;
by [
    (* any case with r but with no equivalent r' *)
    case ? : (tnode_not_empty r) => p lookup_not_empty;
        have := (H_lookup (xI p))                       |
    (* any case with l but with no equivalent l' *)
    case ? : (tnode_not_empty l) => p lookup_not_empty;
        have := (H_lookup (xO p))                       |
    (* any case with r' but with no equivalent r *)
    case ? : (tnode_not_empty r') => p;
        have := (H_lookup (xI p)) => /= HS_lookup ;
        rewrite HS_lookup                               |
    (* any case with l' but with no equivalent l *)
    case ? : (tnode_not_empty l') => p;
        have := (H_lookup (xO p)) => /= HS_lookup ;
        rewrite HS_lookup                               |
    (* r = r' *)
    apply: IHr => p;
        have := (H_lookup (xI p))                       |
    (* a = a' *)
    have HS_lookup := (H_lookup xH);
        inversion HS_lookup                             |
    (* l = l' *)
    apply: IHl => p;
        have := (H_lookup (xO p))                       |
].
Qed.

Lemma extensionality: forall (t1 t2: Trie A),
    (forall p, lookup p t1 = lookup p t2) -> t1 = t2.
Proof.
case => [|n1].
    move => t2 H_lookup.
    symmetry.
    apply: extensionality_empty => p.
    by rewrite - H_lookup lookup_empty.
case => [|n2] H_lookup.
    case ? : (tnode_not_empty n1) => p lookup_not_empty.
    by have := (H_lookup p).
f_equal.
by apply: tnode_extensionality.
Qed.

Lemma tnode_elem_of_to_list1: forall (n: TNode A) (p q: positive) a acc,
    PIn (p, a) (tnode_to_list q n acc) ->
    (exists p', p = app p' (rev q) /\ tnode_lookup p' n = Some a) \/
    PIn (p, a) acc.
Proof.
elim => [r IHr| a| a r IHr| l IHl| l IHl r IHr| l IHl a| l IHl a r IHr] /=;
    move => p q a' acc H_in.
-   have := (IHr p (xI q) a' acc H_in).
    move => [[p' []]| in_acc];
        last by right.
    rewrite rev_xI app_assoc /=.
    move => p_app lookup_some.
    left.
    exists (xI p').
    by split.
-   move : H_in => /PIn_cons_or.
    move => [];
        last by right.
    move => [] -> ->.
    left.
    exists xH => /=.
    split => //=.
    by rewrite app_xH_left.
-   move : H_in => /PIn_cons_or.
    move => [[-> ->]|p_in_acc].
        left.
        exists xH.
        by rewrite app_xH_left.
    have := (IHr p (xI q) a' acc p_in_acc).
    move => [[p' []]| in_acc];
        last by right.
    rewrite rev_xI app_assoc /=.
    move => p_app lookup_some.
    left.
    exists (xI p').
    by split.
-   have := (IHl p (xO q) a' acc H_in).
    move => [[p' []]| in_acc];
        last by right.
    rewrite rev_xO app_assoc /=.
    move => p_app lookup_some.
    left.
    exists (xO p').
    by split.
-   have := (IHl p (xO q) a' (tnode_to_list (xI q) r acc) H_in).
    move =>[[p' []]| H_inr].
        rewrite rev_xO app_assoc /=.
        move => p_app lookup_some.
        left.
        exists (xO p').
        by split.
    have := (IHr p (xI q) a' acc H_inr).
    move => [[p' []]| in_acc];
        last by right.
    rewrite rev_xI app_assoc /=.
    move => p_app lookup_some.
    left.
    exists (xI p').
    by split.
-   have := (IHl p (xO q) a' ((rev q, a) :: acc) H_in).
    move => [[p' []]|];
        last first.
        move /PIn_cons_or => [[-> ->]|p_in_acc];
            last by right.
        left.
        exists xH.
        by rewrite app_xH_left.
    rewrite rev_xO app_assoc /=.
    move => p_app lookup_some.
    left.
    exists (xO p').
    by split.
-   have := (IHl p (xO q) a' ((rev q, a) :: tnode_to_list (xI q) r acc) H_in).
    move =>[[p' []]| H_inr].
        rewrite rev_xO app_assoc /=.
        move => p_app lookup_some.
        left.
        exists (xO p').
        by split.
    move : H_inr => /PIn_cons_or.
    move => [[-> ->]|p_in_acc].
        left.
        exists xH.
        by rewrite app_xH_left.
    have := (IHr p (xI q) a' acc p_in_acc).
    move =>[[p' []]|in_acc];
        last by right.
    rewrite rev_xI app_assoc /=.
    move => p_app lookup_some.
    left.
    exists (xI p').
    by split.
Qed.

Lemma tnode_elem_of_to_list2: forall (n: TNode A) (p q: positive) a acc,
    (exists p', p = app p' (rev q) /\
    tnode_lookup p' n = Some a) \/ PIn (p, a) acc ->
    PIn (p, a) (tnode_to_list q n acc).
Proof.
elim => [r IHr| a| a r IHr| l IHl| l IHl r IHr| l IHl a| l IHl a r IHr] /=;
move => p q a' acc [ [[p'|p'|] [eq_pq lookup_some]]| Hin_acc] //=.
-   apply: IHr.
    left.
    exists p'.
    by rewrite rev_xI app_assoc /=.
-   apply: IHr.
    by right.
-   apply: PIn_here_eq.
    move: lookup_some => [eq_aa'] /=.
    by rewrite eq_pq eq_aa' app_xH_left.
-   by apply: pin_future.
-   apply: pin_future.
    apply: IHr.
    left.
    exists p'.
    by rewrite rev_xI app_assoc.
-   apply: PIn_here_eq.
    move : lookup_some => [eq_aa'] /=.
    by rewrite eq_pq eq_aa' app_xH_left.
-   apply: pin_future.
    apply: IHr.
    by right.
-   apply: IHl.
    left.
    exists p'.
    by rewrite rev_xO app_assoc.
-   apply: IHl.
    by right.
-   apply: IHl.
    right.
    apply: IHr.
    left.
    exists p'.
    by rewrite rev_xI app_assoc.
-   apply: IHl.
    left.
    exists p'.
    by rewrite rev_xO app_assoc.
-   apply: IHl.
    right.
    apply: IHr.
    by right.
-   apply: IHl.
    left.
    exists p'.
    by rewrite rev_xO app_assoc.
-   apply: IHl.
    right.
    apply: PIn_here_eq.
    move : lookup_some => [eq_aa'] /=.
    by rewrite eq_pq eq_aa' app_xH_left.
-   apply: IHl.
    right.
    by apply: pin_future.
-   apply: IHl.
    right.
    apply: pin_future.
    apply: IHr.
    left.
    exists p'.
    by rewrite eq_pq rev_xI app_assoc.
-   apply: IHl.
    left.
    exists p'.
    by rewrite eq_pq rev_xO app_assoc.
-   apply: IHl.
    right.
    apply: PIn_here_eq.
    move : lookup_some => [eq_aa'] /=.
    by rewrite eq_pq app_xH_left eq_aa'.
-   apply: IHl.
    right.
    apply: pin_future.
    apply: IHr.
    by right.
Qed.

Lemma elem_of_to_list: forall (t: Trie A) p a,
    PIn (p, a) (to_list t) <->
    lookup p t = Some a.
Proof.
case => [|n] //= p a.
    split.
        by rewrite PIn_empty_false_iff.
    by rewrite lookup_empty.
split.
-   move => in_t.
    have: ((exists p' : positive, p = app p' (rev xH) /\
                        tnode_lookup p' n = Some a) \/
            PIn (p, a) [::]).
        by apply: tnode_elem_of_to_list1.
    case => [[p' [eq_pp' lookup_some]]|] /=.
        by rewrite eq_pp'.
    by rewrite PIn_empty_false_iff.
-   move => lookup_some.
    apply: tnode_elem_of_to_list2.
    left.
    by exists p.
Qed.

Lemma tnode_to_list_no_dup: forall (n: TNode A) p (acc: list (positive * A)),
    (forall p' a, PIn (app p' (rev p), a) acc ->
                  tnode_lookup p' n = None) ->
    PUniq acc ->
    PUniq (tnode_to_list p n acc).
Proof.
elim => [r IHr| a| a r IHr| l IHl| l IHl r IHr| l IHl a| l IHl a r IHr] /=;
    move => p acc Hno_dup unic_acc;
    (apply: IHl || apply: IHr|| apply: puniq_cons || move);
    (split||move) => //=.
-   move => p' a.
    rewrite rev_xI app_assoc => in_acc.
    by have ? := (Hno_dup (xI p') a in_acc).
-   have := (Hno_dup xH a).
    rewrite app_xH_left => /= in_acc_false in_acc.
    by have := in_acc_false in_acc.
-   move => in_acc.
    have := (tnode_elem_of_to_list1 in_acc).
    case => [[p' []]|p_in_acc];
        last first.
        have := (Hno_dup xH a).
        rewrite app_xH_left => /= in_acc_false.
        by have := (in_acc_false p_in_acc).
    rewrite rev_xI app_assoc /= => Hfalse.
    suff: xI p' = xH =>//.
    by apply/(app_eq (xI p') (rev p)).
-   apply: IHr;
        last by [].
    move => p' a'.
    rewrite rev_xI app_assoc => /= in_acc.
    by have ? := (Hno_dup (xI p') a' in_acc).
-   move => p' a.
    rewrite rev_xO app_assoc => in_acc.
    by have ? := (Hno_dup (xO p') a in_acc).
-   move => p' a.
    rewrite rev_xO app_assoc /=.
    move =>  in_acc_r.
    apply: (Hno_dup (xO p') a).
    have := (tnode_elem_of_to_list1 in_acc_r).
    move => [|];
        last by [].
    move => [] x [].
    rewrite rev_xI app_assoc/= => Hfalse.
    by have := app_inj_l Hfalse.
-   apply: IHr;
        last by [].
    move => p' a.
    rewrite rev_xI app_assoc => in_acc.
    by have := (Hno_dup (xI p') a in_acc).
-   move => p' a'.
    rewrite rev_xO app_assoc /=.
    move /PIn_cons_or => [];
        last by apply: (Hno_dup (xO p') a').
    move => [HFalse].
    suff: xO p' = xH => //.
    by apply/(app_eq (xO p') (rev p)).
-   apply: puniq_cons;
        last by [].
    have := (Hno_dup xH a).
    rewrite app_xH_left => /= in_acc_false in_acc.
    by have := in_acc_false in_acc.
-   move => p' a'.
    rewrite rev_xO app_assoc /=.
    move /PIn_cons_or => [].
        move => [] Hfalse.
        suff: (xO p' = xH) => //.
        by apply/(app_eq (xO p') (rev p)).
    move => in_r_acc.
    have := (tnode_elem_of_to_list1 in_r_acc).
    move => [[q []]|];
        last by apply: (Hno_dup (xO p') a').
    rewrite rev_xI app_assoc /= => HFalse.
    suff: xO p' = xI q => //.
    by have := app_inj_l HFalse.
-   apply: puniq_cons;
        last first.
        apply: IHr;
            last by [].
        move => p' a'.
        rewrite rev_xI app_assoc /= => in_acc.
        by apply: (Hno_dup (xI p') a').
    move => in_acc.
    have := (tnode_elem_of_to_list1 in_acc).
    case => [[p' []]|p_in_acc];
        last first.
        have := (Hno_dup xH a).
        rewrite app_xH_left => /= in_acc_false.
        by have := (in_acc_false p_in_acc).
    rewrite rev_xI app_assoc /= => Hfalse.
    suff : xI p' = xH => //.
    by apply/(app_eq (xI p') (rev p)).
Qed.

Lemma to_list_no_dup: forall (t: Trie A), PUniq (to_list t).
Proof.
have ? := puniq_nil.
elim => [|n] //=.
apply: tnode_to_list_no_dup => //.
move => p' a.
by rewrite PIn_empty_false_iff.
Qed.


End Trie_Props.

Section Trie_Merge.
Context {A B C: Type}.
Variable (f: option A -> option B -> option C).
Hypothesis f_None_None: f None None = None.

Definition tnode_merge_l := tnode_fmap (fun a => f (Some a) None).
Definition tnode_merge_r:= tnode_fmap (fun b => f None (Some b)).
Fixpoint tnode_merge (n1: TNode A) (n2: TNode B) {struct n1} : Trie C :=
let tm := tnode_merge in
let f1 a1 := f (Some a1) None in
let f2 a2 := f None (Some a2) in
let f' a1 a2 := f (Some a1) (Some a2) in
let m1 := tnode_merge_l in
let m2 := tnode_merge_r in
match n1, n2 with
| TNode001 r1, TNode001 r2 => builder Tempty None (tm r1 r2)
| TNode001 r1, TNode010 a2 => builder Tempty (f2 a2) (m1 r1)
| TNode001 r1, TNode011 a2 r2 => builder Tempty (f2 a2) (tm r1 r2)
| TNode001 r1, TNode100 l2 => builder (m2 l2) None (m1 r1)
| TNode001 r1, TNode101 l2 r2 => builder (m2 l2) None (tm r1 r2)
| TNode001 r1, TNode110 l2 a2 => builder (m2 l2) (f2 a2) (m1 r1)
| TNode001 r1, TNode111 l2 a2 r2 => builder (m2 l2) (f2 a2) (tm r1 r2)

| TNode010 a1, TNode001 r2 => builder Tempty (f1 a1) (m2 r2)
| TNode010 a1, TNode010 a2 => builder Tempty (f' a1 a2) Tempty
| TNode010 a1, TNode011 a2 r2 => builder Tempty (f' a1 a2) (m2 r2)
| TNode010 a1, TNode100 l2 => builder (m2 l2) (f1 a1) Tempty
| TNode010 a1, TNode101 l2 r2 => builder (m2 l2) (f1 a1) (m2 r2)
| TNode010 a1, TNode110 l2 a2 => builder (m2 l2) (f' a1 a2) Tempty
| TNode010 a1, TNode111 l2 a2 r2 => builder (m2 l2) (f' a1 a2) (m2 r2)

| TNode011 a1 r1, TNode001 r2 => builder Tempty (f1 a1) (tm r1 r2)
| TNode011 a1 r1, TNode010 a2 => builder Tempty (f' a1 a2) (m1 r1)
| TNode011 a1 r1, TNode011 a2 r2 => builder Tempty (f' a1 a2) (tm r1 r2)
| TNode011 a1 r1, TNode100 l2 => builder (m2 l2) (f1 a1) (m1 r1)
| TNode011 a1 r1, TNode101 l2 r2 => builder (m2 l2) (f1 a1) (tm r1 r2)
| TNode011 a1 r1, TNode110 l2 a2 => builder (m2 l2) (f' a1 a2) (m1 r1)
| TNode011 a1 r1, TNode111 l2 a2 r2 => builder (m2 l2) (f' a1 a2) (tm r1 r2)

| TNode100 l1, TNode001 r2 => builder (m1 l1) None (m2 r2)
| TNode100 l1, TNode010 a2 => builder (m1 l1) (f2 a2) Tempty
| TNode100 l1, TNode011 a2 r2 => builder (m1 l1) (f2 a2) (m2 r2)
| TNode100 l1, TNode100 l2 => builder (tm l1 l2) None Tempty
| TNode100 l1, TNode101 l2 r2 => builder (tm l1 l2) None (m2 r2)
| TNode100 l1, TNode110 l2 a2 => builder (tm l1 l2) (f2 a2) Tempty
| TNode100 l1, TNode111 l2 a2 r2 => builder (tm l1 l2) (f2 a2) (m2 r2)

| TNode101 l1 r1, TNode001 r2 => builder (m1 l1) None (tm r1 r2)
| TNode101 l1 r1, TNode010 a2 => builder (m1 l1) (f2 a2) (m1 r1)
| TNode101 l1 r1, TNode011 a2 r2 => builder (m1 l1) (f2 a2) (tm r1 r2)
| TNode101 l1 r1, TNode100 l2 => builder (tm l1 l2) None (m1 r1)
| TNode101 l1 r1, TNode101 l2 r2 => builder (tm l1 l2) None (tm r1 r2)
| TNode101 l1 r1, TNode110 l2 a2 => builder (tm l1 l2) (f2 a2) (m1 r1)
| TNode101 l1 r1, TNode111 l2 a2 r2 => builder (tm l1 l2) (f2 a2) (tm r1 r2)

| TNode110 l1 a1, TNode001 r2 => builder (m1 l1) (f1 a1) (m2 r2)
| TNode110 l1 a1, TNode010 a2 => builder (m1 l1) (f' a1 a2) Tempty
| TNode110 l1 a1, TNode011 a2 r2 => builder (m1 l1) (f' a1 a2) (m2 r2)
| TNode110 l1 a1, TNode100 l2 => builder (tm l1 l2) (f1 a1) Tempty
| TNode110 l1 a1, TNode101 l2 r2 => builder (tm l1 l2) (f1 a1) (m2 r2)
| TNode110 l1 a1, TNode110 l2 a2 => builder (tm l1 l2) (f' a1 a2) Tempty
| TNode110 l1 a1, TNode111 l2 a2 r2 => builder (tm l1 l2) (f' a1 a2) (m2 r2)

| TNode111 l1 a1 r1, TNode001 r2 => builder (m1 l1) (f1 a1) (tm r1 r2)
| TNode111 l1 a1 r1, TNode010 a2 => builder (m1 l1) (f' a1 a2) (m1 r1)
| TNode111 l1 a1 r1, TNode011 a2 r2 => builder (m1 l1) (f' a1 a2) (tm r1 r2)
| TNode111 l1 a1 r1, TNode100 l2 => builder (tm l1 l2) (f1 a1) (m1 r1)
| TNode111 l1 a1 r1, TNode101 l2 r2 => builder (tm l1 l2) (f1 a1) (tm r1 r2)
| TNode111 l1 a1 r1, TNode110 l2 a2 => builder (tm l1 l2) (f' a1 a2) (m1 r1)
| TNode111 l1 a1 r1, TNode111 l2 a2 r2 =>
                                    builder (tm l1 l2) (f' a1 a2) (tm r1 r2)
end.

Definition merge (t1: Trie A) (t2: Trie B) : Trie C :=
match t1, t2 with
| Tempty, Tempty => Tempty
| Tempty, Troot n2 => tnode_merge_r n2
| Troot n1, Tempty => tnode_merge_l n1
| Troot n1, Troot n2 => tnode_merge n1 n2
end.

Lemma tnode_lookup_merge_l: forall (n: TNode A) p,
    lookup p (tnode_merge_l n) = f (tnode_lookup p n) None.
Proof.
move => n p.
rewrite /tnode_merge_l (tnode_lookup_fmap n) /=.
by case ? : (tnode_lookup p n).
Qed.

Lemma tnode_lookup_merge_r: forall (n: TNode B) p,
    lookup p (tnode_merge_r n) = f None (tnode_lookup p n).
Proof.
move => n p.
rewrite /tnode_merge_l tnode_lookup_fmap /=.
by case ? : (tnode_lookup p n).
Qed.

Lemma tnode_lookup_merge: forall n1 n2 p,
    lookup p (tnode_merge n1 n2) = f (tnode_lookup p n1) (tnode_lookup p n2).
Proof.
have ? := tnode_lookup_merge_l.
have ? := tnode_lookup_merge_r.
elim => [r IHr| a| a r IHr| l IHl| l IHl r IHr| l IHl a| l IHl a r IHr];
    case => [r'| a'| a' r'| l'| l' r'| l' a'| l' a' r'] /= [p|p|] /=;
    by rewrite lookup_builderE.
Qed.

Definition tnode_lookup_mergeE :=
    (tnode_lookup_merge_l, tnode_lookup_merge_r, tnode_lookup_merge).

Lemma lookup_merge: forall t1 t2 p,
    lookup p (merge t1 t2) = f (lookup p t1) (lookup p t2).
Proof.
case => [|t1] [|t2] p /=;
    by rewrite ?tnode_lookup_mergeE.
Qed.
End Trie_Merge.

Definition Mixin : hasFinMap.axioms_ positive Trie.
Proof.
econstructor;
    move => V.
-   by apply: extensionality.
-   by apply: lookup_empty.
-   move => f.
    by apply: lookup_alter.
-   move => f.
    by apply: lookup_alter_neq.
-   move => V2 f.
    by apply: lookup_fmap.
-   by apply: to_list_no_dup.
-   by apply: elem_of_to_list.
-   move => V2 f.
    by apply: lookup_map.
-   move => V1 V2 f m1 m2 k Hnone.
    by apply: lookup_merge.
Defined.
End Trie.

HB.instance  Definition _ := Trie.Mixin.
