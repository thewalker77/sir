(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From HB Require Import structures.
From mathcomp Require Import all_ssreflect zify.
From Coq Require Export Strings.String.
From Coq Require Import Strings.Ascii.
From sir.common Require Import pos.
Import Pos.
Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Module SerDeString.
(* shamelessly copied from coq's stdpp *)

Fixpoint digits_to_pos (bs : list bool) : positive :=
match bs with
| [::] => xH
| false :: bs => xO (digits_to_pos bs)
| true :: bs => xI (digits_to_pos bs)
end.

Definition ascii_to_digits (a : Ascii.ascii) : list bool :=
match a with
| Ascii.Ascii b1 b2 b3 b4 b5 b6 b7 b8 =>
    b1::b2::b3::b4::b5::b6::b7::b8::[::]
end.

Fixpoint string_to_pos (s : string) : positive :=
match s with
| EmptyString => xH
| String a s => Pos.app
                (string_to_pos s)
                (digits_to_pos (ascii_to_digits a))
end.

Fixpoint digits_of_pos (p : positive) : list bool :=
match p with
| xH => [::]
| xO p => false :: digits_of_pos p
| xI p => true :: digits_of_pos p
end.

Fixpoint ascii_of_digits (bs : list bool) : ascii :=
match bs with
| [::] => zero
| b :: bs => Ascii.shift b (ascii_of_digits bs)
end.

Fixpoint string_of_digits (bs : list bool) : string :=
match bs with
| b1 :: b2 :: b3 :: b4 :: b5 :: b6 :: b7 :: b8 :: bs =>
    String
    (ascii_of_digits (b1::b2::b3::b4::b5::b6::b7::b8::[::]))
    (string_of_digits bs)
| _ => EmptyString
end.

Definition string_of_pos (p : positive) : string :=
    string_of_digits (digits_of_pos p).

Lemma string_of_to_pos s : string_of_pos (string_to_pos s) = s.
Proof.
rewrite /string_of_pos.
elim: s => [//|].
move => [[][][][][][][][]] s IH /=;
    by f_equal.
Qed.
End SerDeString.

HB.instance Definition _ := hasDecEq.Build string String.eqb_spec.
