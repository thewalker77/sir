(*****************************************************************************)
(* Copyright 2022 TheWalker77                                                *)
(*                                                                           *)
(* Licensed under the Apache License, Version 2.0 (the "License");           *)
(* you may not use this file except in compliance with the License.          *)
(* You may obtain a copy of the License at                                   *)
(*                                                                           *)
(*     http://www.apache.org/licenses/LICENSE-2.0                            *)
(*                                                                           *)
(* Unless required by applicable law or agreed to in writing, software       *)
(* distributed under the License is distributed on an "AS IS" BASIS,         *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *)
(* See the License for the specific language governing permissions and       *)
(* limitations under the License.                                            *)
(*****************************************************************************)
From HB Require Import structures.
From mathcomp Require Import all_ssreflect zify.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(*****************************************************************************)
(* This file implements a data type for representing positive integers in    *)
(* binary form. The main type [positivie] has three constructors: [xH],      *)
(* [xI], and [xO].                                                           *)
(*                                                                           *)
(* [xH] represents the number "one" and it cats as the terminating string.   *)
(* That implies that the smallest representable number is "one". To add more *)
(*  digits, we can use [xO] to add "zeros" or [xI] to add "ones".            *)
(*                                                                           *)
(* Note that numbers are written left to right, but they are read right to   *)
(* left. The following table shows the numbers 1 till 5 in both binary and   *)
(* [positive] representation.                                                *)
(*                                                                           *)
(* |    Decimal     |   Binary      |   [positive]      |                    *)
(* +----------------+---------------+-------------------|                    *)
(* |      1         |      1        |       xH          |                    *)
(* |      2         |      10       |      xO xH        |                    *)
(* |      3         |      11       |      xI xH        |                    *)
(* |      4         |     100       |    xO (xO xH)     |                    *)
(* |      5         |     101       |    xH (xO xH)     |                    *)
(* +----------------+---------------+-------------------|                    *)
(*****************************************************************************)

Module Pos.

Inductive positive : Set :=
| xI : positive -> positive
| xO : positive -> positive
| xH : positive.

(* canonical eqType for positive *)
Fixpoint eqp m n :=
match m, n with
| xH, xH => true
| xI m', xI n' => eqp m' n'
| xO m', xO n' => eqp m' n'
| _, _ => false
end.

Lemma eqpP: Equality.axiom eqp.
Proof.
move => m n.
apply: (iffP idP) => [|->];
    last by elim : n.
elim : n m => [n IHn|n IHn|];
    move => [m|m|] //=;
    by move /IHn ->.
Qed.

(* bitwise appending *)
Fixpoint app (p1 p2 : positive) : positive :=
match p2 with
| xH => p1
| xO p2 => xO (app p1 p2)
| xI p2 => xI (app p1 p2)
end.

Lemma app_assoc: forall p3 p2 p1 ,
    app p1 (app p2 p3) = app (app p1 p2) p3.
Proof.
elim => [p3 IHp3 | p3 IHp3 |] p2 p1 /=;
    by [rewrite IHp3|].
Qed.

Lemma app_xH_left: forall p, app xH p = p.
Proof.
elim => [p IHp| p IHp| ] /=;
    by [rewrite IHp|].
Qed.

Fixpoint rev_go (p1 p2 : positive) : positive :=
match p2 with
| xH => p1
| xO p2' => rev_go (xO p1) p2'
| xI p2' => rev_go (xI p1) p2'
end.

Definition rev : positive -> positive := rev_go xH.

Lemma rev_go_app: forall p1 p2 p3,
    rev_go p3 (app p2 p1) =
    app (rev_go p3 p1) (rev_go xH p2).
Proof.
suff: (forall p1 p2 p3, rev_go (app p2 p3) p1 = app p2 (rev_go p3 p1)).
    move => Hgo.
    elim => [p1 IHp1|p1 IHp1|] p2 p3 /=;
        by [rewrite <- Hgo|].
elim => [p1 IHp1|p1 IHp1|] p2 p3 /=;
    by [|rewrite <- IHp1].
Qed.

Lemma rev_app p1 p2 : rev (app p1 p2) = app (rev p2) (rev p1).
Proof.
by rewrite /rev rev_go_app.
Qed.

Lemma rev_xO p : rev (xO p) = app (xO xH) (rev p).
Proof rev_app p (xO xH).
Lemma rev_xI p : rev (xI p) = app (xI xH) (rev p).
Proof rev_app p (xI xH).

Lemma rev_involution:forall p, rev (rev p) = p.
Proof.
elim => [p IHp | p IHp |] /=.
- by rewrite rev_xI rev_app IHp.
- by rewrite rev_xO rev_app IHp.
- by [].
Qed.


Fixpoint length (p: positive) : nat :=
match p with
| xH => 0
| xI p' | xO p' => 1 + length p'
end.

Lemma app_length: forall p2 p1 , length (app p1 p2) = length p1 + length p2.
Proof.
elim => [p2 IHp2 |p2 IHp2| ] p1 /=.
-   rewrite IHp2.
    by lia.
-   rewrite IHp2.
    by lia.
-   by lia.
Qed.

Lemma app_inj_l: forall p1 p2 p3, app p2 p1 = app p3 p1 -> p2 = p3.
Proof.
elim => [p1 IHp1| p1 IHp1|] p2 p3 /= app_eq.
-   apply: IHp1.
    by inversion app_eq.
-   apply: IHp1.
    by inversion app_eq.
-   by [].
Qed.

Lemma app_eq: forall p1 p2, app p1 p2 = p2 <-> p1 = xH.
Proof.
split;
    move => Heq;
    last first.
    by rewrite Heq app_xH_left.
case :p1 p2 Heq => [p1|p1|] p2 Heq //;
by [
    have := (f_equal length Heq);
    rewrite app_length /=;
    lia
].
Qed.

(* successor and predecessor operations *)
Fixpoint succ (p: positive) : positive :=
match p with
| xH => xO xH
| xO p' => xI p'
| xI p' => xO (succ p')
end.

Fixpoint pred' (p: positive): positive :=
match p with
| xH => xH
| xI p' => xI (xO p')
| xO p' => xI (pred' p')
end.
Definition pred (p: positive): positive :=
match p with
| xH => xH
| xI p' => xO p'
| xO p' => pred' p'
end.

Lemma pred'_succ: forall p, pred' (succ p) = xI p.
Proof.
elim => [p IHp | p IHp|] /=;
    by [rewrite IHp|].
Qed.

Lemma pred_succ: forall p, pred (succ p) = p.
Proof.
case => [p | p |] /=;
    by [rewrite pred'_succ|].
Qed.

Lemma succ_pred': forall p,
    succ (pred' p) = xO p.
Proof.
elim => [p IHp| p IHp |] /=;
    by [|rewrite IHp].
Qed.

End Pos.

HB.instance Definition _ := hasDecEq.Build Pos.positive Pos.eqpP.
