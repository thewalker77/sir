(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)

From mathcomp Require Import all_ssreflect.
Require Import Coq.Logic.FunctionalExtensionality.
From Coq Require Import ZifyClasses ZArith_base.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Definition comp {A B C} (g : B -> C) (f : A -> B) x:=
    g (f x).
Notation " g ∘ f " := (comp g f) (at level 40, left associativity).
Arguments comp {A B C} g f x/.

(* lemmas for comp *)
Lemma compfid A B (f: A -> B): f ∘ id = f.
Proof.
by [].
Qed.

Lemma compidf A B (f: A -> B): id ∘ f  = f.
Proof.
by [].
Qed.

Lemma compA: forall A B C D (f: C -> D) (g: B -> C) (h: A -> B),
    f ∘ (g ∘ h) = (f ∘ g) ∘ h.
Proof.
by [].
Qed.

Definition scons {T} (t : T) (f : nat -> T) (l : nat) : T :=
match l with
| y.+1 => f y
| _ => t
end.
Notation "s .: σ" := (scons s σ)
    (at level 55, σ at level 56, right associativity).

Lemma scons_comp T1 T2 x (f: nat -> T1) (g: T1 -> T2):
    g∘ (x .: f) = g x .: g ∘ f.
Proof.
apply: functional_extensionality.
by move => [|x'].
Qed.

Fixpoint iterate {A} (f : A -> A) n a :=
match n with
| 0 => a
| S n' => f(iterate f n' a)
end.

Lemma iterate_Sr A (a: A) f n:
    iterate f (S n) a = iterate f n (f a).
Proof.
elim: n => [//|n IHn] /=.
by rewrite -IHn.
Qed.

(* This is useful for dealing with Coq-Equation, This is the equivalent of    *)
(* `Set Program Cases` in the Program Fixpoint command                        *)
Definition inspect {A} (a : A) : {b | a = b} :=
    exist (eq a) a erefl.
Notation "[ x | p ]" := (exist _ x p) (only parsing, at level 20).
