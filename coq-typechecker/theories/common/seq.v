(*****************************************************************************)
(* Copyright 2022 TheWalker77                                                *)
(* Permutations: Copyright 2017 IMDEA Software Institute                     *)
(*                                                                           *)
(* Licensed under the Apache License, Version 2.0 (the "License");           *)
(* you may not use this file except in compliance with the License.          *)
(* You may obtain a copy of the License at                                   *)
(*                                                                           *)
(*     http://www.apache.org/licenses/LICENSE-2.0                            *)
(*                                                                           *)
(* Unless required by applicable law or agreed to in writing, software       *)
(* distributed under the License is distributed on an "AS IS" BASIS,         *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *)
(* See the License for the specific language governing permissions and       *)
(* limitations under the License.                                            *)
(*****************************************************************************)

From mathcomp Require Export all_ssreflect zify.
From mathcomp Require Export seq.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Lemma length_zero_iff_nil {A} (s: seq A):
    length s = 0 <-> s = [::].
Proof.
by case :s.
Qed.
(* propositional version of \in this one does not require eqTypev*)
Inductive PIn {A}: A -> seq A -> Prop :=
| pin_here (a : A) l : PIn a (a::l)
| pin_future (a b : A) l : PIn a l -> PIn a (b::l).

Lemma inP (A: eqType): forall (a: A) (l: seq A),  reflect (PIn a l) (a \in l).
Proof.
move => a l.
apply: (iffP idP).
-   move: a.
    have ? := pin_here.
    elim : l  => [|a' l Hind] //= a /[!in_cons].
    move /orP => [|a_in_l].
        by move /eqP ->.
    apply: pin_future.
    by apply: Hind.
-   move => HPIn.
    induction HPIn as [a l| a b l HInd].
        by apply: mem_head.
    rewrite in_cons.
    apply/orP.
    by right.
Qed.

Lemma neg_inP (A: eqType): forall (a: A) (l: seq A),
    reflect (~ PIn a l) (a \notin l).
Proof.
move => a l.
apply: (iffP idP).
-   move /negP => a_not_in_l a_PIn_l.
    apply: a_not_in_l.
    by apply/inP.
-   move => not_PIn_a_l.
    apply/negP => a_in_l.
    apply: not_PIn_a_l.
    by apply/inP.
Qed.

Lemma PIn_cons_or A (a b: A) (l: seq A) : PIn a (b::l) <->
    a = b \/ PIn a l.
Proof.
split.
    move => HPIn.
    inversion HPIn; subst.
        by left.
    by right.
move => [-> | HPIn].
    by apply: pin_here.
by apply: pin_future.
Qed.

Lemma not_PIn_cons_and A (a b: A) (l: seq A) : ~ PIn a (b::l) <->
    a <> b /\ ~ PIn a l.
Proof.
rewrite PIn_cons_or.
split.
    move => Hnot.
    split;
        move => HFalse;
        apply: Hnot.
        by left.
    by right.
by move => [Hneq HnotIn] [Heq | Hin].
Qed.

Lemma PIn_empty_false A (a: A): ~ PIn a [::].
Proof.
move =>HFalse.
inversion HFalse.
Qed.

Lemma PIn_empty_false_iff A (a: A): PIn a [::] <-> False.
Proof.
split.
    by apply: PIn_empty_false.
by [].
Qed.

Lemma PIn_empty A (l: seq A) : l = [::] <-> forall a, ~PIn a l.
Proof.
split.
    move ->.
    by apply: PIn_empty_false.
case : l => [|a l] Hnot_in.
    by [].
have HFalse := (Hnot_in a).
contradict HFalse.
by apply: pin_here.
Qed.

Lemma PIn_here_eq A (a b: A) (l: seq A): a = b -> PIn a (b::l).
Proof.
move ->.
by apply: pin_here.
Qed.

Lemma pin_map1 {A B } (b: B) (l : seq A) f:
    PIn b (map f l) -> exists a, b = f a /\ PIn a l.
Proof.
elim : l => [|a l IHl] /=.
        move => HFalse.
        by inversion HFalse.
move /PIn_cons_or => [->|pin_l].
    exists a.
    split.
        by [].
    apply: pin_here.
case : (IHl pin_l) => [a' [-> Hin]].
exists a'.
split.
    by [].
by apply: pin_future.
Qed.

Lemma pin_map2 {A B} (b: B) (l : seq A) f:
    (exists a, b = f a /\ PIn a l) -> PIn b (map f l).
Proof.
elim : l => [| a l IHl] /=;
    move => [a' []].
    by rewrite !PIn_empty_false_iff.
move => Heq.
move : Heq IHl => -> IHl.
move /PIn_cons_or => [->|Hin].
    by apply: pin_here.
apply: pin_future.
apply: IHl.
by exists a'.
Qed.

Lemma pin_map {A B } (b: B) (l : seq A) f:
    PIn b (map f l) <-> exists a, b = f a /\ PIn a l.
Proof.
split.
    by apply: pin_map1.
by apply: pin_map2.
Qed.


Lemma pin_seq_pin_fst A B (l: seq (A * B)) k v:
    PIn (k, v) l ->
    PIn k (map (fun kv => kv.1) l).
Proof.
elim : l => [| [k' v'] l IHl] /=.
    by rewrite !PIn_empty_false_iff.
move /PIn_cons_or => [[<- Heq]| Hin].
    by apply: pin_here.
apply: pin_future.
by apply: IHl.
Qed.

Lemma PIn_app A a (l1 l2: seq A) : PIn a (l1 ++ l2) <-> PIn a l1 \/ PIn a l2.
Proof.
elim : l1 => [|a' l1 IHl1] /=.
    split.
        by right.
    move => [HFalse | HIn];
        by [|have := PIn_empty_false HFalse].
rewrite !PIn_cons_or IHl1.
split.
    move => [->|[Hin|Hin]].
    -   left.
        by left.
    -   left.
        by right.
    -   by right.
move => [[->|Hin]|Hin].
-   by left.
-   right.
    by left.
-   right.
    by right.
Qed.

Lemma PIn_singlton {A} (a b: A): a = b <-> PIn a [:: b].
Proof.
split.
    move ->.
    apply: pin_here.
rewrite PIn_cons_or.
move => [->|HFalse];
    by [| have := PIn_empty_false HFalse].
Qed.

Lemma PIn_rev A a (l: seq A) : PIn a l <-> PIn a (rev l).
Proof.
elim : l => [|a' l IHl].
    by rewrite /rev /=.
rewrite PIn_cons_or -cat1s.
by rewrite rev_cat PIn_app -PIn_singlton IHl or_comm.
Qed.

Lemma PIn_rcons_or A (a b: A) (l: seq A) : PIn a (rcons l b) <->
    a = b \/ PIn a l.
Proof.
rewrite PIn_rev rev_rcons PIn_cons_or.
by rewrite -PIn_rev.
Qed.

Lemma PIn_split A (a: A) l : PIn a l -> exists s1 s2, l = s1 ++ a :: s2.
Proof.
elim : l => [| a' l IHl].
    by rewrite PIn_empty_false_iff.
rewrite PIn_cons_or.
move => [<-| Hin].
    by exists [::], l.
move : (IHl Hin) => [s1 [s2 Heq]].
exists (a'::s1), s2.
by rewrite Heq.
Qed.

(* propositional version of Uniq and it does not require eqType *)
Inductive PUniq {A} : seq A -> Prop :=
| puniq_nil : PUniq [::]
| puniq_cons (a: A) l: ~ PIn a l -> PUniq l -> PUniq (a::l).

Lemma uniqP (A: eqType): forall (l: seq A), reflect (PUniq l) (uniq l).
Proof.
move => l.
apply: (iffP idP).
-   have ? := puniq_nil.
    elim : l  => [|a l Hind] //=.
    move /andP => [a_not_in_l uniq_l].
    apply: puniq_cons.
        by apply/neg_inP.
    by apply: Hind.
-   move => HPuniq.
    induction HPuniq as [|a l a_not_in_l Hind] => //=.
    apply/andP.
    split => //=.
    by apply/neg_inP.
Qed.

Lemma uniq_inv A a (l: seq A):
    PUniq (a :: l) <->  ~ PIn a l /\ PUniq l.
Proof.
split.
    move => Huniq.
    by inversion Huniq; subst.
move => [Hnot_in Huniq].
by apply: puniq_cons.
Qed.

Lemma puniq_app1 A (s1 s2: seq A):
    PUniq (s1 ++ s2) ->
    PUniq s1 /\ PUniq s2 /\ (forall x, PIn x s1 -> PIn x s2 -> False).
Proof.
elim : s1 s2 => [|a s1 IHs1] s2 /=.
    move => Huniq2.
    split.
        by apply: puniq_nil.
    split.
        by [].
    move => x.
    by rewrite PIn_empty_false_iff.
rewrite !uniq_inv PIn_app.
move => [ Hnotin12 Huniq12].
move : (IHs1 _ Huniq12) => [Huniq1 [Huniq2 Hnotinin]].
split.
    split;
    last by [].
    move => Hina.
    apply Hnotin12.
    by left.
split.
    by [].
move => x.
rewrite PIn_cons_or.
move => [->|Hina1] Hina2.
    apply: Hnotin12.
    by right.
by apply: (Hnotinin x).
Qed.

Lemma puniq_app2 A (s1 s2: seq A):
    PUniq s1 ->
    PUniq s2 ->
    (forall x, PIn x s1 -> PIn x s2 -> False) ->
    PUniq (s1 ++ s2).
Proof.
elim : s1 s2 => [|a s1 IHs1] s2 /=.
    by [].
rewrite !uniq_inv.
move => [Hnotin1 Huniq1] Huni2 Hnotinin.
split.
    move /PIn_app => [Hin1|Hin2].
        by [].
    apply: (Hnotinin a).
        by apply: pin_here.
    by [].
apply: IHs1 => //=.
move => x Hin1 Hin2.
apply: (Hnotinin x);
    last by [].
rewrite PIn_cons_or.
by right.
Qed.

Lemma puniq_app A (s1 s2: seq A):
    PUniq (s1 ++ s2) <->
    PUniq s1 /\ PUniq s2 /\ (forall x, PIn x s1 -> PIn x s2 -> False).
Proof.
split.
    by apply: puniq_app1.
move => [?[]].
by apply: puniq_app2.
Qed.

Lemma puniq_map_inv_fun A B (l: seq A) (f: A -> B):
    (forall a b, PIn a l -> PIn b l -> f a = f b -> a = b) ->
    PUniq l -> PUniq (map f l).
Proof.
elim: l => [|a l IHl] /= Hinv.
    move => _.
    by apply: puniq_nil.
rewrite !uniq_inv.
move => [Hnotin Huniq].
split;
    last first.
    apply: IHl => // a' b' PIn Heq.
    apply: Hinv => //;
        by apply pin_future => //.
move /pin_map => [a' [Hfeq Hin]].
apply: Hnotin.
have := Hinv _ _ _ _ Hfeq.
move => -> //.
-   by apply pin_here.
-   by apply: pin_future.
Qed.

Lemma puniq_map_inv_fun_weak A B (l: seq A) (f: A -> B):
    (forall a b, f a = f b -> a = b) ->
    PUniq l -> PUniq (map f l).
Proof.
move => Hinj.
apply: puniq_map_inv_fun => a b Hina Hinb.
by apply: Hinj.
Qed.


(* properties of associated lists *)
Lemma uniq_map_fst A B (l: seq (A * B)):
    (forall k v1 v2, PIn (k, v1) l -> PIn (k, v2) l -> v1 = v2) ->
    PUniq l -> PUniq (map (fun kv => kv.1) l).
Proof.
elim : l => [|[k v] l IHl] /= Hext Huniql /=.
    by apply: puniq_nil.
apply: puniq_cons;
    last first.
    apply: IHl;
        last by inversion Huniql.
    move => k' v1 v2 Hin1 Hin2.
    apply: (Hext k');
    by apply: pin_future.
move /pin_map => [[a b]] /= [] <- Hin.
inversion Huniql; subst.
have Hin_future: PIn (k,b) ((k, v)::l).
    by apply: pin_future.
have Hinkvl: PIn (k, v) ((k, v):: l).
    by apply: pin_here.
have Heq := Hext k b v Hin_future Hinkvl.
apply: H1.
by rewrite -Heq.
Qed.


Lemma uniq_map_in_in {A B} (l: seq (A * B)) k v1 v2:
    PUniq (map (fun kv => kv.1) l) ->
    PIn (k, v1) l ->
    PIn (k, v2) l ->
    v1 = v2.
Proof.
elim : l => [| [k' v'] l IHl] /=;
    rewrite ?PIn_empty_false_iff => Huniq Hinv1 Hinv2.
    by [].
move : Huniq.
move /uniq_inv => [Hnot_in Huniq].
move : Hinv1 Hinv2 Hnot_in IHl.
move /PIn_cons_or => [[<- <-]| Hin_v1];
    move /PIn_cons_or => [|].
-   by move => [<-].
-   move => Hin_v2 Hnot_in.
    by have := pin_seq_pin_fst  Hin_v2.
-   move => [<- ->] Hnot_in IHl.
    by have := pin_seq_pin_fst Hin_v1.
-   move => Hinv2 Hnot_in IHl.
    by apply: IHl.
Qed.

Definition seq_forall {A} (P: A -> Prop) (s: seq A) :=
    foldl (fun p a => P a /\ p ) True s.

Lemma seq_forall_forall {A} (P: A -> Prop) (s: seq A) :
    seq_forall P s <-> (forall a, PIn a s -> P a).
Proof.
rewrite /seq_forall.
elim/last_ind: s => [|l a IHl].
    split => //.
    move => _ a.
    by rewrite PIn_empty_false_iff.
rewrite foldl_rcons IHl.
split.
    move => [HP HIn] a'.
    move /PIn_rcons_or => [->|].
        by [].
    by apply: HIn.
move => HIn.
split.
    apply: HIn.
    apply/PIn_rcons_or.
    by left.
move => a' HInl.
apply: HIn.
apply/PIn_rcons_or.
by right.
Qed.

Definition seq_exists {A} (P: A -> Prop) (s: seq A) :=
    foldl (fun p a => P a \/ p ) False s.

Lemma seq_exists_exists {A} (P: A -> Prop) (s: seq A) :
    seq_exists P s <-> exists a, PIn a s /\ P a.
Proof.
rewrite /seq_exists.
elim/last_ind: s => [|s a IHs].
    split => //.
    move => [a []].
    by rewrite PIn_empty_false_iff.
rewrite foldl_rcons IHs.
split.
    move => [HP|[a' [HIn HP]]];
        (exists a' || exists a);
        rewrite PIn_rcons_or;
        split => //;
        by [left | right].
move => [a'].
rewrite PIn_rcons_or.
move => [[->| Hin]] HP.
    by left.
right.
by exists a'.
Qed.

(* This section is based on:                                                  *)
(*   https://github.com/imdea-software/fcsl-pcm/blob/master/core/seqperm.v    *)
(* commit id: 79bb8b3eae4d6c682a757b0c26f282ce378dab52                        *)

(****************************************************)
(* A theory of permutations over non-equality types *)
(****************************************************)

Reserved Infix "≡ₚ" (at level 70, no associativity).
Inductive perm {A} : seq A -> seq A -> Prop :=
| permutation_nil : [::] ≡ₚ [::]
| permutation_skip x s1 s2: s1 ≡ₚ s2 -> (x :: s1) ≡ₚ (x :: s2)
| permutation_swap x y s1 s2: s1 ≡ₚ s2 -> [:: x, y & s1] ≡ₚ [:: y, x & s2]
| permutation_trans t s1 s2: s1 ≡ₚ t -> t ≡ₚ s2 -> s1 ≡ₚ s2
where "x ≡ₚ y" := (perm x y).

Section Perm.
Context {A : Type}.


Lemma perm_refl (s : seq A) : s  ≡ₚ s.
Proof.
elim: s=> [| a l IHl].
    by apply: permutation_nil.
by apply: permutation_skip.
Qed.

Lemma perm_nil (s : seq A) : [::] ≡ₚ s <-> s = [::].
Proof.
split.
    move E: {1}[::]=>l H.
    move: H {E}(esym E).
    by elim=>//??? _ IH1 _ IH2 /IH1/IH2.
move=>->.
by apply: permutation_nil.
Qed.

Lemma perm_sym (s1 s2: seq A) : s1 ≡ₚ s2 <-> s2 ≡ₚ s1.
Proof.
suff {s1 s2} L : forall s1 s2, s1 ≡ₚ s2 -> s2 ≡ₚ s1.
    split;
        by apply: L.
move => T s1 s2.
elim {s1 s2} =>[
    |x s1 s2 HP12 HP32
    |x y s1 s2 HP12 HP21
    |s1 s2 s3 HP21 HP12 HP13 HP31].
+   by apply: permutation_nil.
+   by apply: permutation_skip.
+   by apply: permutation_swap.
+   by apply: permutation_trans HP31 HP12.
Qed.

Lemma perm_trans (s2 s1 s3: seq A) : s1 ≡ₚ s2 -> s2 ≡ₚ s3 -> s1 ≡ₚ s3.
Proof.
by apply: permutation_trans.
Qed.

Lemma perm_in (s1 s2: seq A) x : s1 ≡ₚ s2 -> PIn x s1 -> PIn x s2.
Proof.
elim =>
    [|x' s3 s4 Hperm34 Hin34
     |x' y' s3 s4 Hperm34 Hin34
     | s4 s3 s5 Hperm34 Hin34 Hperm45 Hin45];
     rewrite ?PIn_cons_or.
-   by [].
-   move => [<-|Hin3].
        by left.
    right.
    by apply: Hin34.
-   move =>[<-|[<-|Hin3]].
    +   right.
        by left.
    +   by left.
    +   repeat right.
        by apply: Hin34.
-   move => Hin3.
    have Hin4 := Hin34 Hin3.
    by apply: Hin45.
Qed.

Lemma perm_catC (s1 s2: seq A) : (s1 ++ s2) ≡ₚ (s2 ++ s1).
Proof.
elim: s1 s2=>[|x s1 IH1] s2 /=.
    rewrite cats0.
    by apply: perm_refl.
apply: (@perm_trans (x::s2++s1)).
    by apply: permutation_skip.
elim: s2=>[|y s2 IH2] //=.
    by apply: perm_refl.
apply: (@perm_trans (y::x::s2++s1)).
    apply: permutation_swap.
    by apply: perm_refl.
by apply: permutation_skip.
Qed.

Lemma perm_cat2lL (s s1 s2: seq A) : s1 ≡ₚ s2 -> (s ++ s1) ≡ₚ (s ++ s2).
Proof.
elim: s=>[//|e s IH /IH];
    by apply: permutation_skip.
Qed.

Lemma perm_cat2rL (s s1 s2: seq A) : s1 ≡ₚ s2 -> (s1 ++ s) ≡ₚ (s2 ++ s).
Proof.
move=>?.
apply: (@perm_trans (s ++ s1)).
    by apply: perm_catC.
apply: (@perm_trans (s ++ s2)).
    by apply: perm_cat2lL.
by apply: perm_catC.
Qed.

Lemma perm_catL (s1 t1 s2 t2: seq A) :
        s1 ≡ₚ s2 -> t1 ≡ₚ t2 -> (s1 ++ t1) ≡ₚ (s2 ++ t2).
Proof.
move/(perm_cat2rL t1)=>H1 /(perm_cat2lL s2).
by apply: perm_trans.
Qed.

Lemma perm_cat_consL (s1 t1 s2 t2: seq A)  x :
        s1 ≡ₚ s2 -> t1 ≡ₚ t2 -> (s1 ++ x :: t1) ≡ₚ (s2 ++ x :: t2).
Proof.
move=>*.
apply: perm_catL.
    by [].
by apply: permutation_skip.
Qed.

Lemma perm_cons_catCA (s1 s2: seq A) x : (x :: s1 ++ s2) ≡ₚ (s1 ++ x :: s2).
Proof.
rewrite -cat1s -(cat1s _ s2) !catA.
by apply/perm_cat2rL/perm_catC.
Qed.

Lemma perm_cons_catAC (s1 s2: seq A) x : (s1 ++ x :: s2) ≡ₚ (x :: s1 ++ s2).
Proof.
by apply/perm_sym/perm_cons_catCA.
Qed.

Lemma perm_cons_cat_consL (s1 s2 s: seq A) x :
        s  ≡ₚ (s1 ++ s2) -> (x :: s) ≡ₚ (s1 ++ x :: s2).
Proof.
move=>?.
apply: (@perm_trans (x :: (s1 ++ s2))).
    by apply: permutation_skip.
by apply: perm_cons_catCA.
Qed.

Lemma perm_size (l1 l2: seq A): l1 ≡ₚ l2 -> size l1 = size l2.
Proof.
by elim=>//=???? =>[|?|]->.
Qed.

Lemma perm_cat_consR (s1 s2 t1 t2: seq A) x :
        (s1 ++ x :: t1) ≡ₚ (s2 ++ x :: t2) -> (s1 ++ t1) ≡ₚ (s2 ++ t2).
Proof.
move: s1 t1 s2 t2 x.
suff H:
    forall (r1 r2: seq A), r1 ≡ₚ r2 -> forall x s1 t1 s2 t2,
        r1 = s1 ++ x :: t1 -> r2 = s2 ++ x :: t2 -> (s1 ++ t1) ≡ₚ (s2 ++ t2).
    move=>s1 t1 s2 t2 x /H H'. by apply: H'.
move => r1 r2.
elim {r1 r2}=> [];
    last 1 first.
-   move=>s2 s1 s3 H1 IH1 H2 IH2 x r1 t1 r2 t2 E1 E2.
    case: (@PIn_split _ x s2).
    -   apply: perm_in H1 _.
        rewrite E1 PIn_app.
        right.
        left.
    -   move=>s4 [s5] E.
        apply: (@perm_trans (s4++s5)).
            by apply: IH1 E1 E.
        by apply: IH2 E E2.
-   by move=>x [].
-   move=>x t1 t2 H IH y [|b s1] s2 [|c p1] p2 /= [E1 E2] [E3 E4];
        subst x;
        move: H;
        rewrite ?E1 ?E2 ?E3 ?E4 =>// H.
    -   subst y.
        apply: perm_trans H _.
        rewrite perm_sym.
        apply: perm_cons_cat_consL.
        by apply: perm_refl.
    -   apply: perm_trans H.
        apply: perm_cons_cat_consL.
        by apply: perm_refl.
    -   apply: permutation_skip.
        by apply: IH E2 E4.
move=>x y p1 p2 H IH z [|b s1] t1 [|c s2] t2 /= [E1 E2] [E3 E4];
    subst x y;
    move : H IH;
    rewrite -?E2 -?E4 => H IH.
-   by apply: permutation_skip.
-   case: s2 E4=>/=[|a s2][<-]=>[|E4];
        apply: permutation_skip.
        by [].
    subst p2.
    apply: perm_trans H _.
    by apply: perm_cons_catAC.
-   case: s1 E2=>/=[|a s1][<-]=>[|E2];
        apply: permutation_skip.
        by [].
    subst p1.
    apply: perm_trans H.
    by apply: perm_cons_catCA.
case: s1 E2=>/=[|a s1][->]=>E2;
    case: s2 E4=>/=[|d s2][->]=>E4;
    move : H IH;
    rewrite ?E2 ?E4 => H IH.
-   by apply: permutation_skip.
-   apply: (@perm_trans [:: d, z & s2 ++ t2]);
        last first.
        apply: permutation_swap.
        by apply: perm_refl.
    apply: permutation_skip.
    apply: perm_trans H _.
    by apply/perm_cons_catAC.
-   apply: (@perm_trans [:: a, z & s1 ++ t1]).
        apply: permutation_swap.
        by apply: perm_refl.
    apply: permutation_skip.
    by apply/perm_trans/H/perm_cons_catCA.
-   apply: permutation_swap.
    by apply: IH.
Qed.

Lemma perm_cons x (s1 s2: seq A): (x :: s1) ≡ₚ (x :: s2) <-> s1 ≡ₚ s2.
Proof.
split.
   by apply/(@perm_cat_consR [::] [::]).
by apply: permutation_skip.
Qed.

Lemma perm_cat2l (s s1 s2: seq A):  (s ++ s1) ≡ₚ (s ++ s2) <-> s1 ≡ₚ s2.
Proof.
split.
    by elim: s=>// ??? /perm_cons.
by apply: perm_cat2lL.
Qed.

Lemma perm_cat2r (s s1 s2: seq A) : (s1 ++ s) ≡ₚ (s2 ++ s) <-> s1 ≡ₚ s2.
Proof.
split;
    last by apply: perm_cat2rL.
elim: s=>[|??? /perm_cat_consR].
    by rewrite !cats0.
by [].
Qed.

Lemma perm_catAC (s1 s2 s3: seq A) : ((s1 ++ s2) ++ s3) ≡ₚ ((s1 ++ s3) ++ s2).
Proof.
rewrite -!catA perm_cat2l.
by apply: perm_catC.
Qed.

Lemma perm_catCA (s1 s2 s3: seq A) : (s1 ++ s2 ++ s3) ≡ₚ (s2 ++ s1 ++ s3).
Proof.
rewrite !catA perm_cat2r.
by apply: perm_catC.
Qed.

Lemma perm_cons_cat_cons x (s1 s2 s: seq A) :
        (x :: s) ≡ₚ (s1 ++ x :: s2) <-> s  ≡ₚ (s1 ++ s2).
Proof.
split.
    by apply: (@perm_cat_consR [::]).
by apply: perm_cons_cat_consL.
Qed.

Lemma perm_cat_cons x (s1 s2 t1 t2: seq A) :
        (s1 ++ x :: t1) ≡ₚ (s2 ++ x :: t2) <-> (s1 ++ t1) ≡ₚ (s2 ++ t2).
Proof.
split=>[|H].
    by apply: perm_cat_consR.
apply: (@perm_trans (x::s1++t1)).
    by apply: perm_cons_catAC.
apply: (@perm_trans (x::s2++t2)).
    by apply: permutation_skip.
rewrite perm_sym.
apply: perm_cons_catAC.
Qed.

Lemma perm_empty_r (s: seq A) : s = [::] <-> s ≡ₚ [::].
Proof.
split.
    move ->.
    apply: perm_refl.
move Heqs' : [::] => s' HP.
rewrite -Heqs'.
elim : HP Heqs' => {s s'} [
    |x' s1 s2 HP12 IH
    |x' y s1 s2 HP12 Heq
    |s2 s1 s3 HP12 Heq12 HP23 Heq23] H //.
apply: Heq12.
symmetry.
by apply: Heq23.
Qed.

Lemma perm_empty_l (s: seq A) : [::] = s <-> [::] ≡ₚ s.
Proof.
rewrite perm_sym -perm_empty_r.
split;
    by move ->.
Qed.

Lemma perm_singleton_r x (s: seq A): s = [::x] <-> s ≡ₚ [::x].
Proof.
split.
    move ->.
    apply: perm_refl.
move Heqs' : [::x] => s' HP.
rewrite -Heqs'.
elim : HP Heqs' => {s s'} [
    |x' s1 s2 HP12 IH
    |x' y s1 s2 HP12 Heq
    |s2 s1 s3 HP12 Heq12 HP23 Heq23] //=.
-   move => [<-] Hnil.
    move: HP12.
    rewrite -Hnil -perm_empty_r.
    by move ->.
-   move => Heq3.
    apply: Heq12.
    symmetry.
    by apply: Heq23.
Qed.

Lemma perm_singleton_l x (s: seq A): [::x] = s <-> [::x] ≡ₚ s.
Proof.
rewrite perm_sym -perm_singleton_r.
split;
    by move ->.
Qed.

Lemma perm_length (s1 s2: seq A): s1 ≡ₚ s2 -> length s1 = length s2.
Proof.
elim => {s1 s2} [
    |x' s1 s2 HP12 IH
    |x' y s1 s2 HP12 Heq
    |s2 s1 s3 HP12 Heq12 HP23 Heq23] //=;
    by lia.
Qed.

Lemma foldr_perm (f: A -> A -> A) b (s1 s2: seq A):
    associative f ->
    commutative f ->
    s1 ≡ₚ s2 -> foldr f b s1 = foldr f b s2.
Proof.
move => Hassoc Hcomm.
elim => {s1 s2} [
    |x' s1 s2 HP12 IH
    |x' y s1 s2 HP12 Heq
    |s2 s1 s3 HP12 Heq12 HP23 Heq23] /=.
-   by [].
-   by rewrite IH.
-   by rewrite Hassoc (@Hcomm x') -Hassoc Heq.
-   by rewrite Heq12 Heq23.
Qed.

End Perm.

(* perm and map *)
Lemma pperm_map A B (f : A -> B) (s1 s2 : seq A) :
        s1 ≡ₚ s2 -> (map f s1) ≡ₚ (map f s2).
Proof.
elim => [
    |x s3 s4 HP34 HPmap34
    |x y s3 s4 HP34 HPmap34
    |s3 s4 s5 HP34 HPmap34 Hp34 HPmap45] /=.
-   apply: perm_refl.
-   by rewrite perm_cons.
-   by apply: permutation_swap.
-   apply: perm_trans.
        apply: HPmap34.
    apply: HPmap45.
Qed.

(* mapping to ssreflect decidable perm *)
Lemma perm_eq_perm (A : eqType) (s1 s2 : seq A) :
        reflect (s1 ≡ₚ s2) (perm_eq s1 s2).
Proof.
apply: (iffP idP).
    elim: s2 s1 =>[s1 /ssreflect.seq.perm_size/size0nil->// | x s2 IH s1 H].
    -   apply: perm_refl.
    -   move: (seq.perm_mem H x).
        rewrite mem_head=>H'.
        move: H' H.
        move/splitPr=>[p1 p2].
        rewrite -cat1s ssreflect.seq.perm_catCA.
        rewrite ssreflect.seq.perm_cons=>/IH.
        by rewrite -[_::s2]cat0s perm_cat_cons.
elim=>[|||??? _ H1 _ H2] /=.
-   by [].
-   move => x s3 s4.
    by rewrite ssreflect.seq.perm_cons.
-   move => x y s3 s4.
    rewrite -![[:: _, _ & _]]/([::_] ++ [::_] ++ _) ssreflect.seq.perm_catCA.
    by rewrite !ssreflect.seq.perm_cat2l.
-   by apply: ssreflect.seq.perm_trans H1 H2.
Qed.


(* The definition and lemmas for submseteq are taken straight from stdpp. I   *)
(* guess I am too dump to come up with that myself :\                         *)

(* A list [l2] submseteq a list [l1] if [l2] is obtained by removing elements *)
(* from [l1] while possiblity changing the order. *)
Inductive submseteq {A} : seq A -> seq A -> Prop :=
  | submseteq_nil : submseteq [::] [::]
  | submseteq_skip x l1 l2 : submseteq l1 l2 -> submseteq (x :: l1) (x :: l2)
  | submseteq_swap x y l : submseteq (y :: x :: l) (x :: y :: l)
  | submseteq_cons x l1 l2 : submseteq l1 l2 -> submseteq l1 (x :: l2)
  | submseteq_trans l1 l2 l3 : submseteq l1 l2 -> submseteq l2 l3 -> submseteq l1 l3.
Infix "⊆+" := submseteq (at level 70).

Lemma submseteq_length A (l1 l2: seq A) : l1 ⊆+ l2 -> length l1 <= length l2.
Proof.
elim => /=;
    by lia.
Qed.

Lemma submseteq_nil_l A (l: seq A) : [::] ⊆+ l.
Proof.
elim : l => [|a l IHl].
    by constructor.
by constructor.
Qed.

Lemma submseteq_nil_r A (l: seq A) : l ⊆+ [::] <-> l = [::].
Proof.
split;
    last first.
    move ->.
    by constructor.
move /submseteq_length.
case : l => [|a l] /=.
    by [].
by lia.
Qed.

Lemma Permutation_submseteq A (l1 l2: seq A) : l1 ≡ₚ l2 -> l1 ⊆+ l2.
Proof.
elim => {l1 l2} => [
    |x s1 s2 HP12 HS12
    |x y s1 s2 HP12 HS12
    |s2 s1 s3 HP12 HS12 HP23 HS23].
-   by constructor.
-   by constructor.
-   apply: submseteq_trans.
        apply: submseteq_swap.
    by repeat apply: submseteq_skip.
-   apply: submseteq_trans.
    by apply: HS12.
    by apply: HS23.
Qed.

Lemma submseteq_Permutation A (l1 l2: seq A) : l1 ⊆+ l2 -> exists k, l2 ≡ₚ l1 ++ k.
Proof.
elim => {l1 l2} [
    |a l1 l2 HS12 [k HP12k]
    |a b l1
    |a l1 l2 HP12 [k HP12k]
    |l1 l2 l3 HP12 [k HP21k] HP23 [k' HP32k']].
-   exists [::] => /=.
    apply: perm_refl.
-   exists k.
    rewrite cat_cons.
    by apply: permutation_skip.
-   exists [::].
    rewrite cats0.
    apply: permutation_swap.
    apply: perm_refl.
-   exists (a :: k).
    by rewrite perm_cons_cat_cons.
-   exists (k ++ k').
    apply: permutation_trans.
        apply: HP32k'.
    by rewrite catA perm_cat2r.
Qed.
Lemma list_app_length A (l1 l2: seq A):
    length (l1 ++ l2) = length l1 + length l2.
Proof.
elim : l1 l2 => [|a l IHl] l2 /=.
    by lia.
rewrite IHl.
by lia.
Qed.

Lemma submseteq_Permutation_length_le A (l1 l2: seq A) :
    length l2 <= length l1 -> l1 ⊆+ l2 -> l1 ≡ₚ l2.
Proof.
move => Hl12 HS12.
move : (submseteq_Permutation HS12) =>[[|a s3]].
    by rewrite cats0 perm_sym.
rewrite perm_sym.
move => /Permutation_submseteq/submseteq_length.
rewrite list_app_length /=.
by lia.
Qed.

Lemma submseteq_Permutation_length_eq A (l1 l2: seq A) :
  length l2 = length l1 -> l1 ⊆+ l2 -> l1 ≡ₚ l2.
Proof.
move => Hl12.
apply submseteq_Permutation_length_le.
by lia.
Qed.

Lemma submseteq_Permutation_alt A (l1 l2: seq A):
    l1 ⊆+ l2 -> l2 ⊆+ l1 -> l1 ≡ₚ l2.
move => HS12 HS21.
apply: submseteq_Permutation_length_eq;
    last by [].
move: HS12 HS21.
move /submseteq_length => Hl12 /submseteq_length => Hl21.
by lia.
Qed.

Lemma submseteq_inserts_l A (l1 l2 l3: seq A):
    l1 ⊆+ l2 -> l1 ⊆+ l3 ++ l2.
Proof.
elim : l3 l1 l2 => [|a l3 IHl] l1 l2 HS12 /=.
    by [].
constructor.
by apply: IHl.
Qed.

Lemma Puniq_submseteq A (l k: seq A) :
    PUniq l -> (forall x, PIn x l -> PIn x k) -> l ⊆+ k.
Proof.
move => Huniq.
elim : Huniq k => {l} [| a l Hnotinl Huniql IHl] k Hinin.
    by apply: submseteq_nil_l.
have Hinak: PIn a k.
    apply: Hinin.
    by apply: pin_here.
move :(@PIn_split _ a k Hinak) => [s1 [s2 Hk]].
subst.
apply: (@submseteq_trans _ _ (a:: s1 ++ s2));
    last first.
    apply: Permutation_submseteq.
    apply: perm_cons_catCA.
constructor.
apply: IHl => x Hin.
have Hororor: PIn x s1 \/ x = a \/ PIn x s2.
    rewrite -PIn_cons_or -PIn_app.
    apply: Hinin.
    by right.
rewrite PIn_app.
move : Hororor Hin.
move => [Hin1|[->|Hin2]] Hinl.
-   by left.
-   by [].
-   by right.
Qed.

Lemma Puniq_Permutation A (l1 l2: seq A) :
    PUniq l1 -> PUniq l2 -> (forall x, PIn x l1 <-> PIn x l2) -> l1 ≡ₚ l2.
Proof.
move => Huniq1 Huniq2 Hinin.
apply: submseteq_Permutation_alt;
    apply: Puniq_submseteq => //;
    move => x;
    by rewrite Hinin.
Qed.