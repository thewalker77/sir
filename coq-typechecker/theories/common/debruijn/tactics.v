(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(* Based on work made by Tobias Tebbi, Steven Schäfer, et al. for             *)
(* https://github.com/coq-community/autosubst commit tag:495d547fa2a3e40da0f7 *)
(* Base work was licensed Under MIT                                           *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
Require Import Coq.Logic.FunctionalExtensionality.
From mathcomp Require Import all_ssreflect.
From HB Require Import structures.
From sir.common Require Import utils padd debruijn.structs.


Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(* The only thing you should directly use from this file is [asimpl]. If that *)
(* does not work, then this is a real bind. In that case please consult the   *)
(* lemmas in [defs.v], [facts.v] as well as the ones in this file. Also,      *)
(* please check the associated readme file in this directory                  *)
Ltac fsimpl :=
repeat match goal with
| [|- context[?f ∘ id]] => rewrite compfid
| [|- context[id ∘ ?f]] => rewrite compidf
| [|- context[?h ∘ (?g ∘ ?f )]] =>
    rewrite compA
| [|- context[(padd 0)]] => rewrite padd0_id
| [|- context[0 + ?m]] => change (0 + m) with m
| [|- context[?s ∘ succn]] => change (s ∘ succn) with (s ∘ (padd 1))
| [|- context[S ?n + ?m]] => change (S n + m) with (S (n + m))
| [|- context[((?x .: ?xr) ∘ padd (S ?n))]] =>
    change ((x .: xr) ∘ padd (S n)) with (xr ∘ (padd n))
| [|- context[?f ∘ ?x .: (padd (S ?n))]] =>
    change x with (f n); rewrite (@scons_eta _ f n)
| _ => progress (rewrite ?scons_comp ?paddS ?addnS ?padd0 ?addn0 ?paddA ?addnA
                         ?padd_comp ?padd_compR ?padd_eta)
  end.

Lemma rename_substX (T: debruijn) ξ: @rename T ξ = subst (ren ξ).
Proof.
apply: functional_extensionality.
by apply: rename_subst.
Qed.

Lemma upE (T: debruijn) σ : @up T σ = ids 0 .: subst (ren S) ∘ σ.
Proof.
by rewrite /up rename_substX.
Qed.

Lemma id_scompX (T: debruijn) σ : @subst T σ ∘ ids = σ.
Proof.
apply: functional_extensionality.
by apply: id_subst.
Qed.

Lemma id_scompR (T: debruijn) (σ: nat -> T) (f: T -> T) :
    f ∘ subst σ ∘ ids = f ∘ σ.
Proof.
by rewrite -compA id_scompX.
Qed.

Lemma subst_idX (T: debruijn): @subst T ids = id.
Proof.
apply: functional_extensionality.
by apply: subst_id.
Qed.

Lemma subst_compI (T: debruijn) σ τ s :
    s.[σ].[τ] = s.[@subst T τ ∘ σ].
Proof.
by apply subst_scomp.
Qed.

Lemma subst_compX (T: debruijn) σ τ :
    @subst T τ ∘ subst σ  = subst (subst τ ∘ σ).
Proof.
apply: functional_extensionality.
by apply subst_scomp.
Qed.

Lemma subst_compR (T: debruijn) σ τ (f : T -> T) :
    (f ∘ subst τ) ∘ (subst σ) = f ∘ subst (subst τ ∘ σ).
Proof.
by rewrite -subst_compX.
Qed.

Lemma fold_ren_cons (T: debruijn) (x : nat) (ξ : nat -> nat) :
    @ids T x .: ren ξ = ren (x .: ξ).
Proof.
apply functional_extensionality.
by move => [].
Qed.

(* unfold upn *)

Lemma upnSX (T: debruijn) n σ :
    upn (S n) σ = ids 0 .: @subst T (ren S) ∘ upn n σ.
Proof.
by rewrite /upn /iterate upE.
Qed.

Lemma upn0 (T: debruijn) σ : @upn T 0 σ = σ.
Proof.
by [].
Qed.

(* fold up *)
Lemma fold_up (T: debruijn) k σ :
  @ids T k .: (ren (padd (S k)) ⊚ σ)  =
  ren (padd k) ⊚ up σ.
Proof.
rewrite /scomp /ren upE.
fsimpl.
rewrite id_subst subst_compX /=.
rewrite addn0 /ren.
rewrite [_ ∘ (ids ∘ succn)]compA.
rewrite id_scompX.
by fsimpl.
Qed.

Lemma fold_up0 (T: debruijn) (σ: nat-> T) :
  ren (padd 0) ⊚ σ = σ.
Proof.
rewrite/scomp/ren.
fsimpl.
by rewrite subst_idX.
Qed.

(* combine up *)
Lemma fold_up_up (T: debruijn) σ : @up T (up σ) = upn 2 σ.
Proof.
by [].
Qed.

Lemma fold_up_upn (T: debruijn) n σ : @up T (upn n σ) = upn (S n) σ.
Proof.
by [].
Qed.

Lemma fold_upn_up (T: debruijn) n σ : upn n (@up T σ) = upn (S n) σ.
Proof.
by rewrite /upn iterate_Sr.
Qed.

Ltac autosubst_typeclass_normalize :=
repeat match goal with
| [|- context[@ids ?T ?x]] =>
    let s := constr:(@ids T x) in progress change (@ids T x) with s
| [|- context[@ren ?T ?ξ]] =>
    let s := constr:(@ren T ξ) in progress change (@ren T ξ) with s
| [|- context[@rename ?T ?ξ]] =>
    let s := constr:(@rename T ξ) in progress change (@rename T ξ) with s
| [|- context[@subst ?T ?σ]] =>
    let s := constr:(@subst T σ) in progress change (@subst T σ) with s
end.

Ltac autosubst_unfold_up :=
rewrite ?upE ?upnSX;
repeat match goal with
| [|- context[@upn ?T 0 ?σ]] => change (@upn T 0 σ) with σ
end.

Ltac autosubst_unfold :=
autosubst_typeclass_normalize; autosubst_unfold_up;
rewrite ?rename_substX; unfold ren, scomp, upren.

Ltac fold_ren :=
repeat match goal with
| [|- context[(@ids ?T) ∘ ?xi]] => change (@ids T ∘ xi) with (@ren T xi)
| [|- context[(?g ∘ @ids ?T) ∘ ?ξ]] =>
    change ((g ∘ @ids T) ∘ ξ) with (g ∘ (@ren T ξ))
| [|- context[@ren ?T ?ζ ∘ ?ξ]] =>
    change (@ren T ζ ∘ ξ) with (@ren T (ζ ∘ ξ))
| [|- context[(?g ∘ @ren ?T ?ζ) ∘ ?ξ]] =>
    change ((g ∘ @ren T ζ) ∘ ξ) with
           (g ∘ @ren T (ζ ∘ ξ))
| [|- context [@ids ?T ?x .: ?σ]] => first
  [ rewrite fold_ren_cons
  | replace (ids x .: @ids T) with (@ren T (x .: id))
    by (symmetry; apply fold_ren_cons)
]
end.

Ltac fold_comp :=
repeat match goal with
| [|- context[(?h ∘ ?g) ∘ ?f ]] =>
    change ((h ∘ g) ∘ f) with (h ∘ (g ∘ f))
| [|- context[(@subst ?T ?τ) ∘ ?σ]] =>
    change ((@subst T τ) ∘ σ) with (scomp T τ  σ)
end.

Ltac fold_up := rewrite ?fold_up ?fold_up0;
repeat match goal with
| [|- context[@up ?T (up ?sigma)]] =>
    change (@up T (@up T sigma)) with (@upn T 2 sigma)
| [|- context[@up ?T (upn ?n ?sigma)]] =>
    change (@up T (upn n sigma)) with (@upn T (S n) sigma)
| [|- context[0 .: padd 1 ∘ ?ξ]] =>
    change (0 .: padd 1 ∘ ξ) with (upren ξ)
| _ => rewrite fold_upn_up
end.

Ltac asimpl :=
    cbn; autosubst_unfold; repeat first
[ progress (
    cbn; unfold ren, scomp; fsimpl; autosubst_unfold_up;
    rewrite ?id_scompX ?id_scompR ?subst_idX ?subst_compX
            ?subst_compR ?id_subst ?subst_id ?subst_compI
  )
];
fold_ren; fold_comp; fold_up.


