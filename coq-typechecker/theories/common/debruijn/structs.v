(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(* Based on work made by Tobias Tebbi, Steven Schäfer, et al. for             *)
(* https://github.com/coq-community/autosubst commit tag:495d547fa2a3e40da0f7 *)
(* Base work was licensed Under MIT                                           *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From HB Require Import structures.
From sir.common Require Import utils padd.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(******************************************************************************)
(* This file implements basic operations required to construct binders based  *)
(* on Debruijn indices. The main notations are as follows:                    *)
(* - "t.[σ]": That means perform substitution on t using σ. Note that Sigma is *)
(*          a function [nat -> Term]. What σ does is replace every instance   *)
(*          of natural numebr [x] in [var x] with different term. One example *)
(*          for a σ is [ids] which identity substitution which replaces       *)
(*          [var x] by itself.                                                *)
(* -"t.[n/]": That is the same as "t.[n.:id]" which is a substitution that     *)
(*            replaces every instance of [var 0] with n and decrease the      *)
(*            index of every other variable while keeping it it the same      *)
(* - "σ ⊚ τ": This is a notation for substitution composition. Given that σ *)
(*              and Τ are both [nat -> Term]. "σ ⊚ Τ" reads Substitute for  *)
(*              σ after substituting for τ. Note that this notation is        *)
(*              different from normal function composition "g ∘ f". In the    *)
(*              latter case, f will have type [A -> B] while g will have type *)
(*              [B -> C] and "(g ∘ f) x = g (f x)". In our case, we have a    *)
(*              lemma [subst_scomp] that states "t.[σ ⊚ τ] = t.[τ].[σ]"     *)
(*                                                                            *)
(* Proofs strategy                                                            *)
(* Generally you might want to prove that a property holds for "t.[n/]" for   *)
(* some t and n. The best approach is to start from a more general grounds    *)
(* try to prove that property for "t.[σ]" first and then try to prove similar *)
(* property but for substitutions (like σ) instead of terms. This usually     *)
(* works because in this case, "n" is not a term, but rather a substitution   *)
(* function that looks like "n .: ids". An example for this approach is the   *)
(* lemmas: [pred_psubst], [spred_up], [pred_spred], [pspred_up_id] which were *)
(* are necessary to prove the main lemmas [pred_subst]                        *)
(*                                                                            *)
(* This to do and things to not do                                            *)
(* - Do not use "rename ξ t", instead use "t.[ren ξ]"                         *)
(* - It is useful to have lemmas that utulize propositional equality when     *)
(*   describing relations between terms that may contain substition. An       *)
(*   example will be [SynType_eappl], [ParallelReduction_ebeta].              *)
(* - Take extra care when using propositional equality on judgements that may *)
(*   reflexive, for instane you can two goals produced by that looks like     *)
(*    "x ⇒ ?goal1" and "y = ?goal1". These goals can naturally occur when   *)
(*     using [ParallelReduction_ebeta] for instance. In this case, if you     *)
(*     solve the first goal automatically, it may be dismissed by [pred_refl] *)
(*     and the second goal will end up being "y = x" which may not be correct *)
(******************************************************************************)

HB.mixin Record HasIDs T := {
  ids: nat -> T;
}.

#[short(type="ideable")]
HB.structure Definition IDS := { T of HasIDs T }.
#[global] Opaque ids.

HB.mixin Record HasRename T := {
    rename: (nat -> nat) -> T -> T
}.

#[short(type="renamable")]
HB.structure Definition Renamable := { T of HasRename T }.
#[global] Opaque rename.



#[short(type="renamable_ideable")]
HB.structure Definition RenamableAndIDS := { T of HasRename T & HasIDs T}.


HB.mixin Record HasSubst T := {
    subst: (nat -> T) -> T -> T
}.

#[short(type="substable")]
HB.structure Definition Substable := { T of HasSubst T }.
#[global] Opaque subst.
Notation "t .[ σ ]" := (subst σ t) (at level 2, left associativity, format "t .[ σ ]").
Notation "s .[ t /]" := (s.[t .: ids])
    (at level 2,
     t at level 200,
     left associativity,
     format "s .[ t /]").

#[short(type="substable_renamable_ideable")]
HB.structure Definition SubstableRenamableAndIDS :=
    {T of HasSubst T & HasRename T & HasIDs T}.

(* Helper functions*)
Definition upren (ξ : nat -> nat) : (nat -> nat) := 0 .: (padd 1) ∘ ξ.
Definition ren {T: ideable} (ξ : nat -> nat) : nat -> T :=  ids ∘ ξ.
Definition up {T :renamable_ideable} (σ : nat -> T) : nat -> T :=
    (ids 0) .: (rename S) ∘ σ.
Definition upn {T: renamable_ideable} := iterate (@up T).

Definition scomp {T: substable} (g : nat -> T) (f : nat -> T) : nat -> T
  := (subst g) ∘ f.
Notation " g ⊚ f " := (scomp g f) (at level 40, left associativity).

HB.mixin Record HasDebruijnLemmas (T: substable_renamable_ideable) := {
    rename_subst ξ (t: T): rename ξ t = t.[ren ξ];
    subst_id (t: T) : t.[ids] = t;
    id_subst (σ : nat -> T) x : (ids x).[σ] = σ x;
    subst_scomp σ τ (t : T): t.[σ].[τ] = t.[τ ⊚ σ]
}.

#[short(type="debruijn")]
HB.structure Definition Debruijn := { T of HasDebruijnLemmas T}.

Definition Closedn {T: substable_renamable_ideable} (t: T) (n: nat) : Prop := forall σ, t.[@upn T n σ] = t.
Definition Closed {T: substable} (t: T) : Prop := forall σ, t.[σ] = t.
