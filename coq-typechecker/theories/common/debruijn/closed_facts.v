(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)

From mathcomp Require Import all_ssreflect.
From sir.common.debruijn Require Import structs tactics.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Lemma closedn_closed {T: substable_renamable_ideable} (t: T) : Closedn t 0 <-> Closed t.
Proof.
rewrite /Closedn /Closed.
by asimpl.
Qed.

Lemma closedn_closednS {T: debruijn} (t: T) n :
    Closedn t n -> Closedn t n.+1.
Proof.
rewrite /Closedn => Hclosed σ.
have : upn n.+1 σ = upn n (up σ).
    by asimpl.
by move ->.
Qed.
