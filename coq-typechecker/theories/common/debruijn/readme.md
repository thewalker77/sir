# Literature on Debruijn indices an Calculus of Explicit substitutions.


- [Completeness and Decidability of de Bruijn Substitution Algebra in Coq](https://dl.acm.org/doi/10.1145/2676724.2693163)
- [Autosubst: Reasoning with de Bruijn Terms and Parallel Substitutions](https://doi.org/10.1007/978-3-319-22102-1_24)
- [Engineering Formal Systems in Constructive Type Theory](https://www.ps.uni-saarland.de/Publications/documents/Schaefer_2019_Engineering.pdf) Chapter 3
- [Explicit Substitutions](https://pauillac.inria.fr/~levy/pubs/90popljfp.pdf) by M. Abadi, et al.