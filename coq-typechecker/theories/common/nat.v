(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From HB Require Import structures.
From mathcomp Require Import all_ssreflect zify.
From sir.common Require Import pos.
Import Pos.
Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(* serDe for nat *)
Module SerDeNat.
(* operations *)
Fixpoint pos_to_nat_partial (p: positive) : nat :=
match p with
| xH => 1
| xI p' => 1 + 2 * pos_to_nat_partial p'
| xO p' => 2 * pos_to_nat_partial p'
end.

Definition pos_to_nat p := pos_to_nat_partial p - 1.

Fixpoint nat_to_pos (n: nat) : positive :=
match n with
| O => xH
| S n' => succ (nat_to_pos (n'))
end.

(* lemmas *)
Lemma pos_to_nat_partial_g0: forall p, 0 < pos_to_nat_partial p.
Proof.
elim => [p IHp | p IHp|] /=;
    by lia.
Qed.

Lemma pos_to_nat_partial_succ: forall p,
    pos_to_nat_partial (succ p) = S (pos_to_nat_partial p).
Proof.
elim => [p IHp | p IHp |] /=;
by lia.
Qed.

Lemma pos_to_nat_succ: forall p, pos_to_nat (succ p) = S (pos_to_nat p).
Proof.
move => p.
have ? := pos_to_nat_partial_g0 p.
rewrite /pos_to_nat pos_to_nat_partial_succ.
lia.
Qed.

Lemma nat_to_pos_to_nat: forall n, pos_to_nat (nat_to_pos n) = n.
Proof.
elim =>[| n IHn] /=.
    by [].
by rewrite pos_to_nat_succ IHn.
Qed.

End SerDeNat.