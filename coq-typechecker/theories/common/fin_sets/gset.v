(*****************************************************************************)
(* Copyright 2022 TheWalker77                                                *)
(*                                                                           *)
(* Licensed under the Apache License, Version 2.0 (the "License");           *)
(* you may not use this file except in compliance with the License.          *)
(* You may obtain a copy of the License at                                   *)
(*                                                                           *)
(*     http://www.apache.org/licenses/LICENSE-2.0                            *)
(*                                                                           *)
(* Unless required by applicable law or agreed to in writing, software       *)
(* distributed under the License is distributed on an "AS IS" BASIS,         *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *)
(* See the License for the specific language governing permissions and       *)
(* limitations under the License.                                            *)
(*****************************************************************************)

From HB Require Import structures.
From mathcomp Require Import all_ssreflect zify.
From sir.common Require Import seq serde.
From sir.common.fin_maps Require Import gmap fin_maps.
From sir.common.fin_sets Require Export fin_sets.
Export serde fin_sets.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(******************************************************************************)
(* This file is a set impelementation based on the GMap implementation in     *)
(* "gmap.v" The type of set element must implement [eqType] and [SerDe]       *)
(* canonical structures. Both structures are combined in [eqSerDe].           *)
(*                                                                            *)
(* The main data structure is [GSet]                                          *)
(*                                                                            *)
(* The main operations:                                                       *)
(* - [in_set]                                                                 *)
(* - [in_setb]                                                                *)
(* - [empty]                                                                  *)
(* - [singleton]                                                              *)
(* - [union]                                                                  *)
(* - [intersection]                                                           *)
(* - [difference]                                                             *)
(* - [to_list]                                                                *)
(*                                                                            *)
(* The main lemmas:                                                           *)
(* [extensionality]                                                           *)
(* [in_setP]                                                                  *)
(* [not_in_empty]                                                             *)
(* [in_singleton]                                                             *)
(* [in_intersection]                                                          *)
(* [in_difference]                                                            *)
(* [in_to_list]                                                               *)
(* [uniq_to_list]                                                             *)
(******************************************************************************)


Definition GSet (K : eqSerDe) := GTrie K unit.

Module GSet.

Section operations.
Context {K: eqSerDe}.

Definition in_set (k: K) (s: GSet K) : Prop := s !! k = Some tt.

Definition in_setb (k: K) (s: GSet K) : bool := s !! k == Some tt.
Definition empty : GSet K := {[]}.

Definition singleton (k: K) : GSet K := {[ k := tt]}.

Definition union (s1 s2: GSet K) : GSet K := map_union s1 s2.

Definition intersection (s1 s2: GSet K) : GSet K :=
    let intersect (o1 o2: option unit) :=
    match o1, o2 with
    | Some tt, Some tt => o1
    | _, _ => None
    end in
    map_merge intersect s1 s2.

Definition difference (s1 s2: GSet K) : GSet K :=
    let differ (o1 o2: option unit) :=
    match o1, o2 with
    | Some tt, None => o1
    | _, _ => None
    end in
    map_merge differ s1 s2.

Definition to_list (s: GSet K) : seq K :=
    map (fun kv => kv.1 ) (map_to_list s).

End operations.

Section lemmas.
Context {K: eqSerDe}.

Lemma extensionality (s1 s2: GSet K) :
    s1 = s2 <-> forall k, in_set k s1 <-> in_set k s2.
Proof.
split.
    by move ->.
rewrite /in_set.
move => Heq.
apply: map_extensionality => k.
case : (s1!!k) (Heq k) => [[]|] Heqk.
    symmetry.
    by rewrite -Heqk.
case : (s2!!k) Heqk => [[]|] Heqk;
    last by [].
by rewrite Heqk.
Qed.

Lemma in_setP a (s: GSet K): reflect (in_set a s) (in_setb a s).
Proof.
rewrite /in_setb /in_set.
apply: (iffP idP);
    last by move ->.
case (s !! a) => [[]|].
    by [].
by move /eqP.
Qed.

Lemma not_in_empty k: ~ (in_set k ({[]}: GSet K)).
Proof.
rewrite /in_set.
by apply: lookup_empty_some.
Qed.

Lemma in_singleton k1 k2: in_set k1 (singleton k2 : GSet K) <-> k1 = k2.
rewrite /in_set.
split.
    by move /map_lookup_singleton_some => [->].
move ->.
by apply: map_lookup_singleton.
Qed.

Lemma in_union (s1 s2: GSet K) k:
    in_set k (union s1 s2) <-> in_set k s1 \/ in_set k s2.
Proof.
rewrite /in_set /union.
rewrite map_lookup_union_some_raw.
split.
    move => [|[]].
        by left.
    by right.
case (s1 !! k) => [[]|] [|];
    try by [| left | right].
Qed.

Lemma in_intersection (s1 s2: GSet K) k:
    in_set k (intersection s1 s2) <-> in_set k s1 /\ in_set k s2.
Proof.
rewrite /in_set /intersection.
rewrite map_lookup_merge;
    last by [].
case Hlookup1 : (s1 !! k) => [[]|];
    case Hlookup2 : (s2 !! k) => [[]|];
    split => //=;
    by move => [].
Qed.

Lemma in_difference (s1 s2: GSet K) k:
    in_set k (difference s1 s2) <-> in_set k s1 /\ ~ in_set k s2.
Proof.
rewrite /in_set /intersection.
rewrite map_lookup_merge;
    last by [].
case Hlookup1 : (s1 !! k) => [[]|];
    case Hlookup2 : (s2 !! k) => [[]|];
    split => //=;
    by move => [].
Qed.

Lemma in_to_list (s: GSet K) k: in_set k s <-> PIn k (to_list s).
Proof.
rewrite /in_set /to_list -map_elem_of_to_list.
split.
    by apply: pin_seq_pin_fst.
rewrite pin_map.
by move => [[k' []] []] /= <-.
Qed.

Lemma uniq_to_list (s: GSet K): PUniq (to_list s).
Proof.
rewrite /to_list.
by apply: uniq_fst_map_to_list.
Qed.
End lemmas.

Definition Mixin (K: eqSerDe): hasFinSet.axioms_ K (GSet K).
Proof.
econstructor => /=.
-   by apply: extensionality.
-   by apply: in_setP.
-   by apply: not_in_empty.
-   by apply: in_singleton.
-   by apply: in_union.
-   by apply: in_intersection.
-   by apply: in_difference.
-   by apply: in_to_list.
-   by apply: uniq_to_list.
Qed.
End GSet.
HB.instance Definition _ K := GSet.Mixin K.
