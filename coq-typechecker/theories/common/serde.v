(*****************************************************************************)
(* Copyright 2022 TheWalker77                                                *)
(*                                                                           *)
(* Licensed under the Apache License, Version 2.0 (the "License");           *)
(* you may not use this file except in compliance with the License.          *)
(* You may obtain a copy of the License at                                   *)
(*                                                                           *)
(*     http://www.apache.org/licenses/LICENSE-2.0                            *)
(*                                                                           *)
(* Unless required by applicable law or agreed to in writing, software       *)
(* distributed under the License is distributed on an "AS IS" BASIS,         *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *)
(* See the License for the specific language governing permissions and       *)
(* limitations under the License.                                            *)
(*****************************************************************************)
From HB Require Import structures.
From mathcomp Require Import all_ssreflect zify.
From sir.common Require Import pos string nat.
Import Pos.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.


(*****************************************************************************)
(* This file implements basic serialization/deserialization cannonical       *)
(* structure. The to be serializable data type must have two functions:      *)
(* [serialize] and [deserialize]. [serialize] convert type "T" to [positive] *)
(* while [deserialize] converts an arbitrary [positive] back to "T".         *)
(* It is important to note that [deserialize] must be right and left inverse *)
(* for [serialize].                                                          *)
(*****************************************************************************)

HB.mixin Record hasSerDe T := {
    serialize: T -> positive;
    deserialize: positive -> T;
    deserialize_serialize: forall (a: T), deserialize (serialize a) = a;
}.
#[short(type="serDe")]
HB.structureDefinition SerDe :=  { T of hasSerDe T}.

(* Combining SerDe with EqType. *)
#[short(type="eqSerDe")]
HB.structure Definition EqSerDe := {T of Equality T & SerDe T }.

(* derived lemmas *)

Lemma serialize_inv (T: serDe):
    forall (a b: T), a = b <-> serialize a = serialize b.
Proof.
move => a b.
split.
    by move ->.
move => Hser.
have : (deserialize (serialize a) : T) = deserialize (serialize b).
    by rewrite Hser.
by rewrite !deserialize_serialize.
Qed.

Lemma serialize_neq_inv (T: serDe):
forall (a b: T), a <> b <-> serialize a <> serialize b.
Proof.
move => a b.
by rewrite -serialize_inv.
Qed.

HB.instance Definition _ :=
    hasSerDe.Build nat SerDeNat.nat_to_pos_to_nat.

HB.instance Definition _ :=
    hasSerDe.Build string SerDeString.string_of_to_pos.
