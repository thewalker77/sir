(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.core Require Import ast.term debruijn.defs.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.


Definition Context := seq CTerm.

Fixpoint dget (Γ : Context) (n : nat) {struct n} : option CTerm :=
match Γ, n with
| [::], _ => None
| A :: _, 0 => Some A.[ren S]
| _ :: Γ', n.+1 => match (dget Γ' n) with
                      | Some A =>  Some A.[ren S]
                      | None => None
                      end
end.
