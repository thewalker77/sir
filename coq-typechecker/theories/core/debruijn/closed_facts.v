(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)

From mathcomp Require Import all_ssreflect zify.
From sir.core.debruijn Require Import defs elims facts.
From sir.core.ast Require Import term.
From sir.common.debruijn Require Export closed_facts.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Lemma closedn_Cvar1 n idx: Closedn (Cvar idx) n -> idx < n.
Proof.
Transparent ids.
elim : n idx => [|n IHn] [|idx] Hclosedn //=.
-   have := Hclosedn (ren succn).
    by asimpl.
-   have := Hclosedn (ren succn).
    asimpl.
    move => [].
    by lia.
-   apply: IHn.
    rewrite /Closedn /= => σ.
    have := Hclosedn σ.
    asimpl.
    case Heq : (upn n σ idx) => [idx'||||||||]//.
    asimpl.
    by move => [->].
Opaque ids.
Qed.

Lemma closedn_Cvar2 n idx: idx < n -> Closedn (Cvar idx) n.
Proof.
elim : n idx => [|n IHn] [|idx] Hlt //=.
rewrite /Closedn /= => σ.
have := IHn _ Hlt σ.
asimpl.
case Heq : (upn n σ idx) => [idx'||||||||]//.
move => [->].
Transparent ids.
by asimpl.
Opaque ids.
Qed.

Lemma closedn_Cvar n idx: Closedn (Cvar idx) n <-> idx < n.
split.
    by apply: closedn_Cvar1.
by apply: closedn_Cvar2.
Qed.

Lemma closedn_Cuniv n lvl: Closedn (Cuniv lvl) n <-> True.
Proof.
by split.
Qed.

Lemma closedn_Cpi1 A B n:
    Closedn (Cpi A B) n -> Closedn A n /\ Closedn B n.+1.
Proof.
elim: n A B => [|n IHn] A B;
    rewrite /Closedn /=;
    move => Hclosed;
    split => σ;
    move : (Hclosed σ);
    rewrite CsubstE;
    by move => [].
Qed.

Lemma closedn_Cpi2 A B n:
    Closedn A n /\ Closedn B n.+1 ->
    Closedn (Cpi A B) n.
Proof.
elim: n A B => /= [|n IHn] A B /=;
    rewrite /Closedn /=;
    move => [HclosedA HclosedB] => σ;
    by rewrite CsubstE HclosedA HclosedB.
Qed.

Lemma closed_Cpi A B n:
    Closedn (Cpi A B) n <-> Closedn A n /\ Closedn B n.+1.
Proof.
split.
    by apply: closedn_Cpi1.
by apply: closedn_Cpi2.
Qed.

Lemma closedn_Clam1 t n:
    Closedn (Clam t) n -> Closedn t n.+1.
Proof.
elim: n t => [|n IHn] t;
    rewrite /Closedn /=;
    move => Hclosed σ;
    move : (Hclosed σ);
    rewrite CsubstE;
    by move => [].
Qed.

Lemma closedn_Clam2 t n:
    Closedn t n.+1 ->
    Closedn (Clam t) n.
Proof.
elim: n t => /= [|n IHn] t /=;
    rewrite /Closedn /=;
    move => Hclosedt σ;
    by rewrite CsubstE Hclosedt.
Qed.

Lemma closed_Clam t n:
    Closedn (Clam t) n <-> Closedn t n.+1.
Proof.
split.
    by apply: closedn_Clam1.
by apply: closedn_Clam2.
Qed.

Lemma closedn_Cappl1 m m' n:
    Closedn (Cappl m m') n -> Closedn m n /\ Closedn m' n.
Proof.
elim: n m m' => [|n IHn] m m';
    rewrite /Closedn /=;
    move => Hclosed;
    split => σ;
    move : (Hclosed σ);
    rewrite CsubstE;
    by move => [].
Qed.

Lemma closedn_Cappl2 m m' n:
    Closedn m n /\ Closedn m' n ->
    Closedn (Cappl m m') n.
Proof.
elim: n m m' => /= [|n IHn] m m' /=;
    rewrite /Closedn /=;
    move => [Hclosedm Hclosedm'] => σ;
    by rewrite CsubstE Hclosedm Hclosedm'.
Qed.

Lemma closed_Cappl m m' n:
    Closedn (Cappl m m') n <-> Closedn m n /\ Closedn m' n.
Proof.
split.
    by apply: closedn_Cappl1.
by apply: closedn_Cappl2.
Qed.

Lemma closedn_Csigma1 A B n:
    Closedn (Csigma A B) n -> Closedn A n /\ Closedn B n.+1.
Proof.
elim: n A B => [|n IHn] A B;
    rewrite /Closedn /=;
    move => Hclosed;
    split => σ;
    move : (Hclosed σ);
    rewrite CsubstE;
    by move => [].
Qed.

Lemma closedn_Csigma2 A B n:
    Closedn A n /\ Closedn B n.+1 ->
    Closedn (Csigma A B) n.
Proof.
elim: n A B => /= [|n IHn] A B /=;
    rewrite /Closedn /=;
    move => [HclosedA HclosedB] => σ;
    by rewrite CsubstE HclosedA HclosedB.
Qed.

Lemma closed_Csigma A B n:
    Closedn (Csigma A B) n <-> Closedn A n /\ Closedn B n.+1.
Proof.
split.
    by apply: closedn_Csigma1.
by apply: closedn_Csigma2.
Qed.

Lemma closedn_Cpair1 a b n:
    Closedn (Cpair a b) n ->
    Closedn a n /\ Closedn b n.
Proof.
elim: n a b => [|n IHn] a b;
    rewrite /Closedn /=;
    move => Hclosed;
    split => σ;
    move : (Hclosed σ);
    rewrite CsubstE;
    by move => [].
Qed.

Lemma closedn_Cpair2 a b n:
    Closedn a n /\ Closedn b n ->
    Closedn (Cpair a b) n.
Proof.
elim: n a b => /= [|n IHn] a b /=;
    rewrite /Closedn /=;
    move => [Hcloseda Hclosedb] => σ;
    by rewrite CsubstE Hcloseda Hclosedb.
Qed.

Lemma closed_Cpair a b n:
    Closedn (Cpair a b) n <->
    Closedn a n /\ Closedn b n.
Proof.
split.
    by apply: closedn_Cpair1.
by apply: closedn_Cpair2.
Qed.

Lemma closedn_Cproj11 m n:
    Closedn (Cproj1 m) n -> Closedn m n.
Proof.
elim: n m => [|n IHn] m;
    rewrite /Closedn /=;
    move => Hclosed σ;
    move : (Hclosed σ);
    rewrite CsubstE;
    by move => [].
Qed.

Lemma closedn_Cproj12 m n:
    Closedn m n -> Closedn (Cproj1 m) n.
Proof.
elim: n m => /= [|n IHn] m /=;
    rewrite /Closedn /=;
    move => Hclosedm σ;
    by rewrite CsubstE Hclosedm.
Qed.

Lemma closed_Cproj1 m n:
    Closedn (Cproj1 m) n <-> Closedn m n.
Proof.
split.
    by apply: closedn_Cproj11.
by apply: closedn_Cproj12.
Qed.

Lemma closedn_Cproj21 m n:
    Closedn (Cproj2 m) n -> Closedn m n.
Proof.
elim: n m => [|n IHn] m;
    rewrite /Closedn /=;
    move => Hclosed σ;
    move : (Hclosed σ);
    rewrite CsubstE;
    by move => [].
Qed.

Lemma closedn_Cproj22 m n:
    Closedn m n -> Closedn (Cproj2 m) n.
Proof.
elim: n m => /= [|n IHn] m /=;
    rewrite /Closedn /=;
    move => Hclosedm σ;
    by rewrite CsubstE Hclosedm.
Qed.

Lemma closed_Cproj2 m n:
    Closedn (Cproj2 m) n <-> Closedn m n.
Proof.
split.
    by apply: closedn_Cproj21.
by apply: closedn_Cproj22.
Qed.

Definition CClosednE := (
    closedn_Cvar,    closedn_Cuniv,   closed_Cpi,      closed_Clam,
    closed_Cappl,    closed_Csigma,   closed_Cpair,    closed_Cproj1,
    closed_Cproj2).