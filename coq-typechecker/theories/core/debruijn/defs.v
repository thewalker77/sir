(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(* Based on work made by Tobias Tebbi, Steven Schäfer, et al. for             *)
(* https://github.com/coq-community/autosubst commit tag:495d547fa2a3e40da0f7 *)
(* Base work was licensed Under MIT                                           *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From HB Require Import structures.
From mathcomp Require Import all_ssreflect.
From sir.core Require Import ast.term.

From sir.common.debruijn Require Export structs.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Module Defs1.

Definition ids : nat -> CTerm := Cvar.

Fixpoint rename(ξ : nat -> nat)  (t: CTerm)  : CTerm :=
match t with
| Cvar idx => Cvar (ξ idx)
| Cuniv l => Cuniv l
| Cpi A B => Cpi (rename ξ A) (rename (upren ξ) B)
| Clam y => Clam (rename (upren ξ) y)
| Cappl m n => Cappl (rename ξ m) (rename ξ n)
| Csigma A B => Csigma (rename ξ A) (rename (upren ξ) B)
| Cpair m n => Cpair (rename ξ m) (rename ξ n)
| Cproj1 m => Cproj1 (rename ξ m)
| Cproj2 m => Cproj2 (rename ξ m)
end.
End Defs1.

HB.instance Definition _ := HasIDs.Build _ Defs1.ids.
HB.instance Definition _ := HasRename.Build _ Defs1.rename.

Module Defs2.

Fixpoint subst (σ: nat -> CTerm) (t: CTerm) : CTerm :=
match t with
| Cvar idx => σ idx
| Cuniv l => Cuniv l
| Cpi A B => Cpi (subst σ A) (subst (up σ) B)
| Clam y => Clam (subst (up σ) y)
| Cappl m n => Cappl (subst σ m) (subst σ n)
| Csigma A B => Csigma (subst σ A) (subst (up σ) B)
| Cpair m n => Cpair (subst σ m) (subst σ n)
| Cproj1 m => Cproj1 (subst σ m)
| Cproj2 m => Cproj2 (subst σ m)
end.

End Defs2.

HB.instance Definition _ := HasSubst.Build _ Defs2.subst.




