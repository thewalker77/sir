(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.core.debruijn Require Import defs.
From sir.core Require Import ast.term.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.


Module Rename_Elim.
Lemma rename_var ξ idx: rename ξ (Cvar idx) = Cvar (ξ idx).
Proof.
by [].
Qed.

Lemma rename_univ ξ l: rename ξ (Cuniv l) = Cuniv l.
Proof.
by [].
Qed.

Lemma rename_pi ξ A B:
    rename ξ (Cpi A B) = Cpi (rename ξ A) (rename (upren ξ) B).
Proof.
by [].
Qed.

Lemma rename_lam ξ y:
    rename ξ (Clam y) = Clam (rename (upren ξ) y).
Proof.
by [].
Qed.

Lemma rename_appl ξ m n:
    rename ξ (Cappl m n) = Cappl (rename ξ m) (rename ξ n).
Proof.
by [].
Qed.

Lemma rename_sigma ξ A B:
    rename ξ (Csigma A B) = Csigma (rename ξ A) (rename (upren ξ) B).
Proof.
by [].
Qed.

Lemma rename_pair ξ a b:
    rename ξ (Cpair a b) =
    Cpair (rename ξ a) (rename ξ b).
Proof.
by [].
Qed.

Lemma rename_proj1 ξ m: rename ξ (Cproj1 m) = Cproj1 (rename ξ m).
Proof.
by [].
Qed.

Lemma rename_proj2 ξ m: rename ξ (Cproj2 m) = Cproj2 (rename ξ m).
Proof.
by [].
Qed.
End Rename_Elim.

Definition CrenameE := (
    Rename_Elim.rename_var, Rename_Elim.rename_univ, Rename_Elim.rename_pi,
    Rename_Elim.rename_lam, Rename_Elim.rename_appl, Rename_Elim.rename_sigma,
    Rename_Elim.rename_pair, Rename_Elim.rename_proj1, Rename_Elim.rename_proj2).

Module Subst_Elim.

Lemma subst_var σ idx: (Cvar idx).[σ] = σ idx.
Proof.
by [].
Qed.

Lemma subst_univ σ l: (Cuniv l).[σ] = Cuniv l.
Proof.
by [].
Qed.

Lemma subst_pi σ A B: (Cpi A B).[σ] = Cpi A.[σ] B.[up σ].
Proof.
by [].
Qed.

Lemma subst_lam σ y: (Clam y).[σ] = Clam y.[up σ].
Proof.
by [].
Qed.

Lemma subst_appl σ m n: (Cappl m n).[σ] = Cappl m.[σ] n.[σ].
Proof.
by [].
Qed.

Lemma subst_sigma σ A B: (Csigma A B).[σ] = Csigma A.[σ] B.[up σ].
Proof.
by [].
Qed.

Lemma subst_pair σ a b:
    (Cpair a b).[σ] = Cpair a.[σ] b.[σ].
Proof.
by [].
Qed.

Lemma subst_proj1 σ m: (Cproj1 m).[σ] = Cproj1 m.[σ].
Proof.
by [].
Qed.

Lemma subst_proj2 σ m: (Cproj2 m).[σ] = Cproj2 m.[σ].
Proof.
by [].
Qed.

End Subst_Elim.


Definition CsubstE := (
    Subst_Elim.subst_var, Subst_Elim.subst_univ, Subst_Elim.subst_pi,
    Subst_Elim.subst_lam, Subst_Elim.subst_appl, Subst_Elim.subst_sigma,
    Subst_Elim.subst_pair, Subst_Elim.subst_proj1, Subst_Elim.subst_proj2).
