(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(* Based on work made by Tobias Tebbi, Steven Schäfer, et al. for             *)
(* https://github.com/coq-community/autosubst commit tag:495d547fa2a3e40da0f7 *)
(* Base work was licensed Under MIT                                           *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
Require Import Coq.Logic.FunctionalExtensionality.
From HB Require Import structures.
From mathcomp Require Import all_ssreflect.
From sir.core.debruijn Require Import defs.
From sir.core Require Import ast.term.
From sir.common Require Export utils padd.
From sir.core.debruijn Require Export elims.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Module Facts.
Lemma ren_up ξ: @ren CTerm (upren ξ) = up (ren ξ).
Proof.
apply: functional_extensionality.
by case.
Qed.

Lemma up_ids: @up CTerm ids = ids.
Proof.
apply: functional_extensionality.
by case.
Qed.

Lemma up_comp_up_upren ξ (σ: nat -> CTerm):
    up (σ ∘ ξ) = up σ ∘ upren ξ.
Proof.
apply: functional_extensionality.
by case.
Qed.

Lemma rename_subst ξ (t: CTerm):
    rename ξ t = t.[ren ξ].
Proof.
move: t ξ.
CTermElim => ξ;
    rewrite CrenameE //=.
-   (* pi *)
    by rewrite IHA IHB ren_up.
-   (* lam *)
    by rewrite IHy ren_up.
-   (* appl *)
    by rewrite IHm IHn.
-   by rewrite IHA IHB ren_up.
-   by rewrite IHm IHn.
-   by rewrite IHm.
-   by rewrite IHm.
Qed.

Lemma subst_id (t: CTerm): t.[ids] = t.
Proof.
move: t.
CTermElim;
    rewrite CsubstE => //=.
-   (* pi *)
    by rewrite up_ids IHA IHB.
-   (* lam *)
    by rewrite up_ids IHy.
-   (* appl *)
    by rewrite IHm IHn.
-   (* sigma *)
    by rewrite up_ids IHA IHB.
    (* pair *)
-   by rewrite IHm IHn.
-   by rewrite IHm.
-   by rewrite IHm.
Qed.

Lemma id_subst (σ: nat -> CTerm) idx:
    (ids idx).[σ] = σ idx.
Proof.
by elim : idx => [| idx]/=.
Qed.

Lemma rename_subst_comp σ ξ (t: CTerm):
    (rename ξ t).[σ] = t.[σ ∘ ξ].
Proof.
move: t ξ σ.
CTermElim => ξ σ;
    rewrite CrenameE CsubstE => //=.
-   (* pi *)
    by rewrite IHA IHB -up_comp_up_upren.
-   (* lam *)
    by rewrite IHy -up_comp_up_upren.
-   (* appl *)
    by rewrite IHm IHn.
-   by rewrite IHA IHB -up_comp_up_upren.
-   by rewrite IHm IHn.
-   by rewrite IHm.
-   by rewrite IHm.
Qed.

Lemma rename_upren_comp_up (ξ: nat -> nat) (σ: nat -> CTerm):
    rename (upren ξ) ∘ up σ = up (rename ξ ∘ σ).
Proof.
apply: functional_extensionality.
move => [|idx].
    by [].
rewrite /up /comp /=.
rewrite [rename succn (rename _ _)]rename_subst rename_subst_comp.
by rewrite [rename (upren _) _]rename_subst rename_subst_comp.
Qed.

Lemma subst_rename_comp σ ξ (t: CTerm):
    rename ξ t.[σ] = t.[rename ξ ∘ σ].
Proof.
move: t σ ξ.
CTermElim => σ ξ //=;
    rewrite CsubstE CrenameE/=.
-   (* pi *)
    by rewrite IHA IHB rename_upren_comp_up.
-   (* lam *)
    by rewrite IHy rename_upren_comp_up.
-   (* appl *)
    by rewrite IHm IHn.
-   by rewrite IHA IHB rename_upren_comp_up.
-   by rewrite IHm IHn.
-   by rewrite IHm.
-   by rewrite IHm.
Qed.

Lemma up_scomp (σ τ: nat-> CTerm):
    up (σ ⊚ τ) = up σ ⊚ up τ.
Proof.
apply: functional_extensionality.
move => [//|idx].
rewrite /up /scomp /comp /=.
by rewrite rename_subst_comp subst_rename_comp.
Qed.

Lemma subst_scomp σ τ (t: CTerm): t.[σ].[τ] = t.[τ ⊚ σ].
Proof.
move: t σ τ.
CTermElim => σ τ;
    rewrite !CsubstE => //=.
-   (* pi *)
    by rewrite IHA IHB -up_scomp.
-   (* lam *)
    by rewrite IHy -up_scomp.
-   (* appl *)
    by rewrite IHm IHn.
-   by rewrite IHA IHB -up_scomp.
-   by rewrite IHm IHn.
-   by rewrite IHm.
-   by rewrite IHm.
Qed.

End Facts.

HB.instance Definition _ := HasDebruijnLemmas.Build _
    Facts.rename_subst Facts.subst_id Facts.id_subst Facts.subst_scomp.

From sir.common.debruijn Require Export tactics.
