(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.core Require Import ast.term.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Inductive NormalForm : CTerm -> Prop :=
| NormalForm_univ: forall l, NormalForm (Cuniv l)
| NormalForm_pi A B:
    NormalForm A ->
    NormalForm B ->
    NormalForm (Cpi A B)
| NormalForm_lam t:
    NormalForm t ->
    NormalForm (Clam t)
| NormalForm_neutral t:
    NeutralForm t ->
    NormalForm t
| NormalForm_sigma A B:
    NormalForm A ->
    NormalForm B ->
    NormalForm (Csigma A B)
| NormalForm_pair m n:
    NormalForm m ->
    NormalForm n ->
    NormalForm (Cpair m n)

with NeutralForm: CTerm -> Prop :=
| NeutralForm_var: forall idx, NeutralForm (Cvar idx)
| NeutralForm_appl: forall f x,
    NeutralForm f ->
    NormalForm x ->
    NeutralForm (Cappl f x)
| NeutralForm_proj1 p:
    NeutralForm p ->
    NeutralForm (Cproj1 p)
| NeutralForm_proj2 p:
    NeutralForm p ->
    NeutralForm (Cproj2 p).

Inductive Value : CTerm -> Prop :=
| Value_univ: forall l, Value (Cuniv l)
| Value_pi A B:
    Value A ->
    Value B ->
    Value (Cpi A B)
| Value_lam t:
    Value t ->
    Value (Clam t)
| Value_sigma A B:
    Value A ->
    Value B ->
    Value (Csigma A B)
| Value_pair m n:
    Value m ->
    Value n ->
    Value (Cpair m n)
.