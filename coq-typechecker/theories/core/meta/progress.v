(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.core Require Import ast.term debruijn.defs.
From sir.core.meta Require Import typing reduction value conversion.
From sir.core.meta Require Import preservation confluence red_facts.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Lemma univ_not_pi Γ l A B:
    Γ ⊢ Cuniv l : Cpi A B -> False.
Proof.
move Heq : (Cuniv l) => t.
move /CSynType_type_move => [T []] Htype.
move: Htype l A B Heq.
CSynTypeElim Γ t T => // l A B Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_pi => [A' [B' [-> ?]]].
    by move /mred_inv_univ.
-   apply: IHT => //.
    apply: (CConv_trans _ T').
        by apply: Hequiv.
    by apply: CConv_symm.
Qed.

Lemma pi_not_pi Γ A' B' A B:
    Γ ⊢ Cpi A' B' : Cpi A B -> False.
Proof.
move Heq : (Cpi A' B') => t.
move /CSynType_type_move => [T []] Htype.
move: Htype A' B' A B Heq.
CSynTypeElim Γ t T => // A1 B1 A2 B2 Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_pi => [A' [B' [-> ?]]].
    by move /mred_inv_univ.
-   apply: IHT => //.
    apply: (CConv_trans _ T').
        by apply: Hequiv.
    by apply: CConv_symm.
Qed.

Lemma sigma_not_pi Γ A' B' A B:
    Γ ⊢ Csigma A' B' : Cpi A B -> False.
Proof.
move Heq : (Csigma A' B') => t.
move /CSynType_type_move => [T []] Htype.
move: Htype A' B' A B Heq.
CSynTypeElim Γ t T => // A1 B1 A2 B2 Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_pi => [A' [B' [-> ?]]].
    by move /mred_inv_univ.
-   apply: IHT => //.
    apply: (CConv_trans _ T').
        by apply: Hequiv.
    by apply: CConv_symm.
Qed.

Lemma pair_not_pi Γ m n A B:
    Γ ⊢ Cpair m n : Cpi A B -> False.
Proof.
move Heq : (Cpair m n) => t.
move /CSynType_type_move => [T []] Htype.
move: Htype m n A B Heq.
CSynTypeElim Γ t T => // m1 n1 A2 B2 Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_pi => [A' [B' [-> [? ?]]]].
    by move /mred_inv_sigma => [? [? []]].
-   apply: IHT => //.
    apply: (CConv_trans _ T').
        by apply: Hequiv.
    by apply: CConv_symm.
Qed.

Lemma univ_not_sigma Γ l A B:
    Γ ⊢ Cuniv l : Csigma A B -> False.
Proof.
move Heq : (Cuniv l) => t.
move /CSynType_type_move => [T []] Htype.
move: Htype l A B Heq.
CSynTypeElim Γ t T => // l A B Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_sigma => [A' [B' [-> ?]]].
    by move /mred_inv_univ.
-   apply: IHT => //.
    apply: (CConv_trans _ T').
        by apply: Hequiv.
    by apply: CConv_symm.
Qed.

Lemma pi_not_sigma Γ A' B' A B:
    Γ ⊢ Cpi A' B' : Csigma A B -> False.
Proof.
move Heq : (Cpi A' B') => t.
move /CSynType_type_move => [T []] Htype.
move: Htype A' B' A B Heq.
CSynTypeElim Γ t T => // A1 B1 A2 B2 Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_sigma => [A' [B' [-> ?]]].
    by move /mred_inv_univ.
-   apply: IHT => //.
    apply: (CConv_trans _ T').
        by apply: Hequiv.
    by apply: CConv_symm.
Qed.

Lemma sigma_not_sigma Γ A' B' A B:
    Γ ⊢ Csigma A' B' : Csigma A B -> False.
Proof.
move Heq : (Csigma A' B') => t.
move /CSynType_type_move => [T []] Htype.
move: Htype A' B' A B Heq.
CSynTypeElim Γ t T => // A1 B1 A2 B2 Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_sigma => [A' [B' [-> ?]]].
    by move /mred_inv_univ.
-   apply: IHT => //.
    apply: (CConv_trans _ T').
        by apply: Hequiv.
    by apply: CConv_symm.
Qed.

Lemma lam_not_sigma Γ n A B:
    Γ ⊢ Clam n : Csigma A B -> False.
Proof.
move Heq : (Clam n) => t.
move /CSynType_type_move => [T []] Htype.
move: Htype n A B Heq.
CSynTypeElim Γ t T => // n A2 B2 Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_sigma => [A' [B' [-> [? ?]]]].
    by move /mred_inv_pi => [? [? []]].
-   apply: IHT => //.
    apply: (CConv_trans _ T').
        by apply: Hequiv.
    by apply: CConv_symm.
Qed.

Lemma type_progress Γ t T:
    Γ ⊢ t : T ->
    (exists t', t ⟶ t') \/ NormalForm t.
Proof.
CSynTypeElim Γ t T.
-   right.
    apply: NormalForm_neutral.
    by apply: NeutralForm_var.
-   right.
    by apply: NormalForm_univ.
-   move : IHA => [[A' HredAA']|HnormA].
        left.
        exists (Cpi A' B).
        by apply: CReduction_pil.
    move : IHB => [[B' HredBB']|HnormB].
        left.
        exists (Cpi A B').
        by apply: CReduction_pir.
    right.
    by apply: NormalForm_pi.
-   move : IHt => [[t' Hredtt']|Hnormt].
        left.
        exists (Clam t').
        by apply: CReduction_lam.
    right.
    by apply: NormalForm_lam.
-   move : IHm => [[m' Hredmm']|Hnormm].
        left.
        exists (Cappl m' n).
        by apply: CReduction_appll.
    move : IHn => [[n' Hrednn']|Hnornn].
        left.
        exists (Cappl m n').
        by apply: CReduction_applr.
    inversion Hnormm ;
        subst.
    +   by have := univ_not_pi Htypem.
    +   by have :=  pi_not_pi Htypem.
    +   left.
        exists (t.[n/]).
        by apply: CReduction_beta.
    +   right.
        apply: NormalForm_neutral.
        by apply: NeutralForm_appl.
    +   by have := sigma_not_pi Htypem.
    +   by have := pair_not_pi Htypem.
-   move: IHA => [[A' HredAA']|HnormA].
        left.
        exists (Csigma A' B).
        by apply: CReduction_sigmal.
    move : IHB => [[B' HredBB']|HnormB].
        left.
        exists (Csigma A B').
        by apply: CReduction_sigmar.
    right.
    by apply: NormalForm_sigma.
-   move : IHm => [[m' Hredmm']|Hnormm].
        left.
        exists (Cpair m' n).
        by apply: CReduction_pairl.
    move : IHn => [[n' Hrednn']|Hnornn].
        left.
        exists (Cpair m n').
        by apply: CReduction_pairr.
    right.
    by apply: NormalForm_pair.
-   move: IHm => [[m' Hmredmm']|Hnormm].
        left.
        exists (Cproj1 m').
        by apply: CReduction_proj1.
    inversion Hnormm ;
        subst.
    +   by have:= univ_not_sigma Htypem.
    +   by have := pi_not_sigma Htypem.
    +   by have := lam_not_sigma Htypem.
    +   right.
        apply: NormalForm_neutral.
        by apply: NeutralForm_proj1.
    +   by have := sigma_not_sigma Htypem.
    +   left.
        exists m0.
        by apply: CReduction_proj1_beta.
-   move: IHm => [[m' Hmredmm']|Hnormm].
        left.
        exists (Cproj2 m').
        by apply: CReduction_proj2.
    inversion Hnormm ;
        subst.
    +   by have:= univ_not_sigma Htypem.
    +   by have := pi_not_sigma Htypem.
    +   by have := lam_not_sigma Htypem.
    +   right.
        apply: NormalForm_neutral.
        by apply: NeutralForm_proj2.
    +   by have := sigma_not_sigma Htypem.
    +   left.
        exists n.
        by apply: CReduction_proj2_beta.
-   by [].
Qed.
