(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.core Require Import ast.term debruijn.defs.
From sir.core.debruijn Require Import defs facts.
From sir.common Require Import utils.
(* Meta theoritic representation of Definitional equality (Also known as      *)
(* conversion) rule, this is representation that does not particularly care   *)
(* about types of the terms being converted                                   *)
Reserved Notation "x '≅' y" (at level 20, y at next level).
Inductive CConv: CTerm -> CTerm -> Prop :=
| CConv_beta: forall t t' u , t' = t.[u/] -> Cappl (Clam t) u ≅ t'
| CConv_refl: forall t, t ≅ t
| CConv_symm: forall t t',
                t ≅ t' ->
                t' ≅ t
| CConv_trans: forall t1 t2 t3,
                t1 ≅ t2 ->
                t2 ≅ t3 ->
                t1 ≅ t3
| CConv_pi: forall A A' B B',
                A ≅ A' ->
                B ≅ B' ->
                Cpi A B ≅ Cpi A' B'
| CConv_lam: forall t t',
                t ≅ t' ->
                Clam t ≅ Clam t'
| CConv_appl: forall m m' n n',
                m ≅ m' ->
                n ≅ n' ->
                Cappl m n ≅ Cappl m' n'
| CConv_sigma: forall A A' B B',
                A ≅ A' ->
                B ≅ B' ->
                Csigma A B ≅ Csigma A' B'
| CConv_pair: forall m m' n n',
                m ≅ m' ->
                n ≅ n' ->
                Cpair m n ≅ Cpair m' n'
| CConv_proj1: forall m m',
                m ≅ m' ->
                Cproj1 m ≅ Cproj1 m'
| CConv_proj2: forall m m',
                m ≅ m' ->
                Cproj2 m ≅ Cproj2 m'
| CConv_proj1_beta: forall m n,
                Cproj1 (Cpair m n) ≅ m
| CConv_proj2_beta: forall m n,
                Cproj2 (Cpair m n) ≅ n
where "t1 '≅' t2" := (CConv t1 t2).

Tactic Notation "CConvElim" ident(T) ident(T'):=
elim => {T T'} /=
    [ t t' u Heqtt'
    | t
    | t t' IHtt' Hconvtt'
    | t1 t2 t3  Hconv12 IH12 Hconv23 IH23
    | A A' B B' HconvAA' IHA HconvBB' IHB
    | t t' Hconvtt' IHt
    | m m' n n' Hconvmm' IHm Hconvnn' IHn
    | A A' B B' HconvAA' IHA HconvBB' IHB
    | m m' n n' Hconvmm' IHm Hconvnn' IHn
    | m m' Hconvmm' IHm
    | m m' Hconvmm' IHm
    | m n
    | m n
    ].

Lemma conv_subst t t' σ: t ≅ t' -> t.[σ] ≅ t'.[σ].
Proof.
move => Hconv.
move: Hconv σ.
CConvElim t t' => σ.
-   (* Conv_beta *)
    rewrite !CsubstE.
    apply: CConv_beta.
    rewrite Heqtt'.
    by asimpl.
-   by apply: CConv_refl.
-   by apply: CConv_symm.
-   by apply: CConv_trans.
-   by apply: CConv_pi.
-   by apply: CConv_lam.
-   by apply: CConv_appl.
-   by apply: CConv_sigma.
-   by apply: CConv_pair.
-   by apply: CConv_proj1.
-   by apply: CConv_proj2.
-   by apply: CConv_proj1_beta.
-   by apply: CConv_proj2_beta.
Qed.