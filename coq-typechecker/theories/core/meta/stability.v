(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.core.meta Require Import typing conversion.
From sir.core Require Import ast.term context.
From sir.core.debruijn Require Import defs facts.
From sir.common Require Import utils.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Definition CtxRename ξ Γ1 Γ2 : Prop:= forall idx T1,
    dget Γ1 idx = Some T1 ->
    dget Γ2 (ξ idx) = Some T1.[ren ξ].

(* This is a property that appears when proving stability under renaming for  *)
(* both pi types and sigma types                                              *)
Lemma ctx_rename_cons A Γ1 Γ2 ξ:
    CtxRename ξ Γ1 Γ2 ->
    CtxRename (upren ξ) (A::Γ1) (A.[ren ξ]:: Γ2).
Proof.
move => Hrename [|idx] T1 /=.
    move => [<-].
    by asimpl.
case Heq: (dget (Γ1) idx) => [t|//] [<-].
rewrite (Hrename _ _ Heq).
by asimpl.
Qed.

Lemma CSynType_eproj2 Γ m A B B':
    Γ ⊢ m : Csigma A B ->
    B' = B.[Cproj1 m /] ->
    Γ ⊢ Cproj2 m : B'.
Proof.
move => Htype ->.
apply: CSynType_proj2.
by apply: Htype.
Qed.

Lemma type_rename_stability ξ Γ1 Γ2 t T:
    Γ1 ⊢ t : T ->
    ⊢ Γ2 ->
    CtxRename ξ Γ1 Γ2 ->
    Γ2 ⊢ t.[ren ξ] : T.[ren ξ].
Proof.
move => HType.
move: HType ξ Γ2.
CSynTypeElim Γ1 t T => ξ Γ2 HWFΓ2 Hrename;
    rewrite ?CsubstE.
-   asimpl.
    apply: CSynType_var => //.
    by apply: Hrename.
-   by apply: CSynType_univ.
-   apply: CSynType_pi.
        by apply: IHA.
    asimpl.
    apply: IHB.
       apply: CLocalWF_cons => //.
       by apply: IHA.
    by apply: ctx_rename_cons.
-   apply: CSynType_lam.
        by apply: IHA.
    asimpl.
    apply: IHt.
        apply: CLocalWF_cons=> //.
        by apply: IHA.
    by apply: ctx_rename_cons.
-   apply: CSynType_eappl.
    +   by apply: IHm.
    +   by apply: IHn.
    +   by asimpl.
-   apply: CSynType_sigma.
        by apply: IHA.
    asimpl.
    apply: IHB.
        apply: CLocalWF_cons => //.
        by apply: IHA.
    by apply: ctx_rename_cons.
-   apply: CSynType_pair.
    +   by apply: IHm.
    +   asimpl.
        have : B.[m.[ren ξ] .: ren ξ] = B.[m/].[ren ξ].
            by asimpl.
        move => ->.
        by apply: IHn.
-   apply: CSynType_proj1.
    by apply: IHm.
-   asimpl.
    apply: CSynType_eproj2.
        by apply: IHm.
    by asimpl.
-   apply: CSynType_conv.
        by apply: IHT.
    by apply: conv_subst.
Qed.

Lemma type_weakening_stability Γ t T A l:
    Γ ⊢ t : T ->
    ⊢ Γ ->
    Γ ⊢ A : Cuniv l ->
    (A::Γ) ⊢ t.[ren (padd 1)] : T.[ren (padd 1)].
Proof.
move => Htypet HWF HtypeT'.
apply: (type_rename_stability Htypet) => //.
apply: (CLocalWF_cons (l:= l)) => //.
move => idx T1 Heq.
asimpl.
by rewrite Heq.
Qed.

Lemma type_eweakening_stability Γ t A T T' l:
    Γ ⊢ t : T ->
    ⊢ Γ ->
    Γ ⊢ A : Cuniv l ->
    T' =  T.[ren (padd 1)] ->
    (A::Γ) ⊢ t.[ren (padd 1)] : T'.
Proof.
move => ? ? ? ->.
by apply: (@type_weakening_stability _ _ _ _ l).
Qed.

Definition CtxSubtitution Γ1 Γ2 σ := forall x T,
    dget Γ1 x = Some T -> Γ2 ⊢ σ x : T.[σ].

Lemma ctx_subst_append Γ1 Γ2 σ A l:
    Γ2 ⊢ A.[σ] : Cuniv l ->
    ⊢ Γ2 ->
    CtxSubtitution Γ1 Γ2 σ ->
    CtxSubtitution (A :: Γ1) (A.[σ] :: Γ2) (up σ).
Proof.
move => HtypA HWF.
move => Hsubst [|x] T /=.
    move => [<-].
    apply: CSynType_var.
    by asimpl.
case Heq : (dget Γ1 x) => [t|//].
move => [<-].
asimpl.
apply: type_eweakening_stability.
+   by apply: (Hsubst _ t).
+   by [].
+   by apply: HtypA.
+   by asimpl.
Qed.

(* Note in the third hypothesis, if we have non empty Γ1 *)
(* we can use it to establish that ⊢ Γ2, but this is not *)
(* possible in case of empty Γ1                          *)
Lemma type_substitution_stability Γ1 Γ2 σ t T:
    Γ1 ⊢ t: T ->
    ⊢ Γ2 ->
    CtxSubtitution Γ1 Γ2 σ ->
    Γ2 ⊢ t.[σ]: T.[σ].
Proof.
move => Htype.
move : Htype Γ2 σ.
CSynTypeElim Γ1 t T => Γ2 σ HWF Hmap;
    rewrite ?CsubstE.
-   by apply: Hmap.
-   by apply: CSynType_univ.
-   apply: CSynType_pi.
        by apply IHA.
    apply: IHB.
        apply: CLocalWF_cons => //.
        by apply: IHA.
    apply: ctx_subst_append => //.
    by apply: IHA.
-   apply: CSynType_lam.
        by apply: IHA.
    apply: IHt.
        apply: CLocalWF_cons => //.
        by apply: IHA.
    apply: ctx_subst_append => //.
    by apply: IHA.
-   have:= (IHm _ σ HWF).
    rewrite ?CsubstE => IHM.
    apply: CSynType_eappl.
    +   by apply IHM.
    +   by apply: IHn.
    +   by asimpl.
-   apply: CSynType_sigma.
        by apply: IHA.
    apply: IHB.
        apply: CLocalWF_cons => //.
        by apply: IHA.
    apply: ctx_subst_append => //.
    by apply: IHA.
-   apply: CSynType_pair.
        by apply: IHm.
    have : B.[up σ].[m.[σ]/] = B.[m/].[σ].
        by asimpl.
    move ->.
    by apply IHn.
-   apply: CSynType_proj1.
    have := IHm _ σ.
    rewrite ?CsubstE => IHM.
    by apply: IHM.
-   have := IHm _ σ.
    rewrite ?CsubstE => IHM.
    apply: CSynType_eproj2.
        by apply: IHm.
    by asimpl.
-   apply: CSynType_conv.
        by apply: IHT.
    by apply: conv_subst.
Qed.

Lemma type_conversion_stability A A' Γ t T:
    A' ≅ A ->
    ⊢ (A'::Γ) ->
    (A::Γ) ⊢ t: T ->
    (A'::Γ) ⊢ t :T.
Proof.
move => Hconv HWF Htypet.
suff : ((A' :: Γ) ⊢ t.[ids] : T.[ids]).
    by asimpl.
apply: (type_substitution_stability Htypet) => //.
move => [|x] T0//=.
    move => [<-].
    apply: CSynType_conv; last first.
        asimpl.
        apply: conv_subst.
        by apply: Hconv.
    by apply: CSynType_var.
case Heq: (dget (Γ) x) => [t'|//] [<-].
apply: CSynType_var => //.
asimpl.
by rewrite Heq.
Qed.

