(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.core.meta Require Import reduction conversion.
From sir.core Require Import ast.term.
From sir.core.debruijn Require Import defs.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(* inversion principle for terms/types under CReduction*)
Lemma red_inv_var x t': Cvar x ⟶ t' -> t' = Cvar x.
move Heq: (Cvar x) => t Hred.
by case: Hred Heq.
Qed.

Lemma red_inv_univ l t': Cuniv l ⟶ t' -> t' = Cuniv l.
Proof.
move Heq: (Cuniv l) => t Hred.
by case: Hred Heq.
Qed.

Lemma red_inv_pi A B t':
    Cpi A B ⟶ t' ->
    (exists A', t' = Cpi A' B /\ (A ⟶ A')) \/
    (exists B', t' = Cpi A B' /\ (B ⟶ B')).
Proof.
move => Hred.
inversion Hred;
    subst.
-   left.
    by exists A'.
-   right.
    by exists B'.
Qed.

Lemma red_inv_lam t f:
    Clam t ⟶ f ->
    (exists t', f = Clam t' /\ (t ⟶ t')).
Proof.
move => Hred.
inversion Hred.
subst.
by exists t'.
Qed.

Lemma red_inv_appl m n v:
    Cappl m n ⟶ v ->
    (exists m', v = Cappl m' n /\ m ⟶ m') \/
    (exists n', v = Cappl m n' /\ n ⟶ n') \/
    (exists t, m = (Clam t) /\ v = t.[n/] /\ Cappl m n ⟶ t.[n/]).
Proof.
move => Hred.
inversion Hred;
    subst.
-   repeat right.
    by exists t.
-   left.
    by exists m'.
-   right.
    left.
    by exists n'.
Qed.

Lemma red_inv_sigma A B t':
    Csigma A B ⟶ t' ->
    (exists A', t' = Csigma A' B /\ (A ⟶ A')) \/
    (exists B', t' = Csigma A B' /\ (B ⟶ B')).
Proof.
move => Hred.
inversion Hred;
    subst.
-   left.
    by exists A'.
-   right.
    by exists B'.
Qed.

Lemma red_inv_pair m n t':
    Cpair m n ⟶ t' ->
    (exists m', t' = Cpair m' n /\ m ⟶ m') \/
    (exists n', t' = Cpair m n' /\ n ⟶ n').
Proof.
move => Hred.
inversion Hred;
    subst.
-   left.
    by exists m'.
-   right.
    by exists n'.
Qed.

Lemma red_inv_proj1 m t:
    Cproj1 m ⟶ t ->
    (exists m', t = Cproj1 m' /\ m ⟶ m') \/
    (exists m1 m2, m = (Cpair m1 m2) /\ t = m1 /\ Cproj1 m ⟶ m1).
Proof.
move => Hred.
inversion Hred;
    subst.
-   left.
    by exists m'.
-   right.
    by exists t, n.
Qed.

Lemma red_inv_proj2 m t:
    Cproj2 m ⟶ t ->
    (exists m', t = Cproj2 m' /\ m ⟶ m') \/
    (exists m1 m2, m = (Cpair m1 m2) /\ t = m2 /\ Cproj2 m ⟶ m2).
Proof.
move => Hred.
inversion Hred;
    subst.
-   left.
    by exists m'.
-   right.
    by exists m0, t.
Qed.

(* properties of CMulti *)
Lemma multi_trans R T1 T2 T3:
    CMulti R T1 T2 ->
    CMulti R T2 T3 ->
    CMulti R T1 T3.
Proof.
elim => {T1 T2} => [T1 T2|T1 T2 T4].
    by [].
move =>  Hred12 Hmred24 Hmred4323 Hmred43.
apply: (CMulti_single (t2 := T2)) => //.
by apply: Hmred4323.
Qed.

(* properties of CMulti CReduction*)

Lemma mred_trans_lam t t':
    t ⟶* t' ->
    Clam t ⟶* Clam t'.
Proof.
elim => {t t'} => [t|t1 t2 t3].
    by apply: CMulti_refl.
move => Hred12 Hmred23 IH.
apply: (CMulti_single (t2 := Clam t2)) => //.
by apply: CReduction_lam.
Qed.

Lemma mred_trans_appll m m' n:
    m ⟶* m' ->
    Cappl m n ⟶* Cappl m' n.
Proof.
elim => {m m'} => [m|m1 m2 m3].
    by apply: CMulti_refl.
move => Hred12 Hmred23 IH.
apply: (CMulti_single (t2 := Cappl m2 n)) => //.
by apply: CReduction_appll.
Qed.

Lemma mred_trans_applr m n n':
    n ⟶* n' ->
    Cappl m n ⟶* Cappl m n'.
Proof.
elim => {n n'} => [n|n1 n2 n3].
    by apply: CMulti_refl.
move => Hred12 Hmred23 IH.
apply: (CMulti_single (t2 := Cappl m n2)) => //.
by apply: CReduction_applr.
Qed.

Lemma mred_trans_appl m m' n n':
    m ⟶* m' ->
    n ⟶* n' ->
    Cappl m n ⟶* Cappl m' n'.
Proof.
elim => {m m'} [m|m1 m2 m3].
    by apply: mred_trans_applr.
move => Hred12 Hmred23 IH Hmredn.
apply: CMulti_single;
    last by apply: IH.
by apply: CReduction_appll.
Qed.

Lemma mred_trans_appl_beta t n t' n':
    t ⟶* t' ->
    n ⟶* n' ->
    Cappl (Clam t) n ⟶* t'.[n'/].
Proof.
have ? := CMulti_refl.
move => Hmredtt' Hmrednn'.
apply: (@multi_trans _ _ (Cappl (Clam t') n')).
    apply: mred_trans_appl => //.
    by apply: mred_trans_lam.
apply: (CMulti_single (t2:= t'.[n'/])) => //.
by apply: CReduction_beta.
Qed.

Lemma mred_trans_pil A A' B:
    A ⟶* A' ->
    Cpi A B ⟶* Cpi A' B.
Proof.
elim => {A A'} => [A|A1 A2 A3].
    by apply: CMulti_refl.
move => Hred12 Hmred23 IH.
apply: (CMulti_single (t2:= Cpi A2 B)) => //.
by apply: CReduction_pil.
Qed.

Lemma mred_trans_pir A B B':
    B ⟶* B' ->
    Cpi A B ⟶* Cpi A B'.
Proof.
elim => {B B'} => [B|B1 B2 B3].
    by apply: CMulti_refl.
move => Hred12 Hmred23 IH.
apply: (CMulti_single (t2 := Cpi A B2)) => //.
by apply: CReduction_pir.
Qed.

Lemma mred_trans_pi A A' B B':
    A ⟶* A' ->
    B ⟶* B' ->
    Cpi A B ⟶* Cpi A' B'.
Proof.
elim => {A A'} => [A|A1 A2 A3].
    by apply: mred_trans_pir.
move => Hred12 Hmred23 IH Hmredn.
apply: CMulti_single;
    last by apply: IH.
by apply: CReduction_pil.
Qed.

Lemma mred_trans_sigmar A B B':
    B ⟶* B' ->
    Csigma A B ⟶* Csigma A B'.
Proof.
elim => {B B'} => [B|B1 B2 B3].
    by apply: CMulti_refl.
move => Hred12 Hmred23 IH.
apply: (CMulti_single (t2 := Csigma A B2)) => //.
by apply: CReduction_sigmar.
Qed.

Lemma mred_trans_sigma A B A' B':
    A ⟶* A' ->
    B ⟶* B' ->
    Csigma A B ⟶* Csigma A' B'.
Proof.
elim => {A A'} => [A|A1 A2 A3].
    by apply: mred_trans_sigmar.
move => Hred12 Hmred23 IH HmredB.
apply: CMulti_single;
    last by apply: IH.
by apply: CReduction_sigmal.
Qed.

Lemma mred_trans_pair m n m' n':
    m ⟶* m' ->
    n ⟶* n' ->
    Cpair m n ⟶* Cpair m' n'.
Proof.
elim => {m m'} => [m|m1 m2 m3];
    last first.
    move => Hred12 Hmred23 IH Hmredn.
    apply: CMulti_single;
        last by apply: IH.
    by apply: CReduction_pairl.
elim => {n n'} => [n|n1 n2 n3].
    by apply: CMulti_refl.
move => Hred12 Hmred23 IH.
apply: CMulti_single;
    last by apply: IH.
by apply: CReduction_pairr.
Qed.

Lemma mred_trans_proj1 m m':
    m ⟶* m' ->
    Cproj1 m ⟶* Cproj1 m'.
Proof.
elim => {m m'} => [m|m1 m2 m3].
    by apply: CMulti_refl.
move => Hred12 Hmred23 IH.
apply: CMulti_single;
    last by apply: IH.
by apply: CReduction_proj1.
Qed.

Lemma mred_trans_proj2 m m':
    m ⟶* m' ->
    Cproj2 m ⟶* Cproj2 m'.
Proof.
elim => {m m'} => [m|m1 m2 m3].
    by apply: CMulti_refl.
move => Hred12 Hmred23 IH.
apply: CMulti_single;
    last by apply: IH.
by apply: CReduction_proj2.
Qed.

Lemma mred_trans_proj1_beta m n m':
    m ⟶* m' ->
    Cproj1 (Cpair m n) ⟶* m'.
Proof.
elim => {m m'} => [m|m1 m2 m3].
    apply: CMulti_single.
        apply: CReduction_proj1_beta.
    by apply: CMulti_refl.
move => Hred12 Hmred23 IH.
apply: CMulti_single;
    last by apply: IH.
apply: CReduction_proj1.
by apply: CReduction_pairl.
Qed.

Lemma mred_trans_proj2_beta m n n':
    n ⟶* n' ->
    Cproj2 (Cpair m n) ⟶* n'.
Proof.
elim => {n n'} => [n|n1 n2 n3].
    apply: CMulti_single.
        apply: CReduction_proj2_beta.
    by apply: CMulti_refl.
move => Hred12 Hmred23 IH.
apply: CMulti_single;
    last by apply: IH.
apply: CReduction_proj2.
by apply: CReduction_pairr.
Qed.

(* inversion principles for types under CMulti CReduction*)
Lemma mred_inv_var x t': Cvar x ⟶* t' -> t' = Cvar x.
Proof.
move Heq: (Cvar x) => t Hmred.
elim: Hmred Heq => //.
move => t1 t2 t3 + + + Heq.
move: Heq => <-.
move => /red_inv_var -> Hmred3 IH.
by apply: IH.
Qed.

Lemma mred_inv_univ l t': Cuniv l ⟶* t' -> t' = Cuniv l.
Proof.
move Heq: (Cuniv l) => t Hmred.
elim: Hmred Heq => //.
move => t1 t2 t3 + + + Heq.
move: Heq => <-.
move => /red_inv_univ -> Hmred3 IH.
by apply: IH.
Qed.

Lemma mred_inv_pi A B t':
    Cpi A B ⟶* t' ->
    exists A' B', t' = Cpi A' B' /\ A ⟶* A' /\ B ⟶* B'.
Proof.
move Heq : (Cpi A B) => t.
move => Hmred.
elim : Hmred A B Heq => {t t'} [t| t1 t2 t3 Hred12 Hmred23 IH] A B Heq.
    move : Heq => <-.
    exists A, B.
    by have ? := (CMulti_refl CReduction).
move : Heq Hred12 => <-.
clear t1.
move /red_inv_pi => [[A2 [/esym Heqt2 HredAA2]]|[B2 [/esym Heqt2 HredBB2]]];
    move : (IH _ _ Heqt2) => [A3 [B3 [->]]].
    move => [HmredA23 HmredBB3].
    exists A3, B3.
    repeat split => //.
    by apply: (CMulti_single (t2 := A2)).
move => [HmredAA3 HmredB23].
exists A3, B3.
repeat split => //.
by apply: (CMulti_single (t2 := B2)).
Qed.

Lemma mred_inv_sigma A B t':
    Csigma A B ⟶* t' ->
    exists A' B', t' = Csigma A' B' /\ A ⟶* A' /\ B ⟶* B'.
Proof.
move Heq : (Csigma A B) => t.
move => Hmred.
elim : Hmred A B Heq => {t t'} [t| t1 t2 t3 Hred12 Hmred23 IH] A B Heq.
    move : Heq => <-.
    exists A, B.
    by have ? := (CMulti_refl CReduction).
move : Heq Hred12 => <-.
clear t1.
move /red_inv_sigma => [[A2 [/esym Heqt2 HredAA2]]|[B2 [/esym Heqt2 HredBB2]]];
    move : (IH _ _ Heqt2) => [A3 [B3 [->]]].
    move => [HmredA23 HmredBB3].
    exists A3, B3.
    repeat split => //.
    by apply: (CMulti_single (t2 := A2)).
move => [HmredAA3 HmredB23].
exists A3, B3.
repeat split => //.
by apply: (CMulti_single (t2 := B2)).
Qed.