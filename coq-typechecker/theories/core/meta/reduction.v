(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.core Require Import ast.term debruijn.defs.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Inductive CMulti (R: CTerm -> CTerm -> Prop): CTerm -> CTerm -> Prop :=
| CMulti_refl: forall t, CMulti R t t
| CMulti_single: forall t1 t2 t3,
    R t1 t2 ->
    CMulti R t2  t3 ->
    CMulti R t1  t3.

Reserved Notation "x '⟶' y" (at level 20, y at next level).
Inductive CReduction: CTerm -> CTerm -> Prop :=
| CReduction_beta: forall t n, (Cappl (Clam t) n) ⟶ t.[n/]
| CReduction_pil: forall A A' B, A ⟶ A' -> (Cpi A B) ⟶ (Cpi A' B)
| CReduction_pir: forall A B B', B ⟶ B' -> (Cpi A B) ⟶ (Cpi A B')
| CReduction_lam: forall t t', t ⟶ t' -> (Clam t) ⟶ (Clam t')
| CReduction_appll: forall m m' n, m ⟶ m' -> (Cappl m n) ⟶ (Cappl m' n)
| CReduction_applr: forall m n n', n ⟶ n' -> (Cappl m n) ⟶ (Cappl m n')
| CReduction_sigmal: forall A A' B, A ⟶ A' -> (Csigma A B) ⟶ (Csigma A' B)
| CReduction_sigmar: forall A B B', B ⟶ B' -> (Csigma A B) ⟶ (Csigma A B')
| CReduction_pairl: forall m m' n, m ⟶ m' -> (Cpair m n) ⟶ (Cpair m' n)
| CReduction_pairr: forall m n n', n ⟶ n' -> (Cpair m n) ⟶ (Cpair m n')
| CReduction_proj1: forall m m', m ⟶ m' -> (Cproj1 m) ⟶ (Cproj1 m')
| CReduction_proj1_beta: forall m n, (Cproj1 (Cpair m n)) ⟶ m
| CReduction_proj2: forall m m', m ⟶ m' -> (Cproj2 m) ⟶ (Cproj2 m')
| CReduction_proj2_beta: forall m n, (Cproj2 (Cpair m n)) ⟶ n
where "t1 '⟶' t2" := (CReduction t1 t2).
Notation "x '⟶*' y" :=
    (CMulti CReduction x y) (at level 20, y at next level).

Tactic Notation "CReductionElim" ident(t1) ident(t2) :=
elim => {t1 t2} /=
[ t2 n
| A A' B HredA IHA
| A B B' HredB IHB
| t t' Hredt IHt
| m m' n Hredm IHm
| m n n' Hredn IHn
| A A' B HredA IHA
| A B B' HredB IHB
| m m' n Hredm IHm
| m n n' Hredn IHn
| m m' Hredm IHm
| m n
| m m' Hredm IHm
| m n
].

Lemma CReduction_ebeta: forall t n x,
    x = t.[n/] ->
    (Cappl (Clam t) n) ⟶ x.
Proof.
move => ? ? ? ->.
by apply: CReduction_beta.
Qed.


Reserved Notation "x '⇒' y" (at level 20, y at next level).
Inductive ParallelReduction: CTerm -> CTerm -> Prop :=
| ParallelReduction_var: forall idx, Cvar idx ⇒ Cvar idx
| ParallelReduction_univ: forall lvl, Cuniv lvl ⇒ Cuniv lvl
| ParallelReduction_appl : forall m n m' n',
    m ⇒ m' ->
    n ⇒ n' ->
    (Cappl m n) ⇒ (Cappl m' n')
| ParallelReduction_beta : forall t n t' n',
    t ⇒ t' ->
    n ⇒ n' ->
    (Cappl (Clam t) n) ⇒ t'.[n'/]
| ParallelReduction_pi: forall A B A' B',
    A ⇒ A' ->
    B ⇒ B' ->
    Cpi A B ⇒ Cpi A' B'
| ParallelReduction_lam: forall t t',
    t ⇒ t' ->
    Clam t ⇒ Clam t'
| ParallelReduction_sigma: forall A B A' B',
    A ⇒ A' ->
    B ⇒ B' ->
    Csigma A B ⇒ Csigma A' B'
| ParallelReduction_pair : forall m n m' n',
    m ⇒ m' ->
    n ⇒ n' ->
    (Cpair m n) ⇒ (Cpair m' n')
| ParallelReduction_proj1: forall m m',
    m ⇒ m' ->
    (Cproj1 m) ⇒ (Cproj1 m')
| ParallelReduction_proj1_beta:
    forall m m' n n',
    m ⇒ m' ->
    n ⇒ n' ->
    (Cproj1 (Cpair m n)) ⇒ m'
| ParallelReduction_proj2: forall m m',
    m ⇒ m' ->
    (Cproj2 m) ⇒ (Cproj2 m')
| ParallelReduction_proj2_beta:
    forall m m' n n',
    m ⇒ m' ->
    n ⇒ n' ->
    (Cproj2 (Cpair m n)) ⇒ n'
where "t1 '⇒' t2" := (ParallelReduction t1 t2).

Notation "x '⇒*' y" :=
    (CMulti ParallelReduction x y) (at level 20, y at next level).

Tactic Notation "ParallelReductionElim" ident(t1) ident(t2) :=
elim => {t1 t2} /=
[ idx
| lvl
| m n m' n' Hpredmm' IHmm' Hprednn' IHnn'
| t n t' n' Hpredtt' IHtt' Hprednn' IHnn'
| A B A' B' HpredAA' IHAA' HpredBB' IHBB'
| t t' Hpredtt' IHtt'
| A B A' B' HpredAA' IHAA' HpredBB' IHBB'
| m n m' n' Hpredmm' IHmm' Hprednn' IHnn'
| m m' Hpredmm' IHmm'
| m m' n n' Hpredmm' IHmm' Hprednn' IHnn'
| m m' Hpredmm' IHmm'
| m m' n n' Hpredmm' IHmm' Hprednn' IHnn'
].

Lemma ParallelReduction_ebeta: forall t n t' x n',
    t ⇒ t' ->
    n ⇒ n' ->
    x = t'.[n'/] ->
    (Cappl (Clam t) n) ⇒ x.
Proof.
move => ? ? ? ? ? ? ? ->.
by apply: ParallelReduction_beta.
Qed.

Definition ParallelReductionSubst σ τ : Prop :=
    forall (x: nat), σ x ⇒τ x.
Infix "⇒ₛ" := ParallelReductionSubst (at level 20).

(* best parallel reduct *)
Fixpoint ρ (t: CTerm): CTerm:=
match t with
| Cvar x => Cvar x
| Cuniv l => Cuniv l
| Clam t => Clam (ρ t)
| Cpi A B => Cpi (ρ A) (ρ B)
| Cappl (Clam t) n => (ρ t).[ρ n/]
| Cappl m n => Cappl (ρ m) (ρ n)
| Csigma A B => Csigma (ρ A) (ρ B)
| Cpair m n => Cpair (ρ m) (ρ n)
| Cproj1 (Cpair m n) => ρ m
| Cproj1 m => Cproj1 (ρ m)
| Cproj2 (Cpair m n) => ρ n
| Cproj2 m => Cproj2 (ρ m)
end.
