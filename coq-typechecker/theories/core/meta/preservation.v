(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir Require Import common.utils.
From sir.core Require Import ast.term context.
From sir.core.meta Require Import typing reduction confluence stability.
From sir.core.meta Require Import conversion red_facts.
From sir.core.debruijn Require Import defs facts.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(* Typing inversion Principle*)
Lemma typing_lam_inversion Γ t T:
    Γ ⊢ Clam t : T ->
    exists A B, T ≅ Cpi A B /\ (A::Γ) ⊢ t : B.
Proof.
move Heq : (Clam t) => f.
move => Htype.
move : Htype t Heq.
CSynTypeElim Γ f T => // t'.
-   move => [ ->].
    have ? := CConv_refl.
    by exists A, B.
-   move => Heq.
    move: (IHT _ Heq) => [A [B [HconvT Htypet']]].
    exists A, B.
    split => //.
    apply: CConv_trans;
        last by apply: HconvT.
    by apply: CConv_symm.
Qed.

Lemma typing_pair_inversion Γ m1 m2 T:
    Γ ⊢ Cpair m1 m2 : T ->
    exists A B,
    T ≅ Csigma A B /\
    Γ ⊢ m1: A /\
    Γ ⊢ m2: B.[m1/].
Proof.
move Heq : (Cpair m1 m2) => p.
move => Htype.
move : Htype m1 m2 Heq.
CSynTypeElim Γ p T => // m1 m2.
-   move => [ -> ->] {m1 m2}.
    exists A, B.
    by have ? := CConv_refl.
-   move => Heq.
    move: (IHT _ _ Heq) => [A [B[HconvT [? ?]]]].
    exists A, B.
    split => //.
    apply: (@CConv_trans _ T) => //.
    by apply: CConv_symm.
Qed.

Lemma pi_type_injectivity A A' B B':
    Cpi A B ≅ Cpi A' B' ->
    A ≅ A' /\ B ≅ B'.
Proof.
rewrite !conv_mred_iff.
move => [C []].
move /mred_inv_pi => [A2 [B2 [-> [HmredAA2 HmredBB2]]]].
move /mred_inv_pi => [A3 [B3 [[<- <-] [H H1]]]].
split.
    by exists A2.
by exists B2.
Qed.

Lemma sigma_type_injectivity A A' B B':
    Csigma A B ≅ Csigma A' B' ->
    A ≅ A' /\ B ≅ B'.
Proof.
rewrite !conv_mred_iff.
move => [C []].
move /mred_inv_sigma => [A2 [B2 [-> [HmredAA2 HmredBB2]]]].
move /mred_inv_sigma => [A3 [B3 [[<- <-] [H H1]]]].
split.
    by exists A2.
by exists B2.
Qed.

Lemma pi_preservation Γ A1 A2 B i j:
    (A1 :: Γ) ⊢ B : Cuniv j ->
    ⊢ Γ ->
    Γ ⊢ A2 : Cuniv i ->
    A1 ⟶ A2 ->
    Γ ⊢ Cpi A2 B : Cuniv (maxn i j).
Proof.
move => HtypeB HWF HtypeA2 Hred12.
apply: CSynType_pi => //.
apply: (type_conversion_stability (A := A1))  => //.
    apply: CConv_symm.
    by apply: red_conv.
by apply: (CLocalWF_cons (l := i)).
Qed.

Lemma appl_preservation Γ m n1 n2 A B:
    Γ ⊢ m : Cpi A B ->
    Γ ⊢ n2 : A ->
    n1 ⟶ n2 ->
    Γ ⊢ Cappl m n2 : B.[n1/].
Proof.
move => Htypem Htypen2 Hredn12.
apply: (CSynType_conv (T := B.[n2/])).
    by apply: (CSynType_appl (A:= A)).
apply: CConv_symm.
apply: mred_conv.
apply: pred_mred.
apply: pred_subst.
    by apply: pred_refl.
by apply: red_pred.
Qed.

Lemma sigma_preservation Γ A1 A2 B i j:
    Γ ⊢ A2 : Cuniv i ->
    ⊢ Γ ->
    (A1 :: Γ) ⊢ B : Cuniv j ->
    A1 ⟶ A2 ->
    Γ ⊢ Csigma A2 B : Cuniv (maxn i j).
Proof.
move => HtypeA2 HWF Htypet HredA12.
apply: CSynType_sigma => //.
apply: (type_conversion_stability (A := A1))  => //.
    apply: CConv_symm.
    by apply: red_conv.
by apply: (CLocalWF_cons (l := i)).
Qed.

(*Lemma pair_preservation Γ A1 A2 B a b i j:
    Γ ⊢ A2 : Cuniv i ->
    ⊢ Γ ->
    Γ ⊢ a : A1 ->
    Γ ⊢ b : B.[a/] ->
    (A1 :: Γ) ⊢ B : Cuniv j ->
    A1 ⟶ A2 ->
    Γ ⊢ Cpair A2 B a b : Csigma A1 B.
Proof.
have ? := CConv_refl.
move => HtypeA2 Hwf Htypea Htypeb HtypeB HredA12.
have ? : Γ ⊢ a : A2.
    apply: CSynType_conv.
        by apply: Htypea.
    by apply: red_conv.
apply: (CSynType_conv (T := Csigma A2 B));
    last first.
    apply: CConv_symm.
    apply CConv_sigma => //.
    by apply: red_conv.
apply: (CSynType_pair (i := i) (j := j)) => //.
apply: (type_conversion_stability (A := A1)) => //.
-   apply: CConv_symm.
    by apply: red_conv.
-   by apply: (CLocalWF_cons (l := i)).
Qed.*)

Lemma type_preservation Γ T t t':
    Γ ⊢ t: T ->
    ⊢ Γ ->
    t ⟶ t' ->
    Γ ⊢ t' : T.
Proof.
move => Htype.
move : Htype t'.
CSynTypeElim Γ t T => t2 HWF Hred.
-   by inversion Hred.
-   by inversion Hred.
-   move : Hred  => /red_inv_pi [[A' [-> HredA]]|[B' [-> HredB]]].
    +   apply: (pi_preservation (A1 := A)) => //.
        by apply: IHA.
    +   apply: CSynType_pi => //.
        apply: IHB => //.
        apply: CLocalWF_cons => //.
        by apply: HtypeA.
-   move : Hred => /red_inv_lam [t' [-> Hredt]].
    apply: (CSynType_lam (l := l)) => //.
    apply: IHt =>//.
    apply: CLocalWF_cons => //.
    by apply: HtypeA.
-   move : Hred => /red_inv_appl
        [[m' [-> Hredm]]|[[n' [-> Hredn]]|[t' [Heqm [-> Hredbeta]]]]].
    +   apply: (CSynType_eappl (A := A) (B:= B)) => //.
        by apply: IHm.
    +   apply: (appl_preservation (A := A)) => //.
        by apply: IHn.
    +   move : Heqm Hredbeta IHm Htypem => -> Hredbeta IHm Htypem.
        move : (typing_lam_inversion Htypem) => [A' [B' []]].
        move => /pi_type_injectivity [HconvAA' HconvBB'] Htypet'.
        apply: type_substitution_stability => //.
            apply: CSynType_conv.
                by apply: Htypet'.
            by apply: CConv_symm.
        move => [|x] T /=.
            move => [<-].
            asimpl.
            by apply: (CSynType_conv (T := A)).
        move Heq: (dget Γ x) => [A2|] //.
        move => [<-].
        asimpl.
        by apply: CSynType_var.
-   move: Hred => /red_inv_sigma
        [[A' [-> HredA]]|[B' [-> HredB]]].
    +   apply: (sigma_preservation (A1 := A)) => //.
        by apply: IHA.
    +   apply: CSynType_sigma => //.
        apply: IHB => //.
        apply: CLocalWF_cons => //.
        by apply: HtypeA.
-   move : Hred => /red_inv_pair [[m' [-> Hredmm']]|[n' [-> Hrednn']]].
    +   apply: CSynType_pair.
            by apply: IHm.
        apply: CSynType_conv.
        apply: Htypen.
        apply: conv_gen_subst.
            apply: CConv_refl.
        by apply: red_conv.
    +   apply: CSynType_pair => //.
        by apply: IHn.
-   move: Hred => /red_inv_proj1
        [[m' [-> Hredmm']]|[m1 [m2 [Heq [-> Hredmm1]]]]].
        apply: CSynType_proj1.
        by apply: IHm.
    move: Heq Hredmm1 IHm Htypem => -> Hredmm1 IHm.
    move /typing_pair_inversion => [A' [B'[]]].
    move /sigma_type_injectivity => [ConvAA' _] [Htypem1 _].
    apply: (@CSynType_conv _ A') => //.
    by apply: CConv_symm.
-   move: Hred => /red_inv_proj2
        [[m' [-> Hredmm']]|[m1 [m2 [Heq [-> Hredmm1]]]]].
        apply: CSynType_conv.
            apply: CSynType_proj2.
            by apply: IHm.
        apply: conv_gen_subst.
            by apply: CConv_refl.
        apply: CConv_proj1.
        apply: CConv_symm.
        by apply: red_conv.
    move: Heq Hredmm1 IHm Htypem => -> Hredmm1 IHm.
    move /typing_pair_inversion => [A'[B'[]]].
    move /sigma_type_injectivity => [ConvAA' ConvBB'] [Htypem1 Htypem2].
    apply: (@CSynType_conv _ B'.[m1/]) => //.
    apply: conv_gen_subst;
        apply: CConv_symm => //.
    by apply: CConv_proj1_beta.
-   apply: (CSynType_conv (T:= T)) => //.
    by apply: IHT.
Qed.

Lemma type_mred_preservation Γ t t' T:
    Γ ⊢ t: T ->
    ⊢ Γ ->
    t ⟶* t' ->
    Γ ⊢ t' : T.
Proof.
move => + + Hmred.
elim : Hmred => [//|t1 t2 t3 Hred12 Hmred23 IH] Htype1 HWF.
apply: IH => //.
apply: type_preservation => //.
    by apply: Htype1.
by apply: Hred12.
Qed.
