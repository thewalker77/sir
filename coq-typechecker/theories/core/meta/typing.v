(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.core Require Import context ast.term meta.conversion.
From sir.core.debruijn Require Import defs facts.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(* Syntactic Typing rules optimized for proving metatheoritic properties      *)
Reserved Notation "Γ '⊢' t : T" (at level 20, t at next level).
Inductive CSynType: Context -> CTerm -> CTerm -> Prop :=
| CSynType_var:  forall Γ idx T,
                dget Γ idx = Some T ->
                Γ ⊢ (Cvar idx) : T
| CSynType_univ: forall Γ l,
                Γ ⊢ Cuniv l : Cuniv (S l)
| CSynType_pi: forall Γ A B i j,
                Γ ⊢ A: Cuniv i ->
                (A::Γ) ⊢ B: Cuniv j ->
                Γ ⊢ Cpi A B: Cuniv(maxn i j)
| CSynType_lam: forall Γ A B t l,
                Γ ⊢ A : Cuniv l ->
                (A :: Γ) ⊢ t: B ->
                Γ ⊢ Clam t : Cpi A B
| CSynType_appl: forall Γ A B m n,
                Γ ⊢ m : Cpi A B ->
                Γ ⊢ n : A ->
                Γ ⊢ Cappl m n : B.[n/]
| CSynType_sigma: forall Γ A B i j,
                 Γ ⊢ A: Cuniv i ->
                 (A::Γ) ⊢ B: Cuniv j ->
                 Γ ⊢ Csigma A B: Cuniv(maxn i j)
| CSynType_pair: forall Γ A B m n,
                Γ ⊢ m : A ->
                Γ ⊢ n: B.[m/] ->
                Γ ⊢ Cpair m n : Csigma A B
| CSynType_proj1: forall Γ m A B,
                    Γ ⊢ m : Csigma A B ->
                    Γ ⊢ Cproj1 m : A
| CSynType_proj2: forall Γ m A B,
                    Γ ⊢ m : Csigma A B ->
                    Γ ⊢ Cproj2 m : B.[Cproj1 m /]
| CSynType_conv: forall Γ T T' t,
                Γ ⊢ t: T ->
                T ≅ T' ->
                Γ ⊢ t: T'
where "Γ  '⊢' t : T" := (CSynType Γ t T).

Reserved Notation "'⊢' Γ" (at level 20).
Inductive CLocalWF: Context -> Prop :=
| CLocalWF_empty: ⊢ [::]
| CLocalWF_cons: forall Γ T l,
                    ⊢ Γ ->
                    Γ ⊢ T: (Cuniv l) ->
                    ⊢ (T:: Γ)
where "'⊢' Γ" := (CLocalWF Γ).

Tactic Notation "CSynTypeElim" ident(Γ) ident(t) ident(T):=
elim => {Γ t T} => /=
    [ Γ idx T Hdget
    | Γ lvl
    | Γ A B i j HtypeA IHA HtypeB IHB
    | Γ A B t l HtypeA IHA Htypet IHt
    | Γ A B m n Htypem IHm Htypen IHn
    | Γ A B i j HtypeA IHA HtypeB IHB
    | Γ A B m n Htypem IHm Htypen IHn
    | Γ m A B Htypem IHm
    | Γ m A B Htypem IHm
    | Γ T T' t HtypeT IHT HconvTT'
    ].

Lemma CSynType_eappl Γ A B T m n:
    Γ ⊢ m : Cpi A B ->
    Γ ⊢ n : A ->
    T = B.[n/] ->
    Γ ⊢ Cappl m n : T.
Proof.
move => ? ? ->.
by apply: (@CSynType_appl _ A B).
Qed.

Lemma CSynType_type_move Γ t T: Γ ⊢ t : T -> exists T', Γ ⊢ t: T' /\ T ≅ T' .
Proof.
move => H.
exists T.
split => //.
apply: CConv_refl.
Qed.