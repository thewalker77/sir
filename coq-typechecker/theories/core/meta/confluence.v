(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.core.meta Require Import reduction conversion red_facts.
From sir Require Import core.ast.term common.utils.
From sir.core.debruijn Require Import defs facts.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(* properties of parallel reduction and multi parallel reduction *)
Lemma pred_refl t: t ⇒ t.
Proof.
move: t.
CTermElim.
-   by apply: ParallelReduction_var.
-   by apply: ParallelReduction_univ.
-   by apply: ParallelReduction_pi.
-   by apply: ParallelReduction_lam.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_sigma.
-   by apply: ParallelReduction_pair.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj2.
Qed.

Lemma pred_psubst t t' σ:
    t ⇒t' ->
    t.[σ] ⇒ t'.[σ].
Proof.
have ? := pred_refl.
move => Hpred.
move : Hpred  σ.
ParallelReductionElim t t' => σ /=.
-   by move: idx => [|idx].
-   by [].
-   by apply: ParallelReduction_appl.
-   apply: ParallelReduction_ebeta.
    +   by apply: IHtt'.
    +   by apply: IHnn'.
    +   by asimpl.
-   by apply: ParallelReduction_pi.
-   by apply: ParallelReduction_lam.
-   by apply: ParallelReduction_sigma.
-   by apply: ParallelReduction_pair.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1_beta.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2_beta.
Qed.

Lemma spred_up σ τ:
    σ ⇒ₛ τ ->
    up σ ⇒ₛ up τ.
Proof.
move => Hpreds [|x];
    asimpl.
    apply: pred_refl.
by apply: pred_psubst.
Qed.

Lemma pred_spred t t' σ τ:
    t ⇒ t' ->
    σ ⇒ₛ τ ->
    t.[σ] ⇒ t'.[τ].
Proof.
move => Hpred.
move: Hpred σ τ.
ParallelReductionElim t t' => σ τ Hspred;
    have ? := pred_refl;
    rewrite ?CsubstE.
    asimpl.
-   by apply: Hspred.
-   by [].
-   apply: ParallelReduction_appl.
        by apply: IHmm'.
    by apply: IHnn'.
-   apply: ParallelReduction_ebeta.
    +   apply: IHtt'.
        by apply: spred_up.
    +   by apply: IHnn'.
    +   by asimpl.
-   apply: ParallelReduction_pi.
        by apply: IHAA'.
    apply: IHBB'.
    by apply: spred_up.
-   apply: ParallelReduction_lam.
    apply: IHtt'.
    by apply: spred_up.
-   apply: ParallelReduction_sigma.
        by apply: IHAA'.
    apply: IHBB'.
    by apply: spred_up.
-   apply: ParallelReduction_pair.
        by apply: IHmm'.
    by apply: IHnn'.
-   apply: ParallelReduction_proj1.
    by apply: IHmm'.
-   apply: ParallelReduction_proj1_beta.
        by apply: IHmm'.
    by apply: IHnn'.
-   apply: ParallelReduction_proj2.
    by apply: IHmm'.
-   apply: ParallelReduction_proj2_beta.
        by apply: IHmm'.
    by apply: IHnn'.
Qed.

(* this lemma are recurring cases in pred_subst *)
Lemma spred_up_id n n':
    n ⇒ n' ->
    up(n .: ids) ⇒ₛ up(n' .: ids).
Proof.
move => Hprednn'.
apply: spred_up.
move => [|x];
    asimpl => //.
by apply: pred_refl.
Qed.

Lemma pred_subst t t' n n':
    t ⇒ t' ->
    n ⇒ n' ->
    t .[n/] ⇒ t'.[n'/].
move => Hpred.
move: Hpred n n'.
ParallelReductionElim t t' => n1 n2 Hpredn12 /=;
    have ? := pred_refl;
    rewrite ?CsubstE.
-   case : idx;
        by asimpl.
-   by [].
-   apply ParallelReduction_appl.
        by apply IHmm'.
    by apply: IHnn'.
-   apply: ParallelReduction_ebeta.
    +   apply: (pred_spred Hpredtt').
        by apply: (@spred_up_id _ n2).
    +   by apply: (@IHnn' _ n2).
    +   by asimpl.
-   apply ParallelReduction_pi.
        by apply IHAA'.
    apply: pred_spred => //.
    by apply: spred_up_id.
-   apply ParallelReduction_lam.
    apply: pred_spred => //.
    by apply: spred_up_id.
-   apply: ParallelReduction_sigma.
        by apply: IHAA'.
    apply: pred_spred => //.
    by apply: spred_up_id.
-   apply: ParallelReduction_pair.
        by apply: IHmm'.
    by apply: IHnn'.
-   apply: ParallelReduction_proj1.
    by apply: IHmm'.
-   apply: ParallelReduction_proj1_beta.
        by apply: IHmm'.
    by apply: IHnn'.
-   apply: ParallelReduction_proj2.
    by apply: IHmm'.
-   apply: ParallelReduction_proj2_beta.
        by apply: IHmm'.
    by apply: IHnn'.
Qed.

Lemma mpred_subst_special t n n':
    n ⇒* n' ->
    t .[n/] ⇒* t.[n'/].
Proof.
elim => {n n'} [n|n1 n2 n3 Hpred12 Hmpred23 IH].
    by apply: CMulti_refl.
apply: CMulti_single;
    last by apply: IH.
apply: pred_subst => //.
by apply: pred_refl.
Qed.

Lemma mpred_subst t t' n n':
    t ⇒* t' ->
    n ⇒* n' ->
    t .[n/] ⇒* t'.[n'/].
Proof.
elim => {t t'} [t| t1 t2 t3 Hpred12 Hmpred23 IH] Hmprednn'.
    by apply: mpred_subst_special.
apply: CMulti_single;
    last by apply: IH.
apply: pred_subst => //.
by apply: pred_refl.
Qed.

Lemma pred_triangle_appl m m' n n':
    m ⇒ m' ->
    m' ⇒ ρ m ->
    n ⇒ n' ->
    n' ⇒ ρ n ->
    Cappl m' n' ⇒ ρ (Cappl m n).
Proof.
move : m m' n n'.
CTermElim => m' n1 n2 Hpredmm' Hpredρmm' Hpredn12 Hpredρn12.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_appl.
-   inversion Hpredmm'; subst.
    inversion Hpredρmm'; subst.
    by apply: ParallelReduction_beta.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_appl.
Qed.

Lemma pred_triangle_proj1 m m':
    m ⇒ m' ->
    m' ⇒ ρ m ->
    Cproj1 m' ⇒ ρ (Cproj1 m).
Proof.
move: m m'.
CTermElim => m' Hpredmm' Hpredρmm'.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1.
-   inversion Hpredmm'; subst.
    inversion Hpredρmm'; subst.
    by apply: (@ParallelReduction_proj1_beta _ _ _ (ρ n)).
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1.
Qed.

Lemma pred_triangle_proj2 m m':
    m ⇒ m' ->
    m' ⇒ ρ m ->
    Cproj2 m' ⇒ ρ (Cproj2 m).
Proof.
move: m m'.
CTermElim => m' Hpredmm' Hpredρmm'.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2.
-   inversion Hpredmm'; subst.
    inversion Hpredρmm'; subst.
    by apply: (@ParallelReduction_proj2_beta _ (ρ m)).
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2.
Qed.

Lemma pred_triangle t t':
    t ⇒ t' ->
    t' ⇒ ρ t.
Proof.
have ? := pred_refl.
ParallelReductionElim t t'.
-   by [].
-   by [].
-   by apply: pred_triangle_appl.
-   by apply: pred_subst.
-   by apply: ParallelReduction_pi.
-   by apply: ParallelReduction_lam.
-   by apply: ParallelReduction_sigma.
-   by apply: ParallelReduction_pair.
-   by apply: pred_triangle_proj1.
-   by [].
-   by apply: pred_triangle_proj2.
-   by [].
Qed.

Lemma pred_diamond t t1 t2:
    t ⇒ t1 ->
    t ⇒ t2 ->
    exists t',
    t1 ⇒ t' /\ t2 ⇒ t'.
Proof.
move => /pred_triangle Hpredtt1 /pred_triangle Hpredtt2.
by exists (ρ t).
Qed.

Lemma pred_confluence_helper t1 t2 t3:
    t1 ⇒* t2 ->
    t1 ⇒ t3 ->
    exists t',
    t2 ⇒ t' /\ t3 ⇒* t'.
Proof.
move => Hred.
have ? := CMulti_refl.
elim : Hred t3 => {t1 t2} [t1| t1 t2 t3 Hpred12 Hpred23 IH] t4 Hpred14.
    by exists t4.
move : (pred_diamond Hpred12 Hpred14) => [t5 [Hpred25 Hpred45]].
move : (IH _ Hpred25) => [t6 [Hpred36 Hpred56]].
exists t6.
split => //.
by apply: (@CMulti_single _ _ t5).
Qed.

Lemma pred_confluence t1 t2 t3:
    t1 ⇒* t2 ->
    t1 ⇒* t3 ->
    exists t',
    t2 ⇒* t' /\ t3 ⇒* t'.
Proof.
move => Hred.
have ? := CMulti_refl.
elim : Hred t3 => {t1 t2} [t1| t1 t2 t3 Hpred12 Hpred23 IH] t4 Hpred14.
    by exists t4.
move : (pred_confluence_helper Hpred14 Hpred12) => [t5 [Hpred45 Hmpred25]].
move : (IH _ Hmpred25) => [t6 [Hmpred36 Hmpred56]].
exists t6.
split => //.
by apply: (@CMulti_single _ _ t5).
Qed.

(* ParallelReduction, MultiReduction, & Reduction *)
Lemma red_pred t t':
    t ⟶ t' ->
    t ⇒ t'.
Proof.
have ? := pred_refl.
CReductionElim t t'.
-   by apply: ParallelReduction_beta.
-   by apply: ParallelReduction_pi.
-   by apply: ParallelReduction_pi.
-   by apply: ParallelReduction_lam.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_sigma.
-   by apply: ParallelReduction_sigma.
-   by apply: ParallelReduction_pair.
-   by apply: ParallelReduction_pair.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1_beta.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2_beta.
Qed.

Lemma pred_mred t t':
    t ⇒ t' ->
    t ⟶* t'.
Proof.
ParallelReductionElim t t'.
-   by apply: CMulti_refl.
-   by apply: CMulti_refl.
-   by apply: mred_trans_appl.
-   by apply: mred_trans_appl_beta.
-   by apply: mred_trans_pi.
-   by apply: mred_trans_lam.
-   by apply: mred_trans_sigma.
-   by apply: mred_trans_pair.
-   by apply: mred_trans_proj1.
-   by apply: mred_trans_proj1_beta.
-   by apply: mred_trans_proj2.
-   by apply: mred_trans_proj2_beta.
Qed.

Lemma mred_mpred1 t t':
    t ⟶* t' ->
    t ⇒* t'.
Proof.
elim => {t t'} => [t|t1 t2 t3].
    by apply: CMulti_refl.
move => /red_pred Hpred12 Hred23 Hmpred23.
by apply: (@CMulti_single _ _ t2).
Qed.

Lemma mred_mpred2 t t':
    t ⇒* t' ->
    t ⟶* t'.
Proof.
elim => {t t'} => [t|t1 t2 t3].
    by apply: CMulti_refl.
move => /pred_mred Hmred12 Hpred23 Hmred23.
by apply: (@multi_trans _ _ t2).
Qed.

Lemma mred_mpred_iff t t':
    t ⇒* t' <->
    t ⟶* t'.
split.
    by apply: mred_mpred2.
by apply: mred_mpred1.
Qed.

Lemma mpred_pred t t':
    t ⇒ t' ->
    t ⇒* t'.
Proof.
have ? := CMulti_refl.
ParallelReductionElim t t' => //;
    (apply: CMulti_single;
        last by apply: CMulti_refl).
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_beta.
-   by apply: ParallelReduction_pi.
-   by apply: ParallelReduction_lam.
-   by apply: ParallelReduction_sigma.
-   by apply: ParallelReduction_pair.
-   by apply: ParallelReduction_proj1.
-   by apply: (@ParallelReduction_proj1_beta  _ _ _ n').
-   by apply: ParallelReduction_proj2.
-   by apply: (@ParallelReduction_proj2_beta  _ m').
Qed.

Lemma mred_confluence t1 t2 t:
    t ⟶* t1 ->
    t ⟶* t2 ->
    exists t',
    t1 ⟶* t' /\ t2 ⟶* t'.
Proof.
rewrite -!mred_mpred_iff => Hmpred1 Hmpred2.
have := (pred_confluence Hmpred1 Hmpred2).
move => [t3 [? ?]].
exists t3.
by rewrite -!mred_mpred_iff.
Qed.


(* relationship between conversion and reduction *)
Lemma red_conv A A':
    A ⟶ A' ->
    A ≅ A'.
Proof.
have ? := CConv_refl.
CReductionElim A A'.
-   by apply: CConv_beta.
-   by apply: CConv_pi.
-   by apply: CConv_pi.
-   by apply: CConv_lam.
-   by apply: CConv_appl.
-   by apply: CConv_appl.
-   by apply: CConv_sigma.
-   by apply: CConv_sigma.
-   by apply: CConv_pair.
-   by apply: CConv_pair.
-   by apply: CConv_proj1.
-   by apply: CConv_proj1_beta.
-   by apply: CConv_proj2.
-   by apply: CConv_proj2_beta.
Qed.

Lemma mred_conv A A':
    A ⟶* A' ->
    A ≅ A'.
Proof.
elim => {A A'} [A|t1 t2 t3 /red_conv Hconv12 Hpred23 Hconv23].
    by apply: CConv_refl.
by apply: (CConv_trans _ t2).
Qed.

(* Church Rosser Property *)
Lemma mred_cr A A':
    A ≅ A' ->
    exists B, A ⟶* B /\ A' ⟶* B.
Proof.
have ? := CMulti_refl CReduction.
CConvElim A A'.
-   rewrite Heqtt'.
    exists t.[u/].
    split => //.
    apply: (CMulti_single (t2 :=  t.[u/])) => //.
    by apply: CReduction_beta.
-   by exists t.
-   move: Hconvtt' => [B [? ?]].
    by exists B.
-   move : IH12 IH23 => [t4 [Hmred14 Hmred24]] [t5 [Hmred25 Hmred35]].
    move : (mred_confluence Hmred24 Hmred25) => [t6 [Hred46 Hred56]].
    exists t6.
    split.
        by apply (multi_trans (T2 := t4)).
    by apply: (multi_trans (T2 := t5)).
-   move: IHA IHB => [C [HmredAC HmredA'C]] [D [HmredBD HmredB'D]].
    exists (Cpi C D).
    split;
    by apply: mred_trans_pi.
-   move: IHt => [d [Hmredtd Hmredt'd]].
    exists (Clam d).
    split;
        by apply: mred_trans_lam.
-   move: IHm IHn => [o [Hmredmo Hmredm'o]] [p [Hmrednp Hmredn'p]].
    exists (Cappl o p).
    split;
        by apply: mred_trans_appl.
-   move: IHA IHB => [A1 [HmredAA1 HmredA'A1]] [B1 [HmredBB1 HmredB'B1]].
    exists (Csigma A1 B1).
    split;
        by apply: mred_trans_sigma.
-   move: IHm  => [p [Hmredmp Hmredm'p]].
    move: IHn => [q [Hmrednq Hmredn'q]].
    exists (Cpair p q).
    split;
        by apply: mred_trans_pair.
-   move: IHm => [n [IHmn IHm'n]].
    exists (Cproj1 n).
    split;
        by apply: mred_trans_proj1.
-   move: IHm => [n [IHmn IHm'n]].
    exists (Cproj2 n).
    split;
        by apply: mred_trans_proj2.
-   exists m.
    split => //.
    apply: (CMulti_single (t2 :=  m)) => //.
    by apply: CReduction_proj1_beta.
-   exists n.
    split => //.
    apply: (CMulti_single (t2 :=  n)) => //.
    by apply: CReduction_proj2_beta.
Qed.

Lemma conv_mred_iff' A A' B:
    A ⟶* B -> A' ⟶* B ->
    A ≅ A'.
Proof.
move => /mred_conv HconvAB /mred_conv HconvA'B.
apply: CConv_trans.
    by apply: HconvAB.
by apply: CConv_symm.
Qed.

Lemma conv_mred_iff A A':
    A ≅ A' <->
    (exists B, A ⟶* B /\ A' ⟶* B).
Proof.
split.
    by apply: mred_cr.
move => [? []].
by apply: conv_mred_iff'.
Qed.

Lemma mred_subst t t' n n':
    t ⟶* t' ->
    n ⟶* n' ->
    t .[n/] ⟶* t'.[n'/].
Proof.
rewrite -!mred_mpred_iff.
by apply: mpred_subst.
Qed.

Lemma conv_gen_subst t t' n n':
    t ≅ t' ->
    n ≅ n' ->
    t .[n/] ≅ t'.[n'/].
rewrite !conv_mred_iff.
move => [t1 [Hmredtt1 Hmredt't1]] [n1 [Hmrednn1 Hmredn'n1]].
exists t1.[n1/].
split;
    by apply: mred_subst.
Qed.