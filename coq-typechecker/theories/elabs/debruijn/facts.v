(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(* Based on work made by Tobias Tebbi, Steven Schäfer, et al. for             *)
(* https://github.com/coq-community/autosubst commit tag:495d547fa2a3e40da0f7 *)
(* Base work was licensed Under MIT                                           *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
Require Import Coq.Logic.FunctionalExtensionality.
From HB Require Import structures.
From mathcomp Require Import all_ssreflect.
From sir.elabs.debruijn Require Import defs.
From sir.elabs Require Import ast.term.
From sir.common Require Export utils padd.
From sir.elabs.debruijn Require Export elims.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Module Facts.
Lemma ren_up ξ: @ren Term (upren ξ) = up (ren ξ).
Proof.
apply: functional_extensionality.
by case.
Qed.

Lemma up_ids: @up Term ids = ids.
Proof.
apply: functional_extensionality.
by case.
Qed.

Lemma up_comp_up_upren ξ (σ: nat -> Term):
    up (σ ∘ ξ) = up σ ∘ upren ξ.
Proof.
apply: functional_extensionality.
by case.
Qed.

Lemma rename_subst ξ (t: Term):
    rename ξ t = t.[ren ξ].
Proof.
move: t ξ.
TermElim => ξ;
    rewrite renameE //=.
-   (* pi *)
    by rewrite IHA IHB ren_up.
-   (* lam *)
    by rewrite IHT IHy ren_up.
-   (* appl *)
    by rewrite IHm IHn.
-   by rewrite IHA IHB ren_up.
-   by rewrite IHM IHN IHm IHn ren_up.
-   by rewrite IHm.
-   by rewrite IHm.
Qed.

Lemma subst_id (t: Term): t.[ids] = t.
Proof.
move: t.
TermElim;
    rewrite substE => //=.
-   (* pi *)
    by rewrite up_ids IHA IHB.
-   (* lam *)
    by rewrite up_ids IHT IHy.
-   (* appl *)
    by rewrite IHm IHn.
-   (* sigma *)
    by rewrite up_ids IHA IHB.
    (* pair *)
-   by rewrite up_ids IHM IHN IHm IHn.
-   by rewrite IHm.
-   by rewrite IHm.
Qed.

Lemma id_subst (σ: nat -> Term) idx:
    (ids idx).[σ] = σ idx.
Proof.
by elim : idx => [| idx]/=.
Qed.

Lemma rename_subst_comp σ ξ (t: Term):
    (rename ξ t).[σ] = t.[σ ∘ ξ].
Proof.
move: t ξ σ.
TermElim => ξ σ;
    rewrite renameE substE => //=.
-   (* pi *)
    by rewrite IHA IHB -up_comp_up_upren.
-   (* lam *)
    by rewrite IHT IHy -up_comp_up_upren.
-   (* appl *)
    by rewrite IHm IHn.
-   by rewrite IHA IHB -up_comp_up_upren.
-   by rewrite IHM IHN IHm IHn -up_comp_up_upren.
-   by rewrite IHm.
-   by rewrite IHm.
Qed.

Lemma rename_upren_comp_up (ξ: nat -> nat) (σ: nat -> Term):
    rename (upren ξ) ∘ up σ = up (rename ξ ∘ σ).
Proof.
apply: functional_extensionality.
move => [|idx].
    by [].
rewrite /up /comp /=.
rewrite [rename succn (rename _ _)]rename_subst rename_subst_comp.
by rewrite [rename (upren _) _]rename_subst rename_subst_comp.
Qed.

Lemma subst_rename_comp σ ξ (t: Term):
    rename ξ t.[σ] = t.[rename ξ ∘ σ].
Proof.
move: t σ ξ.
TermElim => σ ξ //=;
    rewrite substE renameE/=.
-   (* pi *)
    by rewrite IHA IHB rename_upren_comp_up.
-   (* lam *)
    by rewrite IHT IHy rename_upren_comp_up.
-   (* appl *)
    by rewrite IHm IHn.
-   by rewrite IHA IHB rename_upren_comp_up.
-   by rewrite IHm IHn IHN IHM rename_upren_comp_up.
-   by rewrite IHm.
-   by rewrite IHm.
Qed.

Lemma up_scomp (σ τ: nat-> Term):
    up (σ ⊚ τ) = up σ ⊚ up τ.
Proof.
apply: functional_extensionality.
move => [//|idx].
rewrite /up /scomp /comp /=.
by rewrite rename_subst_comp subst_rename_comp.
Qed.

Lemma subst_scomp σ τ (t: Term): t.[σ].[τ] = t.[τ ⊚ σ].
Proof.
move: t σ τ.
TermElim => σ τ;
    rewrite !substE => //=.
-   (* pi *)
    by rewrite IHA IHB -up_scomp.
-   (* lam *)
    by rewrite IHT IHy -up_scomp.
-   (* appl *)
    by rewrite IHm IHn.
-   by rewrite IHA IHB -up_scomp.
-   by rewrite IHm IHn IHM IHN -up_scomp.
-   by rewrite IHm.
-   by rewrite IHm.
Qed.

End Facts.

HB.instance Definition _ := HasDebruijnLemmas.Build _
    Facts.rename_subst Facts.subst_id Facts.id_subst Facts.subst_scomp.

From sir.common.debruijn Require Export tactics.
