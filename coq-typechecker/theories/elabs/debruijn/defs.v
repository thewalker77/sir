(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(* Based on work made by Tobias Tebbi, Steven Schäfer, et al. for             *)
(* https://github.com/coq-community/autosubst commit tag:495d547fa2a3e40da0f7 *)
(* Base work was licensed Under MIT                                           *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From HB Require Import structures.
From mathcomp Require Import all_ssreflect.
From sir.elabs Require Import ast.term.

From sir.common.debruijn Require Export structs.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Module Defs1.

Definition ids : nat -> Term := var.

Fixpoint rename(ξ : nat -> nat)  (t: Term)  : Term :=
match t with
| var idx => var (ξ idx)
| univ l => univ l
| pi A B => pi (rename ξ A) (rename (upren ξ) B)
| lam T y => lam (rename ξ T) (rename (upren ξ) y)
| appl m n => appl (rename ξ m) (rename ξ n)
| sigma A B => sigma (rename ξ A) (rename (upren ξ) B)
| pair M N m n => pair (rename ξ M) (rename (upren ξ) N) (rename ξ m) (rename ξ n)
| proj1 m => proj1 (rename ξ m)
| proj2 m => proj2 (rename ξ m)
| gvar t => gvar t
end.
End Defs1.

HB.instance Definition _ := HasIDs.Build _ Defs1.ids.
HB.instance Definition _ := HasRename.Build _ Defs1.rename.

Module Defs2.

Fixpoint subst (σ: nat -> Term) (t: Term) : Term :=
match t with
| var idx => σ idx
| univ l => univ l
| pi A B => pi (subst σ A) (subst (up σ) B)
| lam T y => lam (subst σ T) (subst (up σ) y)
| appl m n => appl (subst σ m) (subst σ n)
| sigma A B => sigma (subst σ A) (subst (up σ) B)
| pair M N m n => pair (subst σ M) (subst (up σ) N) (subst σ m) (subst σ n)
| proj1 m => proj1 (subst σ m)
| proj2 m => proj2 (subst σ m)
| gvar t => gvar t
end.

End Defs2.

HB.instance Definition _ := HasSubst.Build _ Defs2.subst.




