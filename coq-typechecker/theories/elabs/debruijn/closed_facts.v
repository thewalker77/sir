(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)

From mathcomp Require Import all_ssreflect zify.
From sir.elabs.debruijn Require Import defs elims facts.
From sir.elabs.ast Require Import term.
From sir.common.debruijn Require Export closed_facts.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Lemma closedn_var1 n idx: Closedn (var idx) n -> idx < n.
Proof.
Transparent ids.
elim : n idx => [|n IHn] [|idx] Hclosedn //=.
-   have := Hclosedn (ren succn).
    by asimpl.
-   have := Hclosedn (ren succn).
    asimpl.
    move => [].
    by lia.
-   apply: IHn.
    rewrite /Closedn /= => σ.
    have := Hclosedn σ.
    asimpl.
    case Heq : (upn n σ idx) => [idx'|||||||||]//.
    asimpl.
    by move => [->].
Opaque ids.
Qed.

Lemma closedn_var2 n idx: idx < n -> Closedn (var idx) n.
Proof.
elim : n idx => [|n IHn] [|idx] Hlt //=.
rewrite /Closedn /= => σ.
have := IHn _ Hlt σ.
asimpl.
case Heq : (upn n σ idx) => [idx'|||||||||]//.
move => [->].
Transparent ids.
by asimpl.
Opaque ids.
Qed.

Lemma closedn_var n idx: Closedn (var idx) n <-> idx < n.
split.
    by apply: closedn_var1.
by apply: closedn_var2.
Qed.

Lemma closedn_univ n lvl: Closedn (univ lvl) n <-> True.
Proof.
by split.
Qed.

Lemma closedn_pi1 A B n:
    Closedn (pi A B) n -> Closedn A n /\ Closedn B n.+1.
Proof.
elim: n A B => [|n IHn] A B;
    rewrite /Closedn /=;
    move => Hclosed;
    split => σ;
    move : (Hclosed σ);
    rewrite substE;
    by move => [].
Qed.

Lemma closedn_pi2 A B n:
    Closedn A n /\ Closedn B n.+1 ->
    Closedn (pi A B) n.
Proof.
elim: n A B => /= [|n IHn] A B /=;
    rewrite /Closedn /=;
    move => [HclosedA HclosedB] => σ;
    by rewrite substE HclosedA HclosedB.
Qed.

Lemma closed_pi A B n:
    Closedn (pi A B) n <-> Closedn A n /\ Closedn B n.+1.
Proof.
split.
    by apply: closedn_pi1.
by apply: closedn_pi2.
Qed.

Lemma closedn_lam1 A t n:
    Closedn (lam A t) n -> Closedn A n /\ Closedn t n.+1.
Proof.
elim: n A t => [|n IHn] A t;
    rewrite /Closedn /=;
    move => Hclosed;
    split => σ;
    move : (Hclosed σ);
    rewrite substE;
    by move => [].
Qed.

Lemma closedn_lam2 A t n:
    Closedn A n /\ Closedn t n.+1 ->
    Closedn (lam A t) n.
Proof.
elim: n A t => /= [|n IHn] A t /=;
    rewrite /Closedn /=;
    move => [HclosedA Hclosedt] => σ;
    by rewrite substE HclosedA Hclosedt.
Qed.

Lemma closed_lam A t n:
    Closedn (lam A t) n <-> Closedn A n /\ Closedn t n.+1.
Proof.
split.
    by apply: closedn_lam1.
by apply: closedn_lam2.
Qed.

Lemma closedn_appl1 m m' n:
    Closedn (appl m m') n -> Closedn m n /\ Closedn m' n.
Proof.
elim: n m m' => [|n IHn] m m';
    rewrite /Closedn /=;
    move => Hclosed;
    split => σ;
    move : (Hclosed σ);
    rewrite substE;
    by move => [].
Qed.

Lemma closedn_appl2 m m' n:
    Closedn m n /\ Closedn m' n ->
    Closedn (appl m m') n.
Proof.
elim: n m m' => /= [|n IHn] m m' /=;
    rewrite /Closedn /=;
    move => [Hclosedm Hclosedm'] => σ;
    by rewrite substE Hclosedm Hclosedm'.
Qed.

Lemma closed_appl m m' n:
    Closedn (appl m m') n <-> Closedn m n /\ Closedn m' n.
Proof.
split.
    by apply: closedn_appl1.
by apply: closedn_appl2.
Qed.

Lemma closedn_sigma1 A B n:
    Closedn (sigma A B) n -> Closedn A n /\ Closedn B n.+1.
Proof.
elim: n A B => [|n IHn] A B;
    rewrite /Closedn /=;
    move => Hclosed;
    split => σ;
    move : (Hclosed σ);
    rewrite substE;
    by move => [].
Qed.

Lemma closedn_sigma2 A B n:
    Closedn A n /\ Closedn B n.+1 ->
    Closedn (sigma A B) n.
Proof.
elim: n A B => /= [|n IHn] A B /=;
    rewrite /Closedn /=;
    move => [HclosedA HclosedB] => σ;
    by rewrite substE HclosedA HclosedB.
Qed.

Lemma closed_sigma A B n:
    Closedn (sigma A B) n <-> Closedn A n /\ Closedn B n.+1.
Proof.
split.
    by apply: closedn_sigma1.
by apply: closedn_sigma2.
Qed.

Lemma closedn_pair1 A B a b n:
    Closedn (pair A B a b) n ->
    Closedn A n /\ Closedn B n.+1 /\ Closedn a n /\ Closedn b n.
Proof.
elim: n A B a b => [|n IHn] A B a b;
    rewrite /Closedn /=;
    move => Hclosed;
    (repeat split) => σ;
    move : (Hclosed σ);
    rewrite substE;
    by move => [].
Qed.

Lemma closedn_pair2 A B a b n:
    Closedn A n /\ Closedn B n.+1 /\ Closedn a n /\ Closedn b n ->
    Closedn (pair A B a b) n.
Proof.
elim: n A B a b => /= [|n IHn] A B a b /=;
    rewrite /Closedn /=;
    move => [HclosedA [HclosedB [Hcloseda Hclosedb]]] => σ;
    by rewrite substE HclosedA HclosedB Hcloseda Hclosedb.
Qed.

Lemma closed_pair A B a b n:
    Closedn (pair A B a b) n <->
    Closedn A n /\ Closedn B n.+1 /\ Closedn a n /\ Closedn b n.
Proof.
split.
    by apply: closedn_pair1.
by apply: closedn_pair2.
Qed.

Lemma closedn_proj11 m n:
    Closedn (proj1 m) n -> Closedn m n.
Proof.
elim: n m => [|n IHn] m;
    rewrite /Closedn /=;
    move => Hclosed σ;
    move : (Hclosed σ);
    rewrite substE;
    by move => [].
Qed.

Lemma closedn_proj12 m n:
    Closedn m n -> Closedn (proj1 m) n.
Proof.
elim: n m => /= [|n IHn] m /=;
    rewrite /Closedn /=;
    move => Hclosedm σ;
    by rewrite substE Hclosedm.
Qed.

Lemma closed_proj1 m n:
    Closedn (proj1 m) n <-> Closedn m n.
Proof.
split.
    by apply: closedn_proj11.
by apply: closedn_proj12.
Qed.

Lemma closedn_proj21 m n:
    Closedn (proj2 m) n -> Closedn m n.
Proof.
elim: n m => [|n IHn] m;
    rewrite /Closedn /=;
    move => Hclosed σ;
    move : (Hclosed σ);
    rewrite substE;
    by move => [].
Qed.

Lemma closedn_proj22 m n:
    Closedn m n -> Closedn (proj2 m) n.
Proof.
elim: n m => /= [|n IHn] m /=;
    rewrite /Closedn /=;
    move => Hclosedm σ;
    by rewrite substE Hclosedm.
Qed.

Lemma closed_proj2 m n:
    Closedn (proj2 m) n <-> Closedn m n.
Proof.
split.
    by apply: closedn_proj21.
by apply: closedn_proj22.
Qed.

Lemma closedn_gvar n name: Closedn (gvar name) n <-> True.
Proof.
by split.
Qed.


Definition ClosednE := (
    closedn_var,    closedn_univ,   closed_pi,      closed_lam,
    closed_appl,    closed_sigma,   closed_pair,    closed_proj1,
    closed_proj2,   closedn_gvar).