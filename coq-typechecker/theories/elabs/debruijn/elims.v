(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.elabs.debruijn Require Import defs.
From sir.elabs Require Import ast.term.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.


Module Rename_Elim.
Lemma rename_var ξ idx: rename ξ (var idx) = var (ξ idx).
Proof.
by [].
Qed.

Lemma rename_univ ξ l: rename ξ (univ l) = univ l.
Proof.
by [].
Qed.

Lemma rename_pi ξ A B:
    rename ξ (pi A B) = pi (rename ξ A) (rename (upren ξ) B).
Proof.
by [].
Qed.

Lemma rename_lam ξ T y:
    rename ξ (lam T y) = lam (rename ξ T) (rename (upren ξ) y).
Proof.
by [].
Qed.

Lemma rename_appl ξ m n:
    rename ξ (appl m n) = appl (rename ξ m) (rename ξ n).
Proof.
by [].
Qed.

Lemma rename_sigma ξ A B:
    rename ξ (sigma A B) = sigma (rename ξ A) (rename (upren ξ) B).
Proof.
by [].
Qed.

Lemma rename_pair ξ A B a b:
    rename ξ (pair A B a b) =
    pair (rename ξ A) (rename (upren ξ) B) (rename ξ a) (rename ξ b).
Proof.
by [].
Qed.

Lemma rename_proj1 ξ m: rename ξ (proj1 m) = proj1 (rename ξ m).
Proof.
by [].
Qed.

Lemma rename_proj2 ξ m: rename ξ (proj2 m) = proj2 (rename ξ m).
Proof.
by [].
Qed.

Lemma rename_gvar ξ n: rename ξ (gvar n) = gvar n.
Proof.
by [].
Qed.

End Rename_Elim.

Import Rename_Elim.

Definition renameE := (
    rename_var, rename_univ, rename_pi,
    rename_lam, rename_appl, rename_sigma,
    rename_pair, rename_proj1, rename_proj2, rename_gvar).

Module Subst_Elim.

Lemma subst_var σ idx: (var idx).[σ] = σ idx.
Proof.
by [].
Qed.

Lemma subst_univ σ l: (univ l).[σ] = univ l.
Proof.
by [].
Qed.

Lemma subst_pi σ A B: (pi A B).[σ] = pi A.[σ] B.[up σ].
Proof.
by [].
Qed.

Lemma subst_lam σ T y: (lam T y).[σ] = lam T.[σ] y.[up σ].
Proof.
by [].
Qed.

Lemma subst_appl σ m n: (appl m n).[σ] = appl m.[σ] n.[σ].
Proof.
by [].
Qed.

Lemma subst_sigma σ A B: (sigma A B).[σ] = sigma A.[σ] B.[up σ].
Proof.
by [].
Qed.

Lemma subst_pair σ A B a b:
    (pair A B a b).[σ] = pair A.[σ] B.[up σ] a.[σ] b.[σ].
Proof.
by [].
Qed.

Lemma subst_proj1 σ m: (proj1 m).[σ] = proj1 m.[σ].
Proof.
by [].
Qed.

Lemma subst_proj2 σ m: (proj2 m).[σ] = proj2 m.[σ].
Proof.
by [].
Qed.

Lemma subst_gvar σ n: (gvar n).[σ] = gvar n.
Proof.
by [].
Qed.

End Subst_Elim.

Import Subst_Elim.
Definition substE := (
    subst_var, subst_univ, subst_pi, subst_lam, subst_appl, subst_sigma,
    subst_pair, subst_proj1, subst_proj2, subst_gvar).
