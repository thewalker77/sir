(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.elabs Require Import ast.term.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Inductive NormalForm : Term -> Prop :=
| NormalForm_univ: forall l, NormalForm (univ l)
| NormalForm_pi A B:
    NormalForm A ->
    NormalForm B ->
    NormalForm (pi A B)
| NormalForm_lam A t:
    NormalForm A ->
    NormalForm t ->
    NormalForm (lam A t)
| NormalForm_neutral t:
    NeutralForm t ->
    NormalForm t
| NormalForm_sigma A B:
    NormalForm A ->
    NormalForm B ->
    NormalForm (sigma A B)
| NormalForm_pair M N m n:
    NormalForm M ->
    NormalForm N ->
    NormalForm m ->
    NormalForm n ->
    NormalForm (pair M N m n)

with NeutralForm: Term -> Prop :=
| NeutralForm_var: forall idx, NeutralForm (var idx)
| NeutralForm_appl: forall f x,
    NeutralForm f ->
    NormalForm x ->
    NeutralForm (appl f x)
| NeutralForm_proj1 p:
    NeutralForm p ->
    NeutralForm (proj1 p)
| NeutralForm_proj2 p:
    NeutralForm p ->
    NeutralForm (proj2 p).

Inductive Value : Term -> Prop :=
| Value_univ: forall l, Value (univ l)
| Value_pi A B:
    Value A ->
    Value B ->
    Value (pi A B)
| Value_lam A t:
    Value A ->
    Value t ->
    Value (lam A t)
| Value_sigma A B:
    Value A ->
    Value B ->
    Value (sigma A B)
| Value_pair M N m n:
    Value M ->
    Value N ->
    Value m ->
    Value n ->
    Value (pair M N m n)
.