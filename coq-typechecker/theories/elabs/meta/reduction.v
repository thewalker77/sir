(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.elabs.ast Require Import term module.
From sir.elabs Require Import debruijn.defs.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Inductive Multi (R: Module -> Term -> Term -> Prop): Module -> Term -> Term -> Prop :=
| Multi_refl: forall Σ t, Multi R Σ t t
| Multi_single: forall Σ t1 t2 t3,
    R Σ t1 t2 ->
    Multi R Σ t2  t3 ->
    Multi R Σ t1  t3.

Reserved Notation "Σ '⊢' x '⟶' y" (at level 20, x at next level, y at next level).
Inductive Reduction: Module ->  Term -> Term -> Prop :=
| Reduction_beta: forall Σ A t n, Σ ⊢ (appl (lam A t) n) ⟶ t.[n/]
| Reduction_pil: forall Σ A A' B, Σ ⊢ A ⟶ A' -> Σ ⊢ (pi A B) ⟶ (pi A' B)
| Reduction_pir: forall Σ A B B', Σ ⊢ B ⟶ B' -> Σ ⊢ (pi A B) ⟶ (pi A B')
| Reduction_laml: forall Σ A A' t, Σ ⊢ A ⟶ A' -> Σ ⊢ (lam A t) ⟶ (lam A' t)
| Reduction_lamr: forall Σ A t t', Σ ⊢ t ⟶ t' -> Σ ⊢ (lam A t) ⟶ (lam A t')
| Reduction_appll: forall Σ m m' n, Σ ⊢ m ⟶ m' -> Σ ⊢ (appl m n) ⟶ (appl m' n)
| Reduction_applr: forall Σ m n n', Σ ⊢ n ⟶ n' -> Σ ⊢ (appl m n) ⟶ (appl m n')
| Reduction_sigmal: forall Σ A A' B, Σ ⊢ A ⟶ A' -> Σ ⊢ (sigma A B) ⟶ (sigma A' B)
| Reduction_sigmar: forall Σ A B B', Σ ⊢ B ⟶ B' -> Σ ⊢ (sigma A B) ⟶ (sigma A B')
| Reduction_pairTl: forall Σ M M' N m n, Σ ⊢ M ⟶ M' -> Σ ⊢ (pair M N m n) ⟶ (pair M' N m n)
| Reduction_pairTr: forall Σ M N N' m n, Σ ⊢ N ⟶ N' -> Σ ⊢ (pair M N m n) ⟶ (pair M N' m n)
| Reduction_pairl: forall Σ M N m m' n, Σ ⊢ m ⟶ m' -> Σ ⊢ (pair M N m n) ⟶ (pair M N m' n)
| Reduction_pairr: forall Σ M N m n n', Σ ⊢ n ⟶ n' -> Σ ⊢ (pair M N m n) ⟶ (pair M N m n')
| Reduction_proj1: forall Σ m m', Σ ⊢ m ⟶ m' -> Σ ⊢ (proj1 m) ⟶ (proj1 m')
| Reduction_proj1_beta: forall Σ M N m n, Σ ⊢ (proj1 (pair M N m n)) ⟶ m
| Reduction_proj2: forall Σ m m', Σ ⊢ m ⟶ m' -> Σ ⊢ (proj2 m) ⟶ (proj2 m')
| Reduction_proj2_beta: forall Σ M N m n, Σ ⊢ (proj2 (pair M N m n)) ⟶ n
| Reduction_gvar: forall Σ n t, mod_get_term Σ n = Some t -> Σ ⊢ gvar n ⟶ t
where "Σ '⊢' t1 '⟶' t2" := (Reduction Σ t1 t2).
Notation "Σ '⊢' x '⟶*' y" :=
    (Multi Reduction Σ x y) (at level 20, x at next level, y at next level).

Tactic Notation "ReductionElim" ident(Σ) ident(t1) ident(t2) :=
elim => {Σ t1 t2} /=
[ Σ t1 t2 n
| Σ A A' B HredA IHA
| Σ A B B' HredB IHB
| Σ A A' t HredA IHA
| Σ A t t' Hredt IHt
| Σ m m' n Hredm IHm
| Σ m n n' Hredn IHn
| Σ A A' B HredA IHA
| Σ A B B' HredB IHB
| Σ M M' N m n HredMM' IHMM'
| Σ M N N' m n HredNN' IHNN'
| Σ M N m m' n Hredm IHm
| Σ M N m n n' Hredn IHn
| Σ m m' Hredm IHm
| Σ M N m n
| Σ m m' Hredm IHm
| Σ M N m n
| Σ n t Hmodget
].

Reserved Notation "Σ '⊢' x '⇒' y" (at level 20, x at next level, y at next level).

Inductive ParallelReduction: Module -> Term -> Term -> Prop :=
| ParallelReduction_var: forall Σ idx, Σ ⊢ var idx ⇒ var idx
| ParallelReduction_univ: forall Σ lvl, Σ ⊢ univ lvl ⇒ univ lvl
| ParallelReduction_appl : forall Σ m n m' n',
    Σ ⊢ m ⇒ m' ->
    Σ ⊢ n ⇒ n' ->
    Σ ⊢ (appl m n) ⇒ (appl m' n')
| ParallelReduction_beta : forall Σ T t n t' n',
    Σ ⊢ t ⇒ t' ->
    Σ ⊢ n ⇒ n' ->
    Σ ⊢ (appl (lam T t) n) ⇒ t'.[n'/]
| ParallelReduction_pi: forall Σ A B A' B',
    Σ ⊢ A ⇒ A' ->
    Σ ⊢ B ⇒ B' ->
    Σ ⊢ pi A B ⇒ pi A' B'
| ParallelReduction_lam: forall Σ A t A' t',
    Σ ⊢ A ⇒ A' ->
    Σ ⊢ t ⇒ t' ->
    Σ ⊢ lam A t ⇒ lam A' t'
| ParallelReduction_sigma: forall Σ A B A' B',
    Σ ⊢ A ⇒ A' ->
    Σ ⊢ B ⇒ B' ->
    Σ ⊢ sigma A B ⇒ sigma A' B'
| ParallelReduction_pair : forall Σ M N m n M' N' m' n',
    Σ ⊢ M ⇒ M' ->
    Σ ⊢ N ⇒ N' ->
    Σ ⊢ m ⇒ m' ->
    Σ ⊢ n ⇒ n' ->
    Σ ⊢ (pair M N m n) ⇒ (pair M' N' m' n')
| ParallelReduction_proj1: forall Σ m m',
    Σ ⊢ m ⇒ m' ->
    Σ ⊢ (proj1 m) ⇒ (proj1 m')
| ParallelReduction_proj1_beta:
    forall Σ M M' N N' m m' n n',
    Σ ⊢ M ⇒ M' ->
    Σ ⊢ N ⇒ N' ->
    Σ ⊢ m ⇒ m' ->
    Σ ⊢ n ⇒ n' ->
    Σ ⊢ (proj1 (pair M N m n)) ⇒ m'
| ParallelReduction_proj2: forall Σ m m',
    Σ ⊢ m ⇒ m' ->
    Σ ⊢ (proj2 m) ⇒ (proj2 m')
| ParallelReduction_proj2_beta:
    forall Σ M M' N N' m m' n n',
    Σ ⊢ M ⇒ M' ->
    Σ ⊢ N ⇒ N' ->
    Σ ⊢ m ⇒ m' ->
    Σ ⊢ n ⇒ n' ->
    Σ ⊢ (proj2 (pair M N m n)) ⇒ n'
| ParallelReduction_gvar_refl: forall Σ n, Σ ⊢ gvar n ⇒ gvar n
| ParallelReduction_gvar_beta: forall Σ n t,
    mod_get_term Σ n = Some t ->
    Σ ⊢ gvar n ⇒ t
where "Σ '⊢' t1 '⇒' t2" := (ParallelReduction Σ t1 t2).

Notation "Σ '⊢' x '⇒*' y" :=
    (Multi ParallelReduction Σ x y) (at level 20, x at next level, y at next level).

Tactic Notation "ParallelReductionElim" ident(Σ) ident(t1) ident(t2) :=
elim => {Σ t1 t2} /=
[ Σ idx
| Σ lvl
| Σ m n m' n' Hpredmm' IHmm' Hprednn' IHnn'
| Σ T t n t' n' Hpredtt' IHtt' Hprednn' IHnn'
| Σ A B A' B' HpredAA' IHAA' HpredBB' IHBB'
| Σ A t A' t' HpredAA' IHAA' Hpredtt' IHtt'
| Σ A B A' B' HpredAA' IHAA' HpredBB' IHBB'
| Σ M N m n M' N' m' n' HpreMM' IHMM' HpredNN' IHNN' Hpredmm' IHmm' Hprednn' IHnn'
| Σ m m' Hpredmm' IHmm'
| Σ M M' N N' m m' n n' HpredMM' IHMM' HpredNN' IHNN' Hpredmm' IHmm' Hprednn' IHnn'
| Σ m m' Hpredmm' IHmm'
| Σ M M' N N' m m' n n' HpredMM' IHMM' HpredNN' IHNN' Hpredmm' IHmm' Hprednn' IHnn'
| Σ n
| Σ n t Hmodget
].

Lemma ParallelReduction_ebeta: forall Σ T t n t' x n',
    Σ ⊢ t ⇒ t' ->
    Σ ⊢ n ⇒ n' ->
    x = t'.[n'/] ->
    Σ ⊢ (appl (lam T t) n) ⇒ x.
Proof.
move => ? ? ? ? ? ? ? ? ? ->.
by apply: ParallelReduction_beta.
Qed.

Definition ParallelReductionSubst Σ σ τ : Prop :=
    forall (x: nat), Σ ⊢ σ x ⇒τ x.
Notation "Σ '⊢' σ '⇒ₛ' τ" := (ParallelReductionSubst Σ σ τ) (at level 20, σ at next level, τ at next level).

Fixpoint ρ (Σ: Module) (t: Term): Term:=
match t with
| var x => var x
| univ l => univ l
| lam A t => lam (ρ Σ A) (ρ Σ t)
| pi A B => pi (ρ Σ A) (ρ Σ B)
| appl (lam A t) n => (ρ Σ t).[ρ Σ n/]
| appl m n => appl (ρ Σ m) (ρ Σ n)
| sigma A B => sigma (ρ Σ A) (ρ Σ B)
| pair M N m n => pair (ρ Σ M) (ρ Σ N) (ρ Σ m) (ρ Σ n)
| proj1 (pair M N m n) => ρ Σ m
| proj1 m => proj1 (ρ Σ m)
| proj2 (pair M N m n) => ρ Σ n
| proj2 m => proj2 (ρ Σ m)
| gvar n => match mod_get_term Σ n with
            | Some t => t
            | None => gvar n
            end
end.
