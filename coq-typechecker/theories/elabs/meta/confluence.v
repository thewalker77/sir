(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.elabs.meta Require Import reduction conversion red_facts.
From sir.elabs.ast Require Import term module facts.
From sir.elabs.debruijn Require Import defs facts.
From sir.common Require Import utils.
Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(* properties of parallel reduction and multi parallel reduction *)
Lemma pred_refl Σ t: Σ ⊢ t ⇒ t.
Proof.
move: t.
TermElim.
-   by apply: ParallelReduction_var.
-   by apply: ParallelReduction_univ.
-   by apply: ParallelReduction_pi.
-   by apply: ParallelReduction_lam.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_sigma.
-   by apply: ParallelReduction_pair.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_gvar_refl.
Qed.

Lemma pred_psubst Σ t t' σ:
    Σ ⊢ t ⇒t' ->
    Σ ⊢ t.[σ] ⇒ t'.[σ].
Proof.
have ? := pred_refl.
move => Hpred.
move : Hpred σ.
ParallelReductionElim Σ t t' => σ /=.
-   by move: idx => [|idx].
-   by [].
-   by apply: ParallelReduction_appl.
-   apply: ParallelReduction_ebeta.
    +   by apply: IHtt'.
    +   by apply: IHnn'.
    +   by asimpl.
-   by apply: ParallelReduction_pi.
-   by apply: ParallelReduction_lam.
-   by apply: ParallelReduction_sigma.
-   by apply: ParallelReduction_pair.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1_beta.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2_beta.
-   by [].
-   apply: ParallelReduction_gvar_beta.
    by move : (mod_get_term_closed Hmodget) => ->.
Qed.

Lemma spred_up Σ σ τ:
    Σ ⊢ σ ⇒ₛ τ ->
    Σ ⊢ up σ ⇒ₛ up τ.
Proof.
move => Hpreds [|x];
    asimpl.
    apply: pred_refl.
by apply: pred_psubst.
Qed.

Lemma pred_spred Σ t t' σ τ:
    Σ ⊢ t ⇒ t' ->
    Σ ⊢ σ ⇒ₛ τ ->
    Σ ⊢ t.[σ] ⇒ t'.[τ].
Proof.
move => Hpred.
move: Hpred σ τ.
ParallelReductionElim Σ t t' => σ τ Hspred;
    have ? := pred_refl;
    rewrite ?substE.
    asimpl.
-   by apply: Hspred.
-   by [].
-   apply: ParallelReduction_appl.
        by apply: IHmm'.
    by apply: IHnn'.
-   apply: ParallelReduction_ebeta.
    +   apply: IHtt'.
        by apply: spred_up.
    +   by apply: IHnn'.
    +   by asimpl.
-   apply: ParallelReduction_pi.
        by apply: IHAA'.
    apply: IHBB'.
    by apply: spred_up.
-   apply: ParallelReduction_lam.
        by apply: IHAA'.
    apply: IHtt'.
    by apply: spred_up.
-   apply: ParallelReduction_sigma.
        by apply: IHAA'.
    apply: IHBB'.
    by apply: spred_up.
-   apply: ParallelReduction_pair.
    +   by apply: IHMM'.
    +   apply: IHNN'.
        by apply: spred_up.
    +   by apply: IHmm'.
    +   by apply: IHnn'.
-   apply: ParallelReduction_proj1.
    by apply: IHmm'.
-   apply: ParallelReduction_proj1_beta.
    +   by apply:IHMM'.
    +   apply: IHNN'.
        by apply: spred_up.
    +   by apply: IHmm'.
    +   by apply: IHnn'.
-   apply: ParallelReduction_proj2.
    by apply: IHmm'.
-   apply: ParallelReduction_proj2_beta.
    +   by apply:IHMM'.
    +   apply: IHNN'.
        by apply: spred_up.
    +   by apply: IHmm'.
    +   by apply: IHnn'.
-   by [].
-   apply: ParallelReduction_gvar_beta.
    by move : (mod_get_term_closed Hmodget) => ->.
Qed.

(* this lemma are recurring cases in pred_subst *)
Lemma spred_up_id Σ n n':
    Σ ⊢ n ⇒ n' ->
    Σ ⊢ up(n .: ids) ⇒ₛ up(n' .: ids).
Proof.
move => Hprednn'.
apply: spred_up.
move => [|x];
    asimpl => //.
by apply: pred_refl.
Qed.

Lemma pred_subst Σ t t' n n':
    Σ ⊢ t ⇒ t' ->
    Σ ⊢ n ⇒ n' ->
    Σ ⊢ t .[n/] ⇒ t'.[n'/].
move => Hpred.
move: Hpred n n'.
ParallelReductionElim Σ t t' => n1 n2 Hpredn12 /=;
    have ? := pred_refl;
    rewrite ?substE.
-   case : idx;
        by asimpl.
-   by [].
-   apply ParallelReduction_appl.
        by apply IHmm'.
    by apply: IHnn'.
-   apply: ParallelReduction_ebeta.
    +   apply: (pred_spred Hpredtt').
        by apply: (@spred_up_id _ _ n2).
    +   by apply: (@IHnn' _ n2).
    +   by asimpl.
-   apply ParallelReduction_pi.
        by apply IHAA'.
    apply: pred_spred => //.
    by apply: spred_up_id.
-   apply ParallelReduction_lam.
        by apply: IHAA'.
    apply: pred_spred => //.
    by apply: spred_up_id.
-   apply: ParallelReduction_sigma.
        by apply: IHAA'.
    apply: pred_spred => //.
    by apply: spred_up_id.
-   apply: ParallelReduction_pair.
    +   by apply: IHMM'.
    +   apply: pred_spred => //.
        by apply: spred_up_id.
    +   by apply: IHmm'.
    +   by apply: IHnn'.
-   apply: ParallelReduction_proj1.
    by apply: IHmm'.
-   apply: ParallelReduction_proj1_beta.
    +    by apply: IHMM'.
    +   by apply: pred_spred.
    +   by apply: IHmm'.
    +   by apply: IHnn'.
-   apply: ParallelReduction_proj2.
    by apply: IHmm'.
-   apply: ParallelReduction_proj2_beta.
    +   by apply: IHMM'.
    +   by apply: pred_spred.
    +   by apply: IHmm'.
    +   by apply: IHnn'.
-   by [].
-   apply: ParallelReduction_gvar_beta.
    by move : (mod_get_term_closed Hmodget) => ->.
Qed.

Lemma mpred_subst_special Σ t n n':
    Σ ⊢ n ⇒* n' ->
    Σ ⊢ t .[n/] ⇒* t.[n'/].
Proof.
elim => {Σ n n'} [Σ n|Σ n1 n2 n3 Hpred12 Hmpred23 IH].
    by apply: Multi_refl.
apply: Multi_single;
    last by apply: IH.
apply: pred_subst => //.
by apply: pred_refl.
Qed.

Lemma mpred_subst Σ t t' n n':
    Σ ⊢ t ⇒* t' ->
    Σ ⊢ n ⇒* n' ->
    Σ ⊢ t .[n/] ⇒* t'.[n'/].
Proof.
elim => {Σ t t'} [Σ t|Σ t1 t2 t3 Hpred12 Hmpred23 IH] Hmprednn'.
    by apply: mpred_subst_special.
apply: Multi_single;
    last by apply: IH.
apply: pred_subst => //.
by apply: pred_refl.
Qed.

Lemma pred_triangle_appl Σ m m' n n':
    Σ ⊢ m ⇒ m' ->
    Σ ⊢ m' ⇒ ρ Σ m ->
    Σ ⊢ n ⇒ n' ->
    Σ ⊢ n' ⇒ ρ Σ n ->
    Σ ⊢ appl m' n' ⇒ ρ Σ (appl m n).
Proof.
move : m m' n n'.
TermElim => m' n1 n2 Hpredmm' Hpredρmm' Hpredn12 Hpredρn12.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_appl.
-   inversion Hpredmm'; subst.
    inversion Hpredρmm'; subst.
    by apply: ParallelReduction_beta.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_appl.
Qed.

Lemma pred_triangle_proj1 Σ m m':
    Σ ⊢ m ⇒ m' ->
    Σ ⊢ m' ⇒ ρ Σ m ->
    Σ ⊢ proj1 m' ⇒ ρ Σ (proj1 m).
Proof.
move: m m'.
TermElim => m' Hpredmm' Hpredρmm'.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1.
-   inversion Hpredmm'; subst.
    inversion Hpredρmm'; subst.
    by apply: (@ParallelReduction_proj1_beta _ _ (ρ Σ M) _ (ρ Σ N) _ _ _ (ρ Σ n)).
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1.
Qed.

Lemma pred_triangle_proj2 Σ m m':
    Σ ⊢ m ⇒ m' ->
    Σ ⊢ m' ⇒ ρ Σ m ->
    Σ ⊢ proj2 m' ⇒ ρ Σ (proj2 m).
Proof.
move: m m'.
TermElim => m' Hpredmm' Hpredρmm'.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2.
-   inversion Hpredmm'; subst.
    inversion Hpredρmm'; subst.
    by apply: (@ParallelReduction_proj2_beta _ _ (ρ Σ M) _ (ρ Σ N)_ (ρ Σ m)).
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2.
Qed.

Lemma pred_triangle Σ t t':
    Σ ⊢ t ⇒ t' ->
    Σ ⊢ t' ⇒ ρ Σ t.
Proof.
have ? := pred_refl.
ParallelReductionElim Σ t t'.
-   by [].
-   by [].
-   by apply: pred_triangle_appl.
-   by apply: pred_subst.
-   by apply: ParallelReduction_pi.
-   by apply: ParallelReduction_lam.
-   by apply: ParallelReduction_sigma.
-   by apply: ParallelReduction_pair.
-   by apply: pred_triangle_proj1.
-   by [].
-   by apply: pred_triangle_proj2.
-   by [].
-   move Hget: (mod_get_term Σ n) => [t|//].
    by apply: ParallelReduction_gvar_beta.
-   by rewrite Hmodget.
Qed.

Lemma pred_diamond Σ t t1 t2:
    Σ ⊢ t ⇒ t1 ->
    Σ ⊢ t ⇒ t2 ->
    exists t',
    Σ ⊢ t1 ⇒ t' /\ Σ ⊢ t2 ⇒ t'.
Proof.
move => /pred_triangle Hpredtt1 /pred_triangle Hpredtt2.
by exists (ρ Σ t).
Qed.

Lemma pred_confluence_helper Σ t1 t2 t3:
    Σ ⊢ t1 ⇒* t2 ->
    Σ ⊢ t1 ⇒ t3 ->
    exists t',
    Σ ⊢ t2 ⇒ t' /\ Σ ⊢ t3 ⇒* t'.
Proof.
move => Hred.
have ? := Multi_refl.
elim : Hred t3 => {Σ t1 t2} [Σ t1|Σ  t1 t2 t3 Hpred12 Hpred23 IH] t4 Hpred14.
    by exists t4.
move : (pred_diamond Hpred12 Hpred14) => [t5 [Hpred25 Hpred45]].
move : (IH _ Hpred25) => [t6 [Hpred36 Hpred56]].
exists t6.
split => //.
by apply: (@Multi_single _ _ _ t5).
Qed.

Lemma pred_confluence Σ t1 t2 t3:
    Σ ⊢ t1 ⇒* t2 ->
    Σ ⊢ t1 ⇒* t3 ->
    exists t',
    Σ ⊢ t2 ⇒* t' /\ Σ ⊢ t3 ⇒* t'.
Proof.
move => Hred.
have ? := Multi_refl.
elim : Hred t3 => {Σ t1 t2} [Σ t1|Σ  t1 t2 t3 Hpred12 Hpred23 IH] t4 Hpred14.
    by exists t4.
move : (pred_confluence_helper Hpred14 Hpred12) => [t5 [Hpred45 Hmpred25]].
move : (IH _ Hmpred25) => [t6 [Hmpred36 Hmpred56]].
exists t6.
split => //.
by apply: (@Multi_single _ _ _ t5).
Qed.

(* ParallelReduction, MultiReduction, & Reduction *)
Lemma red_pred Σ t t':
    Σ ⊢ t ⟶ t' ->
    Σ ⊢ t ⇒ t'.
Proof.
have ? := pred_refl.
ReductionElim Σ t t'.
-   by apply: ParallelReduction_beta.
-   by apply: ParallelReduction_pi.
-   by apply: ParallelReduction_pi.
-   by apply: ParallelReduction_lam.
-   by apply: ParallelReduction_lam.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_sigma.
-   by apply: ParallelReduction_sigma.
-   by apply: ParallelReduction_pair.
-   by apply: ParallelReduction_pair.
-   by apply: ParallelReduction_pair.
-   by apply: ParallelReduction_pair.
-   by apply: ParallelReduction_proj1.
-   by apply: ParallelReduction_proj1_beta.
-   by apply: ParallelReduction_proj2.
-   by apply: ParallelReduction_proj2_beta.
-   by apply: ParallelReduction_gvar_beta.
Qed.

Lemma pred_mred Σ t t':
    Σ ⊢ t ⇒ t' ->
    Σ ⊢ t ⟶* t'.
Proof.
ParallelReductionElim Σ t t'.
-   by apply: Multi_refl.
-   by apply: Multi_refl.
-   by apply: mred_trans_appl.
-   by apply: mred_trans_appl_beta.
-   by apply: mred_trans_pi.
-   by apply: mred_trans_lam.
-   by apply: mred_trans_sigma.
-   by apply: mred_trans_pair.
-   by apply: mred_trans_proj1.
-   by apply: mred_trans_proj1_beta.
-   by apply: mred_trans_proj2.
-   by apply: mred_trans_proj2_beta.
-   by apply: Multi_refl.
-   apply: Multi_single;
        last by apply: Multi_refl.
    by apply: Reduction_gvar.
Qed.

Lemma mred_mpred1 Σ t t':
    Σ ⊢ t ⟶* t' ->
    Σ ⊢ t ⇒* t'.
Proof.
elim => {Σ t t'} => [Σ t|Σ t1 t2 t3].
    by apply: Multi_refl.
move => /red_pred Hpred12 Hred23 Hmpred23.
by apply: (@Multi_single _ _ _ t2).
Qed.

Lemma mred_mpred2 Σ t t':
    Σ ⊢ t ⇒* t' ->
    Σ ⊢ t ⟶* t'.
Proof.
elim => {Σ t t'} => [Σ t|Σ t1 t2 t3].
    by apply: Multi_refl.
move => /pred_mred Hmred12 Hpred23 Hmred23.
by apply: (@multi_trans _ _ _ t2).
Qed.

Lemma mred_mpred_iff Σ t t':
    Σ ⊢ t ⇒* t' <->
    Σ ⊢ t ⟶* t'.
split.
    by apply: mred_mpred2.
by apply: mred_mpred1.
Qed.

Lemma mpred_pred Σ t t':
    Σ ⊢ t ⇒ t' ->
    Σ ⊢ t ⇒* t'.
Proof.
have ? := Multi_refl.
ParallelReductionElim Σ t t' => //;
    (apply: Multi_single;
        last by apply: Multi_refl).
-   by apply: ParallelReduction_appl.
-   by apply: ParallelReduction_beta.
-   by apply: ParallelReduction_pi.
-   by apply: ParallelReduction_lam.
-   by apply: ParallelReduction_sigma.
-   by apply: ParallelReduction_pair.
-   by apply: ParallelReduction_proj1.
-   by apply: (@ParallelReduction_proj1_beta _ _ M' _ N' _ _ _ n').
-   by apply: ParallelReduction_proj2.
-   by apply: (@ParallelReduction_proj2_beta _ _ M' _ N' _ m').
-   by apply: ParallelReduction_gvar_beta.
Qed.

Lemma mred_confluence Σ t1 t2 t:
    Σ ⊢ t ⟶* t1 ->
    Σ ⊢ t ⟶* t2 ->
    exists t',
    Σ ⊢ t1 ⟶* t' /\ Σ ⊢ t2 ⟶* t'.
Proof.
rewrite -!mred_mpred_iff => Hmpred1 Hmpred2.
have := (pred_confluence Hmpred1 Hmpred2).
move => [t3 [? ?]].
exists t3.
by rewrite -!mred_mpred_iff.
Qed.


(* relationship between conversion and reduction *)
Lemma red_conv Σ A A':
    Σ ⊢ A ⟶ A' ->
    Σ ⊢ A ≅ A'.
Proof.
have ? := Conv_refl.
ReductionElim Σ A A'.
-   by apply: Conv_beta.
-   by apply: Conv_pi.
-   by apply: Conv_pi.
-   by apply: Conv_lam.
-   by apply: Conv_lam.
-   by apply: Conv_appl.
-   by apply: Conv_appl.
-   by apply: Conv_sigma.
-   by apply: Conv_sigma.
-   by apply: Conv_pair.
-   by apply: Conv_pair.
-   by apply: Conv_pair.
-   by apply: Conv_pair.
-   by apply: Conv_proj1.
-   by apply: Conv_proj1_beta.
-   by apply: Conv_proj2.
-   by apply: Conv_proj2_beta.
-   by apply: Conv_gvar.
Qed.

Lemma mred_conv Σ A A':
    Σ ⊢ A ⟶* A' ->
    Σ ⊢ A ≅ A'.
Proof.
elim => {Σ A A'} [Σ A|Σ t1 t2 t3 /red_conv Hconv12 Hpred23 Hconv23].
    by apply: Conv_refl.
by apply: (@Conv_trans _ _ t2).
Qed.

(* Church Rosser Property *)
Lemma mred_cr Σ A A':
    Σ ⊢ A ≅ A' ->
    exists B, Σ ⊢ A ⟶* B /\ Σ ⊢ A' ⟶* B.
Proof.
have ? := Multi_refl Reduction.
ConvElim Σ A A'.
-   rewrite Heqtt'.
    exists t.[u/].
    split => //.
    apply: (Multi_single (t2 :=  t.[u/])) => //.
    by apply: Reduction_beta.
-   by exists t.
-   move: Hconvtt' => [B [? ?]].
    by exists B.
-   move : IH12 IH23 => [t4 [Hmred14 Hmred24]] [t5 [Hmred25 Hmred35]].
    move : (mred_confluence Hmred24 Hmred25) => [t6 [Hred46 Hred56]].
    exists t6.
    split.
        by apply (multi_trans (T2 := t4)).
    by apply: (multi_trans (T2 := t5)).
-   move: IHA IHB => [C [HmredAC HmredA'C]] [D [HmredBD HmredB'D]].
    exists (pi C D).
    split;
    by apply: mred_trans_pi.
-   move: IHA IHt => [C [HmredAC HmredA'C]] [d [Hmredtd Hmredt'd]].
    exists (lam C d).
    split;
        by apply: mred_trans_lam.
-   move: IHm IHn => [o [Hmredmo Hmredm'o]] [p [Hmrednp Hmredn'p]].
    exists (appl o p).
    split;
        by apply: mred_trans_appl.
-   move: IHA IHB => [A1 [HmredAA1 HmredA'A1]] [B1 [HmredBB1 HmredB'B1]].
    exists (sigma A1 B1).
    split;
        by apply: mred_trans_sigma.
-   move: IHMM' => [P [HmredNP HmredM'P]].
    move: IHNN' => [Q [HmredNQ HmredN'Q]].
    move: IHm  => [p [Hmredmp Hmredm'p]].
    move: IHn => [q [Hmrednq Hmredn'q]].
    exists (pair P Q p q).
    split;
        by apply: mred_trans_pair.
-   move: IHm => [n [IHmn IHm'n]].
    exists (proj1 n).
    split;
        by apply: mred_trans_proj1.
-   move: IHm => [n [IHmn IHm'n]].
    exists (proj2 n).
    split;
        by apply: mred_trans_proj2.
-   exists m.
    split => //.
    apply: (Multi_single (t2 :=  m)) => //.
    by apply: Reduction_proj1_beta.
-   exists n.
    split => //.
    apply: (Multi_single (t2 :=  n)) => //.
    by apply: Reduction_proj2_beta.
-   exists t.
    split => //.
    apply: Multi_single;
        last by apply: Multi_refl.
    by apply: Reduction_gvar.
Qed.

Lemma conv_mred_iff' Σ A A' B:
    Σ ⊢ A ⟶* B -> Σ ⊢ A' ⟶* B ->
    Σ ⊢ A ≅ A'.
Proof.
move => /mred_conv HconvAB /mred_conv HconvA'B.
apply: Conv_trans.
    by apply: HconvAB.
by apply: Conv_symm.
Qed.

Lemma conv_mred_iff Σ A A':
    Σ ⊢ A ≅ A' <->
    (exists B, Σ ⊢ A ⟶* B /\ Σ ⊢ A' ⟶* B).
Proof.
split.
    by apply: mred_cr.
move => [? []].
by apply: conv_mred_iff'.
Qed.

Lemma mred_subst Σ t t' n n':
    Σ ⊢ t ⟶* t' ->
    Σ ⊢ n ⟶* n' ->
    Σ ⊢ t .[n/] ⟶* t'.[n'/].
Proof.
rewrite -!mred_mpred_iff.
by apply: mpred_subst.
Qed.

Lemma conv_gen_subst Σ t t' n n':
    Σ ⊢ t ≅ t' ->
    Σ ⊢ n ≅ n' ->
    Σ ⊢ t .[n/] ≅ t'.[n'/].
rewrite !conv_mred_iff.
move => [t1 [Hmredtt1 Hmredt't1]] [n1 [Hmrednn1 Hmredn'n1]].
exists t1.[n1/].
split;
    by apply: mred_subst.
Qed.