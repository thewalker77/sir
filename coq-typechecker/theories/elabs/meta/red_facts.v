(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.elabs.meta Require Import reduction conversion.
From sir.elabs Require Import ast.term.
From sir.elabs.debruijn Require Import defs.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(* inversion principle for terms/types under Reduction*)
Lemma red_inv_var Σ x t': Σ ⊢ var x ⟶ t' -> t' = var x.
move Heq: (var x) => t Hred.
by case: Hred Heq.
Qed.

Lemma red_inv_univ Σ l t': Σ ⊢ univ l ⟶ t' -> t' = univ l.
Proof.
move Heq: (univ l) => t Hred.
by case: Hred Heq.
Qed.

Lemma red_inv_pi Σ A B t':
    Σ ⊢ pi A B ⟶ t' ->
    (exists A', t' = pi A' B /\ (Σ ⊢ A ⟶ A')) \/
    (exists B', t' = pi A B' /\ (Σ ⊢ B ⟶ B')).
Proof.
move => Hred.
inversion Hred;
    subst.
-   left.
    by exists A'.
-   right.
    by exists B'.
Qed.

Lemma red_inv_lam Σ A t f:
    Σ ⊢ lam A t ⟶ f ->
    (exists A', f = lam A' t /\ (Σ ⊢ A ⟶ A')) \/
    (exists t', f = lam A t' /\ (Σ ⊢ t ⟶ t')).
Proof.
move => Hred.
inversion Hred;
    subst.
-   left.
    by exists A'.
-   right.
    by exists t'.
Qed.

Lemma red_inv_appl Σ m n v:
    Σ ⊢ appl m n ⟶ v ->
    (exists m', v = appl m' n /\ Σ ⊢ m ⟶ m') \/
    (exists n', v = appl m n' /\ Σ ⊢ n ⟶ n') \/
    (exists A t, m = (lam A t) /\ v = t.[n/] /\ Σ ⊢ appl m n ⟶ t.[n/]).
Proof.
move => Hred.
inversion Hred;
    subst.
-   repeat right.
    by exists A, t.
-   left.
    by exists m'.
-   right.
    left.
    by exists n'.
Qed.

Lemma red_inv_sigma Σ A B t':
    Σ ⊢ sigma A B ⟶ t' ->
    (exists A', t' = sigma A' B /\ (Σ ⊢ A ⟶ A')) \/
    (exists B', t' = sigma A B' /\ (Σ ⊢ B ⟶ B')).
Proof.
move => Hred.
inversion Hred;
    subst.
-   left.
    by exists A'.
-   right.
    by exists B'.
Qed.

Lemma red_inv_pair Σ M N m n t':
    Σ ⊢ pair M N m n ⟶ t' ->
    (exists M', t' = pair M' N m n /\ Σ ⊢ M ⟶ M') \/
    (exists N', t' = pair M N' m n /\ Σ ⊢ N ⟶ N') \/
    (exists m', t' = pair M N m' n /\ Σ ⊢ m ⟶ m') \/
    (exists n', t' = pair M N m n' /\ Σ ⊢ n ⟶ n').
Proof.
move => Hred.
inversion Hred;
    subst.
-   left.
    by exists M'.
-   right.
    left.
    by exists N'.
-   right.
    right.
    left.
    by exists m'.
-   repeat right.
    by exists n'.
Qed.

Lemma red_inv_proj1 Σ m t:
    Σ ⊢ proj1 m ⟶ t ->
    (exists m', t = proj1 m' /\ Σ ⊢ m ⟶ m') \/
    (exists M N m1 m2, m = (pair M N m1 m2) /\ t = m1 /\ Σ ⊢ proj1 m ⟶ m1).
Proof.
move => Hred.
inversion Hred;
    subst.
-   left.
    by exists m'.
-   right.
    by exists M, N, t, n.
Qed.

Lemma red_inv_proj2 Σ m t:
    Σ ⊢ proj2 m ⟶ t ->
    (exists m', t = proj2 m' /\ Σ ⊢ m ⟶ m') \/
    (exists M N m1 m2, m = (pair M N m1 m2) /\ t = m2 /\ Σ ⊢ proj2 m ⟶ m2).
Proof.
move => Hred.
inversion Hred;
    subst.
-   left.
    by exists m'.
-   right.
    by exists M, N, m0, t.
Qed.

(* properties of Multi *)
Lemma multi_trans R Σ T1 T2 T3:
    Multi R Σ T1 T2 ->
    Multi R Σ T2 T3 ->
    Multi R Σ T1 T3.
Proof.
elim => {Σ T1 T2} => [Σ T1 T2|Σ T1 T2 T4].
    by [].
move =>  Hred12 Hmred24 Hmred4323 Hmred43.
apply: (Multi_single (t2 := T2)) => //.
by apply: Hmred4323.
Qed.

(* properties of Multi Reduction*)
Lemma mred_trans_laml Σ T T' t:
    Σ ⊢ T ⟶* T' ->
    Σ ⊢ lam T t ⟶* lam T' t.
Proof.
elim => {Σ T T'} => [Σ T|Σ T1 T2 T3].
    by apply: Multi_refl.
move => Hred12 Hmred23 IH.
apply: (Multi_single (t2 :=lam T2 t)) => //.
by apply: Reduction_laml.
Qed.

Lemma mred_trans_lamr Σ T t t':
    Σ ⊢ t ⟶* t' ->
    Σ ⊢ lam T t ⟶* lam T t'.
Proof.
elim => {Σ t t'} => [Σ t|Σ t1 t2 t3].
    by apply: Multi_refl.
move => Hred12 Hmred23 IH.
apply: (Multi_single (t2 := lam T t2)) => //.
by apply: Reduction_lamr.
Qed.

Lemma mred_trans_lam Σ T T' t t':
    Σ ⊢ T ⟶* T' ->
    Σ ⊢ t ⟶* t' ->
    Σ ⊢ lam T t ⟶* lam T' t'.
Proof.
elim => {Σ T T'} => [Σ T|Σ T1 T2 T3].
    by apply: mred_trans_lamr.
move => Hred12 Hmred23 IH Hmredn.
apply: Multi_single;
    last by apply: IH.
by apply: Reduction_laml.
Qed.

Lemma mred_trans_appll Σ m m' n:
    Σ ⊢ m ⟶* m' ->
    Σ ⊢ appl m n ⟶* appl m' n.
Proof.
elim => {Σ m m'} => [Σ m|Σ m1 m2 m3].
    by apply: Multi_refl.
move => Hred12 Hmred23 IH.
apply: (Multi_single (t2 := appl m2 n)) => //.
by apply: Reduction_appll.
Qed.

Lemma mred_trans_applr Σ m n n':
    Σ ⊢ n ⟶* n' ->
    Σ ⊢ appl m n ⟶* appl m n'.
Proof.
elim => {Σ n n'} => [Σ n|Σ n1 n2 n3].
    by apply: Multi_refl.
move => Hred12 Hmred23 IH.
apply: (Multi_single (t2 := appl m n2)) => //.
by apply: Reduction_applr.
Qed.

Lemma mred_trans_appl Σ m m' n n':
    Σ ⊢ m ⟶* m' ->
    Σ ⊢ n ⟶* n' ->
    Σ ⊢ appl m n ⟶* appl m' n'.
Proof.
elim => {Σ m m'} [Σ m|Σ m1 m2 m3].
    by apply: mred_trans_applr.
move => Hred12 Hmred23 IH Hmredn.
apply: Multi_single;
    last by apply: IH.
by apply: Reduction_appll.
Qed.

Lemma mred_trans_appl_beta Σ T t n t' n':
    Σ ⊢ t ⟶* t' ->
    Σ ⊢ n ⟶* n' ->
    Σ ⊢ appl (lam T t) n ⟶* t'.[n'/].
Proof.
have ? := Multi_refl.
move => Hmredtt' Hmrednn'.
apply: (@multi_trans _ _ _ (appl (lam T t') n')).
    apply: mred_trans_appl => //.
    by apply: mred_trans_lam.
apply: (Multi_single (t2:= t'.[n'/])) => //.
by apply: Reduction_beta.
Qed.

Lemma mred_trans_pil Σ A A' B:
    Σ ⊢ A ⟶* A' ->
    Σ ⊢ pi A B ⟶* pi A' B.
Proof.
elim => {Σ A A'} => [Σ A|Σ A1 A2 A3].
    by apply: Multi_refl.
move => Hred12 Hmred23 IH.
apply: (Multi_single (t2:= pi A2 B)) => //.
by apply: Reduction_pil.
Qed.

Lemma mred_trans_pir Σ A B B':
    Σ ⊢ B ⟶* B' ->
    Σ ⊢ pi A B ⟶* pi A B'.
Proof.
elim => {Σ B B'} => [Σ B|Σ B1 B2 B3].
    by apply: Multi_refl.
move => Hred12 Hmred23 IH.
apply: (Multi_single (t2 := pi A B2)) => //.
by apply: Reduction_pir.
Qed.

Lemma mred_trans_pi Σ A A' B B':
    Σ ⊢ A ⟶* A' ->
    Σ ⊢ B ⟶* B' ->
    Σ ⊢ pi A B ⟶* pi A' B'.
Proof.
elim => {Σ A A'} => [Σ A|Σ A1 A2 A3].
    by apply: mred_trans_pir.
move => Hred12 Hmred23 IH Hmredn.
apply: Multi_single;
    last by apply: IH.
by apply: Reduction_pil.
Qed.

Lemma mred_trans_sigmar Σ A B B':
    Σ ⊢ B ⟶* B' ->
    Σ ⊢ sigma A B ⟶* sigma A B'.
Proof.
elim => {Σ B B'} => [Σ B|Σ B1 B2 B3].
    by apply: Multi_refl.
move => Hred12 Hmred23 IH.
apply: (Multi_single (t2 := sigma A B2)) => //.
by apply: Reduction_sigmar.
Qed.

Lemma mred_trans_sigma Σ A B A' B':
    Σ ⊢ A ⟶* A' ->
    Σ ⊢ B ⟶* B' ->
    Σ ⊢ sigma A B ⟶* sigma A' B'.
Proof.
elim => {Σ A A'} => [Σ A|Σ A1 A2 A3].
    by apply: mred_trans_sigmar.
move => Hred12 Hmred23 IH HmredB.
apply: Multi_single;
    last by apply: IH.
by apply: Reduction_sigmal.
Qed.

Lemma mred_trans_pair Σ M M' N N' m n m' n':
    Σ ⊢ M ⟶* M' ->
    Σ ⊢ N ⟶* N' ->
    Σ ⊢ m ⟶* m' ->
    Σ ⊢ n ⟶* n' ->
    Σ ⊢ pair M N m n ⟶* pair M' N' m' n'.
Proof.
elim => {Σ M M'} => [Σ M|Σ M1 M2 M3];
    last first.
    move => Hred12 Hmred23 IH HmredN Hmredm Hmredn.
    apply: Multi_single;
        last by apply: IH.
    by apply: Reduction_pairTl.
elim => {Σ N N'} => [Σ N|Σ N1 N2 N3];
    last first.
    move => Hred12 Hmred23 IH Hmredm Hmredn.
    apply: Multi_single;
        last by apply: IH.
    by apply: Reduction_pairTr.
elim => {Σ m m'} => [Σ m|Σ m1 m2 m3];
    last first.
    move => Hred12 Hmred23 IH Hmredn.
    apply: Multi_single;
        last by apply: IH.
    by apply: Reduction_pairl.
elim => {Σ n n'} => [Σ n|Σ n1 n2 n3].
    by apply: Multi_refl.
move => Hred12 Hmred23 IH.
apply: Multi_single;
    last by apply: IH.
by apply: Reduction_pairr.
Qed.

Lemma mred_trans_proj1 Σ m m':
    Σ ⊢ m ⟶* m' ->
    Σ ⊢ proj1 m ⟶* proj1 m'.
Proof.
elim => {Σ m m'} => [Σ m|Σ m1 m2 m3].
    by apply: Multi_refl.
move => Hred12 Hmred23 IH.
apply: Multi_single;
    last by apply: IH.
by apply: Reduction_proj1.
Qed.

Lemma mred_trans_proj2 Σ m m':
    Σ ⊢ m ⟶* m' ->
    Σ ⊢ proj2 m ⟶* proj2 m'.
Proof.
elim => {Σ m m'} => [Σ m|Σ m1 m2 m3].
    by apply: Multi_refl.
move => Hred12 Hmred23 IH.
apply: Multi_single;
    last by apply: IH.
by apply: Reduction_proj2.
Qed.

Lemma mred_trans_proj1_beta Σ M N m n m':
    Σ ⊢ m ⟶* m' ->
    Σ ⊢ proj1 (pair M N m n) ⟶* m'.
Proof.
elim => {Σ m m'} => [Σ m|Σ m1 m2 m3].
    apply: Multi_single.
        apply: Reduction_proj1_beta.
    by apply: Multi_refl.
move => Hred12 Hmred23 IH.
apply: Multi_single;
    last by apply: IH.
apply: Reduction_proj1.
by apply: Reduction_pairl.
Qed.

Lemma mred_trans_proj2_beta Σ M N m n n':
    Σ ⊢ n ⟶* n' ->
    Σ ⊢ proj2 (pair M N m n) ⟶* n'.
Proof.
elim => {Σ n n'} => [Σ n|Σ n1 n2 n3].
    apply: Multi_single.
        apply: Reduction_proj2_beta.
    by apply: Multi_refl.
move => Hred12 Hmred23 IH.
apply: Multi_single;
    last by apply: IH.
apply: Reduction_proj2.
by apply: Reduction_pairr.
Qed.

(* inversion principles for types under Multi Reduction*)
Lemma mred_inv_var Σ x t': Σ ⊢ var x ⟶* t' -> t' = var x.
Proof.
move Heq: (var x) => t Hmred.
elim: Hmred Heq => {Σ} //.
move => Σ t1 t2 t3 + + + Heq.
move: Heq => <-.
move => /red_inv_var -> Hmred3 IH.
by apply: IH.
Qed.

Lemma mred_inv_univ Σ l t': Σ ⊢ univ l ⟶* t' -> t' = univ l.
Proof.
move Heq: (univ l) => t Hmred.
elim: Hmred Heq => {Σ}//.
move => Σ t1 t2 t3 + + + Heq.
move: Heq => <-.
move => /red_inv_univ -> Hmred3 IH.
by apply: IH.
Qed.

Lemma mred_inv_pi Σ  A B t':
    Σ ⊢ pi A B ⟶* t' ->
    exists A' B', t' = pi A' B' /\ Σ ⊢ A ⟶* A' /\ Σ ⊢ B ⟶* B'.
Proof.
move Heq : (pi A B) => t.
move => Hmred.
elim : Hmred A B Heq => {Σ t t'} [Σ t|Σ t1 t2 t3 Hred12 Hmred23 IH] A B Heq.
    move : Heq => <-.
    exists A, B.
    by have ? := (Multi_refl Reduction).
move : Heq Hred12 => <-.
clear t1.
move /red_inv_pi => [[A2 [/esym Heqt2 HredAA2]]|[B2 [/esym Heqt2 HredBB2]]];
    move : (IH _ _ Heqt2) => [A3 [B3 [->]]].
    move => [HmredA23 HmredBB3].
    exists A3, B3.
    repeat split => //.
    by apply: (Multi_single (t2 := A2)).
move => [HmredAA3 HmredB23].
exists A3, B3.
repeat split => //.
by apply: (Multi_single (t2 := B2)).
Qed.

Lemma mred_inv_sigma Σ A B t':
    Σ ⊢ sigma A B ⟶* t' ->
    exists A' B', t' = sigma A' B' /\ Σ ⊢ A ⟶* A' /\ Σ ⊢ B ⟶* B'.
Proof.
move Heq : (sigma A B) => t.
move => Hmred.
elim : Hmred A B Heq => {Σ t t'} [Σ t|Σ t1 t2 t3 Hred12 Hmred23 IH] A B Heq.
    move : Heq => <-.
    exists A, B.
    by have ? := (Multi_refl Reduction).
move : Heq Hred12 => <-.
clear t1.
move /red_inv_sigma => [[A2 [/esym Heqt2 HredAA2]]|[B2 [/esym Heqt2 HredBB2]]];
    move : (IH _ _ Heqt2) => [A3 [B3 [->]]].
    move => [HmredA23 HmredBB3].
    exists A3, B3.
    repeat split => //.
    by apply: (Multi_single (t2 := A2)).
move => [HmredAA3 HmredB23].
exists A3, B3.
repeat split => //.
by apply: (Multi_single (t2 := B2)).
Qed.