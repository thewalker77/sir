(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.elabs.ast Require Import term module.
From sir.elabs Require Import context meta.conversion.
From sir.elabs.debruijn Require Import defs facts.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(* Syntactic Typing rules optimized for proving metatheoritic properties      *)
Reserved Notation "Σ ';' Γ '⊢' t : T" (at level 20, Γ at next level, t at next level, T at next level).
Reserved Notation "'⊢' Σ ';' Γ" (at level 20, Σ at next level, Γ at next level).
Reserved Notation "'⊢' Σ" (at level 20, Σ at next level).
Inductive SynType: Module -> Context -> Term -> Term -> Prop :=
| SynType_var:  forall Σ Γ idx T,
                dget Γ idx = Some T ->
                Σ; Γ ⊢ (var idx) : T
| SynType_univ: forall Σ Γ l,
                Σ; Γ ⊢ univ l : univ (S l)
| SynType_pi: forall Σ Γ A B i j,
                Σ; Γ ⊢ A: univ i ->
                Σ; (A::Γ) ⊢ B: univ j ->
                Σ; Γ ⊢ pi A B: univ(maxn i j)
| SynType_lam: forall Σ Γ A B t l,
                Σ; Γ ⊢ A : univ l ->
                Σ; (A :: Γ) ⊢ t: B ->
                Σ; Γ ⊢ lam A t : pi A B
| SynType_appl: forall Σ Γ A B m n,
                Σ; Γ ⊢ m : pi A B ->
                Σ; Γ ⊢ n : A ->
                Σ; Γ ⊢ appl m n : B.[n/]
| SynType_sigma: forall Σ Γ A B i j,
                 Σ; Γ ⊢ A: univ i ->
                 Σ; (A::Γ) ⊢ B: univ j ->
                 Σ; Γ ⊢ sigma A B: univ(maxn i j)
| SynType_pair: forall Σ Γ m n A B i j,
                Σ; Γ ⊢ A: univ i ->
                Σ; (A::Γ) ⊢ B: univ j ->
                Σ; Γ ⊢ m : A ->
                Σ; Γ ⊢ n: B.[m/] ->
                Σ; Γ ⊢ pair A B m n : sigma A B
| SynType_proj1: forall Σ Γ m A B,
                    Σ; Γ ⊢ m : sigma A B ->
                    Σ; Γ ⊢ proj1 m : A
| SynType_proj2: forall Σ Γ m A B,
                    Σ; Γ ⊢ m : sigma A B ->
                    Σ; Γ ⊢ proj2 m : B.[proj1 m /]
| SynType_conv: forall Σ Γ T T' t,
                Σ ; Γ ⊢ t: T ->
                Σ ⊢ T ≅ T' ->
                Σ ; Γ ⊢ t: T'
| SynType_gvar: forall Σ Γ T name,
                mod_get_type Σ name = Some T ->
                Σ; Γ ⊢ gvar name : T
where "Σ ';' Γ '⊢' t : T" := (SynType Σ Γ t T)
with
LocalWF: Module -> Context -> Prop :=
| LocalWF_empty: forall Σ,
                    ⊢ Σ ->
                    ⊢ Σ; [::]
| LocalWF_cons: forall Σ Γ T l,
                    ⊢ Σ; Γ ->
                    Σ; Γ ⊢ T: (univ l) ->
                    ⊢ Σ; (T:: Γ)

where "'⊢' Σ ';' Γ" := (LocalWF Σ Γ)
with
GlobalWF: Module -> Prop :=
| GlobalWF_empty: ⊢ [::]
| GlobalWF_cons: forall Σ name t T ct cT,
                    mod_get Σ name = None ->
                    Σ; [::] ⊢ t : T ->
                    ⊢ Σ ->
                    ⊢ ((@module_local name t T ct cT):: Σ)
where "'⊢' Σ" := (GlobalWF Σ).

Tactic Notation "SynTypeElim" ident(Σ) ident(Γ) ident(t) ident(T):=
elim => {Σ Γ t T} => /=
[ Σ Γ idx T Hdget
| Σ Γ lvl
| Σ Γ A B i j HtypeA IHA HtypeB IHB
| Σ Γ A B t l HtypeA IHA Htypet IHt
| Σ Γ A B m n Htypem IHm Htypen IHn
| Σ Γ A B i j HtypeA IHA HtypeB IHB
| Σ Γ m n A B i j HtypeA IHA HtypeB IHB Htypem IHm Htypen IHn
| Σ Γ m A B Htypem IHm
| Σ Γ m A B Htypem IHm
| Σ Γ T T' t HtypeT IHT HconvTT'
| Σ Γ T name Hget
].

Lemma SynType_eappl Σ Γ A B T m n:
    Σ ; Γ ⊢ m : pi A B ->
    Σ ; Γ ⊢ n : A ->
    T = B.[n/] ->
    Σ ; Γ ⊢ appl m n : T.
Proof.
move => ? ? ->.
by apply: (@SynType_appl _ _ A B).
Qed.

Lemma SynType_type_move Σ Γ t T:
    Σ ; Γ ⊢ t : T ->
    exists T',
    Σ ; Γ ⊢ t: T' /\
    Σ ⊢ T ≅ T' .
Proof.
move => H.
exists T.
split => //.
apply: Conv_refl.
Qed.

Lemma SynType_eproj2 Σ Γ m A B B':
    Σ; Γ ⊢ m : sigma A B ->
    B' = B.[proj1 m /] ->
    Σ; Γ ⊢ proj2 m : B'.
Proof.
move => Htype ->.
apply: SynType_proj2.
by apply: Htype.
Qed.
