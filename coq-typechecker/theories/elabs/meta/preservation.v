(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.elabs.ast Require Import term module facts.
From sir.common Require Import string.
From sir.elabs Require Import context.
From sir.elabs.meta Require Import typing reduction confluence stability.
From sir.elabs.meta Require Import conversion red_facts.
From sir.elabs.debruijn Require Import defs facts.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(* Typing inversion Principle*)
Lemma typing_lam_inversion Σ Γ A t T:
    Σ; Γ ⊢ lam A t : T ->
    exists B, Σ ⊢ T ≅ pi A B /\ Σ; (A::Γ) ⊢ t : B.
Proof.
move Heq : (lam A t) => f.
move => Htype.
move : Htype A t Heq.
SynTypeElim Σ Γ f T => // A' t'.
-   move => [-> ->].
    have ? := Conv_refl.
    by exists B.
-   move => Heq.
    move: (IHT _ _ Heq) => [B [HconvT Htypet']].
    exists B.
    split => //.
    apply: Conv_trans;
        last by apply: HconvT.
    by apply: Conv_symm.
Qed.

Lemma typing_pair_inversion Σ Γ A B m1 m2 T:
    Σ; Γ ⊢ pair A B m1 m2 : T ->
    Σ ⊢ T ≅ sigma A B /\
    Σ; Γ ⊢ m1: A /\
    Σ; Γ ⊢ m2: B.[m1/].
Proof.
move Heq : (pair A B m1 m2) => p.
move => Htype.
move : Htype A B m1 m2 Heq.
SynTypeElim Σ Γ p T => // A' B' m1 m2.
-   move => [ -> -> -> ->] {A' B' m1 m2}.
    by have ? := Conv_refl.
-   move => Heq.
    move: (IHT _ _ _ _ Heq) => [HconvT [? ?]].
    split => //.
    apply: (@Conv_trans _ _ T) => //.
    by apply: Conv_symm.
Qed.

Lemma pi_type_injectivity Σ A A' B B':
    Σ ⊢ pi A B ≅ pi A' B' ->
    Σ ⊢ A ≅ A' /\ Σ ⊢ B ≅ B'.
Proof.
rewrite !conv_mred_iff.
move => [C []].
move /mred_inv_pi => [A2 [B2 [-> [HmredAA2 HmredBB2]]]].
move /mred_inv_pi => [A3 [B3 [[<- <-] [H H1]]]].
split.
    by exists A2.
by exists B2.
Qed.

Lemma sigma_type_injectivity Σ A A' B B':
    Σ ⊢ sigma A B ≅ sigma A' B' ->
    Σ ⊢ A ≅ A' /\ Σ ⊢ B ≅ B'.
Proof.
rewrite !conv_mred_iff.
move => [C []].
move /mred_inv_sigma => [A2 [B2 [-> [HmredAA2 HmredBB2]]]].
move /mred_inv_sigma => [A3 [B3 [[<- <-] [H H1]]]].
split.
    by exists A2.
by exists B2.
Qed.

Lemma pi_preservation Σ Γ A1 A2 B i j:
    ⊢ Σ; Γ ->
    Σ; (A1 :: Γ) ⊢ B : univ j ->
    Σ; Γ ⊢ A2 : univ i ->
    Σ ⊢ A1 ⟶ A2 ->
    Σ; Γ ⊢ pi A2 B : univ (maxn i j).
Proof.
move => HWF HtypeB HtypeA2 Hred12.
apply: SynType_pi => //.
apply: (type_conversion_stability (A := A1))  => //.
    apply: Conv_symm.
    by apply: red_conv.
by apply: (LocalWF_cons (l := i)).
Qed.

Lemma lam_preservation Σ Γ A1 A2 t B l:
    ⊢ Σ; Γ ->
    Σ; Γ ⊢ A2 : univ l ->
    Σ; (A1 :: Γ) ⊢ t : B ->
    Σ ⊢ A1 ⟶ A2 ->
    Σ; Γ ⊢ lam A2 t : pi A1 B.
Proof.
have ? := Conv_refl.
move => HWF HtypeA2 HtypeB HredA12.
apply: (SynType_conv (T := pi A2 B));
    last first.
    apply: Conv_symm.
    apply Conv_pi => //.
    by apply: red_conv.
apply: (SynType_lam (l := l)) => //.
apply: (type_conversion_stability (A := A1)) => //.
-   apply: Conv_symm.
    by apply: red_conv.
-   by apply: (LocalWF_cons (l := l)).
Qed.

Lemma appl_preservation Σ Γ m n1 n2 A B:
    Σ; Γ ⊢ m : pi A B ->
    Σ; Γ ⊢ n2 : A ->
    Σ ⊢ n1 ⟶ n2 ->
    Σ; Γ ⊢ appl m n2 : B.[n1/].
Proof.
move => Htypem Htypen2 Hredn12.
apply: (SynType_conv (T := B.[n2/])).
    by apply: (SynType_appl (A:= A)).
apply: Conv_symm.
apply: mred_conv.
apply: pred_mred.
apply: pred_subst.
    by apply: pred_refl.
by apply: red_pred.
Qed.

Lemma sigma_preservation Σ Γ A1 A2 B i j:
    ⊢ Σ; Γ ->
    Σ; Γ ⊢ A2 : univ i ->
    Σ; (A1 :: Γ) ⊢ B : univ j ->
    Σ ⊢ A1 ⟶ A2 ->
    Σ; Γ ⊢ sigma A2 B : univ (maxn i j).
Proof.
move => HWF HtypeA2 Htypet HredA12.
apply: SynType_sigma => //.
apply: (type_conversion_stability (A := A1))  => //.
    apply: Conv_symm.
    by apply: red_conv.
by apply: (LocalWF_cons (l := i)).
Qed.

Lemma pair_preservation Σ Γ A1 A2 B a b i j:
    ⊢ Σ; Γ ->
    Σ; Γ ⊢ A2 : univ i ->
    Σ; Γ ⊢ a : A1 ->
    Σ; Γ ⊢ b : B.[a/] ->
    Σ; (A1 :: Γ) ⊢ B : univ j ->
    Σ ⊢ A1 ⟶ A2 ->
    Σ; Γ ⊢ pair A2 B a b : sigma A1 B.
Proof.
have ? := Conv_refl.
move => HWF HtypeA2 Htypea Htypeb HtypeB HredA12.
have ? : Σ; Γ ⊢ a : A2.
    apply: SynType_conv.
        by apply: Htypea.
    by apply: red_conv.
apply: (SynType_conv (T := sigma A2 B));
    last first.
    apply: Conv_symm.
    apply Conv_sigma => //.
    by apply: red_conv.
apply: (SynType_pair (i := i) (j := j)) => //.
apply: (type_conversion_stability (A := A1)) => //.
-   apply: Conv_symm.
    by apply: red_conv.
-   by apply: (LocalWF_cons (l := i)).
Qed.

Lemma gvar_preservation Σ n t T ct cT:
    mod_get Σ n = Some (@module_local n t T ct cT) ->
    ⊢ Σ ->
    Σ; [::] ⊢ t : T.
Proof.
elim: Σ n t T ct cT => [//|[n t T ct cT] Σ IHΣ]
        n' t' T' ct' cT' /=.
case: (@eqP _ n n') => [Heq|Hneq].
    move => [? ? ?].
    subst.
    move => HWF.
    inversion HWF.
    subst.
    apply: type_eweakening_empty_stability => //.
        by apply: type_mod_weakening_stability.
    by apply: LocalWF_empty.
move => Hmodget HWF.
inversion HWF.
subst.
apply: type_mod_weakening_stability => //.
apply: IHΣ => //.
by apply: Hmodget.
Qed.

Lemma type_preservation Σ Γ T t t':
    Σ; Γ ⊢ t: T ->
    ⊢ Σ; Γ ->
    Σ ⊢ t ⟶ t' ->
    Σ; Γ ⊢ t' : T.
Proof.
move => Htype.
move : Htype t'.
SynTypeElim Σ Γ t T => t2 HWF Hred.
-   by inversion Hred.
-   by inversion Hred.
-   move : Hred  => /red_inv_pi [[A' [-> HredA]]|[B' [-> HredB]]].
    +   apply: (pi_preservation (A1 := A)) => //.
        by apply: IHA.
    +   apply: SynType_pi => //.
        apply: IHB => //.
        apply: LocalWF_cons => //.
        by apply: HtypeA.
-   move : Hred => /red_inv_lam [[A' [-> HredA]]|[t' [-> Hredt]]].
    +   apply: (lam_preservation (l := l)) => //.
        by apply: IHA.
    +   apply: (SynType_lam (l := l)) => //.
        apply: IHt => //.
        apply: LocalWF_cons => //.
        by apply: HtypeA.
-   move : Hred => /red_inv_appl
        [[m' [-> Hredm]]|[[n' [-> Hredn]]|[A' [t' [Heqm [-> Hredbeta]]]]]].
    +   apply: (SynType_eappl (A := A) (B:= B)) => //.
        by apply: IHm.
    +   apply: (appl_preservation (A := A)) => //.
        by apply: IHn.
    +   move : Heqm Hredbeta IHm Htypem => -> Hredbeta IHm Htypem.
        move : (typing_lam_inversion Htypem) => [B' []].
        move => /pi_type_injectivity [HconvAA' HconvBB'] Htypet'.
        apply: type_substitution_stability => //.
            apply: SynType_conv.
                by apply: Htypet'.
            by apply: Conv_symm.
        move => [|x] T /=.
            move => [<-].
            asimpl.
            by apply: (SynType_conv (T := A)).
        move Heq: (dget Γ x) => [A2|] //.
        move => [<-].
        asimpl.
        by apply: SynType_var.
-   move: Hred => /red_inv_sigma
        [[A' [-> HredA]]|[B' [-> HredB]]].
    +   apply: (sigma_preservation (A1 := A)) => //.
        by apply: IHA.
    +   apply: SynType_sigma => //.
        apply: IHB => //.
        apply: LocalWF_cons => //.
        by apply: HtypeA.
-   move : Hred => /red_inv_pair [|[|[]]] =>
        [[M'[-> HpredAM']]|[N' [-> HredBN']]|[m' [-> Hredmm']]|[n' [-> Hrednn']]].
    +   apply: (pair_preservation (i := i) (j := j)) => //.
        by apply: IHA.
    +   apply: (@SynType_conv _ _ (sigma A N')).
            apply: (@SynType_pair _ _ _ _ _ _ i j) => //.
                apply: IHB => //.
                apply: LocalWF_cons => //.
                by apply: HtypeA.
            apply: (@SynType_conv _ _ B.[m/]) => //.
            apply: conv_subst.
            by apply: red_conv.
        apply: Conv_sigma.
            by apply: Conv_refl.
        apply: Conv_symm.
        by apply: red_conv.
    +   apply: (@SynType_pair _ _ _ _ _ _ i j) => //.
            by apply: IHm.
        apply: SynType_conv.
        apply: Htypen.
        apply: conv_gen_subst.
            apply: Conv_refl.
        by apply: red_conv.
    +   apply: (@SynType_pair _ _ _ _ _ _ i j) => //.
        by apply: IHn.
-   move: Hred => /red_inv_proj1
        [[m' [-> Hredmm']]|[M [N[m1 [m2 [Heq [-> Hredmm1]]]]]]].
        apply: SynType_proj1.
        by apply: IHm.
    move: Heq Hredmm1 IHm Htypem => -> Hredmm1 IHm.
    move /typing_pair_inversion => [].
    move /sigma_type_injectivity => [ConvAA' _] [Htypem1 _].
    apply: (@SynType_conv _ _ M) => //.
    by apply: Conv_symm.
-   move: Hred => /red_inv_proj2
        [[m' [-> Hredmm']]|[M[N [m1 [m2 [Heq [-> Hredmm1]]]]]]].
        apply: SynType_conv.
            apply: SynType_proj2.
            by apply: IHm.
        apply: conv_gen_subst.
            by apply: Conv_refl.
        apply: Conv_proj1.
        apply: Conv_symm.
        by apply: red_conv.
    move: Heq Hredmm1 IHm Htypem => -> Hredmm1 IHm.
    move /typing_pair_inversion => [].
    move /sigma_type_injectivity => [ConvAA' ConvBB'] [Htypem1 Htypem2].
    apply: (@SynType_conv _ _ N.[m1/]) => //.
    apply: conv_gen_subst;
        apply: Conv_symm => //.
    by apply: Conv_proj1_beta.
-   apply: (SynType_conv (T:= T)) => //.
    by apply: IHT.
-   inversion Hred; subst.
    have ? := validity HWF.
    move: (mod_get_type_some_get_some Hget) => [t [ct [cT Hgetm]]].
    have := mod_get_some_mod_get_term_some Hgetm.
    rewrite H1.
    move => [->].
    apply: type_eweakening_empty_stability => //.
    apply: gvar_preservation => //.
    by apply: Hgetm.
Qed.

Lemma type_mred_preservation Σ Γ t t' T:
    Σ; Γ ⊢ t: T ->
    ⊢ Σ; Γ ->
    Σ ⊢ t ⟶* t' ->
    Σ; Γ ⊢ t' : T.
Proof.
move => + + Hmred.
elim : Hmred => {Σ t t'} [//|Σ t1 t2 t3 Hred12 Hmred23 IH] Htype1 HWF.
apply: IH => //.
apply: type_preservation => //.
    by apply: Htype1.
by apply: Hred12.
Qed.

