(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.elabs.ast Require Import term module facts.
From sir.elabs Require Import debruijn.defs.
From sir.elabs.meta Require Import typing reduction value conversion.
From sir.elabs.meta Require Import preservation confluence red_facts.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Lemma univ_not_pi Σ Γ l A B:
    Σ; Γ ⊢ univ l : pi A B -> False.
Proof.
move Heq : (univ l) => t.
move /SynType_type_move => [T []] Htype.
move: Htype l A B Heq.
SynTypeElim Σ Γ t T => // l A B Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_pi => [A' [B' [-> ?]]].
    by move /mred_inv_univ.
-   apply: IHT => //.
    apply: (@Conv_trans _ _ T').
        by apply: Hequiv.
    by apply: Conv_symm.
Qed.

Lemma pi_not_pi Σ Γ A' B' A B:
    Σ; Γ ⊢ pi A' B' : pi A B -> False.
Proof.
move Heq : (pi A' B') => t.
move /SynType_type_move => [T []] Htype.
move: Htype A' B' A B Heq.
SynTypeElim Σ Γ t T => // A1 B1 A2 B2 Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_pi => [A' [B' [-> ?]]].
    by move /mred_inv_univ.
-   apply: IHT => //.
    apply: (@Conv_trans _ _ T').
        by apply: Hequiv.
    by apply: Conv_symm.
Qed.

Lemma sigma_not_pi Σ Γ A' B' A B:
    Σ; Γ ⊢ sigma A' B' : pi A B -> False.
Proof.
move Heq : (sigma A' B') => t.
move /SynType_type_move => [T []] Htype.
move: Htype A' B' A B Heq.
SynTypeElim Σ Γ t T => // A1 B1 A2 B2 Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_pi => [A' [B' [-> ?]]].
    by move /mred_inv_univ.
-   apply: IHT => //.
    apply: (@Conv_trans _ _ T').
        by apply: Hequiv.
    by apply: Conv_symm.
Qed.

Lemma pair_not_pi Σ Γ M N m n A B:
    Σ; Γ ⊢ pair M N m n : pi A B -> False.
Proof.
move Heq : (pair M N m n) => t.
move /SynType_type_move => [T []] Htype.
move: Htype M N m n A B Heq.
SynTypeElim Σ Γ t T => // M N m1 n1 A2 B2 Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_pi => [A' [B' [-> [? ?]]]].
    by move /mred_inv_sigma => [? [? []]].
-   apply: IHT => //.
    apply: (@Conv_trans _ _ T').
        by apply: Hequiv.
    by apply: Conv_symm.
Qed.

Lemma univ_not_sigma Σ Γ l A B:
    Σ; Γ ⊢ univ l : sigma A B -> False.
Proof.
move Heq : (univ l) => t.
move /SynType_type_move => [T []] Htype.
move: Htype l A B Heq.
SynTypeElim Σ Γ t T => // l A B Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_sigma => [A' [B' [-> ?]]].
    by move /mred_inv_univ.
-   apply: IHT => //.
    apply: (@Conv_trans _ _ T').
        by apply: Hequiv.
    by apply: Conv_symm.
Qed.

Lemma pi_not_sigma Σ Γ A' B' A B:
    Σ; Γ ⊢ pi A' B' : sigma A B -> False.
Proof.
move Heq : (pi A' B') => t.
move /SynType_type_move => [T []] Htype.
move: Htype A' B' A B Heq.
SynTypeElim Σ Γ t T => // A1 B1 A2 B2 Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_sigma => [A' [B' [-> ?]]].
    by move /mred_inv_univ.
-   apply: IHT => //.
    apply: (@Conv_trans _ _ T').
        by apply: Hequiv.
    by apply: Conv_symm.
Qed.

Lemma sigma_not_sigma Σ Γ A' B' A B:
    Σ; Γ ⊢ sigma A' B' : sigma A B -> False.
Proof.
move Heq : (sigma A' B') => t.
move /SynType_type_move => [T []] Htype.
move: Htype A' B' A B Heq.
SynTypeElim Σ Γ t T => // A1 B1 A2 B2 Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_sigma => [A' [B' [-> ?]]].
    by move /mred_inv_univ.
-   apply: IHT => //.
    apply: (@Conv_trans _ _ T').
        by apply: Hequiv.
    by apply: Conv_symm.
Qed.

Lemma lam_not_sigma Σ Γ X n A B:
    Σ; Γ ⊢ lam X n : sigma A B -> False.
Proof.
move Heq : (lam X n) => t.
move /SynType_type_move => [T []] Htype.
move: Htype X n A B Heq.
SynTypeElim Σ Γ t T => // X n A2 B2 Heq Hequiv;
    subst.
-   move: Hequiv.
    rewrite conv_mred_iff.
    move => [T []].
    move /mred_inv_sigma => [A' [B' [-> [? ?]]]].
    by move /mred_inv_pi => [? [? []]].
-   apply: IHT => //.
    apply: (@Conv_trans _ _ T').
        by apply: Hequiv.
    by apply: Conv_symm.
Qed.

Lemma type_progress Σ Γ t T:
    Σ; Γ ⊢ t : T ->
    (exists t', Σ ⊢ t ⟶ t') \/ NormalForm t.
Proof.
SynTypeElim Σ Γ t T.
-   right.
    apply: NormalForm_neutral.
    by apply: NeutralForm_var.
-   right.
    by apply: NormalForm_univ.
-   move : IHA => [[A' HredAA']|HnormA].
        left.
        exists (pi A' B).
        by apply: Reduction_pil.
    move : IHB => [[B' HredBB']|HnormB].
        left.
        exists (pi A B').
        by apply: Reduction_pir.
    right.
    by apply: NormalForm_pi.
-   move : IHA => [[A' HredAA']|HnormA].
        left.
        exists (lam A' t).
        by apply: Reduction_laml.
    move : IHt => [[t' Hredtt']|Hnormt].
        left.
        exists (lam A t').
        by apply: Reduction_lamr.
    right.
    by apply: NormalForm_lam.
-   move : IHm => [[m' Hredmm']|Hnormm].
        left.
        exists (appl m' n).
        by apply: Reduction_appll.
    move : IHn => [[n' Hrednn']|Hnornn].
        left.
        exists (appl m n').
        by apply: Reduction_applr.
    inversion Hnormm ;
        subst.
    +   by have := univ_not_pi Htypem.
    +   by have :=  pi_not_pi Htypem.
    +   left.
        exists (t.[n/]).
        by apply: Reduction_beta.
    +   right.
        apply: NormalForm_neutral.
        by apply: NeutralForm_appl.
    +   by have := sigma_not_pi Htypem.
    +   by have := pair_not_pi Htypem.
-   move: IHA => [[A' HredAA']|HnormA].
        left.
        exists (sigma A' B).
        by apply: Reduction_sigmal.
    move : IHB => [[B' HredBB']|HnormB].
        left.
        exists (sigma A B').
        by apply: Reduction_sigmar.
    right.
    by apply: NormalForm_sigma.
-   move: IHA => [[A' HmredAA']|HnormA].
        left.
        exists (pair A' B m n).
        by apply: Reduction_pairTl.
    move: IHB => [[B' HmredBB']|HnormB].
        left.
        exists (pair A B' m n).
        by apply: Reduction_pairTr.
    move : IHm => [[m' Hredmm']|Hnormm].
        left.
        exists (pair A B m' n).
        by apply: Reduction_pairl.
    move : IHn => [[n' Hrednn']|Hnornn].
        left.
        exists (pair A B m n').
        by apply: Reduction_pairr.
    right.
    by apply: NormalForm_pair.
-   move: IHm => [[m' Hmredmm']|Hnormm].
        left.
        exists (proj1 m').
        by apply: Reduction_proj1.
    inversion Hnormm ;
        subst.
    +   by have:= univ_not_sigma Htypem.
    +   by have := pi_not_sigma Htypem.
    +   by have := lam_not_sigma Htypem.
    +   right.
        apply: NormalForm_neutral.
        by apply: NeutralForm_proj1.
    +   by have := sigma_not_sigma Htypem.
    +   left.
        exists m0.
        by apply: Reduction_proj1_beta.
-   move: IHm => [[m' Hmredmm']|Hnormm].
        left.
        exists (proj2 m').
        by apply: Reduction_proj2.
    inversion Hnormm ;
        subst.
    +   by have:= univ_not_sigma Htypem.
    +   by have := pi_not_sigma Htypem.
    +   by have := lam_not_sigma Htypem.
    +   right.
        apply: NormalForm_neutral.
        by apply: NeutralForm_proj2.
    +   by have := sigma_not_sigma Htypem.
    +   left.
        exists n.
        by apply: Reduction_proj2_beta.
-   by [].
-   left.
    move: (mod_get_type_some_get_some Hget) => [t [ct [cT Hgetm]]].
    exists t.
    apply: Reduction_gvar.
    apply: mod_get_some_mod_get_term_some.
        by apply: cT.
    move => HcT.
    by rewrite [HcT](@ Closed_irrel _ _ cT).
Qed.
