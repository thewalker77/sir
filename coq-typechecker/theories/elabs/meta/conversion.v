(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.common Require Import utils string.
From sir.elabs.ast Require Import term module facts.
From sir.elabs.debruijn Require Import defs facts.
Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(* Meta theoritic representation of Definitional equality (Also known as      *)
(* conversion) rule, this is representation that does not particularly care   *)
(* about types of the terms being converted                                   *)
Reserved Notation "Σ '⊢' x '≅' y" (at level 20, x at next level, y at next level).
Inductive Conv: Module -> Term -> Term -> Prop :=
| Conv_beta: forall Σ A t t' u , t' = t.[u/] -> Σ ⊢ appl (lam A t) u ≅ t'
| Conv_refl: forall Σ t, Σ ⊢ t ≅ t
| Conv_symm: forall Σ t t',
                Σ ⊢ t ≅ t' ->
                Σ ⊢ t' ≅ t
| Conv_trans: forall Σ t1 t2 t3,
                Σ ⊢ t1 ≅ t2 ->
                Σ ⊢ t2 ≅ t3 ->
                Σ ⊢ t1 ≅ t3
| Conv_pi: forall Σ A A' B B',
                Σ ⊢ A ≅ A' ->
                Σ ⊢ B ≅ B' ->
                Σ ⊢ pi A B ≅ pi A' B'
| Conv_lam: forall Σ A A' t t',
                Σ ⊢ A ≅ A' ->
                Σ ⊢ t ≅ t' ->
                Σ ⊢ lam A t ≅ lam A' t'
| Conv_appl: forall Σ m m' n n',
                Σ ⊢ m ≅ m' ->
                Σ ⊢ n ≅ n' ->
                Σ ⊢ appl m n ≅ appl m' n'
| Conv_sigma: forall Σ A A' B B',
                Σ ⊢ A ≅ A' ->
                Σ ⊢ B ≅ B' ->
                Σ ⊢ sigma A B ≅ sigma A' B'
| Conv_pair: forall Σ M M' N N' m m' n n',
                Σ ⊢ M ≅ M' ->
                Σ ⊢ N ≅ N' ->
                Σ ⊢ m ≅ m' ->
                Σ ⊢ n ≅ n' ->
                Σ ⊢ pair M N m n ≅ pair M' N' m' n'
| Conv_proj1: forall Σ m m',
                Σ ⊢ m ≅ m' ->
                Σ ⊢ proj1 m ≅ proj1 m'
| Conv_proj2: forall Σ m m',
                Σ ⊢ m ≅ m' ->
                Σ ⊢ proj2 m ≅ proj2 m'
| Conv_proj1_beta: forall Σ M N m n,
                Σ ⊢ proj1 (pair M N m n) ≅ m
| Conv_proj2_beta: forall Σ M N m n,
                Σ ⊢ proj2 (pair M N m n) ≅ n
| Conv_gvar: forall Σ n t,
                mod_get_term Σ n = Some t ->
                Σ ⊢ gvar n ≅ t
where "Σ '⊢' t1 '≅' t2" := (Conv Σ t1 t2).

Tactic Notation "ConvElim" ident(Σ) ident(T) ident(T'):=
elim => {Σ T T'} /=
    [ Σ A t t' u Heqtt'
    | Σ t
    | Σ t t' IHtt' Hconvtt'
    | Σ t1 t2 t3  Hconv12 IH12 Hconv23 IH23
    | Σ A A' B B' HconvAA' IHA HconvBB' IHB
    | Σ A A' t t' HconvAA' IHA Hconvtt' IHt
    | Σ m m' n n' Hconvmm' IHm Hconvnn' IHn
    | Σ A A' B B' HconvAA' IHA HconvBB' IHB
    | Σ M M' N N' m m' n n' HconvMM' IHMM' HconvNN' IHNN' Hconvmm' IHm Hconvnn' IHn
    | Σ m m' Hconvmm' IHm
    | Σ m m' Hconvmm' IHm
    | Σ M N m n
    | Σ M N m n
    | Σ n t Hget_term
    ].

Lemma conv_subst Σ t t' σ: Σ ⊢ t ≅ t' -> Σ ⊢ t.[σ] ≅ t'.[σ].
Proof.
move => Hconv.
move: Hconv σ.
ConvElim Σ t t' => σ.
-   (* Conv_beta *)
    rewrite !substE.
    apply: Conv_beta.
    rewrite Heqtt'.
    by asimpl.
-   by apply: Conv_refl.
-   by apply: Conv_symm.
-   by apply: Conv_trans.
-   by apply: Conv_pi.
-   by apply: Conv_lam.
-   by apply: Conv_appl.
-   by apply: Conv_sigma.
-   by apply: Conv_pair.
-   by apply: Conv_proj1.
-   by apply: Conv_proj2.
-   by apply: Conv_proj1_beta.
-   by apply: Conv_proj2_beta.
-   rewrite substE.
    apply: Conv_gvar.
    have Hclosed := mod_get_term_closed Hget_term.
    by rewrite Hclosed.
Qed.

Lemma conv_weakening Σ t t' t1 T1 ct1 cT1 name:
    Σ ⊢ t ≅ t' ->
    mod_get Σ name = None ->
    (@module_local name t1 T1 ct1 cT1 :: Σ) ⊢ t ≅ t'.
Proof.
move =>Helim.
move: Helim t1 T1 ct1 cT1 name.
ConvElim Σ t t' => t4 t5 ct4 ct5 name Hget.
-   by apply: Conv_beta.
-   by apply: Conv_refl.
-   apply: Conv_symm.
    by by apply: Hconvtt'.
-   apply: Conv_trans.
        by apply: IH12.
    by apply: IH23.
-   apply: Conv_pi.
        by apply: IHA.
    by apply: IHB.
-   apply: Conv_lam.
        by apply: IHA.
    by apply: IHt.
-   apply: Conv_appl.
        by apply: IHm.
    by apply: IHn.
-   apply: Conv_sigma.
        by apply: IHA.
    by apply: IHB.
-   apply: Conv_pair.
    -   by apply: IHMM'.
    -   by apply: IHNN'.
    -   by apply: IHm.
    -   by apply: IHn.
-   apply: Conv_proj1.
    by apply: IHm.
-   apply: Conv_proj2.
    by apply: IHm.
-   by apply: Conv_proj1_beta.
-   by apply: Conv_proj2_beta.
-   apply: Conv_gvar => /=.
    move: Hget Hget_term.
    case: (@eqP _ name n)=> [->|Hneq].
        by move => /mod_get_none_mod_term ->.
    by rewrite mod_get_term_cons_neq.
Qed.
