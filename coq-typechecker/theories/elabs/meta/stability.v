(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.elabs.meta Require Import typing conversion.
From sir.elabs.ast Require Import term module facts.
From sir.elabs Require Import context.
From sir.elabs.debruijn Require Import defs facts.
From sir.common Require Import utils string.
Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Lemma validity  Σ Γ: ⊢ Σ; Γ -> ⊢ Σ.
Proof.
by elim.
Qed.

Definition CtxRename ξ Γ1 Γ2 : Prop:= forall idx T1,
    dget Γ1 idx = Some T1 ->
    dget Γ2 (ξ idx) = Some T1.[ren ξ].

(* This is a property that appears when proving stability under renaming for  *)
(* both pi types and sigma types                                              *)
Lemma ctx_rename_cons A Γ1 Γ2 ξ:
    CtxRename ξ Γ1 Γ2 ->
    CtxRename (upren ξ) (A::Γ1) (A.[ren ξ]:: Γ2).
Proof.
move => Hrename [|idx] T1 /=.
    move => [<-].
    by asimpl.
case Heq: (dget (Γ1) idx) => [t|//] [<-].
rewrite (Hrename _ _ Heq).
by asimpl.
Qed.

Lemma type_rename_stability Σ ξ Γ1 Γ2 t T:
    Σ; Γ1 ⊢ t : T ->
    ⊢ Σ; Γ2 ->
    CtxRename ξ Γ1 Γ2 ->
    Σ; Γ2 ⊢ t.[ren ξ] : T.[ren ξ].
Proof.
move => HType.
move: HType ξ Γ2.
SynTypeElim Σ Γ1 t T => ξ Γ2 HWFΓ2 Hrename;
    rewrite ?substE.
-   asimpl.
    apply: SynType_var => //.
    by apply: Hrename.
-   by apply: SynType_univ.
-   apply: SynType_pi.
        by apply: IHA.
    asimpl.
    apply: IHB.
       apply: LocalWF_cons => //.
       by apply: IHA.
    by apply: ctx_rename_cons.
-   apply: SynType_lam.
        by apply: IHA.
    asimpl.
    apply: IHt.
        apply: LocalWF_cons=> //.
        by apply: IHA.
    by apply: ctx_rename_cons.
-   apply: SynType_eappl.
    +   by apply: IHm.
    +   by apply: IHn.
    +   by asimpl.
-   apply: SynType_sigma.
        by apply: IHA.
    asimpl.
    apply: IHB.
        apply: LocalWF_cons => //.
        by apply: IHA.
    by apply: ctx_rename_cons.
-   apply: SynType_pair.
    +   by apply: IHA.
    +   asimpl.
        apply: IHB;
            last by apply: ctx_rename_cons.
        apply: LocalWF_cons => //.
        by apply: IHA.
    +   by apply: IHm.
    +   asimpl.
        have : B.[m.[ren ξ] .: ren ξ] = B.[m/].[ren ξ].
            by asimpl.
        move => ->.
        by apply: IHn.
-   apply: SynType_proj1.
    by apply: IHm.
-   asimpl.
    apply: SynType_eproj2.
        by apply: IHm.
    by asimpl.
-   apply: SynType_conv.
        by apply: IHT.
    by apply: conv_subst.
-   move: (mod_get_type_some_get_some Hget) => [t [ct [cT Hget']]].
    rewrite cT.
    by apply:SynType_gvar.
Qed.

Lemma type_weakening_stability Σ Γ t T A l:
    Σ; Γ ⊢ t : T ->
    ⊢ Σ; Γ ->
    Σ; Γ ⊢ A : univ l ->
    Σ; (A::Γ) ⊢ t.[ren (padd 1)] : T.[ren (padd 1)].
Proof.
move => Htypet HWF HtypeT'.
apply: (type_rename_stability Htypet) => //.
apply: (LocalWF_cons (l:= l)) => //.
move => idx T1 Heq.
asimpl.
by rewrite Heq.
Qed.

Lemma type_eweakening_stability Σ Γ t A T T' l:
    Σ; Γ ⊢ t : T ->
    Σ; Γ ⊢ A : univ l ->
    ⊢ Σ; Γ ->
    T' =  T.[ren (padd 1)] ->
    Σ; (A::Γ) ⊢ t.[ren (padd 1)] : T'.
Proof.
move => ? ? ? ->.
by apply: (@type_weakening_stability _ _ _ _ _ l).
Qed.

Lemma type_weakening_empty_stability Σ Γ t T:
    Σ; [::] ⊢ t : T ->
    ⊢ Σ; Γ ->
    Σ; Γ ⊢ t.[ren (padd (Coq.Lists.List.length Γ))] : T.[ren (padd (Coq.Lists.List.length Γ))].
Proof.
elim: Γ Σ t T => [|a Γ IHΓ] /= Σ t T Htypet HWF.
    asimpl.
    by [].
have Hpadd : forall (u: Term), u.[ren (padd (Coq.Lists.List.length Γ).+1)] =
                        u.[ren (padd (Coq.Lists.List.length Γ))].[ren (padd 1)].
    move => u.
    by asimpl.
rewrite !Hpadd.
inversion HWF.
subst.
apply: (@type_weakening_stability _ _ _ _ _ l) => //.
by apply: IHΓ.
Qed.

Lemma type_eweakening_empty_stability Σ Γ t T t' T':
    Σ; [::] ⊢ t : T ->
    ⊢ Σ; Γ ->
    t' = t.[ren (padd (Coq.Lists.List.length Γ))] ->
    T' = T.[ren (padd (Coq.Lists.List.length Γ))] ->
    Σ; Γ ⊢ t' : T'.
Proof.
move => ? ? -> ->.
by apply: type_weakening_empty_stability.
Qed.

Definition CtxSubtitution Σ Γ1 Γ2 σ := forall x T,
    dget Γ1 x = Some T -> Σ; Γ2 ⊢ σ x : T.[σ].

Lemma ctx_subst_append Σ Γ1 Γ2 σ A l:
    Σ; Γ2 ⊢ A.[σ] : univ l ->
    ⊢ Σ; Γ2 ->
    CtxSubtitution Σ Γ1 Γ2 σ ->
    CtxSubtitution Σ (A :: Γ1) (A.[σ] :: Γ2) (up σ).
Proof.
move => HtypA HWF2.
move => Hsubst [|x] T /=.
    move => [<-].
    apply: SynType_var.
    by asimpl.
case Heq : (dget Γ1 x) => [t|//].
move => [<-].
asimpl.
apply: type_eweakening_stability => //.
+   by apply: (Hsubst _ t).
+   by apply: HtypA.
+   by asimpl.
Qed.

(* Note in the third hypothesis, if we have non empty Γ1 *)
(* we can use it to establish that ⊢ Γ2, but this is not *)
(* possible in case of empty Γ1                          *)
Lemma type_substitution_stability Σ Γ1 Γ2 σ t T:
    Σ; Γ1 ⊢ t: T ->
    ⊢ Σ; Γ2 ->
    CtxSubtitution Σ Γ1 Γ2 σ ->
    Σ; Γ2 ⊢ t.[σ]: T.[σ].
Proof.
move => Htype.
move : Htype Γ2 σ.
SynTypeElim Σ Γ1 t T => Γ2 σ HWF Hmap;
    rewrite ?substE.
-   by apply: Hmap.
-   by apply: SynType_univ.
-   apply: SynType_pi.
        by apply IHA.
    apply: IHB.
        apply: LocalWF_cons => //.
        by apply: IHA.
    apply: ctx_subst_append => //.
    by apply: IHA.
-   apply: SynType_lam.
        by apply: IHA.
    apply: IHt.
        apply: LocalWF_cons => //.
        by apply: IHA.
    apply: ctx_subst_append => //.
    by apply: IHA.
-   have:= (IHm _ σ HWF).
    rewrite ?substE => IHM.
    apply: SynType_eappl.
    +   by apply IHM.
    +   by apply: IHn.
    +   by asimpl.
-   apply: SynType_sigma.
        by apply: IHA.
    apply: IHB.
        apply: LocalWF_cons => //.
        by apply: IHA.
    apply: ctx_subst_append => //.
    by apply: IHA.
-   apply: SynType_pair.
    +   by apply: IHA.
    +   apply: IHB.
            apply: LocalWF_cons => //.
            by apply: IHA.
        apply: ctx_subst_append => //.
        by apply: IHA.
    +   by apply: IHm.
    +   have : B.[up σ].[m.[σ]/] = B.[m/].[σ].
            by asimpl.
        move ->.
        by apply IHn.
-   apply: SynType_proj1.
    have := IHm _ σ.
    rewrite ?substE => IHM.
    by apply: IHM.
-   have := IHm _ σ.
    rewrite ?substE => IHM.
    apply: SynType_eproj2.
        by apply: IHm.
    by asimpl.
-   apply: SynType_conv.
        by apply: IHT.
    by apply: conv_subst.
-   move: (mod_get_type_some_get_some Hget) => [t [ct [cT Hget']]].
    rewrite cT.
    by apply: SynType_gvar.
Qed.

Lemma type_conversion_stability Σ A A' Γ t T:
    Σ ⊢ A' ≅ A ->
    ⊢ Σ; (A'::Γ) ->
    Σ; (A::Γ) ⊢ t: T ->
    Σ; (A'::Γ) ⊢ t :T.
Proof.
move => Hconv HWF Htypet.
suff : (Σ; (A' :: Γ) ⊢ t.[ids] : T.[ids]).
    by asimpl.
apply: (type_substitution_stability Htypet) => //.
move => [|x] T0//=.
    move => [<-].
    apply: SynType_conv; last first.
        asimpl.
        apply: conv_subst.
        by apply: Hconv.
    by apply: SynType_var.
case Heq: (dget (Γ) x) => [t'|//] [<-].
apply: SynType_var => //.
asimpl.
by rewrite Heq.
Qed.

Lemma type_mod_weakening_stability Σ Γ name t T t' T' ct' cT':
    Σ; Γ ⊢ t : T ->
    Σ; [::] ⊢ t' : T' ->
    mod_get Σ name = None ->
    (@module_local name t' T' ct' cT' :: Σ); Γ ⊢ t: T.
Proof.
move => Htype.
move: Htype name t' T' ct' cT'.
SynTypeElim Σ Γ t T => name' t1 T1 ct1 cT1 HtypeT1 Hmod_get.
-   by apply: SynType_var.
-   by apply: SynType_univ.
-   apply: SynType_pi.
        by apply: IHA.
    by apply: IHB.
-   apply: SynType_lam.
        by apply: IHA.
    by apply: IHt.
-   apply: SynType_appl.
        by apply: IHm.
    by apply: IHn.
-   apply: SynType_sigma.
        by apply: IHA.
    by apply: IHB.
-   apply: SynType_pair.
    +   by apply: IHA.
    +   by apply: IHB.
    +   by apply: IHm.
    +   by apply: IHn.
-   apply: SynType_proj1.
    by apply: IHm.
-   apply: SynType_proj2.
    by apply: IHm.
-   apply: SynType_conv.
        by apply: IHT.
    by apply: conv_weakening.
-   apply: SynType_gvar.
    case: (@eqP _ name' name) => [Heq|Heq].
        subst.
        move: Hget.
        by rewrite /mod_get_type Hmod_get.
    by rewrite mod_get_type_cons_neq.
Qed.
