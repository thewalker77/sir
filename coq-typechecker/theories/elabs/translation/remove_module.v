(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From HB Require Import structures.
From mathcomp Require Import all_ssreflect.
From sir.common Require Import string.
From sir.common.fin_sets Require Import gset.
From sir.elabs.ast Require Import term module.
From sir.elabs.translation Require Import wf.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Fixpoint subst_module (name: string) (σ: Term) t : Term :=
match t with
| var x => var x
| univ l => univ l
| pi A B => pi (subst_module name σ A) (subst_module name σ B)
| lam A t => lam (subst_module name σ A) (subst_module name σ t)
| appl m n => appl (subst_module name σ m) (subst_module name σ n)
| sigma A B => sigma (subst_module name σ A) (subst_module name σ B)
| pair A B a b => pair (subst_module name σ A) (subst_module name σ B) (subst_module name σ a) (subst_module name σ b)
| proj1 m => proj1 (subst_module name σ m)
| proj2 m => proj2 (subst_module name σ m)
| gvar name' => if name == name' then σ else gvar name'
end.

Fixpoint remove_module Σ t : Term :=
match Σ with
| @module_local name σ _ _ _ :: Σ' => remove_module Σ' (subst_module name σ t)
| [::] => t
end.

Lemma TermWF_union t t' n Σ:
    n ∉ term_get_gvars t' ->
    n ∉ module_get_gvars Σ ->
    TermWF t' Σ ->
    term_get_gvars t ⊆ {[n]} ∪ module_get_gvars Σ ->
    TermWF (subst_module n t' t) Σ.
Proof.
move : t t' n Σ.
TermElim => t' n' Σ => Hnotint' HnotinΣ Hwft';
    rewrite ?TermWFE //.
-   rewrite setE.
    move => [??].
    split.
        by apply: IHA.
    by apply: IHB.
-  rewrite setE.
    move => [??].
    split.
        by apply: IHT.
    by apply: IHy.
-  rewrite setE.
    move => [??].
    split.
        by apply: IHm.
    by apply: IHn.
-   rewrite setE.
    move => [??].
    split.
        by apply: IHA.
    by apply: IHB.
-   rewrite setE setE setE.
    move => [[[? ?] ?]?].
    repeat split.
    +   by apply: IHM.
    +   by apply: IHN.
    +   by apply: IHm.
    +   by apply: IHn.
-   by apply: IHm.
-   by apply: IHm.
-   rewrite set_in_subseteq_singleton ?setE.
    move => [->|].
        by rewrite eq_refl.
    move => Hin.
    case (@eqP _ n' name) => // Hneq.
    by rewrite /TermWF /= set_in_subseteq_singleton.
Qed.

Lemma TermWF_remove_name t n t' T' ct' cT' Σ:
    n ∉ term_get_gvars t' ->
    n ∉ module_get_gvars Σ ->
    TermWF t' Σ ->
    TermWF t (module_local n (t:=t') (T:=T') ct' cT' :: Σ) ->
    TermWF (subst_module n t' t) Σ.
Proof.
rewrite /TermWF /= {T' ct' cT'}.
by apply: TermWF_union.
Qed.

Module RemModRewrite.
Lemma remove_module_var Σ idx: remove_module Σ (var idx) = (var idx).
Proof.
elim : Σ => [//| [name t T ct cT] Σ IH] /=.
by rewrite IH.
Qed.

Lemma remove_module_univ Σ lvl: remove_module Σ (univ lvl) = (univ lvl).
Proof.
elim : Σ => [//| [name t T ct cT] Σ IH] /=.
by rewrite IH.
Qed.

Lemma remove_module_pi Σ A B:
    remove_module Σ (pi A B) = pi (remove_module Σ A) (remove_module Σ B).
Proof.
elim : Σ A B => [//| [name t T ct cT] Σ IH] /= A B.
by rewrite IH.
Qed.

Lemma remove_module_lam Σ A t:
    remove_module Σ (lam A t) = lam (remove_module Σ A) (remove_module Σ t).
Proof.
elim : Σ A t => [//| [name t T ct cT] Σ IH] /= A t'.
by rewrite IH.
Qed.

Lemma remove_module_appl Σ m n:
    remove_module Σ (appl m n) = appl (remove_module Σ m) (remove_module Σ n).
Proof.
elim : Σ m n => [//| [name t T ct cT] Σ IH] /= m n.
by rewrite IH.
Qed.

Lemma remove_module_sigma Σ A B:
    remove_module Σ (sigma A B) = sigma (remove_module Σ A) (remove_module Σ B).
Proof.
elim : Σ A B => [//| [name t T ct cT] Σ IH] /= A B.
by rewrite IH.
Qed.

Lemma remove_module_pair Σ A B a b:
    remove_module Σ (pair A B a b) =
    pair (remove_module Σ A) (remove_module Σ B) (remove_module Σ a) (remove_module Σ b).
Proof.
elim : Σ A B a b=> [//| [name t T ct cT] Σ IH] /= A B a b.
by rewrite IH.
Qed.

Lemma remove_module_proj1 Σ m:
    remove_module Σ (proj1 m) = proj1 (remove_module Σ m).
Proof.
elim : Σ m => [//| [name t T ct cT] Σ IH] /= m.
by rewrite IH.
Qed.

Lemma remove_module_proj2 Σ m:
    remove_module Σ (proj2 m) = proj2 (remove_module Σ m).
Proof.
elim : Σ m => [//| [name t T ct cT] Σ IH] /= m.
by rewrite IH.
Qed.

End RemModRewrite.

Definition remove_moduleE :=(
    RemModRewrite.remove_module_var,    RemModRewrite.remove_module_univ,
    RemModRewrite.remove_module_pi,     RemModRewrite.remove_module_lam,
    RemModRewrite.remove_module_appl,   RemModRewrite.remove_module_sigma,
    RemModRewrite.remove_module_pair,   RemModRewrite.remove_module_proj1,
    RemModRewrite.remove_module_proj2
).

Lemma WFTerm_subst_id n t t':
    n ∉ term_get_gvars t ->
    subst_module n t' t = t.
Proof.
move: t t' n.
TermElim => t' n' //;
    rewrite ?setE.
-   move => [? ?].
    f_equal.
        by apply: IHA.
    by apply: IHB.
-   move => [? ?].
    f_equal.
        by apply: IHT.
    by apply: IHy.
-   move => [? ?].
    f_equal.
        by apply: IHm.
    by apply: IHn.
-   move => [ ? ?].
    f_equal.
        by apply: IHA.
    by apply: IHB.
-   move => [[[? ?]?]?].
    f_equal.
    +   by apply: IHM.
    +   by apply: IHN.
    +   by apply: IHm.
    +   by apply: IHn.
-   move => ?.
    f_equal.
    by apply: IHm.
-   move => ?.
    f_equal.
    by apply: IHm.
-   by case (@eqP _ n' name).
Qed.