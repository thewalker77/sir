(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From HB Require Import structures.
From mathcomp Require Import all_ssreflect.
From sir.common Require Import string decide.
From sir.common.fin_sets Require Import gset.
From sir.elabs.ast Require Import term module facts.
From sir.elabs.meta Require Import typing stability conversion confluence reduction red_facts.
From sir.elabs.debruijn Require Import defs facts.
From sir.elabs Require Import context.
Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Fixpoint term_get_gvars (t: Term) : GSet string :=
match t with
| var x => ∅
| univ l => ∅
| pi A B => term_get_gvars A ∪ term_get_gvars B
| lam A t => term_get_gvars A ∪ term_get_gvars t
| appl m n => term_get_gvars m ∪ term_get_gvars n
| sigma A B => term_get_gvars A ∪ term_get_gvars B
| pair A B a b => term_get_gvars A ∪ term_get_gvars B ∪ term_get_gvars a ∪ term_get_gvars b
| proj1 m => term_get_gvars m
| proj2 m => term_get_gvars m
| gvar name => {[name]}
end.

Fixpoint module_get_gvars (Σ : Module) : GSet string :=
match Σ with
| nil => ∅
| (module_local name _ _ _ _)::Σ' => {[name]} ∪ module_get_gvars Σ'
end.

Definition TermWF t Σ : Prop := term_get_gvars t ⊆ module_get_gvars Σ.

Inductive ModuleWF : Module -> Prop :=
| WF_empty: ModuleWF [::]
| WF_cons name t T ct cT Σ:
    ModuleWF Σ ->
    TermWF t Σ ->
    name ∉ module_get_gvars Σ ->
    ModuleWF (( @module_local name t T ct cT)::Σ).


Module TermWFRewrite.

Lemma TermWFvar idx Σ: TermWF (var idx) Σ <-> True.
Proof.
rewrite /TermWF /=.
split => // _.
by apply: set_empty_subseteq.
Qed.

Lemma TermWFuniv lvl Σ: TermWF (univ lvl) Σ <-> True.
Proof.
rewrite /TermWF /=.
split => // _.
by apply: set_empty_subseteq.
Qed.

Lemma TermWFpi A B Σ :
    TermWF (pi A B) Σ <->
    TermWF A Σ /\ TermWF B Σ.
Proof.
rewrite /TermWF /= setE.
split.
    by move => [].
by move => [].
Qed.

Lemma TermWFlam T y Σ :
    TermWF (lam T y) Σ <->
    TermWF T Σ /\ TermWF y Σ.
Proof.
rewrite /TermWF /= setE.
split.
    by move => [].
by move => [].
Qed.

Lemma TermWFappl m n Σ :
    TermWF (appl m n) Σ <->
    TermWF m Σ /\ TermWF n Σ.
Proof.
rewrite /TermWF /= setE.
split.
    by move => [].
by move => [].
Qed.

Lemma TermWFsigma A B Σ :
    TermWF (sigma A B) Σ <->
    TermWF A Σ /\ TermWF B Σ.
Proof.
rewrite /TermWF /= setE.
split.
    by move => [].
by move => [].
Qed.

Lemma TermWFpair M N m n Σ :
    TermWF (pair M N m n) Σ <->
    TermWF M Σ /\ TermWF N Σ /\ TermWF m Σ /\ TermWF n Σ .
Proof.
rewrite /TermWF /= setE setE setE.
split.
    by move => [[[]]].
by move => [? [? []]].
Qed.

Lemma TermWFproj1 m Σ :
    TermWF (proj1 m) Σ <-> TermWF m Σ.
Proof.
by rewrite /TermWF /=.
Qed.

Lemma TermWFproj2 m Σ :
    TermWF (proj2 m) Σ <-> TermWF m Σ.
Proof.
by rewrite /TermWF /=.
Qed.

Lemma TermWFgvar name Σ :
    TermWF (gvar name) Σ <-> name ∈ module_get_gvars Σ.
Proof.
by rewrite /TermWF /= set_in_subseteq_singleton.
Qed.

End TermWFRewrite.

Definition TermWFE := (
    TermWFRewrite.TermWFvar,   TermWFRewrite.TermWFuniv,
    TermWFRewrite.TermWFpi,    TermWFRewrite.TermWFlam,
    TermWFRewrite.TermWFappl,  TermWFRewrite.TermWFsigma,
    TermWFRewrite.TermWFpair,  TermWFRewrite.TermWFproj1,
    TermWFRewrite.TermWFproj2, TermWFRewrite.TermWFgvar
).

Lemma not_in_module_not_in_term n t Σ:
    n ∉ module_get_gvars Σ ->
    TermWF t Σ ->
    n ∉ term_get_gvars t.
Proof.
rewrite /TermWF.
by apply: set_not_in_weaken.
Qed.

Lemma TermWF_dec t Σ: {TermWF t Σ} + {~ TermWF t Σ}.
Proof.
move: t Σ.
TermElim => // Σ.
-   left.
    by rewrite TermWFE.
-   left.
    by rewrite TermWFE.
-   move: (IHA Σ) => [HWFA| HnotWFA];
        last first.
        right.
        rewrite TermWFE.
        move => [? ?].
        by apply: HnotWFA.
    move: (IHB Σ) => [HWFB| HnotWFB].
        left.
        by rewrite TermWFE.
    right.
    rewrite TermWFE.
    move => [? ?].
    by apply: HnotWFB.
-   move: (IHT Σ) => [HWFT| HnotWFT];
        last first.
        right.
        rewrite TermWFE.
        move => [? ?].
        by apply: HnotWFT.
    move: (IHy Σ) => [HWFy| HnotWFy].
        left.
        by rewrite TermWFE.
    right.
    rewrite TermWFE.
    move => [? ?].
    by apply: HnotWFy.
-   move: (IHm Σ) => [HWFm| HnotWFm];
        last first.
        right.
        rewrite TermWFE.
        move => [? ?].
        by apply: HnotWFm.
    move: (IHn Σ) => [HWFn| HnotWFn].
        left.
        by rewrite TermWFE.
    right.
    rewrite TermWFE.
    move => [? ?].
    by apply: HnotWFn.
-   move: (IHA Σ) => [HWFA| HnotWFA];
        last first.
        right.
        rewrite TermWFE.
        move => [? ?].
        by apply: HnotWFA.
    move: (IHB Σ) => [HWFB| HnotWFB].
        left.
        by rewrite TermWFE.
    right.
    rewrite TermWFE.
    move => [? ?].
    by apply: HnotWFB.
-   move: (IHM Σ) => [HWFM| HnotWFM];
        last first.
        right.
        rewrite TermWFE.
        move => [?] [?] [?] ?.
        by apply: HnotWFM.
    move: (IHN Σ) => [HWFN| HnotWFN];
        last first.
        right.
        rewrite TermWFE.
        move => [?] [?] [?] ?.
        by apply: HnotWFN.
    move: (IHm Σ) => [HWFm| HnotWFm];
        last first.
        right.
        rewrite TermWFE.
        move => [?] [?] [?] ?.
        by apply: HnotWFm.
    move: (IHn Σ) => [HWFn| HnotWFn].
        left.
        by rewrite TermWFE.
    right.
    rewrite TermWFE.
    move => [?]  [?] [?] ?.
    by apply: HnotWFn.
-   case: (set_inP name (module_get_gvars Σ)).
        left.
        by rewrite TermWFE.
    right.
    by rewrite TermWFE.
Qed.

Lemma ModuleWF_dec Σ: {ModuleWF Σ} + {~ ModuleWF Σ}.
Proof.
elim : Σ => [|[n t T ct cT] Σ ] /=.
    left.
    by apply: WF_empty.
move => [IH|IH];
    last first.
    right.
    move => HWF.
    by inversion HWF.
case (TermWF_dec t Σ) => [wft|notwft];
    last first.
    right.
    move => HWF.
    by inversion HWF.
case: (set_inP n (module_get_gvars Σ)).
    right.
    move => HWF.
    by inversion HWF.
left.
by apply: WF_cons.
Qed.

Definition DecTermWF t Σ : Prop := @bool_decide (TermWF t Σ) (TermWF_dec t Σ) = true.
Definition DecModuleWF Σ : Prop := @bool_decide (ModuleWF Σ) (ModuleWF_dec Σ) = true.

Lemma DecTermWFTermWF t Σ:
    DecTermWF t Σ <-> TermWF t Σ.
Proof.
by rewrite /DecTermWF bool_decide_true.
Qed.

Lemma DecModuleWFModuleWF Σ:
    DecModuleWF Σ <-> ModuleWF Σ.
Proof.
by rewrite /DecModuleWF bool_decide_true.
Qed.

(* Relation between wellformed terms/modules and typing *)

Lemma mod_get_some_gvar_set Σ n:
    (exists T, mod_get_type Σ n = Some T) <->
    n ∈ module_get_gvars Σ.
Proof.
split;
   elim: Σ n => [|[n t T ct cT] Σ IH] n'//.
-   by move => [T].
-   move => [T'] /=.
    case (@eqP _ n n') => [->| Hneq];
    rewrite !setE.
        by left.
    rewrite mod_get_type_cons_neq // => ?.
    right.
    apply: IH.
    by exists T'.
-   by rewrite setE.
-   move => /=.
    rewrite !setE.
    move => [Heq|Hin].
        subst.
        exists T.
        by rewrite mod_get_type_cons_eq.
    case (@eqP _ n n') => [?|?].
        rewrite mod_get_type_cons_eq //.
        by exists T.
    rewrite mod_get_type_cons_neq //.
    by apply: IH.
Qed.

Lemma mod_get_none_gvar_set Σ n:
    mod_get_type Σ n = None <->
    n ∉ module_get_gvars Σ.
Proof.
rewrite -mod_get_some_gvar_set.
split.
    by move => -> [T].
case Heq: (mod_get_type Σ n) => [T|//].
move => HFalse.
exfalso.
apply: HFalse.
by exists T.
Qed.

Lemma term_types_wf Σ Γ t T:
    Σ; Γ ⊢ t : T ->
    TermWF t Σ.
Proof.
SynTypeElim Σ Γ t T => //;
    rewrite !TermWFE //.
elim: Σ Hget => //.
move => [n t T' ct cT] Σ IH /=.
case (@eqP _ n name) => [->| Hneq];
    rewrite !setE.
    by left.
rewrite mod_get_type_cons_neq //.
right.
by apply: IH.
Qed.

Lemma module_wf_wf Σ:
    ⊢ Σ ->
    ModuleWF Σ.
Proof.
elim => {Σ}.
    by apply: WF_empty.
move => Σ n t T ct cT Hget Htypet IH HWF.
apply: WF_cons => //.
    by apply: (@term_types_wf _ [::] _ T).
rewrite -mod_get_none_gvar_set.
by apply: mod_get_none_mod_type.
Qed.
