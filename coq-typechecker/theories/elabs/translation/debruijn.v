(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect zify.
From Equations Require Import Equations.
From sir.common.fin_sets Require Import gset.
From sir.common Require Import string.

From sir.elabs.ast Require Import term module.
From sir.elabs.debruijn Require Import defs facts closed_facts.
From sir.elabs.translation Require Import term_translation remove_module wf.

From sir.core.debruijn Require Import defs facts closed_facts.
From sir.core.ast Require Import term.


Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(* binders and translation *)
Lemma ren_succn_predn {T: debruijn} (t: T):
    t.[ren succn].[ren predn] = t.
Proof.
by asimpl.
Qed.

Lemma translate_without_module_preserve_closedn t n wf:
    Closedn t n ->
    Closedn (⟦t, wf⟧) n.
Proof.
move: t wf n => /=.
TermElim => wf n'/=;
    rewrite ?ClosednE;
    simp translate_without_module;
    rewrite ?CClosednE.
-   by [].
-   by [].
-   move => [HclosedA HclosedB].
    split.
        by apply: IHA.
    by apply: IHB.
-   move => [HclosedT Hclosedy].
    by apply: IHy.
-   move => [Hclosedm Hclosedn].
    split.
        by apply: IHm.
    by apply: IHn.
-   move => [HclosedA HclosedB].
    split.
        by apply: IHA.
    by apply: IHB.
-   move => [? [? [? ?]]].
    repeat split.
        by apply: IHm.
    by apply: IHn.
-   move => Hclosedm.
    by apply IHm.
-   move => Hclosedm.
    by apply IHm.
-   exfalso.
    move: wf.
    by rewrite DecTermWFTermWF TermWFE /= setE.
Qed.

Lemma translate_without_module_preserve_closed t wf:
    Closed t ->
    Closed (⟦t, wf⟧).
Proof.
rewrite -!closedn_closed.
by apply: translate_without_module_preserve_closedn.
Qed.

Lemma subst_module_preserve_closedn name t t' n:
    Closedn t n ->
    Closedn t' n ->
    Closedn (subst_module name t' t) n.
Proof.
move: t t' name n => /=.
TermElim => t' name' n';
    rewrite !ClosednE.
-   by [].
-   by [].
-   move => [HclosedA HclosedB] Hclosedt'.
    split.
        by apply: IHA.
    apply: IHB =>//.
    by apply: closedn_closednS.
-   move => [HclosedT Hclosedy] Hclosedt'.
    split.
        by apply: IHT.
    apply: IHy => //.
    by apply: closedn_closednS.
-   move => [Hclosedm Hclosedn] Hclosedt'.
    split.
        by apply: IHm.
    by apply: IHn.
-   move => [HclosedA HclosedB] Hclosedt'.
    split.
        by apply: IHA.
    apply: IHB => //.
    by apply: closedn_closednS.
-   move => [? [? [? ?]]] Hclosedt'.
    repeat split.
    +   by apply: IHM.
    +   apply: IHN => //.
        by apply: closedn_closednS.
    +   by apply: IHm.
    +   by apply: IHn.
-   move => Hclosedm Hclosedt'.
    by apply IHm.
-   move => Hclosedm Hclosedt'.
    by apply IHm.
-   move => _ Hclosedt'.
    by case (@eqP _ name' name).
Qed.

Lemma subst_module_preserve_closed name t t':
    Closed t ->
    Closed t' ->
    Closed (subst_module name t' t).
Proof.
rewrite -!closedn_closed.
by apply: subst_module_preserve_closedn.
Qed.

Lemma remove_module_preserve_closed t Σ:
    Closed t ->
    Closed (remove_module Σ t).
Proof.
elim: Σ t => /= [//|[name t T ct cT] Σ IH] t' ct'.
apply: IH.
by apply: subst_module_preserve_closed.
Qed.

Lemma translate_preserve_closed t Σ wft wfΣ:
    Closed t ->
    Closed (⟦Σ, t, wft, wfΣ⟧).
Proof.
rewrite /translate => ct.
apply: translate_without_module_preserve_closed.
by apply: remove_module_preserve_closed.
Qed.

Lemma WFRename1 t ξ Σ: TermWF t Σ -> TermWF t.[ren ξ] Σ.
Proof.
move : t ξ Σ.
TermElim => σ Σ;
    rewrite ?substE ?TermWFE //;
    asimpl;
    auto.
-   move => [? ?].
    split; auto.
-   move => [? ?].
    split; auto.
-   move => [? ?].
    split; auto.
-   move => [? ?].
    split; auto.
-   move => [?] [?] [?] ?.
    split; auto.
Qed.

Lemma WFRename2 t ξ Σ: TermWF t.[ren ξ] Σ -> TermWF t Σ.
Proof.
move : t ξ Σ => /=.
TermElim => σ Σ;
    rewrite ?substE ?TermWFE //;
    asimpl;
    eauto.
-   move => [? ?].
    split; eauto.
-   move => [? ?].
    split; eauto.
-   move => [? ?].
    split; eauto.
-   move => [? ?].
    split; eauto.
-   move => [?] [?] [?] ?.
    repeat split; eauto.
Qed.

Lemma WFRename t ξ Σ: TermWF t Σ <-> TermWF t.[ren ξ] Σ.
Proof.
split.
    by apply: WFRename1.
by apply: WFRename2.
Qed.

Lemma translate_preserve_rename Σ t ξ wft1 wft2 wfΣ:
    ⟦Σ, t.[ren ξ], wft1, wfΣ⟧ = ⟦Σ, t, wft2, wfΣ⟧.[ren ξ].
Proof.
move: t Σ ξ wft1 wft2 wfΣ => /=.
TermElim => /= Σ ξ;
    rewrite !substE;
    asimpl => wft1 wft2 wfΣ /=.
-   by rewrite !translateE.
-   by rewrite !translateE.
-   have := wft1.
    have := wft2.
    rewrite !DecTermWFTermWF !TermWFE -!DecTermWFTermWF.
    move => [? ?] [? ?].
    rewrite !translateE // CsubstE.
    f_equal.
        by apply: IHA.
    asimpl.
    by apply: IHB.
-   have := wft1.
    have := wft2.
    rewrite !DecTermWFTermWF !TermWFE -!DecTermWFTermWF.
    move => [? ?] [? ?].
    rewrite !translateE // CsubstE.
    f_equal.
    asimpl.
    by apply: IHy.
-   have := wft1.
    have := wft2.
    rewrite !DecTermWFTermWF !TermWFE -!DecTermWFTermWF.
    move => [? ?] [? ?].
    rewrite !translateE // CsubstE.
    f_equal.
        by apply: IHm.
    asimpl.
    by apply: IHn.
-   have := wft1.
    have := wft2.
    rewrite !DecTermWFTermWF !TermWFE -!DecTermWFTermWF.
    move => [? ?] [? ?].
    rewrite !translateE // CsubstE.
    f_equal.
        by apply: IHA.
    asimpl.
    by apply: IHB.
-   have := wft1.
    have := wft2.
    rewrite !DecTermWFTermWF !TermWFE -!DecTermWFTermWF.
    move => [? [? [? ?]]] [? [? [? ?]]].
    rewrite !translateE // CsubstE.
    f_equal.
        by apply: IHm.
    by apply: IHn.
-   have := wft1.
    have := wft2.
    rewrite !DecTermWFTermWF !TermWFE -!DecTermWFTermWF.
    move => ? ?.
    rewrite !translateE // CsubstE.
    f_equal.
    by apply: IHm.
-   have := wft1.
    have := wft2.
    rewrite !DecTermWFTermWF !TermWFE -!DecTermWFTermWF.
    move => ? ?.
    rewrite !translateE // CsubstE.
    f_equal.
    by apply: IHm.
-   suff Hclosed: Closed (⟦Σ, gvar name, wft2, wfΣ⟧).
        rewrite Hclosed.
        f_equal.
        by apply: eq_irrelevance.
    by apply: translate_preserve_closed.
Qed.