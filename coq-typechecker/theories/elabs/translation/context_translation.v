(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect zify.
From sir.common Require Import string decide.
From sir.elabs.translation Require Import debruijn wf term_translation.
From sir.elabs.debruijn Require Import defs facts closed_facts.
From sir.core Require Import context.

Definition ContextItemWF Σ Γ:=
    forall idx T,
    elabs.context.dget Γ idx = Some T ->
    TermWF T Σ.

Lemma ContextItemWF_cons1 Σ T Γ:
    ContextItemWF Σ (T :: Γ)
    -> ContextItemWF Σ (Γ) /\ TermWF T Σ.
Proof.
move => Hwf.
split.
    move => idx /= T' Hin.
    erewrite WFRename.
    apply: (Hwf idx.+1) => /=.
    by rewrite Hin.
erewrite WFRename.
by apply: (Hwf 0).
Qed.

Lemma ContextItemWF_cons2 Σ T Γ:
    ContextItemWF Σ (Γ) /\ TermWF T Σ ->
    ContextItemWF Σ (T :: Γ).
Proof.
rewrite /ContextItemWF.
move => [wfΓ wfT] [|idx] T' /=.
    move => [<-].
    by rewrite -WFRename.
case Heq: (elabs.context.dget Γ idx) => [t|//].
move => [<-].
rewrite -WFRename.
apply: wfΓ.
by apply: Heq.
Qed.

Lemma ContextItemWF_cons Σ T Γ:
    ContextItemWF Σ (T :: Γ) <-> ContextItemWF Σ (Γ) /\ TermWF T Σ.
Proof.
split.
    by apply: ContextItemWF_cons1.
by apply: ContextItemWF_cons2.
Qed.

Lemma ContextItemWF_dec Σ Γ: {ContextItemWF Σ Γ} + {~ContextItemWF Σ Γ}.
Proof.
elim: Γ Σ => [|t Γ IH] Σ.
    left.
    by case.
case (TermWF_dec t Σ) => [HWF |HnotWF];
    last first.
    right.
    by move /ContextItemWF_cons => [].
case (IH Σ) => [Hwf|Hnot].
    left.
    by rewrite ContextItemWF_cons.
right.
by move /ContextItemWF_cons => [].
Qed.

Definition DecContextItemWF Σ Γ: Prop :=
    @bool_decide (ContextItemWF Σ Γ) (ContextItemWF_dec Σ Γ) = true.

Lemma DecContextItemWFContextItemWF Σ Γ:
    DecContextItemWF Σ Γ <-> ContextItemWF Σ Γ.
Proof.
by rewrite /DecContextItemWF bool_decide_true.
Qed.

Definition translateΓ Σ Γ (wfΣ: DecModuleWF Σ) (wfΓ: DecContextItemWF Σ Γ) : context.Context.
Proof.
move: Γ wfΓ.
elim => [|t Γ IH].
    move => _.
    by apply: [::].
move /DecContextItemWFContextItemWF/ContextItemWF_cons =>
    [/DecContextItemWFContextItemWF/IH Γ' /DecTermWFTermWF wft].
have TT' := ⟦Σ, t, wft, wfΣ⟧.
by apply: (TT' :: Γ').
Defined.

Lemma translateΓ_cons Σ Γ t wfΣ  wfΓt wfΓ' wf:
    translateΓ Σ (t :: Γ) wfΣ wfΓt =
    ⟦Σ, t, wf, wfΣ⟧:: (translateΓ Σ Γ wfΣ wfΓ').
Proof.
move => /=.
case _: (iffLR (Q:=ContextItemWF Σ Γ /\ TermWF t Σ)
(ContextItemWF_cons Σ t Γ)
(iffLR (Q:=ContextItemWF Σ (t :: Γ))
   (DecContextItemWFContextItemWF Σ (t :: Γ)) wfΓt)
) => [wfΓ1 wft1] /=.
repeat f_equal;
    by apply: eq_irrelevance.
Qed.

Lemma translateΓ_nil Σ wfΣ wf:
    translateΓ Σ nil wfΣ wf =nil.
Proof.
by move => /=.
Qed.
#[global] Opaque translateΓ.