(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From HB Require Import structures.
From mathcomp Require Import all_ssreflect.
From Equations Require Import Equations.
From sir.common Require Import string.
From sir.common.fin_sets Require Import gset.
From sir.core.ast Require Import term.
From sir.elabs.ast Require Import term module facts.
From sir.elabs.translation Require Import wf remove_module.

Equations translate_without_module (t: Term) (wf: DecTermWF t [::]): CTerm :=
translate_without_module (var idx) _ := Cvar idx;
translate_without_module (univ lvl) _ := Cuniv lvl;
translate_without_module (pi A B) wf :=
    Cpi (translate_without_module A _) (translate_without_module B _);
translate_without_module (lam A t) wf :=
    Clam (translate_without_module t _);
translate_without_module (appl m n) wf :=
    Cappl (translate_without_module m _) (translate_without_module n _);
translate_without_module (sigma A B) wf :=
    Csigma (translate_without_module A _) (translate_without_module B _);
translate_without_module (pair A B a b) wf :=
    Cpair (translate_without_module a _) (translate_without_module b _);
translate_without_module (proj1 m) wf :=
    Cproj1 (translate_without_module m _);
translate_without_module (proj2 m) wf :=
    Cproj2 (translate_without_module m _);
translate_without_module (gvar name) t := _.
Next Obligation.
move => /= _ A B.
rewrite !DecTermWFTermWF TermWFE.
by move => [].
Qed.
Next Obligation.
move => /= _ A B.
rewrite !DecTermWFTermWF TermWFE.
by move => [].
Qed.
Next Obligation.
move => /= _ A t.
rewrite !DecTermWFTermWF TermWFE.
by move => [].
Qed.
Next Obligation.
move => /= _ A t.
rewrite !DecTermWFTermWF TermWFE.
by move => [].
Qed.
Next Obligation.
move => /= _ m n.
rewrite !DecTermWFTermWF TermWFE.
by move => [].
Qed.
Next Obligation.
move => /= _ m n.
rewrite !DecTermWFTermWF TermWFE.
by move => [].
Qed.
Next Obligation.
move => /= _ A B.
rewrite !DecTermWFTermWF TermWFE.
by move => [].
Qed.
Next Obligation.
move => /= _ A B a b.
rewrite !DecTermWFTermWF TermWFE.
by move => [? [? []]].
Qed.
Next Obligation.
move => /= _ A B a b.
rewrite !DecTermWFTermWF TermWFE.
by move => [? [? []]].
Qed.
Next Obligation.
move => /= _ m.
by rewrite !DecTermWFTermWF TermWFE.
Qed.
Next Obligation.
move => /= _ m.
by rewrite !DecTermWFTermWF TermWFE.
Qed.
Next Obligation.
move => /= _ m Hfalse.
exfalso.
move: Hfalse.
by rewrite !DecTermWFTermWF TermWFE /= setE.
Qed.
Notation "'⟦' x ',' wf '⟧'" := (translate_without_module x wf) (at level 30, x at next level, wf at next level, format "⟦ x ,  wf ⟧").

Lemma TermWF_remove_module t Σ: DecModuleWF Σ -> DecTermWF t Σ -> DecTermWF (remove_module Σ t) [::].
Proof.
rewrite !DecModuleWFModuleWF !DecTermWFTermWF.
move => HWF.
elim: HWF t => {Σ} //=.
move => name t' T' ct' cT' Σ.
move => HWFΣ IH HWFt' Hnotin t HWFt.
apply: IH.
apply: TermWF_remove_name => //.
apply: not_in_module_not_in_term.
    by apply: Hnotin.
by [].
Qed.

Definition translate Σ t (wft: DecTermWF t Σ) (wfΣ: DecModuleWF Σ): CTerm :=
    translate_without_module (remove_module Σ t) (TermWF_remove_module t Σ wfΣ wft).


Notation "'⟦' Σ ',' x ',' wft ',' wfΣ '⟧'" :=
        (translate Σ x wft wfΣ) (at level 30,
                            Σ at next level,
                            x at next level,
                            wft at next level,
                            wfΣ at next level,
                            format "⟦ Σ ,  x ,  wft ,  wfΣ ⟧").

Module TranslateRewrite.
Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(* rewrite lemmas*)
Lemma translate_var Σ idx wft wfΣ:
⟦Σ, var idx, wft, wfΣ⟧ = Cvar idx.
Proof.
rewrite /translate.
move: (TermWF_remove_module (var idx) Σ wfΣ wft).
case Heq : (remove_module Σ (var idx)) => wf';
    move: Heq;
    rewrite remove_moduleE //.
by move => [->].
Qed.

Lemma translate_univ Σ lvl wft wfΣ: ⟦Σ, univ lvl, wft, wfΣ⟧ = Cuniv lvl.
Proof.
rewrite /translate.
move: (TermWF_remove_module (univ lvl) Σ wfΣ wft).
case Heq : (remove_module Σ (univ lvl)) => wf';
    move: Heq;
    rewrite remove_moduleE //.
by move => [->].
Qed.

Lemma translate_pi Σ A B wft wfΣ wftA wfΣA wftB wfΣB:
    ⟦Σ, (pi A B), wft, wfΣ⟧ =
    Cpi (⟦Σ, A, wftA, wfΣA⟧) (⟦Σ, B, wftB, wfΣB⟧).
Proof.
rewrite /translate/=.
move: (TermWF_remove_module (pi A B) Σ wfΣ wft).
case Heq : (remove_module Σ (pi A B)) => [||A' B'|||||||] wf'//=;
    move: Heq;
    rewrite remove_moduleE //.
move => [? ?].
simp translate_without_module.
subst.
repeat f_equal;
    by apply: eq_irrelevance.
Qed.

Lemma translate_lam Σ T y wft wfΣ wfty wfΣy:
    ⟦Σ, (lam T y), wft, wfΣ⟧ =
    Clam (⟦Σ, y, wfty, wfΣy⟧).
Proof.
rewrite /translate/=.
move: (TermWF_remove_module (lam T y) Σ wfΣ wft).
case Heq : (remove_module Σ (lam T y)) => [|||T' y'||||||] wf'//=;
    move: Heq;
    rewrite remove_moduleE //.
move => [? ?].
simp translate_without_module.
subst.
repeat f_equal;
    by apply: eq_irrelevance.
Qed.

Lemma translate_appl Σ m n wft wfΣ wftm wfΣm wftn wfΣn:
    ⟦Σ, (appl m n), wft, wfΣ⟧ =
    Cappl (⟦Σ, m, wftm, wfΣm⟧) (⟦Σ, n, wftn, wfΣn⟧).
Proof.
rewrite /translate/=.
move: (TermWF_remove_module (appl m n) Σ wfΣ wft).
case Heq : (remove_module Σ (appl m n)) => [||||m' n'|||||] wf'//=;
    move: Heq;
    rewrite remove_moduleE //.
move => [? ?].
simp translate_without_module.
subst.
repeat f_equal;
    by apply: eq_irrelevance.
Qed.

Lemma translate_sigma Σ A B wft wfΣ wftA wfΣA wftB wfΣB:
    ⟦Σ, (sigma A B), wft, wfΣ⟧ =
    Csigma (⟦Σ, A, wftA, wfΣA⟧) (⟦Σ, B, wftB, wfΣB⟧).
Proof.
rewrite /translate/=.
move: (TermWF_remove_module (sigma A B) Σ wfΣ wft).
case Heq : (remove_module Σ (sigma A B)) => [|||||A' B'||||] wf'//=;
    move: Heq;
    rewrite remove_moduleE //.
move => [? ?].
simp translate_without_module.
subst.
repeat f_equal;
    by apply: eq_irrelevance.
Qed.

Lemma translate_pair Σ A B a b wft wfΣ wfta wfΣa wftb wfΣb:
    ⟦Σ, (pair A B a b), wft, wfΣ⟧ =
    Cpair  (⟦Σ, a, wfta, wfΣa⟧)  (⟦Σ, b, wftb, wfΣb⟧) .
Proof.
rewrite /translate/=.
move: (TermWF_remove_module (pair A B a b) Σ wfΣ wft).
case Heq : (remove_module Σ (pair A B a b)) => [||||||A' B' a' b'|||] wf'//=;
    move: Heq;
    rewrite remove_moduleE //.
move => [? ? ? ?].
simp translate_without_module.
subst.
repeat f_equal;
    by apply: eq_irrelevance.
Qed.

Lemma translate_proj1 Σ m wft wfΣ wftm wfΣm:
    ⟦Σ, (proj1 m), wft, wfΣ⟧ =
    Cproj1 (⟦Σ, m, wftm, wfΣm⟧).
Proof.
rewrite /translate/=.
move: (TermWF_remove_module (proj1 m) Σ wfΣ wft).
case Heq : (remove_module Σ (proj1 m)) => [|||||||m'||] wf'//=;
    move: Heq;
    rewrite remove_moduleE //.
move => [?].
simp translate_without_module.
subst.
repeat f_equal.
by apply: eq_irrelevance.
Qed.

Lemma translate_proj2 Σ m wft wfΣ wftm wfΣm:
    ⟦Σ, (proj2 m), wft, wfΣ⟧ =
    Cproj2 (⟦Σ, m, wftm, wfΣm⟧).
Proof.
rewrite /translate/=.
move: (TermWF_remove_module (proj2 m) Σ wfΣ wft).
case Heq : (remove_module Σ (proj2 m)) => [|||||||m'||] wf'//=;
    move: Heq;
    rewrite remove_moduleE //.
move => [?].
simp translate_without_module.
subst.
repeat f_equal.
by apply: eq_irrelevance.
Qed.
End TranslateRewrite.

Definition translateE := (
    TranslateRewrite.translate_var, TranslateRewrite.translate_univ,
    TranslateRewrite.translate_pi, TranslateRewrite.translate_lam,
    TranslateRewrite.translate_appl, TranslateRewrite.translate_sigma,
    TranslateRewrite.translate_pair, TranslateRewrite.translate_proj1,
    TranslateRewrite.translate_proj2
).

Lemma modulewf_termwf Σ n' t:
    ModuleWF Σ ->
    mod_get_term Σ n' = Some t ->
    TermWF t Σ.
Proof.
move => HWF.
elim: HWF n' t => //= {Σ}.
move => n t T ct cT Σ HWFΣ IH HWFT Hnotin n' t'.
case (@eqP _ n n') => [?|Hneq].
    rewrite mod_get_term_cons_eq //.
    move => [?].
    subst.
    rewrite /TermWF /=.
    apply: set_union_subseteq_r'.
    by apply: HWFT.
rewrite mod_get_term_cons_neq //.
move => Hget.
have Hwft':= IH _ _ Hget.
rewrite /TermWF /=.
apply: set_union_subseteq_r'.
by apply: Hwft'.
Qed.

Lemma translate_gvar' Σ n t wf1 wf2:
ModuleWF Σ ->
mod_get_term Σ n = Some t ->
⟦remove_module Σ (gvar n), wf1⟧ =
⟦remove_module Σ t, wf2⟧.
Proof.
move: Σ n t wf1 wf2.
elim => [//| [n t T ct cT] Σ IH] n' t' /=.
case (@eqP _ n n') => [Heq|Hneq];
    move => wf1 wf2 HWFΣ/=.
    rewrite mod_get_term_cons_eq //.
    move => [?].
    inversion HWFΣ.
    subst.
    have Heq : t' = subst_module n' t' t'.
        rewrite WFTerm_subst_id //.
        apply: not_in_module_not_in_term.
            by apply: H5.
        by [].
    move: Heq wf2.
    move : (subst_module n' t' t') => x ?.
    subst => ?.
    f_equal.
    apply: eq_irrelevance.
rewrite mod_get_term_cons_neq //.
move => Hget.
inversion HWFΣ.
subst.
apply: IH => //.
    rewrite WFTerm_subst_id //.
apply: not_in_module_not_in_term.
    by apply: H5.
apply: modulewf_termwf => //.
by apply: Hget.
Qed.

Lemma translate_gvar Σ n t wft1 wft2 wfΣ:
    mod_get_term Σ n = Some t ->
    (⟦Σ, gvar n, wft1, wfΣ⟧) = (⟦Σ, t, wft2, wfΣ⟧).
Proof.
rewrite /translate => Hget.
apply: translate_gvar' => //.
by rewrite -DecModuleWFModuleWF.
Qed.