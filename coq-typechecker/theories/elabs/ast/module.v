(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.common Require Import string.
From sir.elabs Require Import ast.term debruijn.defs.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Inductive ModuleItem :=
(*| module_extern (name: string) (T: Term)*) (* TODO later *)
| module_local (name: string) (t T: Term) (H1: Closed t) (H2: Closed T).

Definition Module := seq ModuleItem.

Fixpoint mod_get (Σ : Module) (name : string) : option ModuleItem :=
match Σ with
| [::] => None
| (module_local name' t T ct cT) :: Σ'  => if name' == name then
                                        Some (@module_local name' t T ct cT)
                                      else
                                        mod_get Σ' name
end.

Definition mod_get_term (Σ : Module) (name : string) : option Term :=
match mod_get Σ name with
|  None => None
| Some (module_local name' t T _ _) => Some t
end.

Definition mod_get_type (Σ : Module) (name : string) : option Term :=
match mod_get Σ name with
| None => None
| Some (module_local name' t T _ _) => Some T
end.

