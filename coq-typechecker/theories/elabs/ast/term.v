(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.common Require Import string.
Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

(* Grammer of a term *)
Inductive Term : Type :=
 (* bounded variable with binder removed,identified using debruijn-levels *)
| var (idx: nat): Term
 (* Universes.. TODO this must be replaced with subtyping based universes *)
| univ (l: nat): Term
 (* Function type from A to B*)
| pi (A: Term) (B: Term)
 (* Lambda Abstraction (Function implementation) *)
| lam (T: Term) (y: Term)
 (* Function Application *)
| appl (m: Term) (n: Term)
 (* sigma type*)
| sigma (A: Term) (B: Term)
  (* dependent pairs*)
| pair (M N: Term) (m: Term) (n: Term)
  (* projection for dependent pair *)
| proj1 (m: Term)
| proj2 (m: Term)
(* global variable pulled from a module *)
| gvar (n: string).

Tactic Notation "TermElim" :=
elim => /=
    [ idx
    | lvl
    | A IHA B IHB
    | T IHT y IHy
    | m IHm n IHn
    | A IHA B IHB
    | M IHM N IHN m IHm n IHn
    | m IHm
    | m IHm
    | name
    ].
