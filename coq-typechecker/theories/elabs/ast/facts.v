(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
Require Import Eqdep_dec.
Require Import Logic.FunctionalExtensionality.
From mathcomp Require Import all_ssreflect.
From sir.common Require Import string.
From sir.elabs.ast Require Import term module.
From sir.elabs.debruijn Require Import defs.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Lemma term_eq_dec (t1 t2: Term): t1 = t2 \/ t1 <> t2.
Proof.
move: t1 t2.
TermElim;
  case =>
  [ idx'
    | lvl'
    | A' B'
    | T' y'
    | m' n'
    | A' B'
    | M' N' m' n'
    | m'
    | m'
    | name'
  ];
  try by right.
-   case (@eqP _ idx idx') => [->|Hneq].
        by left.
    right.
    by move => [].
-   case (@eqP _ lvl lvl') => [->|Hneq].
        by left.
    right.
    by move => [].
-   case : (IHA A') => [->|HneqA];
        last first.
        right.
        by move => [].
    case : (IHB B') => [->|HneqB].
        by left.
        right.
        by move => [].
-   case : (IHT T') => [->|HneqT];
        last first.
        right.
        by move => [].
    case : (IHy y') => [->|Hneqy].
        by left.
        right.
        by move => [].
-   case : (IHm m') => [->|Hneqm];
        last first.
        right.
        by move => [].
    case : (IHn n') => [->|Hneqn].
        by left.
        right.
        by move => [].
-   case : (IHA A') => [->|HneqA];
        last first.
        right.
        by move => [].
    case : (IHB B') => [->|HneqB].
        by left.
        right.
        by move => [].
-   case : (IHM M') => [->|HneqM];
        last first.
        right.
        by move => [].
    case : (IHN N') => [->|HneqN];
        last first.
        right.
        by move => [].
    case : (IHm m') => [->|Hneqm];
        last first.
        right.
        by move => [].
    case : (IHn n') => [->|Hneqn].
        by left.
    right.
    by move => [].
-   case : (IHm m') => [->|Hneqm].
        by left.
    right.
    by move => [].
-   case : (IHm m') => [->|Hneqm].
        by left.
    right.
    by move => [].
-   case : (@eqP _ name name') => [->|Hneq].
        by left.
    right.
    by move => [].
Qed.

Lemma Closed_irrel (t: Term) (p1 p2 : Closed t): p1 = p2.
Proof.
apply: functional_extensionality_dep => σ.
apply eq_proofs_unicity.
by apply: term_eq_dec.
Qed.

Lemma mod_get_term_closed Σ n t:
    mod_get_term Σ n = Some t -> Closed t.
Proof.
rewrite /mod_get_term.
case (mod_get Σ n) => [[? ? ? ? ?]|//].
by move => [<-].
Qed.

Lemma mod_get_type_closed Σ n T:
    mod_get_type Σ n = Some T -> Closed T.
Proof.
rewrite /mod_get_type.
case (mod_get Σ n) => [[? ? ? ? ?]|//].
by move => [<-].
Qed.

Lemma mod_get_none_mod_term Σ n:
    mod_get Σ n =  None ->
    mod_get_term Σ n = None.
Proof.
rewrite /mod_get_term.
by move ->.
Qed.

Lemma mod_get_none_mod_type Σ n:
    mod_get Σ n =  None ->
    mod_get_type Σ n = None.
Proof.
rewrite /mod_get_type.
by move ->.
Qed.

Lemma mod_get_some_mod_get_term_some Σ n t T ct cT:
    mod_get Σ n = Some (@module_local n t T ct cT) ->
    mod_get_term Σ n = Some t.
Proof.
rewrite /mod_get_term.
by move ->.
Qed.

Lemma mod_get_term_cons_neq Σ n1 n2 t1 T ct cT:
    n1 <> n2 ->
    mod_get_term ((@module_local n1 t1 T ct cT) :: Σ) n2 =
    mod_get_term (Σ) n2.
Proof.
rewrite /mod_get_term /=.
by case (@eqP _ n1 n2).
Qed.

Lemma mod_get_type_cons_neq Σ n1 n2 t1 T ct cT:
    n1 <> n2 ->
    mod_get_type ((@module_local n1 t1 T ct cT) :: Σ) n2 =
    mod_get_type (Σ) n2.
Proof.
rewrite /mod_get_type /=.
by case (@eqP _ n1 n2).
Qed.

Lemma mod_get_term_cons_eq Σ n1 n2 t1 T ct cT:
    n1 = n2 ->
    mod_get_term ((@module_local n1 t1 T ct cT) :: Σ) n2 =
    Some t1.
Proof.
move => Heq.
by rewrite /mod_get_term Heq /= eq_refl.
Qed.

Lemma mod_get_type_cons_eq Σ n1 n2 t1 T ct cT:
    n1 = n2 ->
    mod_get_type ((@module_local n1 t1 T ct cT) :: Σ) n2 =
    Some T.
Proof.
move => Heq.
by rewrite /mod_get_type Heq /= eq_refl.
Qed.

Lemma mod_get_some_mod_get_type_some Σ n t T ct cT:
    mod_get Σ n = Some (@module_local n t T ct cT) ->
    mod_get_type Σ n = Some T.
Proof.
rewrite /mod_get_type.
by move ->.
Qed.

Lemma mod_get_name Σ n1 n2 t T ct cT:
    mod_get Σ n1 = Some (@module_local n2 t T ct cT) ->
    n1 = n2.
Proof.
elim: Σ n1 n2 t T ct cT => [|[]]//.
move => n1 t1 T1 ct1 cT1 Σ IH n2 n3 t2 T2 ct2 cT2 /=.
case (@eqP _ n1 n2) => [<-|Hneq].
    by move => [].
by apply: IH.
Qed.

Lemma mod_get_type_some_get_some Σ n T:
    mod_get_type Σ n = Some T ->
    exists t ct cT,
    mod_get Σ n = Some (@module_local n t T ct cT).
Proof.
rewrite /mod_get_type.
case Heq: (mod_get Σ n) => [[n1 t1 T1 ct cT]|//].
move => [<-].
exists t1, ct, cT.
by have := mod_get_name Heq => ->.
Qed.