(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)

From mathcomp Require Import all_ssreflect.
From sir.elabs.bidirectional Require Import typing.
From sir.elabs.ast Require Import term module facts.
From sir.elabs.debruijn Require Import defs.
From sir.elabs.meta Require Import typing conversion confluence reduction red_facts.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Lemma type_inf_pi_complete Σ Ti Tj Γ A B i j:
    Σ; Γ ⊢ A ⇒ Ti ->
    Σ ⊢ univ i ≅ Ti ->
    Σ; (A :: Γ) ⊢ B ⇒ Tj ->
    Σ ⊢ univ j ≅ Tj ->
    Σ; Γ ⊢ pi A B ⇒ univ (maxn i j).
Proof.
move => HinfA HconvTi HinfB HconvTj.
apply: TypeInf_pi;
    apply: TypeInfU_red.
-   by apply: HinfA.
-   move : (mred_cr HconvTi) => [Ti' []].
    by move /mred_inv_univ => ->.
-   by apply: HinfB.
-   move : (mred_cr HconvTj) => [Tj' []].
    by move /mred_inv_univ => ->.
Qed.

Lemma type_inf_lam_complate Σ Tl TB l Γ A B t:
    Σ; Γ ⊢ A ⇒ Tl ->
    Σ ⊢ univ l ≅ Tl ->
    Σ; (A :: Γ) ⊢ t ⇒ TB ->
    Σ ⊢ B ≅ TB ->
    Σ; Γ ⊢ lam A t ⇒ pi A TB.
Proof.
move => HtypeA HconvTl Htypet HconvB.
apply: TypeInf_lam => //.
apply: TypeInfU_red.
    by apply: HtypeA.
move : (mred_cr HconvTl) => [T2' []].
move /mred_inv_univ => -> HmredTl.
by apply: HmredTl.
Qed.

Lemma type_inf_appl_complete Σ Tm Tn A' Γ m n B':
    Σ; Γ ⊢ m ⇒ Tm ->
    Σ; Γ ⊢ n ⇒ Tn ->
    Σ ⊢ Tn ≅ A' ->
    Σ ⊢ Tm ⟶* pi A' B' ->
    Σ; Γ ⊢ appl m n ⇒ B'.[n/].
Proof.
move => Htypem Htypen HconvTn HredTm.
apply: TypeInf_appl.
    apply: TypeInfPI_red.
    +   by apply: Htypem.
    +   by apply: HredTm.
apply: TypeCheck_conv.
    by apply: Htypen.
by [].
Qed.

Lemma type_inf_sigma_complete Σ Ti Tj Γ A B i j:
    Σ; Γ ⊢ A ⇒ Ti ->
    Σ ⊢ univ i ≅ Ti ->
    Σ; (A :: Γ) ⊢ B ⇒ Tj ->
    Σ ⊢ univ j ≅ Tj ->
    Σ; Γ ⊢ sigma A B ⇒ univ (maxn i j).
Proof.
move => HinfA HconvTi HinfB HconvTj.
apply: TypeInf_sigma;
    apply: TypeInfU_red.
-   by apply: HinfA.
-   move : (mred_cr HconvTi) => [Ti' []].
    by move /mred_inv_univ => ->.
-   by apply: HinfB.
-   move : (mred_cr HconvTj) => [Tj' []].
    by move /mred_inv_univ => ->.
Qed.

Lemma type_inf_complete Σ Γ t T:
    Σ; Γ ⊢ t : T ->
    exists T',
        Σ; Γ ⊢ t ⇒ T' /\
        Σ ⊢ T ≅ T'.
Proof.
have ? := Conv_refl.
SynTypeElim Σ Γ t T.
-   exists T.
    split => //.
    by apply: TypeInf_var.
-   exists (univ lvl.+1).
    split => //.
    apply: TypeInf_univ.
-   move : IHA => [T1 [HtypeAT1 HconvT1]].
    move : IHB => [T2 [HtypeBT2 HconvT2]].
    exists (univ (maxn i j)).
    split => //.
    by apply: (@type_inf_pi_complete _ T1 T2).
-   move : IHA => [T1 [HtypeAT1 HconvT1]].
    move : IHt => [T2 [HtypeBT2 HconvT2]].
    exists (pi A T2).
    split.
        by apply: (@type_inf_lam_complate _ T1 T2 l).
    by apply Conv_pi.
-   move : IHm => [T1 [HtypemT1 HconvT1]].
    move : IHn => [T2 [HtypenT2 HconvT2]].
    move : (mred_cr HconvT1) => [T1' []].
    move /mred_inv_pi => [A' [B' [HeqT1]]].
    move : HeqT1 => -> [HmredAA' HmredBB'].
    clear T1' HconvT1.
    move => HmredT1.
    exists B'.[n/].
    split;
        last first.
        apply: conv_subst.
        by apply: mred_conv.
    apply: (@type_inf_appl_complete _ T1 T2 A') => //.
    apply: (@Conv_trans _ T2 A).
        by apply: Conv_symm.
    by apply: mred_conv.
-   move : IHA => [T1 [HtypeAT1 HconvT1]].
    move : IHB => [T2 [HtypeBT2 HconvT2]].
    exists (univ (maxn i j)).
    split => //.
    by apply: (@type_inf_sigma_complete _ T1 T2).
-   move : IHm => [T1 [HtypemT1 HconvT1]].
    move : IHn => [T2 [HtypenT2 HconvT2]].
    move : IHA => [T3 [HtypeAT3 HconvT3]].
    move : IHB => [T4 [HtypeBT4 HconvT4]].
    move: HconvT3 => /mred_cr [T' []] /mred_inv_univ -> {T'} HmredT3.
    move: HconvT4 => /mred_cr [T' []] /mred_inv_univ -> {T'} HmredT4.
    exists (sigma A B).
    split => //.
    apply: TypeInf_pair => //.
    +   apply: TypeInfU_red.
            by apply: HtypeAT3.
        by apply: HmredT3.
    +   apply: TypeInfU_red.
            by apply: HtypeBT4.
        by apply: HmredT4.
    +   apply: TypeCheck_conv.
            by apply: HtypemT1.
        by apply: Conv_symm.
    +   apply: TypeCheck_conv.
            by apply: HtypenT2.
        by apply: Conv_symm.
-   move : IHm => [T1 [HtypemT1 HconvT1]].
    move: HconvT1 => /mred_cr  [T2 []].
    move /mred_inv_sigma => [A' [B' [-> [HmredAA' HmredBB']]]] HmredT1.
    exists A'.
    split;
        last by apply: mred_conv.
    apply: TypeInf_proj1.
    by apply: (@TypeInfSI_red _ _ _ T1 _  B').
-   move : IHm => [T1 [HtypemT1 HconvT1]].
    move: HconvT1 => /mred_cr  [T2 []].
    move /mred_inv_sigma => [A' [B' [-> [HmredAA' HmredBB']]]] HmredT1.
    exists B'.[proj1 m/].
    split;
        last first.
        apply:conv_subst.
        by apply: mred_conv.
    apply: TypeInf_proj2.
    by apply: (@TypeInfSI_red _ _ _ T1 A').
-   move : IHT => [T1 [HtypetT Hconv]].
    exists T1.
    split => //.
    apply (@Conv_trans _ _ T) => //.
    by apply: Conv_symm.
-   exists T.
    split =>//.
    by apply: TypeInf_gvar.
Qed.