(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)

From mathcomp Require Import all_ssreflect.
From sir.elabs.bidirectional Require Import typing.
From sir.elabs.meta Require Import typing reduction conversion confluence.
From sir.elabs.ast Require Import term module facts.
From sir.elabs Require Import context debruijn.defs.
Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Lemma type_inf_sound Σ Γ t T:
    Σ; Γ ⊢ t ⇒ T ->
    Σ; Γ ⊢ t : T.
Proof.
TypeInfElim Σ Γ t T.
-   by apply: SynType_var.
-   by apply: SynType_univ.
-   apply: SynType_pi.
        by apply: IHA.
    by apply: IHB.
-   apply: SynType_lam.
        by apply: IHA.
    by apply: IHt.
-   apply: SynType_appl.
        by apply: IHt.
    by apply: IHn.
-   apply: SynType_sigma.
        by apply: IHA.
    by apply: IHB.
-   apply: SynType_pair.
    +   by apply: IHA.
    +   by apply: IHB.
    +   by apply: IHm.
    +   by apply: IHn.
-   apply: SynType_proj1.
    by apply: IHm.
-   apply: SynType_proj2.
    by apply: IHm.
-   by apply: SynType_gvar.
-   apply: SynType_conv.
        by apply: IHt.
    by [].
-   apply: SynType_conv.
        by apply: IHt.
    by apply: mred_conv.
-   apply: SynType_conv.
        by apply: IHt.
    by apply:mred_conv.
-   apply: SynType_conv.
        by apply: IHt.
    by apply:mred_conv.
Qed.

Lemma type_inf_pi_sound Σ Γ t T:
    Σ; Γ ⊢ t ⇒ₚᵢ T ->
    ⊢ Σ; Γ ->
    Σ; Γ ⊢ t : T.
Proof.
elim => {Σ Γ t T} => Σ Γ t T A B Hinft.
move => /mred_conv Hconv HWF.
apply: SynType_conv;
    last by apply: Hconv.
by apply: type_inf_sound.
Qed.

Lemma type_inf_univ_sound Σ Γ t T:
    Σ; Γ ⊢ t ⇒ᵤ T ->
    ⊢ Σ; Γ ->
    Σ; Γ ⊢ t : T.
Proof.
elim => {Σ Γ t T} => Σ Γ t T l Hinft.
move => /mred_conv Hconv HWF.
apply: SynType_conv;
    last by apply: Hconv.
by apply: type_inf_sound.
Qed.

Lemma type_inf_sigma_sound Σ Γ t T:
    Σ; Γ ⊢ t ⇒ₛᵢ T ->
    ⊢ Σ; Γ ->
    Σ; Γ ⊢ t : T.
Proof.
elim => {Σ Γ t T} => Σ Γ t T A B Hinf.
move => /mred_conv Hconv HWF.
apply: SynType_conv;
    last by apply: Hconv.
by apply: type_inf_sound.
Qed.

Lemma type_check_sound Σ Γ t T:
    Σ; Γ ⊢ t ⟸ T ->
    ⊢ Σ; Γ ->
    Σ; Γ ⊢ t : T.
elim => {Σ Γ t T} => Σ Γ t T T' Hinft Hconv Hwf.
apply: SynType_conv;
    last by apply: Hconv.
by apply: type_inf_sound.
Qed.
