(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From sir.elabs.ast Require Import term module.
From sir.elabs Require Import debruijn.defs context.
From sir.elabs.meta Require Import conversion reduction.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Reserved Notation "Σ ';' Γ '⊢' t ⇒ T" (at level 20, Γ at next level, t at next level, T at next level).
Reserved Notation "Σ ';' Γ '⊢' t '⇒ᵤ' T" (at level 20, Γ at next level, t at next level, T at next level).
Reserved Notation "Σ ';' Γ '⊢' t '⇒ₚᵢ' T" (at level 20, Γ at next level, t at next level, T at next level).
Reserved Notation "Σ ';' Γ '⊢' t '⇒ₛᵢ' T" (at level 20, Γ at next level, t at next level, T at next level).
Reserved Notation "Σ ';' Γ '⊢' t ⟸ T" (at level 20, Γ at next level, t at next level, T at next level).

Inductive TypeInf: Module -> Context -> Term -> Term -> Prop :=
| TypeInf_var Σ Γ idx T:
    dget Γ idx = Some T ->
    Σ; Γ ⊢ (var idx) ⇒ T
| TypeInf_univ Σ Γ l:
    Σ; Γ ⊢ univ l ⇒ univ (S l)
| TypeInf_pi Σ Γ A B i j:
                Σ; Γ ⊢ A ⇒ᵤ univ i ->
                Σ; (A::Γ) ⊢ B ⇒ᵤ univ j ->
                Σ; Γ ⊢ pi A B ⇒ univ(maxn i j)
| TypeInf_lam Σ Γ A B t l:
                Σ; Γ ⊢ A ⇒ᵤ univ l ->
                Σ; (A :: Γ) ⊢ t ⇒ B ->
                Σ; Γ ⊢ lam A t ⇒ pi A B
| TypeInf_appl Σ Γ A B m n:
                Σ; Γ ⊢ m ⇒ₚᵢ pi A B ->
                Σ; Γ ⊢ n ⟸ A ->
                Σ; Γ ⊢ appl m n ⇒ B.[n/]
| TypeInf_sigma Σ Γ A B i j:
                Σ; Γ ⊢ A ⇒ᵤ univ i ->
                Σ; (A :: Γ ) ⊢ B ⇒ᵤ univ j ->
                Σ; Γ ⊢ sigma A B ⇒ univ (maxn i j)
| TypeInf_pair Σ Γ m n A B i j:
                Σ; Γ ⊢ A ⇒ᵤ (univ i) ->
                Σ; (A::Γ) ⊢ B ⇒ᵤ (univ j) ->
                Σ; Γ ⊢ m ⟸ A ->
                Σ; Γ ⊢ n ⟸ B.[m/] ->
                Σ; Γ ⊢ pair A B m n ⇒ sigma A B
| TypeInf_proj1 Σ Γ m A B:
                Σ; Γ ⊢ m ⇒ₛᵢ sigma A B ->
                Σ; Γ ⊢ proj1 m ⇒ A
| TypeInf_proj2 Σ Γ m A B:
                Σ; Γ ⊢ m ⇒ₛᵢ sigma A B ->
                Σ; Γ ⊢ proj2 m ⇒ B.[proj1 m/]
| TypeInf_gvar Σ Γ name T:
                mod_get_type Σ name = Some T ->
                Σ; Γ ⊢ gvar name ⇒ T
where "Σ ';' Γ '⊢' t ⇒ T" := (TypeInf Σ Γ t T)
with TypeCheck: Module -> Context -> Term -> Term -> Prop :=
| TypeCheck_conv Σ Γ t T T':
    Σ; Γ ⊢ t ⇒T' ->
    Σ ⊢ T' ≅ T ->
    Σ; Γ ⊢ t ⟸ T
where "Σ ';' Γ  '⊢' t ⟸ T" := (TypeCheck Σ Γ t T)
with TypeInfU: Module -> Context -> Term -> Term -> Prop :=
| TypeInfU_red Σ Γ t T l:
    Σ; Γ ⊢ t ⇒ T   ->
    Σ ⊢ T ⟶* univ l ->
    Σ; Γ ⊢ t ⇒ᵤ univ l
where "Σ ';' Γ '⊢' t '⇒ᵤ' T" := (TypeInfU Σ Γ t T)

with TypeInfPI: Module -> Context -> Term -> Term -> Prop :=
| TypeInfPI_red Σ Γ t T A B:
    Σ; Γ ⊢ t ⇒ T   ->
    Σ ⊢ T ⟶* pi A B ->
    Σ; Γ ⊢ t ⇒ₚᵢ (pi A B)
where "Σ ';' Γ '⊢' t '⇒ₚᵢ' T" := (TypeInfPI Σ Γ t T)

with TypeInfSI: Module -> Context -> Term -> Term -> Prop :=
| TypeInfSI_red Σ Γ t T A B:
    Σ; Γ ⊢ t ⇒ T   ->
    Σ ⊢ T ⟶* sigma A B ->
    Σ; Γ ⊢ t ⇒ₛᵢ (sigma A B)
where "Σ ';' Γ '⊢' t '⇒ₛᵢ' T" := (TypeInfSI Σ Γ t T).

Scheme TypeInf_mut_ind_strong := Minimality for TypeInf Sort Prop
with TypeCheck_mut_ind := Minimality for TypeCheck Sort Prop
with TypeInfU_mut_ind := Minimality for TypeInfU Sort Prop
with TypeInfPI_mut_ind := Minimality for TypeInfPI Sort Prop
with TypeInfSI_mut_ind := Minimality for TypeInfSI Sort Prop.

Lemma TypeInf_mut_ind: forall P : Module -> Context -> Term -> Term -> Prop,
(forall (Σ : Module) (Γ : Context) (idx : nat) (T : Term),
 dget Γ idx = Some T -> P Σ Γ (var idx) T) ->
(forall (Σ : Module) (Γ : Context) (l : nat),
 P Σ Γ (univ l) (univ l.+1)) ->
(forall (Σ : Module) (Γ : Context) (A B : Term) (i j : nat),
 Σ; Γ ⊢ A ⇒ᵤ univ i ->
 P Σ Γ A (univ i) ->
 Σ; (A :: Γ) ⊢ B ⇒ᵤ univ j ->
 P Σ (A :: Γ) B (univ j) -> P Σ Γ (pi A B) (univ (maxn i j))) ->
(forall (Σ : Module) (Γ : Context) (A B t : Term) (l : nat),
 Σ; Γ ⊢ A ⇒ᵤ univ l ->
 P Σ Γ A (univ l) ->
 Σ; (A :: Γ) ⊢ t ⇒ B -> P Σ (A :: Γ) t B -> P Σ Γ (lam A t) (pi A B)) ->
(forall (Σ : Module) (Γ : Context) (A B m n : Term),
 Σ; Γ ⊢ m ⇒ₚᵢ pi A B ->
 P Σ Γ m (pi A B) ->
 Σ; Γ ⊢ n ⟸ A -> P Σ Γ n A -> P Σ Γ (appl m n) B.[n/]) ->
(forall (Σ : Module) (Γ : Context) (A B : Term) (i j : nat),
 Σ; Γ ⊢ A ⇒ᵤ univ i ->
 P Σ Γ A (univ i) ->
 Σ; (A :: Γ) ⊢ B ⇒ᵤ univ j ->
 P Σ (A :: Γ) B (univ j) -> P Σ Γ (sigma A B) (univ (maxn i j))) ->
(forall (Σ : Module) (Γ : Context) (m n A B : Term) (i j : nat),
 Σ; Γ ⊢ A ⇒ᵤ univ i ->
 P Σ Γ A (univ i) ->
 Σ; (A :: Γ) ⊢ B ⇒ᵤ univ j ->
 P Σ (A :: Γ) B (univ j) ->
 Σ; Γ ⊢ m ⟸ A ->
 P Σ Γ m A ->
 Σ; Γ ⊢ n ⟸ B.[m/] ->
 P Σ Γ n B.[m/] -> P Σ Γ (pair A B m n) (sigma A B)) ->
(forall (Σ : Module) (Γ : Context) (m A B : Term),
 Σ; Γ ⊢ m ⇒ₛᵢ sigma A B -> P Σ Γ m (sigma A B) -> P Σ Γ (proj1 m) A) ->
(forall (Σ : Module) (Γ : Context) (m A B : Term),
 Σ; Γ ⊢ m ⇒ₛᵢ sigma A B ->
 P Σ Γ m (sigma A B) -> P Σ Γ (proj2 m) B.[proj1 m/]) ->
(forall (Σ : Module) (Γ : Context) (name : String.string) (T : Term),
 mod_get_type Σ name = Some T -> P Σ Γ (gvar name) T) ->
(forall (Σ : Module) (Γ : Context) (t T T' : Term),
 Σ; Γ ⊢ t ⇒ T' -> P Σ Γ t T' -> Σ ⊢ T' ≅ T -> P Σ Γ t T) ->
(forall (Σ : Module) (Γ : Context) (t T : Term) (l : nat),
 Σ; Γ ⊢ t ⇒ T -> P Σ Γ t T -> Σ ⊢ T ⟶* univ l -> P Σ Γ t (univ l)) ->
(forall (Σ : Module) (Γ : Context) (t T A B : Term),
 Σ; Γ ⊢ t ⇒ T -> P Σ Γ t T -> Σ ⊢ T ⟶* pi A B -> P Σ Γ t (pi A B)) ->
(forall (Σ : Module) (Γ : Context) (t T A B : Term),
 Σ; Γ ⊢ t ⇒ T ->
 P Σ Γ t T -> Σ ⊢ T ⟶* sigma A B -> P Σ Γ t (sigma A B)) ->
forall (m : Module) (c : Context) (t t0 : Term),
m; c ⊢ t ⇒ t0 -> P m c t t0.
Proof.
move => P.
by apply: TypeInf_mut_ind_strong.
Qed.

Tactic Notation "TypeInfElim" ident(Σ) ident(Γ) ident(t) ident(T):=
elim /TypeInf_mut_ind => {Σ Γ t T} =>
[ Σ Γ idx T Hdget
| Σ Γ l
| Σ Γ A B i j HinfA IHA HinfB IHB
| Σ Γ A B t l HinfA IHA Hinft IHt
| Σ Γ A B t n Hinft IHt Hcheckn IHn
| Σ Γ A B i j HinfA IHA HinfB IHB
| Σ Γ m n A B i j HinfA IHA HinfB IHB Hinfm IHm  Hcheckn IHn
| Σ Γ m A B Hinfm IHm
| Σ Γ m A B Hinfm IHm
| Σ Γ name T Hmodget
| Σ Γ t T T' Hinft IHt HconvTT'
| Σ Γ t T l Hinft IHt ImredT
| Σ Γ t T A B Hinft IHt HmredT
| Σ Γ t T A B Hinft IHt HmredT
].
