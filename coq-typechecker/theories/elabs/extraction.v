(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)

From mathcomp Require Import all_ssreflect.
From sir.common Require Import utils.
From sir.elabs.ast Require Import term module.
From sir.elabs Require Import debruijn.defs.
From sir.elabs.algo Require Import result red conv infer.
From Coq Require Extraction.
From Coq Require Import ExtrOcamlBasic ExtrOcamlNatInt ExtrOcamlString.

(* TODO get rid of ExtrOcamlNatInt and use something more sensible    *)
(* ExtrOcamlNatInt maps to int  in ocaml which maps to u16 in rust :\ *)
(* we assume "nat"                                                    *)
Set Warnings "-extraction-opaque-accessed".
Set Warnings "-extraction-reserved-identifier".

Extraction Inline mathcomp.ssreflect.ssrnat.addn_rec.
Extraction Inline mathcomp.ssreflect.ssrnat.addn.
Extraction Inline Coq.NArith.BinNat.N.add.
Extraction Inline Coq.Init.Nat.add.

Extraction Inline sir.common.utils.comp.
Extraction Inline sir.common.utils.scons.
Extraction Inline sir.common.utils.inspect.

Extraction Inline sir.common.padd.padd.
Extraction Inline sir.common.debruijn.structs.ids.
Extraction Inline sir.common.debruijn.structs.upren.
Extraction Inline sir.common.debruijn.structs.ren.
Extraction Inline sir.common.debruijn.structs.up.
Extraction Inline sir.common.debruijn.structs.upn.
Extract Inductive Result => "result" [ "Ok" "Error"].

Cd "extracted/src".
Extraction "Ast.ml" Term.
Extract Inductive Term => "Ast.term" [
    "Ast.Var"   "Ast.Univ"  "Ast.Pi"    "Ast.Lam"
    "Ast.Appl"  "Ast.Sigma" "Ast.Pair"  "Ast.Proj1"
    "Ast.Proj2" "Ast.Gvar"
    ].
Extraction "Module.ml" ModuleItem mod_get mod_get_term mod_get_type.

Extract Inductive ModuleItem => "Module.moduleItem" ["Module.Module_local"].

(*little hack, otherwise we have `let rec subst = Debruijn.subst` in Reduce.ml*)
(*Extraction Inline subst.*)
Extraction "Error.ml" option Construct Error.
Extract Inductive Error => "Error.error" [
    "Error.UnkownError"         "Error.ReductionError"              "Error.WeakHeadIrreducable"
    "Error.VariableNotFound"    "Error.InferringFailed"             "Error.InferringFailedWithType"
    "Error.TypeCheckFailed"     "Error.GlobalVariableNotDeclared"   "Error.OutOfFuel"
    ].
Extract Inductive Construct => "Error.construct" [
    "Error.Pi"  "Error.Univ"    "Error.Lam" "Error.Var"
    "Error.App" "Error.Sigma"
    ].

Extraction "Reduce.ml" reduce multi_reduce multi_reduce_to_pi multi_reduce_to_sigma multi_reduce_head.
Extract Constant reduce => "Reduce.reduce".
Extract Constant multi_reduce => "Reduce.multi_reduce".
Extract Constant multi_reduce_to_pi => "Reduce.multi_reduce_to_pi".
Extract Constant multi_reduce_to_sigma => "Reduce.multi_reduce_to_sigma".
Extract Constant multi_reduce_head => "Reduce.multi_reduce_head".
Extraction Inline reduce.
Extraction Inline multi_reduce.
Extraction Inline multi_reduce_to_pi.
Extraction Inline multi_reduce_to_sigma.
Extraction Inline multi_reduce_head.

Extraction "Conversion.ml" converts_to.
Extract Constant converts_to => "Conversion.converts_to".
Extraction Inline converts_to.

Extraction "Infer.ml" type_infer type_infer_u type_infer_pi type_infer_si type_check.

Cd "../../".
