(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From Equations Require Import Equations.
From sir.elabs Require Import ast.term ast.module context.
From sir.elabs.debruijn Require Import defs.
From sir.elabs.algo Require Import result red conv.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Equations type_infer (Σ: Module) (Γ :Context) (t: Term) (fuel: nat): Result Term Error:= {
type_infer Σ Γ (var idx) _ with dget Γ idx :={
    | Some t => Ok t
    | None => Err (VariableNotFound idx)
};
type_infer Σ _ (univ l) _ := Ok (univ l.+1) ;
type_infer Σ Γ (pi A B) fuel :=
    match (type_infer_u Σ Γ A fuel), (type_infer_u Σ (A::Γ) B fuel) with
    | Ok l1, Ok l2 => Ok (univ (maxn l1 l2))
    | Err e, _ => Err (InferringFailed (pi A B) e)
    | _, Err e => Err (InferringFailed (pi A B) e)
    end;
type_infer Σ Γ (lam A t) fuel :=
    match (type_infer_u Σ Γ A fuel), (type_infer Σ (A::Γ) t fuel) with
    | Ok _ , Ok B => Ok (pi A B)
    | Err e, _ => Err (InferringFailed (lam A t) e)
    | _, Err e => Err (InferringFailed (lam A t) e)
    end;
type_infer Σ Γ (appl f a) fuel :=
    match type_infer_pi Σ Γ f fuel with
    | Ok (A, B) =>
        if type_check Σ Γ a A fuel then
            Ok (B.[a/])
        else
            Err (InferringFailed (appl f a) (TypeCheckFailed a A))
    | Err e => Err (InferringFailed (appl f a) e)
    end;
type_infer Σ Γ (sigma A B) fuel :=
    match (type_infer_u Σ Γ A fuel), (type_infer_u Σ (A::Γ) B fuel) with
    | Ok l1, Ok l2 => Ok (univ (maxn l1 l2))
    | Err e, _ => Err (InferringFailed (sigma A B) e)
    | _, Err e => Err (InferringFailed (sigma A B) e)
    end;
type_infer Σ Γ (pair A B a b) fuel :=
    match (type_infer_u Σ Γ A fuel), (type_infer_u Σ (A::Γ) B fuel) with
    | Err e, _ => Err (InferringFailed (pair A B a b) e)
    | _, Err e => Err (InferringFailed (pair A B a b) e)
    | Ok _, Ok _ =>
        match (type_check Σ Γ a A fuel), (type_check Σ Γ b B.[a/] fuel) with
        | true, true => Ok (sigma A B)
        | false, _ => Err (InferringFailed (pair A B a b) (TypeCheckFailed a A))
        | _, false => Err (InferringFailed (pair A B a b) (TypeCheckFailed b B))
        end
    end;
type_infer Σ Γ (proj1 m) fuel :=
    match type_infer_si Σ Γ m fuel with
    | Ok (A, B) => Ok A
    | Err e => Err (InferringFailed (proj1 m) e)
    end;
type_infer Σ Γ (proj2 m) fuel :=
    match type_infer_si Σ Γ m fuel with
    | Ok (A, B) => Ok B.[proj1 m/]
    | Err e => Err (InferringFailed (proj2 m) e)
    end;
type_infer Σ Γ (gvar name) fuel :=
    match mod_get_type Σ name with
    | Some (t) => Ok t
    | None => Err (GlobalVariableNotDeclared name)
    end;
}

where type_infer_u (Σ: Module) (Γ : Context) (t: Term) (fuel: nat): Result nat Error :={
type_infer_u Σ Γ t fuel := match type_infer Σ Γ t fuel with
| Ok (univ l) => Ok l
| Ok T => match multi_reduce_to_univ fuel Σ T with
          | Ok l => Ok l
          | Err e => Err (InferringFailedWithType t T e)
          end
| Err e => Err (InferringFailed t e)
end;
}

where type_infer_pi (Σ: Module) (Γ : Context) (t: Term) (fuel: nat) : Result (Term * Term) Error := {
type_infer_pi Σ Γ t fuel := match type_infer Σ Γ t fuel with
| Ok (pi A B) => Ok (A, B)
| Ok T => match multi_reduce_to_pi fuel Σ T with
          | Ok (A, B) => Ok (A, B)
          | Err e => Err (InferringFailedWithType t T e)
          end
| Err e => Err (InferringFailed t e)
end;
}

where type_infer_si (Σ: Module) (Γ : Context) (t: Term) (fuel: nat) : Result (Term * Term) Error := {
type_infer_si Σ Γ t fuel := match type_infer Σ Γ t fuel with
| Ok (sigma A B) => Ok (A, B)
| Ok T => match multi_reduce_to_sigma fuel Σ T with
          | Ok (A, B) => Ok (A, B)
          | Err e => Err (InferringFailedWithType t T e)
          end
| Err e => Err (InferringFailed t e)
end;
}

where type_check (Σ: Module) (Γ : Context) (t T: Term) (fuel: nat) : bool :=
type_check Σ Γ t T fuel := match type_infer Σ Γ t fuel with
                         | Ok T' => converts_to Σ T T' fuel
                         | Err _ => false
                         end.