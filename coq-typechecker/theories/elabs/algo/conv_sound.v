From mathcomp Require Import all_ssreflect.
From Equations Require Import Equations.
From sir.elabs Require Import term module.
From sir.elabs.meta Require Import reduction conversion confluence red_facts.
From sir.elabs.algo Require Import conv red red_sound result.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Lemma conv_applr Σ m2 n2 T fuel:
    (forall Σ' (t1 t2 : Term), converts_to Σ' t1 t2 fuel = true -> Σ' ⊢ t1 ≅ t2) ->
    match multi_reduce_head fuel Σ (appl m2 n2) with
    | Ok ok => converts_to Σ T ok fuel
    | Err _ => false
    end = true -> Σ ⊢ T ≅ appl m2 n2.
Proof.
move => IH.
move Hmred: (multi_reduce_head fuel Σ (appl m2 n2)) => [t|//].
move /IH => Hconv.
apply: Conv_trans.
    by apply: Hconv.
apply: Conv_symm.
apply: mred_conv.
apply: multi_reduce_head_sound.
by apply:Hmred.
Qed.

Lemma conv_appll Σ m1 n1 T fuel:
    (forall Σ' (t1 t2 : Term), converts_to Σ' t1 t2 fuel = true -> Σ' ⊢ t1 ≅ t2) ->
    match multi_reduce_head fuel Σ (appl m1 n1) with
    | Ok ok => converts_to Σ ok T fuel
    | Err _ => false
    end = true -> Σ ⊢ appl m1 n1 ≅ T.
Proof.
move => IH.
move Hmred: (multi_reduce_head fuel Σ (appl m1 n1)) => [t|//].
move /IH => Hconv.
apply: Conv_trans;
    last by apply: Hconv.
apply: mred_conv.
apply: multi_reduce_head_sound.
by apply:Hmred.
Qed.

Lemma conv_proj1r Σ m2 T fuel:
    (forall Σ' (t1 t2 : Term), converts_to Σ' t1 t2 fuel = true -> Σ' ⊢ t1 ≅ t2) ->
    match multi_reduce_head fuel Σ (proj1 m2) with
    | Ok t2 => converts_to Σ T t2 fuel
    | Err _ => false
    end = true -> Σ ⊢ T ≅ proj1 m2.
Proof.
move => IH.
move Hmred: (multi_reduce_head fuel Σ (proj1 m2)) => [t|//].
move /IH => Hconv.
apply: Conv_trans.
    by apply: Hconv.
apply: Conv_symm.
apply: mred_conv.
apply: multi_reduce_head_sound.
by apply:Hmred.
Qed.

Lemma conv_proj1l Σ m1 T fuel:
    (forall Σ' (t1 t2 : Term), converts_to Σ' t1 t2 fuel = true -> Σ' ⊢ t1 ≅ t2) ->
    match multi_reduce_head fuel Σ (proj1 m1) with
    | Ok t1 => converts_to Σ t1 T fuel
    | Err _ => false
    end = true -> Σ ⊢ proj1 m1 ≅ T.
Proof.
move => IH.
move Hmred: (multi_reduce_head fuel Σ (proj1 m1)) => [t|//].
move /IH => Hconv.
apply: Conv_trans;
    last by apply: Hconv.
apply: mred_conv.
apply: multi_reduce_head_sound.
by apply:Hmred.
Qed.

Lemma conv_proj2r Σ m2 T fuel:
    (forall Σ' (t1 t2 : Term), converts_to Σ' t1 t2 fuel = true -> Σ' ⊢ t1 ≅ t2) ->
    match multi_reduce_head fuel Σ (proj2 m2) with
    | Ok t2 => converts_to Σ T t2 fuel
    | Err _ => false
    end = true -> Σ ⊢ T ≅ proj2 m2.
Proof.
move => IH.
move Hmred: (multi_reduce_head fuel Σ (proj2 m2)) => [t|//].
move /IH => Hconv.
apply: Conv_trans.
    by apply: Hconv.
apply: Conv_symm.
apply: mred_conv.
apply: multi_reduce_head_sound.
by apply:Hmred.
Qed.

Lemma conv_proj2l Σ m1 T fuel:
    (forall Σ' (t1 t2 : Term), converts_to Σ' t1 t2 fuel = true -> Σ' ⊢ t1 ≅ t2) ->
    match multi_reduce_head fuel Σ (proj2 m1) with
    | Ok t1 => converts_to Σ t1 T fuel
    | Err _ => false
    end = true -> Σ ⊢ proj2 m1 ≅ T.
Proof.
move => IH.
move Hmred: (multi_reduce_head fuel Σ (proj2 m1)) => [t|//].
move /IH => Hconv.
apply: Conv_trans;
    last by apply: Hconv.
apply: mred_conv.
apply: multi_reduce_head_sound.
by apply:Hmred.
Qed.

Lemma conv_gvarl Σ fuel n t:
(forall (Σ : Module) (t1 t2 : Term), converts_to Σ t1 t2 fuel = true -> Σ ⊢ t1 ≅ t2) ->
match multi_reduce_head fuel Σ (gvar n) with
| Ok t2 => converts_to Σ t t2 fuel
| Err _ => false
end = true -> Σ ⊢ t ≅ gvar n.
Proof.
move => IH.
move Hmred: (multi_reduce_head fuel Σ (gvar n)) => [t'|//].
move /IH => Hconv.
apply: Conv_trans.
    apply: Hconv.
apply: Conv_symm.
apply: mred_conv.
apply: multi_reduce_head_sound.
by apply:Hmred.
Qed.

Lemma conv_gvarr Σ fuel n t:
(forall (Σ : Module) (t1 t2 : Term), converts_to Σ t1 t2 fuel = true -> Σ ⊢ t1 ≅ t2) ->
match multi_reduce_head fuel Σ (gvar n) with
| Ok t1 => converts_to Σ t1 t fuel
| Err _ => false
end = true -> Σ ⊢ gvar n ≅ t.
Proof.
move => IH.
move Hmred: (multi_reduce_head fuel Σ (gvar n)) => [t'|//].
move /IH => Hconv.
apply: Conv_trans;
    last by apply: Hconv.
apply: mred_conv.
apply: multi_reduce_head_sound.
by apply:Hmred.
Qed.


Lemma converts_to_sound Σ t1 t2 fuel: converts_to Σ t1 t2 fuel = true -> Σ ⊢ t1 ≅ t2.
Proof.
elim : fuel Σ t1 t2 => [//|fuel IHfuel] Σ
    [idx1|l1|A1 B1|T1 y1|m1 n1|A1 B1|M1 N1 m1 n1|m1|m1|n1]
    [idx2|l2|A2 B2|T2 y2|m2 n2|A2 B2|M2 N2 m2 n2|m2|m2|n2] //;
    simp converts_to;
    have ? := Conv_refl.
-   by move => /eqP ->.
-   by move => /eqP ->.
-   by move => /eqP ->.
-   by apply: conv_applr.
-   by apply: conv_proj1r.
-   by apply: conv_proj2r.
-   by apply: conv_gvarl.
-   by move => /eqP ->.
-   by apply: conv_applr.
-   by apply: conv_proj1r.
-   by apply: conv_proj2r.
-   by apply: conv_gvarl.
-   move /andP => [].
    move /IHfuel => HconvA12.
    move /IHfuel => HconvB12.
    by apply: Conv_pi.
-   by apply: conv_applr.
-   by apply: conv_proj1r.
-   by apply: conv_proj2r.
-   by apply: conv_gvarl.
-   move /andP => [].
    move /IHfuel => HconvA12.
    move /IHfuel => HconvB12.
    by apply: Conv_lam.
-   by apply: conv_applr.
-   by apply: conv_proj1r.
-   by apply: conv_proj2r.
-   by apply: conv_gvarl.
-   by apply: conv_appll.
-   by apply: conv_appll.
-   by apply: conv_appll.
-   by apply: conv_appll.
-   move Hmred: (multi_reduce_head fuel Σ (appl m1 n1)) => [t|Et];
        last first.
        move /andP => [].
        move /IHfuel => convm12.
        move /IHfuel => convn12.
        by apply: Conv_appl.
    move Hmred': (multi_reduce_head fuel Σ (appl m2 n2)) => [t'|Et'].
        move : Hmred => /multi_reduce_head_sound/mred_conv => Hconvapplt.
        move : Hmred' => /multi_reduce_head_sound/mred_conv => Hconvapplt'.
        move /IHfuel => Hconvtt'.
        apply: Conv_trans.
            by apply: Hconvapplt.
        apply: Conv_trans.
            by apply: Hconvtt'.
        by apply: Conv_symm.
    move /andP => [].
    move /IHfuel => convm12.
    move /IHfuel => convn12.
    by apply: Conv_appl.
-   by apply: conv_appll.
-   by apply: conv_appll.
-   by apply: conv_appll.
-   by apply: conv_appll.
-   by apply: conv_appll.
-   by apply: conv_applr.
-   move /andP => [].
    move /IHfuel => HconvA12.
    move /IHfuel => HconvB12.
    by apply: Conv_sigma.
-   by apply: conv_proj1r.
-   by apply: conv_proj2r.
-   by apply: conv_gvarl.
-   by apply: conv_applr.
-   repeat move /andP => [].
    move /IHfuel => HconvM12.
    move /IHfuel => HconvN12.
    move /IHfuel => Hconvm12.
    move /IHfuel => Hconvn12.
    by apply: Conv_pair.
-   by apply: conv_proj1r.
-   by apply: conv_proj2r.
-   by apply: conv_gvarl.
-   by apply: conv_proj1l.
-   by apply: conv_proj1l.
-   by apply: conv_proj1l.
-   by apply: conv_proj1l.
-   move Hmredp1: (multi_reduce_head fuel Σ (proj1 m1)) => [p1|//].
    move : Hmredp1 => /multi_reduce_head_sound/mred_conv => Hconvproj.
    move /IHfuel => Hconvappl.
    apply: Conv_trans.
        by apply: Hconvproj.
    by [].
-   by apply: conv_proj1l.
-   by apply: conv_proj1l.
-   move Hmred: (multi_reduce_head fuel Σ (proj1 m1)) => [t|Et];
        last first.
        move /IHfuel => Hconv12.
        by apply: Conv_proj1.
    move Hmred': (multi_reduce_head fuel Σ (proj1 m2)) => [t'|Et'];
        move /IHfuel => Hconv;
        last by apply: Conv_proj1.
    move : Hmred => /multi_reduce_head_sound/mred_conv => Hconvt.
    move : Hmred' => /multi_reduce_head_sound/mred_conv => Hconvt'.
    apply: Conv_trans.
        by apply: Hconvt.
    apply:Conv_trans.
        by apply:Hconv.
    by apply: Conv_symm.
-   by apply: conv_proj1l.
-   by apply: conv_proj1l.
-   by apply: conv_proj2l.
-   by apply: conv_proj2l.
-   by apply: conv_proj2l.
-   by apply: conv_proj2l.
-   move Hmredp1: (multi_reduce_head fuel Σ (proj2 m1)) => [p1|//].
    move : Hmredp1 => /multi_reduce_head_sound/mred_conv => Hconvproj.
    move /IHfuel => Hconvappl.
    apply: Conv_trans.
        by apply: Hconvproj.
    by [].
-   by apply: conv_proj2l.
-   by apply: conv_proj2l.
-   by apply: conv_proj2l.
-   move Hmred: (multi_reduce_head fuel Σ (proj2 m1)) => [p2|Err2];
        last first.
        move /IHfuel => convm12.
        by apply: Conv_proj2.
    move Hmred': (multi_reduce_head fuel Σ (proj2 m2)) => [t'|Et'];
        move /IHfuel => Hconv;
        last by apply: Conv_proj2.
    move : Hmred => /multi_reduce_head_sound/mred_conv => Hconvt.
    move : Hmred' => /multi_reduce_head_sound/mred_conv => Hconvt'.
    apply: Conv_trans.
        by apply: Hconvt.
    apply:Conv_trans.
        by apply:Hconv.
    by apply: Conv_symm.
-   by apply: conv_proj2l.
-   by apply: conv_gvarr.
-   by apply: conv_gvarr.
-   by apply: conv_gvarr.
-   by apply: conv_gvarr.
-   by apply: conv_gvarr.
-   by apply: conv_gvarr.
-   by apply: conv_gvarr.
-   by apply: conv_gvarr.
-   by apply: conv_gvarr.
-   by apply: conv_gvarr.
Qed.