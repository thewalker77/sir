(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From sir.elabs Require Import ast.term.
From sir.common Require Import string.

Inductive Construct : Type :=
| Pi | Univ | Lam | Var | App | Sigma.

Inductive Error : Type :=
| UnkownError
| ReductionError (expected: Construct) (got: Term)
| WeakHeadIrreducable (got: Term)
| VariableNotFound (idx: nat)
| InferringFailed (t: Term) (reason: Error)
| InferringFailedWithType (t: Term) (T: Term) (reason: Error)
| TypeCheckFailed (t T: Term)
| GlobalVariableNotDeclared(n: string)
| OutOfFuel.

Inductive Result {okT errT: Type}: Type:=
|   Ok (ok: okT)
|   Err (e: errT).
Arguments Result: clear implicits.
