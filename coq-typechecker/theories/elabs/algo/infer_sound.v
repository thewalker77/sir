(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From Equations Require Import Equations.
From sir.elabs.ast Require Import term module.
From sir.elabs Require Import bidirectional.typing context.
From sir.common Require Import utils.
From sir.elabs.debruijn Require Import defs.
From sir.elabs.meta Require Import reduction conversion.
From sir.elabs.algo Require Import infer result red red_sound conv conv_sound.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Lemma type_infer_u_inv Σ Γ A fuel l: type_infer_u Σ Γ A fuel = Ok l ->
    exists t fuel', type_infer Σ Γ A fuel = Ok t /\ multi_reduce_to_univ fuel' Σ t = Ok l.
Proof.
rewrite /type_infer_u.
case HinfA: (type_infer Σ Γ A fuel) => [TA|//].
move: TA HinfA => [idx | l' | A' B' |A' y| m n|A' B|M N m n|m |m|name] Hinf.
-   move Hmred: (multi_reduce_to_univ fuel Σ (var idx)) => [redu|//].
    move => [<-].
    by exists (var idx), fuel.
-   move => [<-].
    by exists (univ l'), 1.
-   move Hmred: (multi_reduce_to_univ fuel Σ (pi A' B')) => [redu|//].
    move => [<-].
    by exists (pi A' B'), fuel.
-   move Hmred: (multi_reduce_to_univ fuel Σ (lam A' y)) => [redu|//].
    move => [<-].
    by exists (lam A' y), fuel.
-   move Hmred: (multi_reduce_to_univ fuel Σ (appl m n)) => [redu|//].
    move => [<-].
    by exists (appl m n), fuel.
-   move Hmred: (multi_reduce_to_univ fuel Σ (sigma A' B)) => [redu|//].
    move => [<-].
    by exists (sigma A' B), fuel.
-   move Hmred: (multi_reduce_to_univ fuel Σ (pair M N m n)) => [redu|//].
    move => [<-].
    by exists (pair M N m n), fuel.
-   move Hmred: (multi_reduce_to_univ fuel Σ (proj1 m)) => [redu|//].
    move => [<-].
    by exists (proj1 m), fuel.
-   move Hmred: (multi_reduce_to_univ fuel Σ (proj2 m)) => [redu|//].
    move => [<-].
    by exists (proj2 m), fuel.
-   move Hmred: (multi_reduce_to_univ fuel Σ (gvar name)) => [redu|//].
    move => [<-].
    by exists (gvar name), fuel.
Qed.

Lemma type_check_true_inv Σ Γ t T fuel:
    type_check Σ Γ t T fuel = true ->
    exists T',
        type_infer Σ Γ t fuel = Ok T' /\
        converts_to Σ T T' fuel.
Proof.
rewrite type_check_equation_1.
move Hinf : (type_infer Σ Γ t fuel) => [T'|//] Hconv.
by exists T'.
Qed.

Lemma Type_inf_pi_inv Σ Γ t A B fuel:
    type_infer_pi Σ Γ t fuel = Ok (A, B) ->
    type_infer Σ Γ t fuel = Ok(pi A B) \/
    exists T',
        type_infer Σ Γ t fuel = Ok T' /\
        multi_reduce_to_pi fuel Σ T' = Ok(A, B).
Proof.
rewrite type_infer_pi_equation_1.
move Hinf : (type_infer Σ Γ t fuel) => [T|//].
move: T Hinf => [idx|l|A' B'|A' t'| m n|A' B'|M N m n|m |m|name] Hinf.
-   move Hmred: (multi_reduce_to_pi fuel Σ (var idx)) => [[A' B']|//].
    move => [<- <-].
    right.
    by exists (var idx).
-   move Hmred: (multi_reduce_to_pi fuel Σ (univ l)) => [[A' B']|//].
    move => [<- <-].
    right.
    by exists (univ l).
-   move => [<- <-].
    by left.
-   move Hmred: (multi_reduce_to_pi fuel Σ (lam A' t')) => [[A2 B2]|//].
    move => [<- <-].
    right.
    by exists (lam A' t').
-   move Hmred: (multi_reduce_to_pi fuel Σ (appl m n)) => [[A' B']|//].
    move => [<- <-].
    right.
    by exists (appl m n).
-   move Hmred: (multi_reduce_to_pi fuel Σ (sigma A' B')) => [[A2 B2]|//].
    move => [<- <-].
    right.
    by exists (sigma A' B').
-   move Hmred: (multi_reduce_to_pi fuel Σ (pair M N m n)) => [[A2 B2]|//].
    move => [<- <-].
    right.
    by exists (pair M N m n).
-   move Hmred: (multi_reduce_to_pi fuel Σ (proj1 m)) => [[A2 B2]|//].
    move => [<- <-].
    right.
    by exists (proj1 m).
-   move Hmred: (multi_reduce_to_pi fuel Σ (proj2 m)) => [[A2 B2]|//].
    move => [<- <-].
    right.
    by exists (proj2 m).
-   move Hmred: (multi_reduce_to_pi fuel Σ (gvar name)) => [[A2 B2]|//].
    move => [<- <-].
    right.
    by exists (gvar name).
Qed.

Lemma Type_inf_si_inv Σ Γ t A B fuel:
    type_infer_si Σ Γ t fuel = Ok (A, B) ->
    type_infer Σ Γ t fuel = Ok(sigma A B) \/
    exists T',
        type_infer Σ Γ t fuel = Ok T' /\
        multi_reduce_to_sigma fuel Σ T' = Ok(A, B).
Proof.
rewrite type_infer_si_equation_1.
move Hinf : (type_infer Σ Γ t fuel) => [T|//].
move: T Hinf => [idx|l|A' B'|A' t'| m n|A' B'|M N m n|m |m| name] Hinf.
-   move Hmred: (multi_reduce_to_sigma fuel Σ (var idx)) => [[A' B']|//].
    move => [<- <-].
    right.
    by exists (var idx).
-   move Hmred: (multi_reduce_to_sigma fuel Σ (univ l)) => [[A' B']|//].
    move => [<- <-].
    right.
    by exists (univ l).
-   move Hmred: (multi_reduce_to_sigma fuel Σ (pi A' B')) => [[A2 B2]|//].
    move => [<- <-].
    right.
    by exists (pi A' B').
-   move Hmred: (multi_reduce_to_sigma fuel Σ (lam A' t')) => [[A2 B2]|//].
    move => [<- <-].
    right.
    by exists (lam A' t').
-   move Hmred: (multi_reduce_to_sigma fuel Σ (appl m n)) => [[A' B']|//].
    move => [<- <-].
    right.
    by exists (appl m n).
-   move => [<- <-].
    by left.
-   move Hmred: (multi_reduce_to_sigma fuel Σ (pair M N m n)) => [[A2 B2]|//].
    move => [<- <-].
    right.
    by exists (pair M N m n).
-   move Hmred: (multi_reduce_to_sigma fuel Σ (proj1 m)) => [[A2 B2]|//].
    move => [<- <-].
    right.
    by exists (proj1 m).
-   move Hmred: (multi_reduce_to_sigma fuel Σ (proj2 m)) => [[A2 B2]|//].
    move => [<- <-].
    right.
    by exists (proj2 m).
-   move Hmred: (multi_reduce_to_sigma fuel Σ (gvar name)) => [[A2 B2]|//].
    move => [<- <-].
    right.
    by exists (gvar name).
Qed.

Lemma type_inf_univ_ind_goal Σ Γ A i fuel:
    type_infer_u Σ Γ A fuel = Ok i ->
    (forall Σ Γ T fuel, type_infer Σ Γ A fuel = Ok T -> Σ; Γ ⊢ A ⇒ T) ->
    Σ; Γ ⊢ A ⇒ᵤ univ i.
Proof.
move/type_infer_u_inv => [T [fuel'[Hinf Hmred]]] IH.
apply: TypeInfU_red.
apply: IH.
    by apply: Hinf.
apply: multi_reduce_to_univ_sound.
by apply: Hmred.
Qed.

Lemma type_infer_sound t Σ Γ T fuel:
    type_infer Σ Γ t fuel = Ok T ->
    Σ; Γ ⊢ t ⇒ T.
Proof.
move: t Σ Γ T fuel.
TermElim => Σ Γ T' fuel.
-   simp type_infer.
    rewrite /type_infer_clause_1.
    move Hdget : (dget Γ idx) => [T|//].
    move => [<-].
    by apply: TypeInf_var.
-   simp type_infer.
    move => [<-].
    apply: TypeInf_univ.
-   rewrite type_infer_equation_3.
    move HinfA: (type_infer_u Σ Γ A fuel) => [UA|//].
    move HinfB: (type_infer_u Σ (A::Γ) B fuel) => [UB|//].
    move => [<-].
    apply: TypeInf_pi.
        by apply: (type_inf_univ_ind_goal (fuel := fuel)).
    by apply: (type_inf_univ_ind_goal (fuel := fuel)).
-   rewrite type_infer_equation_4.
    move HinfA: (type_infer_u Σ Γ T fuel) => [UA|//].
    move Hinfy: (type_infer Σ (T :: Γ) y fuel) => [Ty|//].
    move => [<-].
    apply: TypeInf_lam.
        apply: type_inf_univ_ind_goal => //.
        by apply: HinfA.
    apply: IHy.
    by apply: Hinfy.
-   rewrite type_infer_equation_5.
    move Hinfm : (type_infer_pi Σ Γ m fuel) => [[A B]|//].
    move Hcheck: (type_check Σ Γ n A fuel) => [|//].
    move => [<-].
    apply: (@TypeInf_appl _ _ A).
        move: Hinfm => /Type_inf_pi_inv [Hinf|[T2 [Hinf Hmred]]];
            apply: TypeInfPI_red.
        +   apply: IHm.
            apply: Hinf.
        +   apply: Multi_refl.
        +   apply: IHm.
            apply: Hinf.
        +   apply: multi_reduce_to_pi_sound.
            by apply: Hmred.
    move: Hcheck => /type_check_true_inv [T2 [Hinf Hconv]].
    apply: TypeCheck_conv.
        apply: IHn.
        by apply: Hinf.
    apply: Conv_symm.
    apply: converts_to_sound.
    by apply: Hconv.
-   rewrite type_infer_equation_6.
    move HinfA: (type_infer_u Σ Γ A fuel) => [UA|//].
    move HinfB: (type_infer_u Σ (A::Γ) B fuel) => [UB|//].
    move => [<-].
    apply: TypeInf_sigma.
        by apply: (type_inf_univ_ind_goal (fuel := fuel)).
    by apply: (type_inf_univ_ind_goal (fuel := fuel)).
-   rewrite type_infer_equation_7.
    move HinfM: (type_infer_u Σ Γ M fuel) => [UM|//].
    move HinfN: (type_infer_u Σ (M::Γ) N fuel) => [UN|//].
    case Hcheckm: (type_check Σ Γ m M fuel) => [|//].
    case Hcheckn: (type_check Σ Γ n N.[m/] fuel) => [|//].
    move => [<-].
    apply: TypeInf_pair.
    +   apply: (type_inf_univ_ind_goal (fuel := fuel)) => //.
        by apply: HinfM.
    +   apply: (type_inf_univ_ind_goal (fuel := fuel)) => //.
        by apply: HinfN.
    +   move: Hcheckm => /type_check_true_inv [T2 [Hinf Hconv]].
        apply: TypeCheck_conv.
            apply: IHm.
            by apply: Hinf.
        apply: Conv_symm.
        apply: converts_to_sound.
        by apply: Hconv.
    +   move: Hcheckn => /type_check_true_inv [T2 [Hinf Hconv]].
        apply: TypeCheck_conv.
            apply: IHn.
            by apply: Hinf.
        apply: Conv_symm.
        apply: converts_to_sound.
        by apply: Hconv.
-   rewrite type_infer_equation_8.
    move Hinf: (type_infer_si Σ Γ m fuel) =>[[A B]|//].
    move => [<-].
    apply: TypeInf_proj1.
    move: Hinf => /Type_inf_si_inv [Hinf|[T2 [Hinf Hmred]]];
    apply: TypeInfSI_red.
    +   apply: IHm.
        by apply: Hinf.
    +   apply: Multi_refl.
    +   apply: IHm.
        by apply: Hinf.
    +   apply: multi_reduce_to_sigma_sound.
        by apply: Hmred.
-   rewrite type_infer_equation_9.
    move Hinf: (type_infer_si Σ Γ m fuel) =>[[A B]|//].
    move => [<-].
    apply: TypeInf_proj2.
    move: Hinf => /Type_inf_si_inv [Hinf|[T2 [Hinf Hmred]]];
    apply: TypeInfSI_red.
    +   apply: IHm.
        by apply: Hinf.
    +   apply: Multi_refl.
    +   apply: IHm.
        by apply: Hinf.
    +   apply: multi_reduce_to_sigma_sound.
        by apply: Hmred.
-   simp type_infer.
    case Hget: (mod_get_type Σ name) => [T|//].
    move => [<-].
    by apply: TypeInf_gvar.
Qed.

Lemma type_check_sound t Σ Γ T fuel:
    type_check Σ Γ t T fuel = true ->
    Σ; Γ ⊢ t ⟸ T.
Proof.
rewrite type_check_equation_1.
move Hinf: (type_infer Σ Γ t fuel) => [T'|//].
move /converts_to_sound/Conv_symm => Hconv.
apply: TypeCheck_conv;
    last by apply: Hconv.
apply: type_infer_sound.
by apply: Hinf.
Qed.