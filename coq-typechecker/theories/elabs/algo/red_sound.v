(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From Equations Require Import Equations.
From sir Require Import common.utils.
From sir.elabs.ast Require Import term module facts.
From sir.elabs.debruijn Require Import defs.
From sir.elabs.meta Require Import reduction.
From sir.elabs.algo Require Import red result.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.


Lemma reduce_sound t1 t2 Σ: reduce Σ t1 = Some t2 -> Σ ⊢ t1 ⟶ t2.
Proof.
move: t1 t2.
TermElim => t2 //.
-   case HeqredA : (reduce Σ A) => [redA|].
        move => [<-].
        apply Reduction_pil.
        by apply: IHA.
    case HeqredB : (reduce Σ B) => [redB|//].
    move => [<-].
    apply Reduction_pir.
    by apply: IHB.
-   case HeqredT : (reduce Σ T) => [redT|].
        move => [<-].
        apply Reduction_laml.
        by apply: IHT.
    case Heqredy : (reduce Σ y) => [redy|//].
    move => [<-].
    apply Reduction_lamr.
    by apply: IHy.
-   case Heqredm : (reduce Σ m) => [redm|].
        move => [<-].
        apply Reduction_appll.
        by apply: IHm.
    case Heqredn : (reduce Σ n) => [redn|].
        move => [<-].
        apply Reduction_applr.
        by apply: IHn.
    clear Heqredn Heqredm IHm.
    case: m => // T y [] <-.
    by apply: Reduction_beta.
-   case HeqredA: (reduce Σ A) => [A'|].
        move => [<-].
        apply: Reduction_sigmal.
        by apply: IHA.
    case HeqredB: (reduce Σ B) => [B'|//].
    move => [<-].
    apply: Reduction_sigmar.
    by apply: IHB.
-   case HeqredM: (reduce Σ M) => [M'|].
        move => [<-].
        apply: Reduction_pairTl.
        by apply: IHM.
    case HeqredN: (reduce Σ N) => [N'|].
    move => [<-].
    apply: Reduction_pairTr.
    by apply: IHN.
    case Heqredm: (reduce Σ m) => [m'|].
        move => [<-].
        apply: Reduction_pairl.
        by apply: IHm.
    case Heqredn: (reduce Σ n) => [n'|//].
    move => [<-].
    apply: Reduction_pairr.
    by apply: IHn.
-   case Heqredm: (reduce Σ m) => [m'|].
        move => [<-].
        apply: Reduction_proj1.
        by apply: IHm.
    case : m {IHm Heqredm} => []// M N m n [<-].
    apply: Reduction_proj1_beta.
-   case Heqredm: (reduce Σ m) => [m'|].
        move => [<-].
        apply: Reduction_proj2.
        by apply: IHm.
    case : m {IHm Heqredm} => []// M N m n [<-].
    apply: Reduction_proj2_beta.
-   case Heq : (mod_get_term Σ name) => [t|//].
    move => [<-].
    by apply: Reduction_gvar.
Qed.

Lemma multi_reduce_sound fuel Σ t1 t2: multi_reduce fuel Σ t1 = Ok t2 -> Σ ⊢ t1 ⟶* t2.
Proof.
elim: fuel Σ t1 t2 => [|fuel IHfuel] Σ t1 t2;
    simp multi_reduce => //.
move Hred1: (reduce Σ t1) => [t1'|].
+   move => Hmred.
    apply: (@Multi_single _ _ _ t1').
        by apply: reduce_sound.
    by apply: IHfuel.
+   move => [->].
    by apply: Multi_refl.
Qed.

Lemma multi_reduce_to_univ_sound n Σ t l:
    multi_reduce_to_univ n Σ t = Ok l ->
    Σ ⊢ t ⟶* univ l.
Proof.
suff : multi_reduce_to_univ n Σ t = Ok l -> multi_reduce n Σ t = Ok (univ l).
    by move => Hred /Hred /multi_reduce_sound.
funelim (multi_reduce_to_univ n Σ t).
-   by rewrite -Heqcall.
-   rewrite -Heqcall.
    by move => [<-].
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_1 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_2 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_3 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_4 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_5 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_6 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_7 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_8 e).
-   by rewrite -Heqcall.
Qed.

Lemma multi_reduce_to_pi_sound n Σ t A B:
    multi_reduce_to_pi n Σ t = Ok (A, B) ->
    Σ ⊢ t ⟶* pi A B.
Proof.
suff : multi_reduce_to_pi n Σ t = Ok (A, B) -> multi_reduce n Σ t = Ok (pi A B).
    by move => Hred /Hred /multi_reduce_sound.
funelim (multi_reduce_to_pi n Σ t).
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   rewrite <- Heqcall.
    by move => [<- <-].
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_1 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_2 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_3 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_4 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_5 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_6 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_7 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_8 e).
-   by rewrite -Heqcall.
Qed.

Lemma multi_reduce_to_sigma_sound n Σ t A B:
    multi_reduce_to_sigma n Σ t = Ok (A, B) ->
    Σ ⊢ t ⟶* sigma A B.
Proof.
suff : multi_reduce_to_sigma n Σ t = Ok (A, B) -> multi_reduce n Σ t = Ok (sigma A B).
    by move => Hred /Hred /multi_reduce_sound.
funelim (multi_reduce_to_sigma n Σ t).
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   rewrite -Heqcall.
    by move => [<- <-].
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   by rewrite -Heqcall.
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_1 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_2 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_3 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_4 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_5 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_6 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_7 e).
-   rewrite -Heqcall /False_rect.
    by case: (red.multi_reduce_to_univ_obligations_obligation_8 e).
-   by rewrite -Heqcall.
Qed.


Lemma multi_reduce_head_sound Σ t1 t2 fuel: multi_reduce_head fuel Σ t1 = Ok t2 -> Σ ⊢ t1 ⟶* t2.
Proof.
have ? := Multi_refl Reduction.
elim: fuel t1 t2 => [|fuel IHfuel] t1 t2.
    by simp multi_reduce_head.
case : t1 => [idx | l | A B | T y | m n|A B|M N m n|m |m|n];
    simp multi_reduce_head.
-   by move => [<-].
-   by move => [<-].
-   by move => [<-].
-   by move => [<-].
-   move Hred: (reduce Σ (appl m n)) => [redt|//] Hmred.
    apply: (@Multi_single _ _ _ redt).
        by apply: reduce_sound.
    by apply: IHfuel.
-   by move => [<-].
-   by move => [<-].
-   move Hred: (reduce Σ (proj1 m)) => [redt|//] Hmred.
    apply: (@Multi_single _ _ _ redt).
        by apply: reduce_sound.
    by apply: IHfuel.
-   move Hred: (reduce Σ (proj2 m)) => [redt|//] Hmred.
    apply: (@Multi_single _ _ _ redt).
        by apply: reduce_sound.
    by apply: IHfuel.
-   move Heq: (mod_get_term Σ n) => [t|//] Hmred.
    apply: (@Multi_single _ _ _ t).
        by apply: Reduction_gvar.
    by apply: IHfuel.
Qed.
