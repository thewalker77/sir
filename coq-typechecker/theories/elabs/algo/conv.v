(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                                            *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From Equations Require Import Equations.
From sir.elabs.ast Require Import term module.
From sir.elabs.algo Require Import result red.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Equations converts_to (Σ: Module) (t1: Term) (t2: Term) (fuel: nat): bool :=
converts_to Σ (var x1) (var x2) _ := x1 == x2;
converts_to Σ (univ l1) (univ l2) _ := l1 == l2;
converts_to Σ (pi A1 B1) (pi A2 B2) (S fuel) :=
    converts_to Σ A1 A2 fuel &&
    converts_to Σ B1 B2 fuel;
converts_to Σ (lam A1 t1) (lam A2 t2) (S fuel) :=
    converts_to Σ A1 A2 fuel &&
    converts_to Σ t1 t2 fuel;
converts_to Σ (sigma A1 B1) (sigma A2 B2) (S fuel) :=
    converts_to Σ A1 A2 fuel &&
    converts_to Σ B1 B2 fuel;
converts_to Σ (pair A1 B1 a1 b1) (pair A2 B2 a2 b2) (S fuel) :=
    converts_to Σ A1 A2 fuel &&
    converts_to Σ B1 B2 fuel &&
    converts_to Σ a1 a2 fuel &&
    converts_to Σ b1 b2 fuel;

converts_to Σ (appl a1 b1) (appl a2 b2) (S fuel) :=
    match (multi_reduce_head fuel Σ (appl a1 b1)), (multi_reduce_head fuel Σ (appl a2 b2)) with
    | Ok t1, Ok t2 => converts_to Σ t1 t2 fuel
    | _, _ => (converts_to Σ a1 a2 fuel) && (converts_to Σ b1 b2 fuel)
    end;
converts_to Σ (proj1 m1) (proj1 m2) (S fuel) :=
    match (multi_reduce_head fuel Σ (proj1 m1)), (multi_reduce_head fuel Σ (proj1 m2)) with
    | Ok t1, Ok t2 => converts_to Σ t1 t2 fuel
    | _, _ => converts_to Σ m1 m2 fuel
    end;
converts_to Σ (proj2 m1) (proj2 m2) (S fuel) :=
    match (multi_reduce_head fuel Σ (proj2 m1)), (multi_reduce_head fuel Σ (proj2 m2)) with
    | Ok t1, Ok t2 => converts_to Σ t1 t2 fuel
    | _, _ => converts_to Σ m1 m2 fuel
    end;

converts_to Σ (appl a1 b1) t2 (S fuel) :=
    match multi_reduce_head fuel Σ (appl a1 b1) with
    | Ok t1 => converts_to Σ t1 t2 fuel
    | _ => false
    end;
converts_to Σ (proj1 m1) t2 (S fuel) :=
    match multi_reduce_head fuel Σ (proj1 m1) with
    | Ok t1 => converts_to Σ t1 t2 fuel
    | _ => false
    end;
converts_to Σ (proj2 m1) t2 (S fuel) :=
    match multi_reduce_head fuel Σ (proj2 m1) with
    | Ok t1 => converts_to Σ t1 t2 fuel
    | _ => false
    end;
converts_to Σ (gvar n) t2 (S fuel) :=
    match multi_reduce_head fuel Σ (gvar n) with
    | Ok t1 => converts_to Σ t1 t2 fuel
    | _ => false
    end;

converts_to Σ t1 (appl a2 b2) (S fuel) :=
    match multi_reduce_head fuel Σ (appl a2 b2) with
    | Ok t2 => converts_to Σ t1 t2 fuel
    | _ => false
    end;
converts_to Σ t1 (proj1 m2) (S fuel) :=
    match multi_reduce_head fuel Σ (proj1 m2) with
    | Ok t2 => converts_to Σ t1 t2 fuel
    | _ => false
    end;
converts_to Σ t1 (proj2 m2) (S fuel) :=
    match multi_reduce_head fuel Σ (proj2 m2) with
    | Ok t2 => converts_to Σ t1 t2 fuel
    | _ => false
    end;
converts_to Σ t1 (gvar n) (S fuel) :=
    match multi_reduce_head fuel Σ (gvar n) with
    | Ok t2 => converts_to Σ t1 t2 fuel
    | _ => false
    end;

converts_to _ _ _ _ := false.
