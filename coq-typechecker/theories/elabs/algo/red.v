(******************************************************************************)
(* Copyright 2022 thewalker77                                                 *)
(*                                                                            *)
(* Licensed under the Apache License, Version 2.0 (the "License");            *)
(* you may not use this file except in compliance with the License.           *)
(* You may obtain a copy of the License at                                    *)
(*                                                                            *)
(*     http://www.apache.org/licenses/LICENSE-2.0                             *)
(*                                                         kx                   *)
(* Unless required by applicable law or agreed to in writing, software        *)
(* distributed under the License is distributed on an "AS IS" BASIS,          *)
(* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *)
(* See the License for the specific language governing permissions and        *)
(* limitations under the License.                                             *)
(******************************************************************************)
From mathcomp Require Import all_ssreflect.
From Equations Require Import Equations.
From sir.elabs.ast Require Import term module.
From sir.elabs.debruijn Require Import defs.
From sir.elabs.algo Require Import result.
From sir.common Require Import utils.

Set Implicit Arguments.
Unset Strict Implicit.
Import Prenex Implicits.

Fixpoint reduce (Σ: Module) (t: Term) : option Term :=
match t with
| var idx => None
| univ l => None
| pi A B => match reduce Σ A with
            | Some A' => Some (pi A' B)
            | None => match reduce Σ B with
                      | Some B' => Some (pi A B')
                      | None => None
                      end
            end
| lam A t => match reduce Σ A with
            | Some A' => Some (lam A' t)
            | None => match reduce Σ t with
                      | Some t' => Some (lam A t')
                      | None => None
                      end
            end
| appl m n => match reduce Σ m with
            | Some m' => Some (appl m' n)
            | None => match reduce Σ n with
                      | Some n' => Some (appl m n')
                      | None => match m with
                                | lam A e => Some (e.[n/])
                                | _ => None
                                end
                      end
            end
| sigma A B => match (reduce Σ A) with
               | Some A' => Some (sigma A' B)
               | None => match (reduce Σ B) with
                         | Some B' => Some (sigma A B')
                         | None => None
                         end
               end
| pair A B a b => match (reduce Σ A) with
                  | Some A' => Some (pair A' B a b)
                  | None => match (reduce Σ B) with
                            | Some B' => Some (pair A B' a b)
                            | None => match (reduce Σ a) with
                                      | Some a' => Some (pair A B a' b)
                                      | None => match (reduce Σ b) with
                                                | Some b' => Some (pair A B a b')
                                                | None => None
                                                end
                                       end
                            end
                  end
| proj1 m => match (reduce Σ m) with
             | Some m' => Some (proj1 m')
             | None => match m with
                       | pair A B a b => Some a
                       | _ => None
                       end
             end
| proj2 m => match (reduce Σ m) with
             | Some m' => Some (proj2 m')
             | None => match m with
                       | pair A B a b => Some b
                       | _ => None
                       end
             end
| gvar n => match (mod_get_term Σ n) with
            | Some t => Some t
            | None => None
            end
end.

Equations multi_reduce (fuel: nat) (Σ: Module) (t: Term) : Result Term Error :=
multi_reduce 0 Σ _ := Err OutOfFuel;
multi_reduce (S fuel) Σ t := match reduce Σ t with
                            | None => Ok t
                            | Some t' => multi_reduce fuel Σ t'
                            end.

Equations multi_reduce_to_univ (fuel: nat) (Σ: Module) (t: Term) : Result nat Error :=
multi_reduce_to_univ fuel Σ t with inspect(multi_reduce fuel Σ t) := {
    |  [Ok (univ l) | _] := Ok l;
    | [Ok t' | _ ] := Err (ReductionError Univ t');
    | [Err OutOfFuel | _] := Err (ReductionError Univ t);
    | _ := False_rect _ _;
}.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.

Equations multi_reduce_to_pi (fuel: nat) (Σ: Module) (t: Term) : Result (Term * Term) Error :=
multi_reduce_to_pi fuel Σ t with inspect(multi_reduce fuel Σ t) := {
    | [Ok (pi A B) | _] := Ok (A, B);
    | [Ok t' | _] := Err (ReductionError Pi t');
    | [Err OutOfFuel | _] := Err (ReductionError Univ t);
    | _ := False_rect _ _;
}.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.

Equations multi_reduce_to_sigma (fuel: nat) (Σ: Module) (t: Term) : Result (Term * Term) Error:=
multi_reduce_to_sigma fuel Σ t with inspect(multi_reduce fuel Σ t) := {
    | [Ok (sigma A B) | _] := Ok (A, B);
    | [Ok t' | _] := Err (ReductionError Sigma t');
    | [Err OutOfFuel | _] := Err (ReductionError Univ t);
    | _ := False_rect _ _;
}.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.
Next Obligation.
elim => [//|fuel IHfuel] Σ t.
rewrite multi_reduce_equation_2.
move Hred: (reduce Σ t) => [redt|//].
by apply: IHfuel.
Qed.

Equations multi_reduce_head (fuel: nat) (Σ: Module) (t: Term) : Result Term Error:=
multi_reduce_head 0 Σ t := Err OutOfFuel;
multi_reduce_head (S n) Σ (var x) := Ok (var x);
multi_reduce_head (S n) Σ (univ l) := Ok (univ l);
multi_reduce_head (S n) Σ (pi A B) := Ok (pi A B);
multi_reduce_head (S n) Σ (lam A t) := Ok (lam A t);
multi_reduce_head (S n) Σ (sigma A B) := Ok (sigma A B);
multi_reduce_head (S n) Σ (pair A B a b) := Ok (pair A B a b);
multi_reduce_head (S n) Σ (appl a b) := match reduce Σ (appl a b) with
                                      |Some t => multi_reduce_head n Σ t
                                      | None => Err (WeakHeadIrreducable (appl a b))
                                      end;
multi_reduce_head (S n) Σ (proj1 m) := match reduce Σ (proj1 m) with
                                     | Some t => multi_reduce_head n Σ t
                                     | None => Err (WeakHeadIrreducable (proj1 m))
                                     end;
multi_reduce_head (S n) Σ (proj2 m) := match reduce Σ (proj2 m) with
                                     | Some t => multi_reduce_head n Σ t
                                     | None => Err (WeakHeadIrreducable (proj2 m))
                                     end;
multi_reduce_head (S n) Σ (gvar name) := match mod_get_term Σ name with
                                     | Some t => multi_reduce_head n Σ t
                                     | None => Err (WeakHeadIrreducable (gvar name))
                                     end.
